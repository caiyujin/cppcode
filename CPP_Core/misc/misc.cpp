﻿#include "misc.h"

extern int h2_cpp_extern; //can be in either of the any translation unit, but not appear in multiple (one definiation rule)
void headerTestFunc1();

int main(int argc, char *argv[], char** envp)
{
    h2_cpp_extern = 10;
    h2_func();

    bit_cast_testing();

    //double* data1 = new double(10);  delete[] data1; //looks like this is also valid operation. instead of normally "delete data1";
    vector<string> argvVec(argv, argv + argc);
    vector<string> envpVec;
    for (int i = 0; *envp != nullptr; envp++)
        envpVec.push_back(string(*envp));

    signal(SIGINT, sigIntHandler);
    signal(SIGTERM, sigTERMHandler);
    signal(SIGSEGV, sigSEGVHandler);

    Foo* f1 = new Foo();
    Endieness_and_Packing();

    wchar_t wideChar1 = L'a'; //typically takes 2 bytes, to cover all languages.
    using TString = basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >; //same as wstring
    TString wcS1 = L"料峭春风吹酒醒，微冷，山头斜照却相迎，回首向来萧瑟处，归去。也无风雨也无晴";
    wcS1 += wideChar1;
    unsigned long long arr[5] = { 11, 22, 44, 21, 41 };
    cout << &arr[0] <<"\n";    // Print address of first element
    cout << arr <<"\n";        // Same as above; do NOT use &arr. it get the address OF the pointer.. not much useful
    cout << *arr <<"\n";        // Same as arr[0] (11)
    cout << *(arr + 1) <<"\n"; // Same as arr[1] (22) ,so its '+' been overriden
    cout << *arr + 2 <<"\n";   // Same as arr[0]+2 =13;
    plusOne(arr, 5);

    // if(NULL==0) //try avoid NULL, it is an integer type with value 0 in c++,
    // volatile int l=100; //preventing Compiler act smart and replace below if to if(True).. //if(l==100)

    //system("clr");
    Test_Func_Param2(1);
    Test_Func_Param2(1, 5);

    unsigned char a11 = 254; //unsigned int , max is only 255, watch out for overflow.
    int new1 = static_cast<unsigned char>(a11 + 10); //new1 is 8. not 264
    //string s7 = "abdeTGHnk";
    //for (auto i = s7.size() - 1; i >= 0; i--) {
    ////auto derived type from right, .size() return unsigned, so unsinged -1 still unsigned, 
    ////and i will be unsigned, when i=0; and minus -1 , i become huge integer number, will results in segmentation fault.

    long long bigNum = 8 << 30;    //eval from right, "8 << 30" (int literal) will evaluted as int, which as 32 bits, move 30 bits will overflow, will not get result desired.
    long long bigNum2 = static_cast<long long>(8) << 32;

    //namespace allow to group entities like, objects, functions, variables under one scope.
    //i feel like it is more like a class's static members,
    cout << "b_NS: a = " << b_NS::a << "\t b_NS: s=" << b_NS::s <<"\n";

    //auto& bad = min(i20, i23+1); // i23 + 1 is rvalue, can not be used as reference

    int* px = nullptr;
    {
        int i = 10;
        px = &i;
    }
    cout << *px; //UB, surprisingly still can print 10, maybe the memory is NOT taken yet..

    int* p20 = new int(1);
    double* pd22 = reinterpret_cast<double*>(p20);
    //the memory location pointed by p20/p22 expanded from 4 bytes to 8 bytes
    //using delete p20 could potentially leak 4 bytes. try not mess round with object of diff size.
    delete pd22;

    //object data layout, adrress
    Foo p;//it dees not have virtual table, so it is same as &p.a

    if ((void*)&p == (void*)&(p.a))
        cout << "nice, good optimizer" <<"\n";

    cout << " typeid(p4):" << typeid(&p).name() <<"\n";

    string s6 = "def";
    {
        // like global vs local!, s6 below will overshadow the outer one.
        string s6;
        s6 += "abc";
        cout << s6 <<"\n";
    }
    cout << s6 <<"\n";

    string(newVarName); //it compiles, equal to "string newVarName;" it is called decoration, can be tricky bugs!!
    unique_lock<mutex>(m1);
    // only RAII type(class) with default constructor will have Decoration!!  proper way is: unique_lock<mutex> M1(m1);

    //quite a number of string allocations..
    vector<char*> v1 = { const_cast<char*>("first const literal string"),
                      const_cast<char*>("second const literal string"),
                      const_cast<char*>(s6.c_str()) };

    cout << "numeric_limits<std::int8_t>::min:" << (std::numeric_limits<std::int8_t>::min) <<"\n";

    //raise(SIGINT);

#ifdef TESTDLL
    fibonacci_init(1, 1,10000);
    // Write out the sequence values until overflow.
    do {
        std::cout << fibonacci_index() << ": "
            << fibonacci_current() << std::endl;
    } while (fibonacci_next());
    
    CencryptedString s1("cai"s);
    cout << s1.getString() <<"\n";
    cout << s1.getOriginal() <<"\n";
#endif

#ifdef TESTMACRO
    // __TIME__,__DATE__,__LINE__,__FILE__,__cplusplus,_CPPLIB_VER,__FUNCSIG__        //string s = __FUNCSIG__;
    cout << "PI_PLUS_ONE*2 =" << PI_PLUS_ONE * 2 <<"\n";
    
    PRINT_MSG("TESTMACRO is defined");
    cout << MyMax(RndEng() % 10, 5) <<"\n"; //could be bug. hard to fix, RndEng will be called twice in the expanded code during compile time
    const char* c1 = toStrings(CAI YUJING IS HERE!); //macro stringizer; return const char*;

    int i91 = 10;
    string s91 = toStrings(i91); //string with value of "i91", the #operator will prevent the evaluation !!!
    int i92 = catNum(10, 20); //will print 1020 //macro pasting

    defclasses(ABC);
    ABC1 abc1;
#endif

#ifdef DefinedInPropertySettingCai
    FUNCMACRO;
#endif

    f_w_o_p(PRINT_MSG);
    funcPtr();

#ifdef TESTENUM
    my_mood = BLUE;
    cout << "my_mood=" << my_mood <<"\n";
    weekdays MyDay1 = fri;
    MyDay0 = mon;
    if (MyDay1 == 23 && MyDay0 == mon)
        cout << "can NOT assign integer value directly to enum variable, but can used to compare...weird!" <<"\n";

    apple apple1 = apple::ROSE;
    if (apple1 == apple::GALA)
        cout << "it is a gala apple" <<"\n";
    //if (apple1==2) //compile error, NO explicite to int!
#endif

#ifdef TESTCONST
    cout << "\n++++++Const PLD Demo +++++\n" <<"\n";
    const int a1 = 9; //a1=6, c-error. modify "read-only" location
    const_cast<int&>(a1) = 10;//const is a promise, can be easily broken by const_cast

    int a2 = 7;
    const int *p1 = &a2;// (*p1)++ => compile error, modify "read-only" location
    p1++; //const is left of *, means "pointed data" CONST; pointer is Not, 

    int a3 = 8;
    int *const p2 = &a3; //const is right of *, means pointer CONST ,pointed-Data is NOT, p2++ => c-error;
    (*p2) *= 100;         //can modify pointed-data

    cout << "\n++++++Const class Demo +++++\n" <<"\n";
    Foo fx;
    fx.pcA = nullptr;
    fx.pA = nullptr;
    //no diff put const before or after Foo, simply not modifiale, but dereferenced data still change
    Foo const fa; 
   // fa.pA = nullptr;
   // fa.a = 999;
    const Foo fb;
   // fb.a = 999;
    *fb.cpA += 1; //it will not change     int* const cpA  =>  const int* const cpA;

    Foo F1;
    Foo const *pcF1 = &F1;
    Foo* pF1 = &F1;
    printFoo(pF1);
    printFoo(pcF1);
    printFoo(const_cast<Foo *>(pcF1)); //if no non-const version, then use const version.

    std::string f = "yj";
    std::string l = "c";
    printName(f); //can choose both, but prefer non-const version
    printName(f + l); //cna only choose the const reference version, it can be implicitly converted to const reference

    printName2(f);//can only choose the const reference version, it can be implicitely cast to const;
    printName2(f + l);//can choose both, but prefer the reference to rvalue one, for better performance

#endif


#ifdef TESTITER
    cout << "\n++++++iterator Demo +++++\n" <<"\n";
    list<int> l1 = list<int>{1, 2, 4, 5, 7, 8, 16, 19, -9, 6, 5, 3, 9, 80};
    list<int>::iterator it1 = l1.begin();    
    *it1 = 10;
    list<int>::const_iterator cit_l1 = l1.begin();//const int *, pointed value is const
    //*cit_l1 = 10;//compile error
    cit_l1++; 
    const list<int>::iterator it2 = l1.begin(); //int *const
    *it2 = 100;
    advance(it1, 3);
    list<int>::iterator it3 = next(it1, 2);
    list<int> l1_new;
    copy(it1, it3, back_inserter(l1_new)); //std::copy(container1.it1, container1.it2, back_inserter(container2)), can be diff type of container, power!
#endif

#ifdef TESTCSTRING
    const char *msg3 = "Cherno"; //const char msg1[] = "Hello"; //normal char array, from string literal, null-terminated (implicitely).
    //most of time shall be "const char* msg3". hover mouse over to "Cherno", it shows Const Char[7] as type.
    //it is string literal, it stored in read-only memory of a .exe file, stack allocated memory, no need delete
    const char msg4[6] = {'C', 'h', 'e', 'r', 'n', 'o'}; //normal char array, NOT null-terminated. 
    const char msg5[8] = "Che\0rno"; // strlen will get 3. Che2\0rno\0 in memory.    
    char msg2[] = {'H', 'e', 'l', 'l', 'o', 0};//normal char array, null-terminated (explicitely). 
    cout << strlen(msg3) << " " << sizeof(msg3) << " " << sizeof(msg3) / sizeof(char) <<"\n";
    cout << sizeof(msg2) << " " << sizeof(msg2) / sizeof(char) <<"\n";

    string s8 = "helloWorld";
    //LPCTSTR s8c = s8.c_str();  const char* s8c =  s8.c_str(); 
    s8.append(sizeof(char), 'd');
    s8.append(msg3); 

    const WCHAR* shi = L"斜阳欲落处一望黯消魂";
    wstring shiWStr;
    wcout << shi;

    for (int i = 0; i < 10; i++)//sizeof(shi) / sizeof(WCHAR) == 4?
        shiWStr.push_back(shi[i]);
#endif

 }