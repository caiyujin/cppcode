#pragma once
#include "getExecutionTime.h"
#include "classes.h"
#include "MathLibrary.h"
#include "h2.h"

#define MyMax(a,b) ( (a<b) ? b :a)
#define TESTDLL
#define TESTENUM
#define TESTITER
#define TESTCONST
#define TESTCSTRING
#define TESTMACRO

#define toStrings(msg) #msg //conver to string;
#define catNum(a,b) a##b
#define defclasses(x) \
    caiclass(x##1) \
    caiclass(x##2) \
    caiclass(x##3)
#define caiclass(y) \
    class y \
    {public:\
        y() { cout << "ctor called" <<endl;}\
        ~y() {}\
        y(const y& rhs){cout << "copy ctor called" <<endl;}\
    private:\
        uint8_t mInt;\
    };

//in C++ coding, lvalue- An object that cocupies some identifiable location/address in memory, otherwise called rvalue.
#define PI_PLUS_ONE 3.14+1 //better use parentheses
#define FUNCMACRO help2(); help2(); help2();
#define int_p_def int* //Define is a preprocessor, the compiler will NEVER see it. 
int_p_def q1, q2, q3; //q2 is not a pointer!
typedef int* int_p_cai; //typedef is a compiler token, the Preprocessor doe NOT care about it, compiler does
int_p_cai p1,p2,p3; //q2 is pointers

volatile std::atomic<bool> running(true);

void sigIntHandler(int param)
{
    //if(running.exchange(false)){...}
    running = false;
    cout << "running is been set true upon receiving of ctrl-c" <<"\n";
}
void sigTERMHandler(int param)
{
    running = false;
    cout << "running is been set true upon receiving of Termantion" <<"\n";
}
void sigSEGVHandler(int param)
{
    getchar();
}

//with or without (default) extern is same.
void help2(void) {
    static int help2_int = 10; // only initilized in the VERY FISRT FUNCTION CALL! 
    help2_int += 2;
}

static int help3_int = 10;//internal linkage , to the cpp file include misc.h;

namespace b_NS {
    int a = 20;
    string s = "b_NS:: s";
}

inline void PRINT_MSG(const string& s) {
     cout << __FUNCSIG__ << ":" << s <<"\n";
}
typedef void(*functype1)(const string&);
void  f_w_o_p(functype1 f) {
    f("f_w_o_p");
}

typedef void(*CFuncPType)(void);
CFuncPType funcPtr = &help2;

void Test_Func_Param1(int a, int b){
    cout << __FUNCSIG__ <<"\n"; 
}

//default Args must on right.
void Test_Func_Param2(int a, int b = 0) { 
    cout << __FUNCSIG__ <<"\n";
}

enum MOODY { EXCITED, MOODY, BLUE, THRILLED } my_mood; //my_mood is a variable. 
//you have no way to get the "name" from the value in C++ because all the symbols are discarded during compilation. 
enum weekdays { mon, tue = 20, wed, thu, fri, sat, sun } MyDay0; //MyDay0 is a variable.
enum class apple :unsigned int { ROSE, GALA, CHESTNUT, GREEN };

struct Foo {
    int a = 0;
    int* pA = &a;
    const int* pcA = &a;
    int* const cpA = &a;

    static const int y4 = 20;
    char v4[y4];
    Foo(){}
};

void printFoo(const Foo* pFoo1) {
    cout << __FUNCSIG__ <<"\n";
    //pFoo1->a = 100;
}

void printName(const std::string& name)
{
    cout << __FUNCSIG__ <<"\n";
}

void printName(std::string& name)
{
    cout << __FUNCSIG__ <<"\n";
}

void printName2(const std::string& name)
{
    std::cout << "[lvalue]:"<<name << std::endl;
}

void printName2(std::string&& name) 
{
    std::cout << "[rvalue]:" << name << std::endl;
}

//They will be treated as int* by the compiler, The size of the array given in [] is ignored, ie 50 will be ignored.
//int max(int*, int);
//int max(int *arr, int size);
//int max(int i[50], int size);
//int max(int arr[], int size);

void plusOne(unsigned long long  arr[], int size) {
    *(arr + 1) += 1;
    *(arr + 4) += 1;
}

unsigned int change_endian(unsigned int x)
{
    unsigned char *ptr = (unsigned char *)&x;
    return (ptr[0] << 24) | (ptr[1] << 16) | (ptr[2] << 8) | ptr[3];
}

//#pragma pack(1)

void Endieness_and_Packing()
{
//int val1 = 100;
//if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__) {
//    cout << "__ORDER_LITTLE_ENDIAN__" <<"\n"; //this is the defaults one
//    cout << __builtin_bswap32(val1) <<"\n"; //convert default little endian to big endian
//}

    unsigned int i = 0;
    unsigned int* p = &i;
    *p = 0xA4B3C2D1;
    cout << "address of i is :" << p <<"\n";
    cout << "value of i is:" << *p <<"\n";

    unsigned int i2 = change_endian(i);
    //check the &i and &i2 in the memory window with hex display

    struct sl
    {
        uint8_t i1;
        uint16_t i2;
        uint32_t i3;
    };
    sl  s2 = { 1,2,3 };
    cout << sizeof(s2) <<"\n";
    //instead of 7 , it become 8. it is performance purpuse.
    //#pragma pack(1), will give result of 7.

    struct S2 {
        char a;   //0th
        int i;    //4th
        char b;   //8th
        int j;    //12th
        long long l; //16th
        char c;   //24th  //and align to 8 bytes, so total will be 32 bytes
    };
    cout << std::alignment_of_v<S2> <<"\n"; //8
    cout << (int)&((S2*)nullptr)->j <<"\n"; //cast nullptr to S2, and get the offset of member j; get value of 12;
    cout << sizeof(S2)<<"\n";
}

void bit_cast_testing()
{
    struct test {
        long a = 1000;
        int b = 10;
        char c = 'A';
        unsigned int d = 0xFFFFFFFF;
    };
    test t3;
    const auto& bytes2 = std::bit_cast<array<std::byte, sizeof(test)>>(t3);
    //_To is the 1st template parameter, it is specified as the first one in <>;
    //the _From is the 2nd template parameter, it is NOT specified in <>, so it is deduced from the argument.

    for (size_t i = 0; i < sizeof(t3); i++)
        cout << std::hex << static_cast<int>(bytes2[i]) << " ";
    cout << "\n";
    cout << std::dec << t3.d << "\n";//shall be huge number, 4294967295
    const int& int1 = std::bit_cast<int>(t3.d);
    cout << std::dec << int1 << "\n";//shall be -1;
}