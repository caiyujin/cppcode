#include "classes.h"
#include "MathLibrary.h"

using namespace CaiUtilities;
void classInstantiate(void);
void fourDefaultFuncs(void);
//void ctorDstrThrowExcp(void);//have to catch the exception in ctr or dstr, 
void DemoOfBase_Derived(void);
void shallowCopy_Ownership(void);
void classCasting(void);
void classFuncOverShadowing(void);
void ClassConstDemo(void);
void virtualTablePtr(void);
void polymorphism2(void);
void ObjectSlicing(void);
void koenig_lookup(void);
void Singleton_Design(void);
void operators(void);
void ip4and8(void);

int main(int argc, char* argv[]) {
    classInstantiate();
    fourDefaultFuncs();
    DemoOfBase_Derived();
    shallowCopy_Ownership();
    classCasting();
    classFuncOverShadowing();
    ClassConstDemo();
    virtualTablePtr();
    polymorphism2();
    ObjectSlicing();
    koenig_lookup();
    Singleton_Design();
    operators();
    ip4and8();
    return 0;
}
void classFuncOverShadowing(void) {
    D2 dObj1;
    dObj1.B1::printInfo();  //explicitely calling base's version.
    dObj1.B1::print(); //explicitely calling base's version.
    dObj1.B1::print1(1);//explicitely calling base's unique func signature.

    dObj1.printInfo(); //polymophism,func must be virtual
    dObj1.print();//calling derived's version. (NOT virtual)
    dObj1.print1("abc"); //this func signature unique to D2.

    //when cast either with ref or pointer, 
    B1& a = static_cast<B1&>(dObj1);    
    a.print();  //call base's vervsion;    
    a.printInfo(); //polymophism

    B1* aP = dynamic_cast<B1*>(&dObj1);
    aP->print();//call base's vervsion;  
    aP->printInfo();

    B1 bObj1;
    cout << sizeof(bObj1) << endl;    
    D2 dObj2;    
    cout << sizeof(dObj2) << endl;

    D1* p1 = new D1();
    p1->f(2.1f);
    p1->f("d1"s);
    p1->f(2); //this will not invoke f(int) if B::f(int) missing in D1 (been overshadowed)
    delete p1;
}

void DemoOfBase_Derived(void) {
    D1 dObj1;
    dObj1.set_B1_a(200);
    dObj1.m_B1_b = 300;
    dObj1.set_B1_a(4); //d1 has mX1 (B1's private member data), but d1 does NOT able to access it even in d1's function (print info). must through B1's member function interface.
    dObj1.printInfo();

    D2 dObj2;
    D2 dObj3;
    dObj2.m_B1_b = 100;
    if (dObj2 < dObj3)    cout << "d2a is smaller than d2b" << endl;
    D2 dObj4 = dObj2 * "new d2"s; //since D2(string s)is not explicit, so implicate calling ctor, operator*(d2a, D2("new d2"));
 
    B1* dObj6= new D1("D1");
    B1* dObj7 = new D1("D4");
    if (*dObj6 == *dObj7)
        cout << "two equal to four" << endl;
    delete dObj6;
    delete dObj7;

    cout << B1::totalInstance << endl;
}

void shallowCopy_Ownership(void) {
    //vector<Birthday_dll> v_Birthday;
    //v_Birthday.push_back(Birthday_dll(11,11,1199));
    //1. an Birthday_dll object is created, 
    //2. vector push_back invoke shallow-copy func of the class (default),
    //3. the temp object is deleted, which also deleted the memory where member pointer "abc" point to, 
    //hence *abc not-determined, ... that is before C++11, SOLUTIONS, use pointers: 

    vector<Birthday_dll*> vec_p;
    vec_p.push_back(new Birthday_dll(11, 12, 1999));
    vec_p.back()->printDate();
    //... after use the vec_p finished. need free the pointer memory... or else use smart pointer.
    for (vector<Birthday_dll*>::iterator it = vec_p.begin(); it < vec_p.end(); it++)
        delete *it;

    //("abc", &obj1);//if obj1 is on stack, it is more likely People_dll is NOT going to be owener
    People_dll ppl_Obje1("abc", new Birthday_dll(10, 8, 1983)); //new in ctor indicate People_dll is ower

}

void classInstantiate(void){
    assert(B1::totalInstance == 0);
    {
        //B1 C1a(); // this is WRONG, it is treated as FUNC call.
        B1 bObj5 = B1();
        B1 bObj6 = B1(9); //CLASSNAME
        B1 bObj7; // same as B1 C1b = B1();
        B1 bObj8(11); //same as bObj6 = B1(9); 

        B1 bObj9 = bObj5 * bObj6 * bObj8;

        B1* pbObj1 = new B1();
        B1* pbObj2 = new B1;
        delete pbObj1;
        delete pbObj2;
        B1* p_arr = new B1[2];
        delete[] p_arr;

        vector<B1*> vPB;
        vPB.push_back(new B1());
        vPB.push_back(new B1(9));
        for (B1* a : vPB) {
            delete a;
        }

        B1 bObj11 = 19; //implicate casting.... try avoid this."Explicite" 
        B1 bObj12 = static_cast<B1>(19);

        vector<unique_ptr<B1> > vUPB;
        vUPB.push_back(make_unique<B1>());
        vUPB.push_back(make_unique<B1>(8));
        vUPB.emplace_back(new D1());
        vUPB.back().reset();

    }
}

void fourDefaultFuncs(void)
{
    class Deft
    {
    public:
        // as long as u defined this most likely the rest 5 cannot generated default implt; 
        // if you want to use this one, 
        // then can use the other funcs of the 5 with = Default, if it is sufficient.
        Deft(const Deft& rhs) { id = rhs.id+10; } 
        Deft() = default;
        int id;
    };

    Deft DeftObj1 = Deft();
   // Deft DeftObj2{ 10 };
    Deft DeftObj3(DeftObj1);
    //DeftObj3 = DeftObj2;
    //Deft DeftObj4(move(DeftObj2));
    //DeftObj4 = move(DeftObj3);
}

void ClassConstDemo(void) {
    assert(B1::totalInstance == 0);
    B1 bObj1;
    //non-cosnt B1 will call the const version, if there is NO non-const version... 
    //cosnt B1 will call the const version, will get compile error if func is NOT const.
    const B1 consbObj1;    
    cout << consbObj1.get_B1_a() << endl;
    //so conclusion... if the getter no need to modify, 
    //then make the func const. if reallly need modify something, 
    //then got to create two copies of the getter..
}

void classCasting(void) {
    assert(B1::totalInstance == 0);
    cout << "\n\n===================Casting / C++ type ID demo===================" << endl;
    B1 bObj1;
    D1 dObj1;
    B1* pbObj1 = &dObj1;
    D1* pdObj1 = static_cast<D1*>(pbObj1);
    B1& rbObj1 = dObj1;

    // 1a. implicit casting.
    //B1* p1 = new D1();    //up_cast, all good.
    //D1* p1_dog = new B1(); // 2 .TRUE down_cast;  create something out of nothing.. Segmentation fault
    B1* pbObj2;
    if (rand() % 2 == 0) 
    {//never know what actual derived type it is
        pbObj2 = new D1();
    }
    else
    {
        pbObj2 = new D2();
    }

    D1* p1 = dynamic_cast<D1*>(pbObj2);
    D2* p2 = dynamic_cast<D2*>(pbObj2);
    if (p1) //nice feature about dynamic cast, can check cast results, if a valid pointer, do action, else skip
        p1->f(double(10.2));
    if (p2)
        p2->f((float)10.2);
    delete pbObj2;
}

void soundFuncGlobal() 
{
    cout << "sound function" << endl;
}

void growFuncGlobal(void* thisPtr,int i) 
{ 
    cout << "grow function" <<i << endl; 
}

void virtualTablePtr(void)
{
    class Animal1 
    { 
        public: virtual void sound() { std::cout << "the animal makes a sound" << endl; }
        public: virtual void grow(int i) { std::cout << "the animal grows a bit" << endl; }
    };
    class Dog1: public Animal1 { public: virtual void sound() { std::cout << "Dog goes woff" << endl; } };
    class Duck1 : public Animal1 { public: virtual void sound() { std::cout << "Duck goes quak" << endl; } };
    class Fish1 : public Animal1 { public: virtual void sound() { std::cout << "Fish goes bluuurb" << endl; } };
    class Fox1 : public Animal1 { public: virtual void sound() { std::cout << "No one knows how fox sound" << endl; } };

    int c=3;
    //cin >> c;

    Animal1* a = nullptr;
    switch (c)
    {
    case 1:        a = new Animal1(); break;
    case 2:        a = new Dog1(); break;
    case 3:        a = new Duck1(); break;
    case 4:        a = new Fish1(); break;
    case 5:        a = new Fox1(); break;
    default:        a = new Animal1(); break;
    }
    a->sound();

    Fish1* f = new Fish1();
    ((Animal1*)f)->sound(); //even though casted, the VT pointer (as part of the object on heap) is Fish1's VT pointer.

    Dog1* d = new Dog1();
    unsigned long long* vtableFish = (unsigned long long*)f;//the instance's first 8 bytes(first pointers) is pointing to the class's virtual table.
    unsigned long long* vtableDog = (unsigned long long*)d;//the class Dog1's virtual table is just a list of function pointers follow the order of declaration in the class Dog1 ;
    unsigned long long tmp = *vtableFish; //virtual table of class is hardware protected, read-only.
    *vtableFish = *vtableDog;
    *vtableDog = tmp;
    f->sound();//woff
    d->sound();//blurrb;

    typedef void(*funcPtr)();
    funcPtr vtable[] = { soundFuncGlobal, (funcPtr)growFuncGlobal };
    Animal1* a2 = new Animal1();
    *((unsigned long long*)a2) = (unsigned long long)vtable; //only instance's VT pointer can change to point to other table.
    a2->sound();
    a2->grow(5);
}

void polymorphism2(void){
    assert(B1::totalInstance == 0);
    B1* dP = new D1;
    dP->method();
    dP->baseMethod();//it will invoke method(), and again it will call D1's method().
    delete dP;

    D1 cFred("Fred"), cTyson("Tyson"), cZeke("Zeke");
    D2 cGarbo("Garbo"), cPooky("Pooky"), cTruffle("Truffle");
    B1* apcAnimals[] = { &cFred, &cGarbo, &cPooky, &cTruffle, &cTyson, &cZeke };
    for (int i = 0; i < 6; i++) {
        cout << apcAnimals[i]->m_Name << " says 'I am calling a ' ";
        apcAnimals[i]->method();
    }
    cGarbo.iWait();//derived class's public interface to access private/protected member of base class.

    apcAnimals[0]->methodWithDefault("d1 objects");
    //virtual func default parameters's value is bind-ed in compile time;  
    //while virtual func are bind-ed at run time; so it invoked D1's methodWithDefault(...) 
    //but using parameter abc="B1". 
}

void ObjectSlicing(void) {
    cout << "\n---------------- Object Slicing -------------" << endl;
    deque<B1> deq1;
    D1 Wolf1("Wolf1");
    deq1.push_front(Wolf1);//copy construcotr of B1.... so the created object will be B1, all the Wolf parts are Sliced Away.
    deq1[0].printInfo();

    deque<B1*> deq2;
    D1 Wolf2("Wolf2");
    deq2.push_front(&Wolf2);
    deq2[0]->printInfo();
}

void koenig_lookup(void) {
    A_NS::X x1;
    g(x1);
    //though g(x1) is not defined in current (main) or global scope, 
    //compiler will auto expand searching scope for g(x1), 
    //in namepace A_NS, where the parameter x1's type is defined
    Ccls1 c1;
    c1.j();
}

void Singleton_Design(void) {
    cout << CSingleton::Instance().val() << endl;
    CSingleton::Instance().setVal("Skeleton_string_new");
    cout << CSingleton::Instance().val() << endl;
}

void operators() {
    cStr c2("11");
    cout << c2->length();

    cStr c3("7");
    c3();
    c2(2.5);
    if (c2)
        cout << "c2.m_size > 9" << endl;
    if(c2>1)
        cout << "c2.m_size > 1" << endl;
    int c2_len = c2;//basically can use c2 as long long variable.

    std::unordered_map<cStr, int> ump1;
    
    ump1[c2] = 111;
    ump1[c3] = 77;
    //ump1[cStr("14")] = 1414;
    // the user defined std::hash<cStr> is so bad, 
    // basically compare string length and the first char of string,
    // "11" and "14" will yield same hash value,
    // compiler will think use call ump1[c2] in the last line, 
    // hence replace 111 with 1414,

    std::unordered_map<cStr, int, cStrNewHasher> ump2;
    ump2[c2] = 111;
    ump2[c3] = 77;
    ump2[cStr("14")] = 1414;

    std::unordered_map<cStr, int, cStrNewHasher2> ump3;
    cStrNewHasher2::base = 100;
    //c2,c3 copy ctor is invoked, if it contain raw heap pointer, watch out
    //could be double delete, if the 6 functions not defined properly.
    ump3[c2] = 111;
    ump3[c3] = 77;
    ump3[cStr("14")] = 1414; 

}

void ip4and8()
{
    string s4 = "1.10.255.0, message1";
    
    ip<4> ip4a(s4);//update the s4 to the message only!
    cout << ip4a.get_group(2) << endl;

    //ip<5> ip5a(std::array<int, 5>{ 1, 2, 3, 4,5 });

    ip<8> ip6a("1234:0abc:00a2:1123:12fe:0001:f100:0000, message2");//each nibble 4 bits, 4 nibble 16 bits (2bytes), 
    cout << ip6a.get_group(2) << endl;
}