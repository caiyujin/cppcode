#include "getExecutionTime.h"
#include "classes.h"
template <typename T>
class Deleter {
public:
    void operator()(T* ptr) {
        ++Deleter::count;
        delete ptr;
    }
private:
    static int count;
};
template <typename T>
int Deleter<T>::count = 0;

typedef Deleter<int> IntDeleter;

void foo(int *p)
{
    p = 0; //not *p=0, here p(copy of original) point to an unknown place (addr 0).
}

void square(int *p1)
{ 
    *p1 *= *p1;
}

void square(int &r)
{
    r *= r;
}

int Csquare(const int &cr)
{
    return cr * cr;
}

//cout<<"\n+++++++++ return type (argument -> return)+++++++++++"<<endl;
int *x2(int *p1)
{
    *p1 *= 2;
    return p1;

    //int var=*p1 * 2;
    //int* p_a=&var;
    //return p_a; //bad, var is destroyed already. by ref or pointer will got problem.
}

int &x2(int &r)
{
    r *= 2;
    return r;
}

//cout<<"\n+++++ Passing Dynamically Allocated Memory in called func+++++++"<<endl;
struct test
{
    test() { cout << "test()" << endl; }
    test(int i) : test(){
        x = i;
        y = i;
        throw 1;
    } //object lifetime started after any constructor has completed. here test() is completed.
    test(test &&a) noexcept { cout << __FUNCTION__ << endl; }
    test &operator=(const test &rhs){ cout << __FUNCTION__ << endl; return *this;}
    test &operator==(test &&rhs){cout << __FUNCTION__ << endl; return *this;}
    ~test() { cout << __FUNCTION__ << endl;}
    int x;
    int y;
};

test *createP(void)
{
    test *p = new test; //step1, object created on Heap
    return p;            //step2:  pointer is on stack, destroyed (NOT "deleted") upon exit, (address) is copied out via return type to a new pointer
}

test create(void)
{
    test t;
    t.x = 999;
    t.y = 999;
    return t; 
    //RVO -return value optimization, the compiler will optimize the return value, so that the object is not copied
}

test &createP2(void)
{
    test *p = new test;
    return *p; //return the content of the object out, by reference.
}

unique_ptr<test> createP3(void)
{
    unique_ptr<test> local_up(new test);
    local_up->x = 100;
    local_up->y = 200;
    return local_up; //it will invoke move(unique_ptr)...
}

int sum(const int *begin, const int *end)
{
    int summ = 0;
    for (const int *p = begin; p != end; p++)
    { //77 excluded
        summ += *p;
    }
    return summ;
}

int count(const char *cstr, const char c)
{ //no need pass in size, cos c-string will default end with \0
    int count = 0;
    while (*cstr != '\0')
    {
        if (*cstr == c)
            ++count;
        ++cstr;
    }
    return count;
}

shared_ptr<dog> gsp(new dog("gsp"));

shared_ptr<dog> share_factory(void)
{
    shared_ptr<dog> p(new dog("NO6"));
    return p; //"move" sematic!
}

void shared_maybeReset(shared_ptr<dog> &sp)
{
    sp.reset(new dog("NO6b"));
    sp->bark();
}

void gsp_reset()
{
    gsp.reset(new dog("gsp changed"));
}

void shared_normalUse(dog *p)
{
    p->bark();
}

void shared_gsp_reset(dog &r)
{
    gsp_reset();
    r.bark(); //memory violation... the old object is gone!!
}

//shared pointer, imagin point to heap allocated "dummy" object, each of the shared pointer 
//has two pointer internally, one is to point to a heap allocated integer to keep current total ref count
//another pointer is the raw pointer to the dummy objects.
//the ref count is thread safe, but the dummy object is not thread safe.

void shared(void)
{
    //{
    //    dog* pDog = new dog("x");        
    //    shared_ptr<dog> a(pDog); 
    //    shared_ptr<dog> b(pDog);
    //    cout << a.use_count() << "  " << b.use_count() << endl;
    //    // a and b not point to the same "resource"... 
    //}
    //delete pDog twice. 
    
    std::shared_ptr<int> sharedPtr1(new int, IntDeleter());
    {
        //shared_ptr<dog> spList(new dog[3]); 
        //dog[1],dog[2] will have memory leak, because here it invoke dog's delete function as default. 
        //and NO way to access the item [1] and [2], unless get the raw pointer and ..
        shared_ptr<dog> spList(new dog[3], [](dog* p) {delete[] p; });
        dog* p = spList.get();
        p[2].bark();
    }

    {
        shared_ptr<dog> a = make_shared<dog>("a");
        shared_ptr<dog> b = make_shared<dog>("b");
        a->makeFriends(b);
        b->makeFriends(a);
        //breakpoint to above line, then set break point to destructor, u will findout 
        //destructor is not going to invoke when u continue (with next breakpoint outside this block!
        //becasue it circular referencing... b and a will never have reference count to 0, hence got memory leak.(no Crash)
    }

    {
        shared_ptr<dog> d = make_shared<dog>("d");
        shared_ptr<dog> c = make_shared<dog>("c");
        d->makeTrueFriends(c);
        c->makeTrueFriends(d);
        d->showTrueFriends();
    }

    shared_ptr<dog> sp1(new dog("NO1"));// raw pointer as augument for its constructor;
    sp1.reset(new dog("NO2"));//dog(NO1) will be destroyed.. and NO2 been pointed
    sp1->bark(); 

    shared_ptr<dog> sp2 = sp1; //Count=2; got two pointers points to same object
    if (sp1 == sp2)
        std::cout << "sp1 and sp2 pointing to same object" << endl;
    (*sp2).bark();
    {
        const shared_ptr<dog> sp3 = sp1;//Count=3;
        auto pp = sp3.get(); //pp will be none const raw pointer.
        sp3->bark(); 

        //after sp2; it is not ->
        std::cout << "sp1.use_count()is:" << sp1.use_count();
    }
    dog c = *share_factory();
    // the "NO6" dog (from the func) is already destroyed on heap when above deref call DONE (copied). 
    //sp5=sp6; //Object dog(NO5) is immediatly deleted.
    //sp5=nullptr;
    //sp5.reset();.

    shared_normalUse(sp1.get());
    shared_maybeReset(sp1);//pass by ref

    ///auto pin = gsp ==> 1++ for the object, it is local, shared_maybeReset2() reset the gsp shared pointer,
    // but not this "pin"!! increase it on top of the stack!!!
    // this technical is used when the pointer (gsp) is not local stack smart pointer!!!.
    // Violating this rule is the number one cause of losing reference counts and finding yourself with a dangling pointer.
    auto pin = gsp;
    shared_gsp_reset(*pin);
    // pin is still holding the original object where gsp is pointed to .
    if (gsp != pin) {
        cout << "gsp is been reseted in the shared_normalUse2, but the original object hold by gsp is still preserved \
            due to the additional ref count yb the pin " << endl;
    }
}

class x
{
public:
    x() 
    { 
        p = new int(10); 
    }
    ~x() 
    {
        delete p; 
    };
private:
    int* p;
};

void unq_sink(unique_ptr<dog> u_p)
{ //take ownership, pass by value... aka. unq_sink...
    u_p->bark();
    cout << "it is killed here!" << endl;
}

unique_ptr<dog> unq_factory(void)
{
    unique_ptr<dog> p(new dog("NO9"));
    return p; //"move" sematic!
}

void unq_maybeReset(unique_ptr<dog> &u_p_r)
{ 
    // Do NOT pass in const unique_ptr<dog>&, it is dumb !
    if (RndEng() % 2 == 1)
        u_p_r.reset(new dog("N10"));
    u_p_r->func3();
}

//still prefer raw pointers or ref for common usage.
void unq_normalUse(dog *p)
{
    p->m_name += "_abc";
}

int* some_c_func()
{
    int* xp = (int* )malloc(sizeof(int));
    *xp = 999;
    return xp;
}
struct cFreeDeleter
{
    void operator()(void* p) const
    {
        free(p);
    }
};

template<class T>
using c_unique_ptr = unique_ptr<T, cFreeDeleter>;

c_unique_ptr<int> some_c_func_wrapper()
{
    return c_unique_ptr<int>(some_c_func());
}

void unique(void)
{
    dog *p = new dog("N04");
    unique_ptr<dog> up4(p); //unique_ptr or shared ptr are used to express ownership!!!!

    //unique_ptr<dog> up5(p); //will results in double delete/free, complier not able to capture

    unique_ptr<dog> up1(new dog("NO1"));
    //dog* p2=up1.release(); //return raw pointer of NO1 instance, and give up ownership! up1 will be == nullptr;

    up1.reset(new dog("NO2")); //NO1 object is destroyed! and new dog(N02) assigned to up1;  //up1.reset(); equal to "up1==nullptr";

    unique_ptr<dog> up3(new dog("NO3"));
    up3->bark();

    up3 = move(up1);     //up3 = up1=>compile error, unique_ptr is not copyable. up1 is nullptr, object is 'moved' to up3.
    unq_sink(move(up3)); //unq_sink(up3)=>compile error, unique_ptr is not copyable. move is a fancy casting, To a rvalue reference of up3. the callee take away this && value and contructed a new Unique pointer.
    unq_sink(unique_ptr<dog>(new dog("NO4")));
    if (up3 == nullptr)
        std::cout << "up3 is nullptr now!" << endl;

    up3 = unq_factory();

    up3->bark();

    unq_normalUse(up3.get());

    unq_maybeReset(up3);

    //since c use malloc/free, we have to use customed deleter. the Unique_ptr default is c++ "delete"
    auto xpUnq = unique_ptr<int, cFreeDeleter>(some_c_func());
    cout << "xpUnq:" << *xpUnq << "\t size of xpUnq:" << sizeof(xpUnq) << "\n"; //same as raw pointer, the deleter no state,

    auto xpUnq2 = unique_ptr<int, decltype(&free)>(some_c_func(),free); //require 2nd parameter, the size will become 16
    cout << "xpUnq2:" << *xpUnq2 << "\t size of xpUnq2:" << sizeof(xpUnq2) << "\n";
    
    auto xpUnq3 = some_c_func_wrapper();
    cout << "xpUnq3:" << *xpUnq3 << "\t size of xpUnq2:" << (sizeof xpUnq3) << "\n";

    std::allocator<int> alloc;
    auto dealloc =[](int* ptr) {std::allocator<int>().deallocate(ptr,1);};//construct the object allocator<int>(), when delete happen.
    auto xp = unique_ptr<int, decltype(dealloc)>(alloc.allocate(1), dealloc);
    cout << "xp:" << *xp << "\t size of xp:" << (sizeof xp) << "\n";

    auto dealloc2 = [&alloc](int* ptr) {alloc.deallocate(ptr, 1); };
    auto xp2 = unique_ptr<int, decltype(dealloc2)>(alloc.allocate(1), dealloc2);//keep the reference of alloc, so the size will be 16
    cout << "xp2:" << *xp2 << "\t size of xp2:" << (sizeof xp2) << "\n";

    //unique_ptr<dog[],[](dog* p){delete[] p;}> up5(new dog[5]); //=make_unique<dog[]>(3); , 
    ////make an array of dog with size 3. call ctor three times. shall defined the customed
    //up5.get()[2].bark(); //up5.get() shall get pointer to the dog[], kind of pointers to pointers. but in reality it is not.
}

void uniquePtrExample()
{
    long long *p1 = new long long(123);
    uniquePtr<long long> up1(p1);
    long long* p2 = new long long(456);
    up1.reset(p2);
    uniquePtr<long long> up2(std::move(up1));

    cout << "up2:" << *up2 << "\t size of up2:" << (sizeof up2) << endl;

    uniquePtr<long long> up3 = make_uniquePtr<long long>(999ll);
}
int main(int argc, char *argv[])
{
    int i1 = 88;
    int i2 = 22;

    int *p1 = &i1; // Explicit referencing
    *p1 = 99;      // Explicit dereferencing

    cout << "addr of int var i1:" << &i1 << endl;
    cout << "content of int var i1:" << i1 << endl;
    cout << "addr of pointer variable: " << &p1 << endl;
    cout << "content of pointer variable. " << p1 << endl;
    cout << "content of 'addr pointed By pointer variable'(or 'content of pointer variable') " << *p1 << endl;
    p1 = &i2; // Pointer can assigned to another address

    int &r1 = i1; // Implicit referencing (NO adress_of sign &), once poitned to i1, can not point to i2 again
    int &r5 = i1; //one variable can have multiple ref.
    r1 = 11;      // Implicit dereferencing (No &)
    cout << "addr of the reference variable:" << &r1 << endl; //r1 addr same as i1;

    r1 = i2; // important!!!... r1 is still an alias to i1, Assign value of i2 (22) to r1 (and i1).
    i2++;
    cout << "r1:" << r1 << "\tnumber1:" << i1 << "\tnumber2:" << i2 << endl; // 22

    cout << "\n++++++++++++++++++++++++++++Func arguments++++++++++++++++++" << endl;
    int i = 8;
    square(&i);

    //pay attention why *p2 NOT Changed!!
    int *p2 = new int(20);
    foo(p2); //p2 wont change value

    i = 9;
    square(i);
    Csquare(i);

    cout << "\n++++++++++++++++ return type (Argument -> PROCESS ed-> Return Type)++++++++++++++++++" << endl;
    i = 11;
    int *p3 = x2(&i); //kind of redundant, both argument and return value 

    i = 12;
    int &r2 = x2(i); // reference is pointer(internally), so this is quite important.

    cout << "\n++++++++++++++++ Passing Dynamically Allocated Memory in called func++++++++++++++++++" << endl;
    test *p4 = createP();
    delete p4; //step4: need to delete the raw pointers, and memeory (pointed) on heap!

    test t1 = create();

    test &r4 = createP2(); //complex rules allow for the lifetime extension of temporaries that are assinged to reference.
    delete &r4;

    unique_ptr<test> up1 = createP3();
    cout << "unique_ptr from func:" << up1->x << "," << up1->y << endl;

    test t2;
    test *P_t_main3 = &t2; //this will be No memory leak, bcos everything on Stack, called statically asignned pointer!

    cout << "\n+++++++++++++smart pointers++++++++++\n" << endl;
    shared();
    unique();
    uniquePtrExample();
    std::vector<std::unique_ptr<dog>> container;
    for (unsigned i = 0; i < 3; ++i)
    {
        std::unique_ptr<dog> p(new dog("ABC"));
        ////remember use 'Move' here! this is to let the unq_consume take over the ownership.
        container.push_back(move(p));
    }
    return 0;
}
