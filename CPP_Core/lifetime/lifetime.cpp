//#include <fmt/format.h>

#include <algorithm>
#include <utility>
#include <vector>
#include <source_location>
#include <iostream>

void print(const std::source_location& l = std::source_location::current()) {
    std::cout <<  "line :" << l.line() << " \t func name: " << l.function_name() << std::endl;
    //l.file_name() <<
}

struct Lifetime {
    void print_moved_from() const noexcept {
        if (moved_from) {
            printf("[moved from] ");
        }

        if (copy) {
            printf("[copy] ");
        }
        print();
    }

    auto operator<=>(const Lifetime& other) const {
        ++comparisons;
        print_moved_from();
        //In computer science, a three-way comparison takes two values A and B belonging to a type with a total order and determines whether A < B, A = B, or A > B in a single operation, 
        //in accordance with the mathematical law of trichotomy.
        printf("operator<=>(const Lifetime &) %d <=> %d  [three-way comparison]\n", id, other.id);
        print();
        return id - other.id;
    }

    explicit Lifetime(int id_) noexcept : id{ id_ } {
        ++objects;
        printf("Lifetime(int) %d [int constructor]\n", id);
        print();
    }
    Lifetime() noexcept {
        ++objects;
        printf("Lifetime() %d [default constructor]\n", id);
        print();
    }
    ~Lifetime() noexcept {
        ++destructions;
        print_moved_from();
        printf("~Lifetime() %d [destructor]\n", id);
        print();
    }
    Lifetime(const Lifetime& other) noexcept
        : id{ other.id }, moved_from{ other.moved_from }, copy{ true } {
        ++copies;
        ++objects;
        print_moved_from();
        printf("Lifetime(const Lifetime &) %d [copy constructor]\n", id);
        print();
    }
    Lifetime(Lifetime&& other) noexcept
        : id{ other.id },
        moved_from{ std::exchange(other.moved_from, true) },
        copy{ other.copy } {
        ++moves;
        ++objects;
        print_moved_from();
        printf("Lifetime(Lifetime &&) %d [move constructor]\n", id);
        print();
    }

    Lifetime& operator=(const Lifetime& other) noexcept {
        print_moved_from();
        if (&other == this) {
            printf("[self assignment]");
            ++self_assignments;
        }
        else {
            ++copy_assignments;
        }
        printf("operator=(const Lifetime &)  %d   =  %d  [copy assignment operator]\n", id, other.id);
        print();
        id = other.id;
        moved_from = other.moved_from;
        copy = true;
        return *this;
    }
    Lifetime& operator=(Lifetime&& other) noexcept {
        print_moved_from();
        if (&other == this) {
            printf("[self assignment]");
            ++self_assignments;
        }
        else {
            ++move_assignments;
        }

        printf("operator=(Lifetime &&)  %d = %d  [move assignment operator]\n", id,other.id);
        id = other.id;
        moved_from = std::exchange(other.moved_from, true);
        copy = other.copy;
        print();
        return *this;
    }

    static inline std::size_t objects{ 0 };           // NOLINT
    static inline std::size_t moves{ 0 };             // NOLINT
    static inline std::size_t copies{ 0 };            // NOLINT
    static inline std::size_t move_assignments{ 0 };  // NOLINT
    static inline std::size_t copy_assignments{ 0 };  // NOLINT
    static inline std::size_t destructions{ 0 };      // NOLINT
    static inline std::size_t self_assignments{ 0 };  // NOLINT
    static inline std::size_t comparisons{ 0 };       // NOLINT

private:
    int id = -1;
    bool moved_from = false;
    bool copy = false;
};

void dump_counters() {
    printf("\n\n******\n");
    printf("* Total Objects:     %u\n", Lifetime::objects);
    printf("* Move Constructors: %u\n", Lifetime::moves);
    printf("* Copy Constructors: %u\n", Lifetime::copies);
    printf("* Move Assignments:  %u\n", Lifetime::move_assignments);
    printf("* Copy Assignments:  %u\n", Lifetime::copy_assignments);
    printf("* Destructors:       %u\n", Lifetime::destructions);
    printf("* Self Assignments:  %u\n", Lifetime::self_assignments);
    printf("* Comparisons:       %u\n", Lifetime::comparisons);
};

struct Dump_Counters {
    Dump_Counters() = default;
    Dump_Counters(const Dump_Counters&) = delete;
    Dump_Counters(Dump_Counters&&) = delete;
    Dump_Counters& operator=(const Dump_Counters&) = delete;
    Dump_Counters& operator=(Dump_Counters&&) = delete;

    ~Dump_Counters() { dump_counters(); }
};

// a static global to dump all of the counts as the program
// shuts down
Dump_Counters LTCD;  // NOLINT

int main() {
    printf("Before lambda construction\n");
    auto lambda = [lft = Lifetime{ 1 }]() mutable { lft = Lifetime{ 2 }; };

    printf("Before lambda call\n");
    lambda();
    printf("After lambda call\n");

    std::vector<Lifetime> vec{};
    vec.emplace_back(6);
    vec.emplace_back(4);
    vec.emplace_back(5);
    vec.emplace_back(3);

    printf("Starting sort\n");
    std::sort(vec.begin(), vec.end());
    printf("Ending sort\n");

    auto vec2 = std::move(vec);
}
