#pragma once
#include "getExecutionTime.h"

//***************** Template function ****************************//

template <typename T1>
void print_size(const string& s)
{
    cout << "Size of this type -" << s << "- in x64 built is:" << sizeof(T1) <<"\n";
}

template <typename T1>
T1 smaller(T1 a, T1 b)
{
    return (a < b ? a : b);
}

template <>
double smaller<double>(double a, double b)
{ //template specialization for type double.
    return static_cast<long>(a * 1000) < static_cast<long>(b * 1000) ? a : b;
}

template <typename T>
string Normalize(const T& t) {
    return std::to_string(t);
}

string Normalize(const std::string& t) { //template specialization
    return t;
}

string Normalize(const char* t) {
    return string(t);
}

template <typename T>
vector<int> myfilter(T f, const vector<int> &v1)
{
    vector<int> v2;
    for (vector<int>::const_iterator it = v1.begin(); it != v1.end(); it++)
    {
        if (f(*it))
        {
            cout << *it << "\t" <<"\n";
            v2.push_back(*it);
        }
    }
    return v2;
}

template <typename To = string, typename From = string> //default type
To CaiTo(From arg)
{
    stringstream ss;
    To res;
    if (!(ss << arg) || !(ss >> res) || !(ss >> std::ws).eof())
        throw runtime_error{ "CaiTo failed" };
    return res;
}
//template <typename T>
//auto get_value(T t) {
//    if(std::is_pointer_v<T>)
template <typename T>
bool isEqual(T lhs, T rhs, true_type)
{ 
    return static_cast<long>((lhs * 10000)) / static_cast<T>(10000.0) == static_cast<long>((rhs * 10000)) / static_cast<T>(10000.0);
}

template <typename T>
bool isEqual(T lhs, T rhs, false_type)
{
    return lhs == rhs;
}

template <typename T>
bool isEqual(T lhs, T rhs)
{
    return isEqual(lhs, rhs, conditional_t<is_floating_point<T>::value, true_type, false_type>{});
}

//SFINAE (substitution failure is not an error), shall be slowly replaced by concept!//, typename = enable_if_t<is_copy_constructible<T>::value>>
template <typename T> 
    requires std::copy_constructible<typename T::iterator::value_type>
typename T::iterator::value_type getFirstCopyValue(T &v1)
{
    if (v1.size() != 0)
        return *(v1.begin());
}

template <typename T, typename U>
struct same_type
{
    static const bool v = false;
};

template <typename T> 
struct same_type<T, T>
{
    static const bool v = true;
};

//the build in is std::is_same_v<T, std::vector<typename T::value_type, T::allocator_type>> --- here check if it is vector.

template <typename T, typename U>
requires same_type<U, typename T::iterator::value_type>::v 
list<string> findTopItems(T t1, U u1)
{
    return list<string>{ CaiTo<string, U>(*(t1.begin()))};
}

template <typename IT, typename Pred>
requires std::_Is_iterator<IT>::value 

vector<typename IT::value_type> returnValueOf(IT begin, IT end, Pred predicate)
{
    vector<typename IT::value_type> v;
    for (auto it = begin; it != end; it++)
    {
        if (predicate(*it))
            v.push_back(*it);
    }
    return v;
}

template <typename T>
requires std::is_integral<T>::value && (!std::is_pointer<T>::value)
bool isOdd(T t1)
{
    return t1 % 2;
}

template <typename C, typename V>
vector<typename C::iterator> findAll(C &c, V v)
{
    vector<typename C::iterator> res;
    for (auto p = c.begin(); p != c.end(); ++p)
    {
        if (*p == v)
            res.push_back(p);
    }
    return res;
}

template <typename RA_IT>
typename std::iterator_traits<RA_IT>::difference_type distance2(RA_IT it1, RA_IT it2, std::random_access_iterator_tag tag1)
{
    return it2 - it1;
}

template <typename In_IT>
typename std::iterator_traits<In_IT>::difference_type distance2(In_IT it1, In_IT it2, std::input_iterator_tag tag1)
{
    typename std::iterator_traits<In_IT>::difference_type diff;
    for (diff = 0; it1 != it2; it1++, diff++) {}
    return diff;
}

template <typename IT>
typename std::iterator_traits<IT>::difference_type distance_itr(IT it1, IT it2)
{
    return distance2(it1, it2, typename std::iterator_traits<IT>::iterator_category());
}

struct sNew
{
    inline static const int sValue = 777;
};

template<typename T>
concept IncrementableCai = requires(T t) { t++; ++t; };

template<typename T>
concept DecrementableCai = requires(T t) { t--; --t; };

template<typename T>
concept IncDecCai = IncrementableCai<T> && DecrementableCai<T>;

void upDownTwice(IncDecCai auto& a)
{
    a++;
    a++;
    a++;
    a--;
}

template<
    template<typename Contained>
    typename ResultType
>
auto get_data()
{
    ResultType<double> res;

    if constexpr (requires {res.reserve(2); })//doesnot make sense for List to reserve...
    {
        res.reserve(2);
    }
    res.push_back(1.1);
    res.push_back(2.2); 

    return res;
}
//***************** variadic Template  ****************************//
void println(ostream& out) //exit call (last func call).
{
    out << "\n";
}

template <typename T, typename... Args>
void println(ostream& out, T const& t, Args const&... arg) {
    out << t;
    if (sizeof...(arg))
        out << ", ";
    println(out, arg...);
}

double sum(void) //exit call (last func call).
{
    return 0;
}

template <typename T, typename... Args>
double sum(T t, Args... arg) 
{
    return t + sum(arg...);
}

template <typename...Args>
auto sum2(Args... arg)
{
    return (... + arg);
}

template <typename...Args>
auto sum3(Args... arg)
{
    return ((sizeof...(arg) % 2) + ... + arg); //if even number +0, if odd number +1.
}

template <typename...Args>
constexpr auto divide_right(Args... arg)
{
    return (arg / ...); //unary right pack ( 7 / (5  / 3)) = 7
}

template <typename...Args>
constexpr auto divide_left(Args... arg)
{
    return (... / arg); //unary left pack (( 7 / 5 ) / 3) = 0
}

template<typename T, typename... Args>
constexpr T mins(const T& a, const T& b, const Args&... arg)
{
    T m = a < b ? a : b; //const 
    if  constexpr (sizeof...(arg)) 
    {
        return mins(m, arg...);
    }
    return m;
}

template <int... arg>
std::array<bool, 10> func() {
    std::array<bool, 10> b = {};
    auto values = { arg... };// initializer_list<int>
    std::for_each(values.begin(), values.end(), [&](int n) 
        {
            if( n%2==0)
                b[n] = true;
            else
                b[n] = false;
        });
    return b;
}

template<typename... Args>
void printer(Args&&... args)
{
    (std::cout << ... << args) << '\n'; 
    //Binary Left Fold, I op ... op E, ((((I op E1) op E2) op ......) op En)
    //((((std::cout << arg1) << arg2) << arg3) << ... << argN)
}

template<typename T, typename... Args>
void push_back_vec(std::vector<T>& v, Args&&... args)
{
    static_assert((std::is_constructible_v<T, Args&&> && ...)); 
    //std::is_constructible_v<T, Args&&>: This is a type trait that checks if T is constructible from each argument in the parameter pack Args... 
    //1) Unary right fold (E op ...) becomes (E1 op (... op (EN-1 op EN)))
    //(std::is_constructible_v<T, Args1&&> && (std::is_constructible_v<T, Args2&&> && (... && std::is_constructible_v<T, ArgsN&&>)))
    (v.push_back(std::forward<Args>(args)), ...);
    /*
    * 
In summary, a unary right fold expression in C++17 iteratively applies an operation (E) to each element of a parameter pack,
combining the results using a binary operator (op) as it progresses from right to left.

        1) Unary right fold (E op ...) becomes (E1 op (... op (EN-1 op EN)))
        expression is , v.push_back(std::forward<Args>(args)).
        op, refer to comma ,
        ...  is the parameter pack args.
       //((((v.push_back(std::forward<Arg1>(arg1)) , v.push_back(std::forward<Arg2>(arg2))) , v.push_back(std::forward<Arg3>(arg3))) , ... , v.push_back(std::forward<ArgN>(argN)))

        the comma, essentially acts as a sequence point, ensuring that the expressions are evaluated in order.
    */
 }

template<class T, std::size_t... Args>
constexpr T bswap_impl(T i, std::index_sequence<Args...>)
{
    T low_byte_mask = (unsigned char)-1;
    T ret{};
    ([&]
        {
            /*
            The line (void)dummy_pack; is used to silence any unused variable warnings for dummy_pack.
            In this context, "unused" refers to the fact that the variable (types info) Args... 
            is NOT actually used within the body of the lambda function. 
            The purpose of this variable is to expand the fold expression;(so the lambda can be called multiple times)
            */
            (void)Args;
            ret <<= CHAR_BIT;
            ret |= i & low_byte_mask;
            i >>= CHAR_BIT;
        }(), ...);
    return ret;
}

constexpr auto bswap(std::unsigned_integral auto i)
{
    return bswap_impl(i, std::make_index_sequence<sizeof(i)>{});
    //if i is of size 2(unsigned short here), it will make a sequence of 2 (eg 0, and 1);
    //it essentially creates an instance of integer_sequence<size_t, 0, 1>.The template parameters 0, 1, 
    //become part of the type information but are not stored as data members.
    //The purpose of integer_sequence is Not to store individual values 
    //but to provide a compile - time sequence of integers that can be used in metaprogramming scenarios.
}

struct MACAdress
{
    std::array<unsigned char, 6> data;
};

template<typename T, std::size_t N, typename U>
constexpr bool Compare(const std::array<T, N>& lhs, const U& b)
{
    return [&]<size_t... Is>(std::index_sequence<Is...>)//variadic template function, Is is a pack of indexes.
    {
        return ((lhs[Is] == b) && ...);
    }(std::make_index_sequence<N>{});
}

template<typename T, std::size_t N>
constexpr bool Compare(const std::array<T, N>& lhs, const std::array<T, N>& rhs)
{
    return [&]<size_t... Is>(std::index_sequence<Is...>)//
    {
        return ((lhs[Is] == rhs[Is]) && ...);
    }(std::make_index_sequence<N>{});
}



//***************** variadic Template func3 with perfect forwarding.****************************//
class Clifetime
{
public:
    int i;
    string s;
    Clifetime() : i(-1) { cout << __FUNCSIG__ << endl; }
    Clifetime(const Clifetime& _c) : i(_c.i), s(_c.s) { cout << __FUNCSIG__ << endl; }
    Clifetime(Clifetime&& _c) : i(move(_c.i)), s(move(_c.s)) { cout << __FUNCSIG__ << endl; }
    Clifetime& operator=(const Clifetime& _c) { i = _c.i; s = _c.s; cout << __FUNCSIG__ << endl; return *this; }
    Clifetime& operator=(Clifetime&& _c) { i = move(_c.i); s = move(_c.s); cout << __FUNCSIG__ << endl; return *this; }
    ~Clifetime() { cout << __FUNCSIG__ << endl; }

    Clifetime(int& _i) : i(_i) { cout << __FUNCSIG__ << endl; }
    Clifetime(float _f) : i(_f) { cout << __FUNCSIG__ << endl; }
    Clifetime(string&& _s) : s(move(_s)){ cout << __FUNCSIG__ << endl; }
    Clifetime(string& _s) : s(_s) { cout << __FUNCSIG__ << endl; }
    Clifetime(int _i, string&& _s) : i(_i), s(move(_s)) { cout << __FUNCSIG__ << endl; }
};
vector<Clifetime> vc;
template <typename... Args>
void add2Vec(Args&&... arg)
{
    vc.emplace_back(forward<Args>(arg)...);
}

template <typename... Args>
void createTuple(Args... arg) {
    tuple<Args...> t1{ arg... }; //T... expand to types only (comma separated)
    for (int i = 0; i < tuple_size<tuple<Args...>>::value; i++)
        cout << i <<"\n";
}

template <typename... Args>
void createTuplePerfect(Args&&... arg) {
    tuple<Args...> t1{ forward<Args>(arg)... };//direct expansion of the parameter pack
    //tuple<Args...> t2{ forward<Args>(arg),... };//comma operator with fold expression;
    for (size_t i = 0; i < tuple_size<tuple<Args...>>::value; i++)
       cout << std::get<0>(t1)<<"\n";
       //cout << std::get<i>(t1) << "\n";//cannot compile, probably got to make the call constexpress?
}
