#include "template_function.h"
#include "classes.h"

int main(int argc, char *argv[])
{
    if (0.41f - 0.21f == 0.2)
        cout << "true" <<"\n";

    double db1 = 11.51;
    double db2 = 11.52;

    cout << "smaller:" << smaller(20, 21) <<"\n";   //literal as int
    cout << "smaller:" << smaller(db1, db2) <<"\n"; //double

    cout << Normalize("abcd") <<"\n";
    cout << Normalize("abcd"s) <<"\n";
    cout << Normalize(1123) <<"\n";

    print_size<string>("s");
    print_size<double>("double"); // 8 bytes
    print_size<long long>("long long"); // 8 bytes
    print_size<float>("float");  //4 byte
    print_size<long>("long"); //4 byte
    print_size<int>("int"); //4 byte
    print_size<short>("short"); //2 byte
    print_size<char>("char"); // 1 byte

    vector<int> v1 = {3, 4, 5, 6, 8, 6, 9, 19, 13};
    vector<int> v2 = myfilter(
        [](const int& x) { return (x % 3 == 0); },
        v1);

    float f5 = CaiTo<float, string>(" 123.4 ");
    char c1 = CaiTo<char, string>("B");
    cout << boolalpha << isEqual(10.1f, 10.2f) << "\t"<<
                         isEqual(static_cast<double>(10.00021), static_cast<double>(10.00022)) << "\t" <<
                         isEqual(10u, 10u) << "\t" <<
                         isEqual(10, 10) << "\t" <<
                         isEqual(10l, 10l) <<"\n";

    vector<shared_ptr<int>> v3{};//it will failed it is vector<unique_ptr<int>>
    v3.emplace_back(new int(100));
    shared_ptr<int> item1 = getFirstCopyValue(v3);

    cout << boolalpha << same_type<int, float>::v << "\t" <<
                         same_type<unsigned int, size_t>::v << "\t" <<
                         is_integral<long long>::value << "\t" <<
                         same_type<float, double>::v <<"\n";

    vector<int> v4{ 6, 4, 5 };
    vector<string> v5{ "ab", "cd", "ef", "gh" };
    auto bRes0 = findTopItems(v4, 5);

    map<int, string> m1 = { {1,"a"}, {2,"b"}, {3,"c"},{4,"d"} };
    vector<pair<const int, string> > v7 = returnValueOf(
        m1.begin(), 
        m1.end(), 
        [](const pair<const int, string> &p) { return p.second == "d"; }
    );

    int i = 99;
    cout << boolalpha << isOdd(i) <<"\n";

    string s1 = "my name i1, i2 ,i3";
    vector<string::iterator> s1_res = findAll(s1, 'i'); 
    int dist1 = distance_itr(s1_res[0], s1_res[1]);

    list<double> l1 = { 12.2, 11, 11.3, 11, 8.99, 34.4,22,11 };
    vector<list<double>::iterator> l1_res = findAll(l1, 11.0);
    int dist2 = distance_itr(*(l1_res.begin()), *(--l1_res.end()));

    int i3 = 10;
    upDownTwice<int>(i3);
    //string s2("val");
    //upDownTwice(s2);

    vector<int> vecInt1(10, 1);
    SortableClass o1(vecInt1);
    //o1.SortIt();

    auto data = get_data<std::vector>();
    auto data2 = get_data<std::list>();

    //*********************variadic template function********************
    vector<Clifetime> vc;
    add2Vec();
    int z = 20; add2Vec(z);
    add2Vec(100.1f);
    string s = "abc"; add2Vec(s);
    add2Vec(move(s));
    add2Vec("abc"s);
    add2Vec(z, "def"s);
    
    println(cout, "1", 65, 9.2, 'c');

    int j1 = 2;
    float j2 = 2.1;
    double j3 = 3.1;
    unsigned int j4 = 4;
    long j5 = 5;
    cout << sum(j1, j2, j3, j4, j5) <<"\n";
    cout << sum2(j1, j2, j3, j4,j5) << "\n";
    cout << sum3(j1, j2, j3, j4,j5) << "\n";

    static_assert( divide_right(7, 5, 3) == 7);
    static_assert( divide_left(7, 5, 3) == 0);//must be calculated compile time.
    static_assert( mins(3, 2, 4, 2, 1, 5) == 1);
    
    const std::array<bool, 10> b = func<1, 3, 6, 7>();

    createTuple(j1, j2, j3, j4);
    createTuplePerfect(j1, j2, j3, "abcdegs"s);

    printer(1, 2, 3, "abc");
    std::vector<int> v;
    push_back_vec(v, 6, 2, 45, 12);
    push_back_vec(v, 1, 2, 9);

    if (bswap<std::uint16_t>(0x1234u) == 0x3412u)
        cout << "bswap<std::uint16_t> is correct" <<"\n";
    if (bswap<std::uint64_t>(0x0123456789abcdefull) == 0xefcdab8967452301ULL)
        cout << "bswap<std::uint64_t> is correct" <<"\n";

    constexpr MACAdress mac0{ 0x04, 0x04, 0x04, 0x04, 0x04, 0x04 };//change any of the field will result in compile failure
    constexpr MACAdress mac1{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
    constexpr MACAdress mac2{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };

    static_assert(Compare(mac0.data, 0x04), "MACAdress compare failed");
    static_assert(Compare(mac1.data, mac2.data), "MACAdress compare failed");
    //***************** Template Class ****************************//
    Bo<int> b1{2, 5, 8, 6, 9, 19, 13};
    Bo<int> b2 = Bo<int>{1, 4, 7, 5, 8, 18, 12, 11};
    Bo<int> b3 {1, 4, 11};
    Bo<int> b4(b1);
    Bo<int> b5(move(b2)); //b2 is expiring lvalue, with move, convert to rvalue, if b5(Bo<int>()), then it is called pure rvalue, no need move()..
    b4 = b3;
    b5 = move(b3);

    Bo<int> b6 = b4 * b5;
    b6.print_Bo();
    
    std::array<int, 5> arr; //created on stack instead of heap.
    arr[0] = 100;

    d1<float> d1Obj1;
    d1Obj1.var1 = 10.1;
    d2<vector<int>, map<string, int> > d2Obj1;
   
    CharArray<24> ca;
    ca[20] = 'g';

    Array<string, 5> strStackArray{ "ab", "cd", "ef", "hi","jk" };
    Array<char, 4> charStackArray{ {'a','b','c','d'} };
    Array<long, 2> lngStackArray{ {100L,200L} };
    const Array<long, 2> lngStackArray2{ {200L,400L} };

    Converter<int> ct1(1);
    Converter<int> ct2{ 2 };
    c1 = 2.1;

    return 0;
}
