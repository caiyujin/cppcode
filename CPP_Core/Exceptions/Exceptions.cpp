// Exceptions.cpp : Defines the entry point for the console application.
#include "Exceptions.h"
#include "createDumpNcallStack.h"
#include "classes.h"

//exceptions for those happen RARELY.
//errors for those cannot be dealt with locally (I/O error), file not found/key not found/operator/ctor...

//do not use for thing SHould NOT happen .
//deref null
//out of range access.
//use after free

struct MyException : public exception
{
    const char * what() const throw()
    {
        return "CxException";
    }
};

double multiples(int a, int b)
{
    if (b < 0){
        //throw invalid_argument("2nd argument cannot be negativee");
        throw inValidArgs("2nd arg can not be 0", SOURCEINFO);
    }
    return (a * b);
}

void func1(B1 b)
{
    try
    {
        b.m_Name = "func1";
        TakeADumpImpl();
        throw out_of_range("func1 exception");  //throw by value
    }
    catch (const bad_alloc &e1) //catch by const ref
    {
        cout << "don't do alloc here in func1" << e1.what() << endl;
    }
}

void func2(B1 b)
{
    b.m_Name = "func2";
    func1(b);
}

void func3(B1 b)
{
    try
    {
        b.m_Name = "func3";
        func2(b);
    }
    catch (const out_of_range & e1)
    {
        cout << "out_of_range catched  @ " << __FUNCTION__ << e1.what() << endl;
        throw;
    }
}

int test2() /*noexcept*/ {
    try {
        throw 1;
    }
    catch (...)
    {
        throw std::runtime_error("new except");
    }
    return 0;
}

int main()
{
    try {
        test2();  //it will not go to catch block and been process, if the func is noexcept!!. so watch  out
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        cout << "do nothing" << endl;
    }

    int i = 1;
    try
    {
        try
        {
            i++;
            throw invalid_argument("abc");
            i++;//this will NOT be called.
        }
        catch (const runtime_error& e)
        {
            cout << "e:" << e.what() << endl;
        }

        i++;//this will NOT be called.
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        cout << "cought @ " << __FUNCTION__ << endl;
    }

    try
    {
        int num2 = 55;
        switch (num2)
        {
        case 25:
            throw "exception caught: num2 ==25";
            break;
        case 35:
            throw MyException();
            break;
        case 45:
            throw bad_exception();
            break;
        case 55:
            {//if u going to define a varialbe in the case statement, must use {} to enclose it.
                int i = 10; 
                multiples(0, -1);
            }
            break;
        case 65:
            throw 1000;
            break;
        default:
            cout << "num2 to the end of 'try' block, NO exception raised!" << endl;
        }
    }
    catch (const int &err)
    {
        cout << err << endl;
    }
    catch (const MyException &e)
    {
        //throw by value, catch by ref, if by value (copying), easily got object slicing happen
    }
    catch (const bad_exception &e)
    {

    }
    catch (const invalid_argument &e)
    {
        raise(SIGSEGV);
        //SIGABRT-abnormal termination, SIGFPE-Floating point exception,
        //SIGILL-invalid instruction, SIGSEGM-Segmentation Fault, SIGTERM...
    }
    catch (const range_error &e)
    {

    }
    catch (...)
    {
        //this catch(...) must be at last....
        //and able to capture all types of exceptions, include int/string/...etc
        cout << "Unknown Exception! continue run" << endl;
    }

    try
    {
        B1 b;
        b.m_Name = "main";
        func3(b);
    }
    catch (const std::exception &e)
    {
        // because most of exception derived from std::exception.
        // std::exception_ptr p = std::current_exception();
        cout << "exception catch @ " << __FUNCTION__ << e.what() << endl;
    }
    return 0;
}
