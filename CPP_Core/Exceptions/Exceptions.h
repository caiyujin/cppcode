#include "getExecutionTime.h"

class sourcedException: public std::exception{
private:
    string mWhere;
    string mName;
public:
    sourcedException(const string& what,
                     const string& function, 
                     const string& file, 
                     const int line):
        mWhere(function+" "+file+" "+to_string(line)),
        mName(what){}

    virtual const char* what() const noexcept
    {
        return mName.c_str();
    }
    string whats() const noexcept {
        return mName;
    }
    string where() const noexcept {
        return mWhere;
    }
};

#define DECLARE_SOURCED_EXCEPTION(ename)                    \
    class ename : public sourcedException                   \
    {public:                                                \
        ename(const string& what,                           \
                const string& function,                     \
                const string& file,                         \
                const int line) :                           \
            sourcedException(what, function, file, line)    \
        {}                                                  \
    }                                                       \

DECLARE_SOURCED_EXCEPTION(IOException);
DECLARE_SOURCED_EXCEPTION(ElementNotFound);
DECLARE_SOURCED_EXCEPTION(inValidArgs);

#define SOURCEINFO __FUNCTION__, __FILE__,__LINE__