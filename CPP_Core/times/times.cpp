#include "getExecutionTime.h"

/*
clock:
1. system_clock: current time according to system. 
   system_clcok can set by user one, so can adjust forward 1hr, 
   actually not one-hour pass yet!! --NOT stedy!

2. steady_clock: goes at a uniform rate

3. hight_resolution_clock: provides smallest possible tick Period
*/

void distr_monteCarloSim(void) {

    long seeds = steady_clock::now().time_since_epoch().count();
    default_random_engine eng3(seeds); //eng3()%6 only provide UNIFORMed distribution

    //A come to office between  6:00-9:59;
    //lets assume 6 is start points, and break into minutes;
    uniform_int_distribution<int> ATimes(0, 60*4); 
    vector<int> arrivalTimeA(10000);
    for (int i = 0; i < 10000; i++) 
        arrivalTimeA[i] = ATimes(eng3);

    //B come to office between 9:00-10:59;
    uniform_int_distribution<int> BTimes(180, 60*5);
    vector<int> arrivalTimeB(10000);

    for (int i = 0; i < 10000; i++)
        arrivalTimeB[i] = BTimes(eng3);

    //projection is 25% (A after 9) * 50% (B before 10) * 50% (the chance of A is 1/2 bigger than B if AB both randomely take from same range)
    //=1/16 ? or about 6.25%;
    int count = 0;
    for (int i = 0; i < 10000; i++)
    {
        if (arrivalTimeA[i] > arrivalTimeB[i]) 
            count++;
    }
    cout << "A is late " << (double)count *100 /10000 << " %" << endl;
}


int main(int argc, char* argv[])
{
    //template <class _Clock, class _Duration = typename _Clock::duration>
    //class time_point { // represents a point in time
    //public:
    //    using clock = _Clock; //can be system_clock, steady_clock, or high-resolution_clock
    //    using duration = _Duration; // the duration type used to represent the time point. it point to template type _Clock's memeber type duration!
    //    using rep = typename _Duration::rep; //the return type by duration::count.
    //    using period = typename _Duration::period;  //the length of period in seconds. eg 1/1000 000 000 second

    steady_clock::time_point t1 = steady_clock::now();
    this_thread::sleep_for(2s);

    steady_clock::time_point t2 = steady_clock::now();

    duration<double> time_spans = duration_cast<duration<double>>(t2 - t1);
    cout << "it takes me " << time_spans.count() << "second" << endl;
   
    duration<double, ratio <1,1000>> time_spans_ms = duration_cast<duration<double>>(t2 - t1);
    cout << "it takes me " << time_spans_ms.count() << "milli, second" << endl;
    
    steady_clock::time_point::duration time_spans_xs = duration_cast<duration<long long>>(t2 - t1);
    cout << "it takes me " << time_spans_xs.count() << "nano second" << endl;

    //steady_clock::time_point::duration d1;  == steady_clock::duration d1; //" based on defination
    //using duration = nanoseconds;
    //using nanoseconds  = duration<long long, nano>;
    //using nano  = ratio<1, 1000000000>;

    cout << system_clock::period::num << "/" << system_clock::period::den << endl; //nominator, and denominator
    microseconds mi(270);                                //270 msec
    nanoseconds na = mi;                                // na=270 000 nanosecond
    milliseconds mil = duration_cast<milliseconds>(mi); // high res to low, need cast! = 2msec, truncated!
    mi += mil;                                            //200+270=470
    this_thread::sleep_for(mi);
    distr_monteCarloSim();


    //get date, week day info. ONLY System_clock time has a relationship with the civil calendar.
    system_clock::time_point t3 = system_clock::now();
    time_t tt1 = system_clock::to_time_t(t3);

    //tm utc_tm;
    //gmtime_s(&utc_tm, &tt1);
    
    tm local_tm;
    localtime_s(&local_tm,&tt1);
    
    cout << "year:" <<local_tm.tm_year + 1900 << endl;
    std::cout << "month:" << local_tm.tm_mon + 1 << endl;
    std::cout << "day:" << local_tm.tm_mday << endl;
    std::cout << "weekday:" << local_tm.tm_wday << endl;

    local_tm.tm_mday++;
    local_tm.tm_hour = 18;
    time_t tt2 = mktime(&local_tm);

    system_clock::time_point t4 = system_clock::from_time_t(tt2);
    cout << "there is only " << duration <double, ratio<3600, 1>>(t4 - t3).count() << " hours left";

    cout << currentDateTime();

}