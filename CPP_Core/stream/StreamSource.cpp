#include "StreamSource.h"
#include "serialize_deserialize.h"

int main(int argc, char *argv[])
{
    serialize_deserialize();

    //const char* name = "caiyujing";
    //std::ofstream f("dump.bin", std::ios::binary);
    //f.write((char*)name, 7);
    //f.close();

    std::ostringstream ss1;
    ss1 << "abc";
    string tmp = ss1.str();
    int i = 101;
    float f = 98.2;
    bool bv = false;
    cout << "\n++++++stream +++++\n" << endl;

    //cout.setf(ios::left);    //cout << i <<endl;
    //cout.setf(ios::right);    //cout << i << endl;
    //cout.setf(ios::showpos);    //cout << i << endl;    //cout.unsetf(ios::showpos);    
    //cout.setf(ios::hex);    //cout << i << endl;
    //cout.setf(ios::showbase);    //cout << i << endl;
    //cout.setf(ios::showpoint);    //cout << f << endl;
    //cout.setf(ios::boolalpha);    //cout << bv << endl;  //    cout.unsetf(ios::boolalpha);

    cout << dec << showbase << i << endl;
    cout << setprecision(5) << scientific << f << endl;
    cout << setfill('*')<<setw(10) << right << i << endl;

    //case 0:
    cin.unsetf(ios::skipws);
    int i2;
    cout << "try input ' 2' then enter:";
    cin >> i2; //try key in " 2" then enter. it will have issue.
    if (cin.good())
    {
        cout << i2;
    }
    else
    {
        cout << "cin.rdstate()" << cin.rdstate() <<endl;
        cout << "cin.good():" << cin.good() << endl;
        cout << "cin.fail()"<<cin.fail() << endl;
        cout << "cin.bad()" <<  cin.bad() << endl;
        cout << "cin.eof()" <<cin.eof() << endl;
        //cin_reset(cin);
    }

    //case 1:
    string res;
    cout << "key in chars, and key in x to quit:";
    for (char c; cin >> c;) //cin as istream object, can be evaluted directory as bool; it is returned from cin>>c (fluid design);
    {
        if (c == 'x') //x to quit
            break;
        else
            res.push_back(c);
    }

    //case 2:
    PhoneEntry e1{ "abc", 1234567 };
    cout << e1 << endl;
    cout << R"(key in {"NAME1", 9876543}:)";
    cin >> e1;
    if (cin.fail())
        cin_reset(cin);

    //case 3:
    vector<double> v;
    double e;
    while (cin >> e)
        v.push_back(e);
    //when user key ENTER, the control will handle over to cin, input "12 2 4 7.1" then enter, the cin>> will auto skip ' '(space) '\t'(tab) and '\n'(newline),
    //so after first time success read, " 2 4 7.1" will be left in the buffer, the 2nd time read it will skip (default) the space and successfully read 2... etc.
    //input "12 1 a b" then enter, enter will trigger it loop run the while three times, when extract ('a'), it failes (set failbit) and exit loop,
    //basically there is NO End-Of-File (no graceful way to exit) in windows (Unix Ctrl+D), end of file is a special char, it is NOT like no-more things left in the stream.

    if (cin.fail())
        cin_reset(cin);

    //case 4,streamstring is stream without IO operation, but it use the formating (cast to int/float or user type) of stream 
    stringstream ss;
    int a_a = 10;
    float b_b = static_cast<float>(15.7);
    char c_c = 'u';
    const char *d_d = "abcde";
    ss << "mixed string is " << a_a << " " << b_b << " " << c_c << " " << d_d << endl; //string s = ss.str(); //d_d NULL terminator is gone. nice.
    ss.clear();//+str("") then can truely clear the stream.
    ss.str("");
    ss << " 1  2  3 4";
    int xa, ya, za;
    ss >> xa;//it will skip the whiespace. and put 1 as integer, not '1' ascii value to he varaible xa.
    ss.ignore(5); //ignore next 5 chars.
    ss >> za; //3 ?
    ss >> noskipws >> xa >> ya >> za >> ws; // becasue noskipws is more for unformated . xa will not be 1, ya will not be 2.

    {
        cout << "\n++++++ofstream +++++\n" << endl;
        ofstream of("0.txt", std::ios::app);
        of << "3 4 5." << endl;
        of << "experience is the mother of wisdom" << endl;
        of << 234 << endl;
        of << 2.34 << endl;
        of << bitset<9>(129) << endl;
        of << complex<double>(2.0, 3.1) << endl;
        of << "honesty is the best policy" << endl;
    }

    {
        ofstream of("1.txt", ofstream::in | ofstream::out);
        of.seekp(8, ios::beg); //move the 10th position since start.
        of << "EX"; //overrite the next two char since 10th position.
        of.seekp(-5, ios::end);
        of << "OL";
        of.seekp(0, ios::end);
        of << "Nothing ventured, nothing gained";
        of.put('.');
        of.flush();

        streambuf* bf = of.rdbuf();
        streambuf* coutBuf = cout.rdbuf();
        cout.rdbuf(bf); //redirect of cout to "of"
        cout << "\n this is the last line of text" << endl;
        cout.rdbuf(coutBuf);
    }


    {
        ifstream inf("mylog.txt");
        int i;
        inf >> i; //i will be 3; formatted read, got overloaded op << for integer.
        int j;
        inf >> j; //j will be 4, white space is auto skipped 
 
        int m= inf.get(); //get next char, here is space, which is 0x20 or 32.
        int n = inf.get();//it will be 53 (ascii value of '5' chart, that is called unformated read.
        inf.unget(); //move back current position by one.

        char buf[100];
        inf.get(buf, 6); //it read to new line "5.", if 6 is more than current position to new line.
        inf.ignore(1);//skip CRLF, maybe CR in earlier line?
        int l1 = inf.get();

        string s;
        getline(inf, s); //this is better, can pass CRLF, so next read (below) is new line!
        inf.ignore(2);//ingore  in 3rd line.
        int l = inf.get(); //ascii value of '4'

        if (inf.bad() || inf.fail() || inf.eof())
            return 0;

        inf.clear(ios::failbit);//set failbit to 1.

        if (inf) //same as if (!inf.fail())
            cout << "read successfule" << endl;
        else
            cout << "read fail" << endl;

        inf.clear();//this is really clear the flags

        if (!inf.good())
            return 0;
    }

    return 0;
}