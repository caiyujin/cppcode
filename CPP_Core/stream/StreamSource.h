#pragma once
#include "getExecutionTime.h"

//cout << "\n++++++stream +++++\n" << endl;
struct PhoneEntry {
    string name;
    int phoneNum;
};

//the reason defined outside the class/struc definiation is, when call, cout << e1; enough, 
//equal to operator<<(cout, e1), and below overloaded func will be invoked.
//but sometimes if need to print out private member, got to make it friend of the 
//PhoneEntry Class/Struct, or provide getter/setter for the memeber.
//if ostream& PhotoEntry::operator<<(ostreams& os), then got to call like e1 << cout; (very anti pattern).
ostream& operator<<(ostream& os, const PhoneEntry& e) noexcept {
    return os << "{\"" << e.name << "\"," << e.phoneNum << "}";
}

istream& operator>>(istream& is, PhoneEntry& e) {
    char c, c2;
    //this is not a perfect string extractor for object phoneEntry, it is just want to demo how a typical extractor works.
    //below expect the first and second char must be { and " respectively, shall able to filter out all the space in between.
    if (is >> c && c == '{' && is >> c2 && c2 == '"') {
        string theName;
        while (is.get(c) && c != '"') //is >> c, since c is single char
            theName += c;
        if (is >> c && c == ',') {
            int thePhoneNum = 0;
            if (is >> thePhoneNum >> c && c == '}') {
                e = { theName,thePhoneNum };
                return is;
            }
        }
    }

    is.setstate(ios_base::failbit); //failbit (recoverable), badbit (unrecoverable), goodbit ( all good), eofbit (EOF bit).
    return is;
}

void cin_reset(istream& is) {
    is.clear(); //cin.clear(ios_base::failbit); can not change status of .eof()

    //string temp;
    //getline(is, temp);//with out the third argument (deliminator), the geline will read whatever left in the 'is' and dump into temp till it encounter \n 

    is.ignore(INT_MAX, '\n');
}