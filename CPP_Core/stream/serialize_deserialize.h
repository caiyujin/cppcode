#include <iostream>
#include <fstream>
#include <string>

class Person2 {
private:
    std::string name;
    int age;
    double height;

public:
    // Constructor
    Person2(const std::string& n = "", int a = 0, double h = 0.0)
        : name(n), age(a), height(h) {}

    // Friend functions for serialization and deserialization
    friend std::ostream& operator<<(std::ostream& os, const Person2& p);
    friend std::istream& operator>>(std::istream& is, Person2& p);

    // Display function for testing
    void display() const {
        std::cout << "Name: " << name << ", Age: " << age
            << ", Height: " << height << '\n';
    }
};

// Overload the << operator for serialization
std::ostream& operator<<(std::ostream& os, const Person2& p) {
    // Write the size of the string first
    size_t nameSize = p.name.size();
    os.write(reinterpret_cast<const char*>(&nameSize), sizeof(nameSize));
    // Write the string characters
    os.write(p.name.data(), nameSize);
    // Write the rest of the data members
    os.write(reinterpret_cast<const char*>(&p.age), sizeof(p.age));
    os.write(reinterpret_cast<const char*>(&p.height), sizeof(p.height));
    return os;
}

// Overload the >> operator for deserialization
std::istream& operator>>(std::istream& is, Person2& p) {
    // Read the size of the string first
    size_t nameSize;
    is.read(reinterpret_cast<char*>(&nameSize), sizeof(nameSize));
    // Resize the string and read its characters
    p.name.resize(nameSize);
    is.read(&p.name[0], nameSize);
    // Read the rest of the data members
    is.read(reinterpret_cast<char*>(&p.age), sizeof(p.age));
    is.read(reinterpret_cast<char*>(&p.height), sizeof(p.height));
    return is;
}

// Main function to test the serialization and deserialization
int serialize_deserialize() {
    // Original object
    Person2 p1("John Doe", 30, 5.9);

    // Serialize to a binary file
    {
        std::ofstream outFile("Person2.dat", std::ios::binary);
        if (outFile) {
            outFile << p1;
        }
        else {
            std::cerr << "Failed to open file for writing.\n";
            return 1;
        }
    }

    // Deserialize from the binary file
    Person2 p2;
    {
        std::ifstream inFile("Person2.dat", std::ios::binary);
        if (inFile) {
            inFile >> p2;
        }
        else {
            std::cerr << "Failed to open file for reading.\n";
            return 1;
        }
    }

    // Display the reconstructed object
    p2.display();

    return 0;
}