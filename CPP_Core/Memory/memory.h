#pragma once
#include <vector>
#include <algorithm>

std::vector<int> test1v;

void test1b()
{
    test1v.push_back(3);
    int& temp = test1v.back();
    temp *= 30;
}

void test1a()
{
    test1v.push_back(2);
    int& temp = test1v.back();
    test1b();
    temp *= 20; 
    //temp in a junk memory address, possibly not "junk",
    //and other valid variable. could be more ...damaging..
    //caused by vector reallocation to expand capacity
}

void test1()
{
    test1v.push_back(1);
    int& temp = test1v.back();
    test1a();
    temp *= 10;//temp in a junk memory address
    for (auto var : test1v)
        std::cout << var << "\t" << std::endl;
}

// solution:
std::vector<int*> test2v;
void test2b()
{
    test2v.push_back(new int(3));
    int* temp = test2v.back();
    *temp *= 30;
}

void test2a()
{
    test2v.push_back(new int(2));
    int* temp = test2v.back();
    test2b();
    *temp *= 20;
}

void test2()
{
    test2v.push_back(new int(1));
    int* temp = test2v.back();
    test2a();
    *temp *= 10;
    for (auto var : test2v)
        std::cout << *var << "\t" << std::endl;
}

std::vector<std::vector<int> > test3v;
void test3b()
{
    test3v.push_back(std::vector<int>());
    std::vector<int>& temp = test3v.back();
    temp.push_back(3*30);
}

void test3a()
{
    test3v.push_back(std::vector<int>());
    std::vector<int>& temp = test3v.back();
    test3b();
    
    temp.push_back(2*20); 
    //1. vector grow (memory realloaciton), 2. pointer invalidation , 3. reference is a pointer
    //access violation. temp now point to some rubbish area,u want to treated it as an vector, and push.. sure got issue.
}

void test3()
{

    test3v.reserve(3);//without this line, we will have memory violation. refer to comment in test3a()

    test3v.push_back(std::vector<int>());
    std::vector<int>& temp = test3v.back();
    test3a();
    temp.push_back(1*10);
    for (auto var : test3v)
    {
        for (auto var2 : var)
        {
            std::cout << var2 << "\t" << std::endl;
        }
        std::cout << std::endl;
    }
}