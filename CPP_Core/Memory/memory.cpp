#include "getExecutionTime.h"
#include "memory.h"

void misc_mem_test()
{
    char c1;
    cout << "\n start of Leak2() func:"; std::cin >> c1; //wait to take memory snapshot
    
    int* arr1 = new int[1000000];//1M*4Byte  ~4 000 000 Byte    
    cout << "\n after allocate 4000 000 Bytes int array:"; std::cin >> c1;
    
    long long* arr2 = new long long[10000];//10K *8Byte ~  80 000 Byte
    cout << "\n after allocate 80 000 Bytes Long long array:"; std::cin >> c1;
       
    delete[] arr2;
    cout << "\n p3 load a DLL Image, and make a call to its function:"; std::cin >> c1;
    
    try
    {
        throw std::out_of_range("abcd"); //caused memory leak, because it skip the delete operation.
        delete[] arr1;
    }
    catch (std::exception& e)
    {
    }
    cout << "\n p4, leak happen:"; std::cin >> c1;
}


int main(int argc, char *argv[])
{
    cout << "at start to main()" << endl;
    //Malloc20GB(); //the memory is not commited, OS can lie .
    void* ptrs[20];
    for (int i = 0; i < 20; i++)
        ptrs[i] = malloc(1 << 20);
    test1();  //container memory re-alloaction (expanding when inserting...) caused reference/pointer invalidated in contiguous storage locations container, such as vector.
    test2(); //solved by making the item/element as Pointer.
    test3(); //solved by Reserve key word, so avoid container memory re-alloaction.
    
    misc_mem_test(); //leak test, with getchar();
    return 0;
}