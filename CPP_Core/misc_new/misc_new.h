#pragma once
#include "getExecutionTime.h"
#include "UDL2.h" //demo of c++11 constexpr
#include "classes.h"

template<class Type>
constexpr Type speedofsound = Type(333.3);

struct sNew
{   //it could be quite useful for .hpp file with struct/class with static member data,
    //usually it need to be defined in the corresponding cpp file, now can be in the same hpp file.
    inline static const int sValue = 777;
};

void Tuple_Explore(void) {
    tuple<int, string, char> t1 = make_tuple(1, "aa", 'a');
    tuple<int, string, char> t2 = { 1, "bb", 'g' };

    int i91;
    char c91;
    std::tie(i91, std::ignore, c91) = t2;

    cout << get<0>(t1) << " " << get<1>(t1) << " " << get<2>(t1) << endl;
    get<0>(t1) = 2; //by ref
    string& s = get<1>(t1);    s = "bb";//by ref
    if (t1 < t2) //great feature of tuple, comparetor defined!!! unlike struct...
        cout << "t1 <t2, after lexicographical comparison, ie, item1 compare, item 2 compare..." << endl;
    t1 = t2; //member by member copy..

    int x; string y; char z;
    tie(x, y, z) = t2; cout << x << "-" << y << "-" << z << endl;     
    //great feature of tuple, can do quick data swap.

    string tmp2 = "abcdefg";
    tuple<string&> t3(tmp2); //METHOD3 initializatoin
    
    auto t4 = tuple_cat(t2, t3); ////very important features!! t4 will be a tuple<int, string, char, string&) ..

    cout << tuple_size<decltype(t4)>::value << endl; //type traits.
    tuple_element<1, decltype(t4)>::type d9;
}

std::optional<string> readFirstLineOfFile(const string& filepath)
{
    ifstream fs(filepath);
    if (fs)
    {
        string res;
        getline(fs, res);
        fs.close();
        return res;
    }
    return {};
}


void Optional_Explore(void) {
    //*********************Optional (useless feature , IMO) *********************
    optional<string> firstLine = readFirstLineOfFile("file1.txt"s);
    if (firstLine.has_value())
        cout << "it is a valid file name passed in" << endl;

    string firstLineStr = firstLine.value_or("no file name passed in");//can skip above "if"...

    std::optional<string> includePath;    
    includePath = "\\c";
    std::optional<D1> dOpt(D1("optional..."));
    if (dOpt)
    {
        cout << "d as value, it is Engaged!" << endl;
        cout << dOpt->m_Name; //when use d, as if it is an pointer!
        cout << dOpt.value().m_Name; //value() will return the value;
    }

    optional<D1> op3 = std::nullopt;
    op3.emplace("newD1Ojb"s);
}


void Variant_Explore(void) {
    //*********************************** Union and Variant *******************************/
    union UU {
        char c;
        int i;
        double d;
    };
    UU unin1;
    unin1.d = 3.14;
    int tmpI2 = unin1.i; //this will even worse, the double number been interpreted as Integer. rubbish number
    int tmpI2b = unin1.d;
    unin1.i = 10; //it will only modify the first 4 bytes of the union, it will NOT reset the last 4 bytes of the union.

    variant <char, int, double> vart1; //variant is a safe union, it will keep track of the type of the data. it keep track of the index (one more byte), so total 9 here
    vart1 = 3.14;
    //int tmpI1 = get<int>(vart1); //will throw bad_variant_access exception, because currently vart1 is of type double.
    int tmpI1b = get<double>(vart1);
    cout << vart1.index() << endl;
    vart1 = 'C';//0x43

    variant<int, double, D1> vart2(std::in_place_index<2>, "d");
    variant<vector<int>, string, D1> vart3;
    vart3 = "cpp string"s;
    if (vector<int>* p1 = std::get_if<vector<int>>(&vart3)) 
    {
        cout << (*p1).back() << endl;
    }
    else
    {
        if (string* p1 = std::get_if<string>(&vart3))
        {
            cout << *p1 << endl;
        }
        else
        {
            cout << get<D1>(vart3).m_Name << endl;
        }
    }
}

void printname(string_view sv)
{
    cout << sv << endl;
}

////*******************************user defined literal and constexpr****************////
constexpr long double operator"" cm(long double  x) { return x * 10; }
constexpr long double operator"" m(long double  x) { return x * 1000; }
constexpr long double operator"" mm(long double  x) { return x; }

constexpr double pow_int_cpp11(double base, int exp) {
    return (exp == 0) ? 1.0 : base * pow_int_cpp11(base, exp - 1);
}

constexpr double wrapper_pow_int_cpp11(double base, int exp) {
    if (exp >= 0) 
    {
        return pow_int_cpp11(base, exp);
    }
    else {
        return pow_int_cpp11(1.0 / base, -1 * exp); //excellent, simplification of math equation.
    }
}

string SpaceName = "Global";
class conststr
{
    const char* p;
    std::size_t sz;
public:
    //"Hello, world!" is a string literal of type const char[14] (include the null terminator)
    template<std::size_t N>
    constexpr conststr(const char(&a)[N]) : p(a), sz(N - 1) {}

    constexpr char operator[](std::size_t n) const
    {    //the checking will up to index 12, eg 13th char.
        return n < sz ? p[n] : throw std::out_of_range("");
    }

    constexpr std::size_t size() const { return sz; }
};
constexpr std::size_t countlower(conststr s, size_t n = 0, size_t c = 0)
{
    if( n== s.size() )
        return c;

    if ('a' <= s[n] && s[n] <= 'z')
        return countlower(s, n + 1, c + 1);
    else
        return countlower(s, n + 1, c);
}
// An output function that requires a compile-time constant, for testing
template<int n>
struct constN
{
    constN() { std::cout << n << '\n'; }
};

template<typename TupleType, typename... FuncTypes>
auto visit_each_func(TupleType&& t, FuncTypes&&... funcs)
{
    return std::array{std::apply(std::forward<FuncTypes>(funcs), std::forward<TupleType>(t))...};
}

using namespace std::chrono_literals;
template<class _Rep, class _Period>
std::future<void> TimerAsync(duration<_Rep, _Period> dur, std::function<void()> callback)
{
    return async(std::launch::async, [dur, callback]()
    {
        this_thread::sleep_for(dur);
        callback();
    }
    );
}