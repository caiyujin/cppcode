#include "misc_new.h"

//void Func(int i, int i1 = 1, int i2 = 2, int i3 = 3)
//{
//    cout << i << " " << i1 << " " << i2 << " " <<i3 << endl;
//}

int main() 
{
    //Func(99, 33);

    //only works in Optimized mode, and for string less than certain length.
    //string shortStr = "LessThan16";   //string LongStr = "much longer than length 16";

    set<char> set1{ 'a','b','c','d' };
    const auto [iter2, ok2] = set1.insert('g');

    std::map<int, char> myMap{ {1,'a'},{3,'c'},{8,'h'} };

    //another useless feature IMO
    if (int i = 99; i == 99) { cout << "c++17 feature, Init-statement for if or switch"; }

    //templated class can also deduce the types, used to be pair<double, string> s(...
    pair p(1.5, "xyz");

    //string_view (why not iterator pairs)
    string myname = "abcdefghijklmn";
    //std::span is the one for contingous memory container, such as vector!!
    string_view s1(myname.c_str(), 3);
    string_view s2(myname.c_str() + 4, 6); //6 is the lengths;
    
    printname(s1);
    printname("jjjjj"); //can pass in const char*, it will be on stack, 0 heap alloc.

    //inline static member data.
    sNew snew1;
    sNew snew2;
    cout << snew1.sValue << "vs " << snew2.sValue << endl;

    //printname2("jjjj"); "void printname2(const string& s); this will NOT be 0 heap alloc;
    //*********************Algebra Data Type*********************

    Tuple_Explore();
    Optional_Explore();
    Variant_Explore();

    //*********************lambda*********************
    dog d6{ 6, 5 };
    list<int> l1 = list<int>{ 1, 2, 4, 5, 7, 8, 16, 19, -9, 6, 5, 3, 9, 80 };
    list<int> l2;
    int i9 = 5; 
    //capture changed to [=] or [&], it will capture all, if [] means NO capture.
    auto lambda1 = [l1, &l2](dog &d6_tmp, int i_tmp) -> int {
        (d6_tmp.age)++;
        //l1.back() += 1; //l1 is captured by const ref, so cannot modifiy here.
        l2.push_back(10);
        cout << l1.back() << endl;
        return i_tmp + d6_tmp.age;
    };
    l1.back() *= 10;//pay attention to the lambda it use old value, before x10;
    l2.push_back(79);//pay attention to the lambda, it use updated value;
    cout <<"l2 before lambda call by l ref:" << l2.size() << endl; //expect 1
    cout << "lambda return value: " << lambda1(d6, i9) <<endl;
    cout << "l2 after lambda call by l ref:" << l2.size() << endl; //expect 2

    cout << SpaceName << endl; //global
    string SpaceName = "LOCAL";
    cout << SpaceName << endl; //overshadow 
    cout << ::SpaceName << endl;

    //

    unique_ptr<double> up1(new double(12.5));
    auto lambda2 = [up2 = move(up1)]{ cout << *up2 <<endl; }; //lambda capture with move.up1 will be nullptr
    lambda2();

    auto result = visit_each_func(
        std::tuple{ 1.5, 2.3 },
        [](auto&& x, auto&& y) { return x + y; },
        [](auto&& x, auto&& y) { return x - y; },
        [](auto&& x, auto&& y) { return x * y; },
        [](auto&& x, auto&& y) { return x / y; }
    );
    auto result2 = visit_each_func(
        std::tuple{ 1.5, 2.3, 3.5 },
        [](auto&& x, auto&& y, auto&& z) { return x + y+z; },
        [](auto&& x, auto&& y, auto&& z) { return x - y-z; },
        [](auto&& x, auto&& y, auto&& z) { return x * y*z; },
        [](auto&& x, auto&& y, auto&& z) { return x / y/z; }
    );
    //******************constexpr*********************
    //
    //for function/class/namespace level scope, constexpr is implicitly inline. need to put static to avoid copied to function stack (if it is huge array) every time the func is called.
    //static constexpr vector<int> x = returnHugeVec;

    constexpr int sos1 = speedofsound<int>;
    constexpr int sos1by100 = sos1 / 100;
    std::array<int, sos1by100> sos1_std_arr;

    constexpr double pow = wrapper_pow_int_cpp11(2.5, 9);
    static_assert(wrapper_pow_int_cpp11(-2, -4) == 0.0625, "yippe"); //compile time
    vector<double> v9(4, pow);

    std::cout << "The number of lowercase letters in \"Hello, world!\" is ";
    constN<countlower("Hello, world!")> out2; // implicitly converted to conststr

    using namespace literals;
    cout << "Hello, world"s.size() << '\n'; // 's' is built in user literal to convert const char* to string.
    cout << 1.2 + 2.1i << '\n'; //complex number, 'i' is built in user literal to convert to complex number.
    long double h1 = 34.0cm;//user defined literal(simple),base is mm(millimeter).
    cout << h1 << " \t" << h1 + 13.1mm << "mm" << endl;

    //defined in UDL2.h. user defined literal, SI system
    Mass myMass = 80 * kg;
    cout << "my mass: " << myMass.Convert(pound) << " pounds" << endl;
    constexpr Length distance100 = 100 * kilometre;
    constexpr Time t1 = 2 * hour;
    constexpr Speed sp1 = distance100 / t1;
    cout << "100 km in 2 hours: " << sp1.Convert(mile / hour) << " miles/hour" << endl;
    cout << "100 km in 2 hours: " << sp1.Convert(metre / second) << " metre / second" << endl;
    constexpr Acceleration Acce = sp1 / (3 * second);

    auto future1 = TimerAsync(5s, []() {cout << "timer finished"; });
    while (true)
    {
        cout << "processing...\n";
        this_thread::sleep_for(1s);

        auto status = future1.wait_for(1ms);
        if (status == future_status::ready)
            break;
    }

    return 0;
}