#include "iostream" //c with .h, c++ no h.
#include <string>   //angular bracket search in default include path.
#include "log.h"    //quote normally relative to current file, quotes is more versitle, see above "iostream"
//error C1208, C means Compile error; //error LNK1516, LNK, linking error;

void Log(const char *message)
{
    std::cout << message << std::endl;
}

//since this case no header file, so we put the duplicate declaration here, so this translation unit
//aware there are defination(implemnentation) of the function somewhere in other translation unit (.o file from the math.cpp)
//if anyhow declear one below "spell wrong" function (no impl), still compile and run without error.
int multiplerrr(int a, int b);
int mathMultiple(int a, int b);

int mathPrint();
//void Log2(const char* message); //since Log2 is static and internal to Math.o only. so can not make it at linking stage.

extern int h2_cpp_extern;//exactly same as logExtern, which is in log.cpp
int main()
{
    banlists b;
    h2_cpp_extern = 1000;
    cout << h2_cpp_extern << endl;

    cout << "main cpp:"  << log_cpp_extern << endl;
    cout << "main cpp:" << log_h_static << endl;
    log_h_static = 90;
    log_cpp_extern = 9;

    cout << "main cpp:" << log_cpp_extern << endl;
    cout << "main cpp:" << log_h_static << endl;

    logPrint();
    cout << "static member data is truely global,banlists::m_instCnt: " << &banlists::m_instCnt << endl;
    cout << "inline data is truely global: " << &inlineS << endl;
    cout << "static data is per T.U log_h_static: " << &log_h_static << endl;

    Log("Hello world!");
    mathPrint();
    mathMultiple(2, 3);

    initLogStatic(); //(an static function, each .obj has its own copy?)
    return 0;
}
