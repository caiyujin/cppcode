#pragma once 
//head guard, supported by most popular compiler gcc,clang single include in a single traslation unit..it can be inlcude in multiple translation unit. 
//it mainly used in the chains of include, which end up include certain header file multiple time in a single translation unit.
#include <mutex>
#include <vector>
#include <string>
#include <iostream>

using namespace std;
void Log(const char* message);

//it is allowed in one translation unit, got multiple same declaration, but only one defination (with body) 
void logPrint(void);
//void InitLog(void);

//then static/or inline to avoid duplicated symbol (func) at linking stage.
//this will create two different internal (for their own translation unit) linkage versions 
//of common_func for log.cpp and main.cpp respectively
static void initLogStatic(void) 
{
    cout << "static func in log.h" << endl;
}

static long log_h_static = 100;

class banlists {
public:
    banlists() :m1(1) {}
    int m1;
public:

    static int m_instCnt;
};
inline string inlineS = {"inline string" };
extern long log_cpp_extern;