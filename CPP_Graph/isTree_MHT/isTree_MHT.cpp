#include "getExecutionTime.h";
#include "graph_infra.h"
 unordered_map<int, vector<int> > adj;
 vector<edge> edges;
 set<char> vertex;

void findMinHTree()
{
    bool cont = true;
    set<int> visited;
    size_t heigth = 0;

    while (cont)
    {
        cont = false;
        for (int i = 0; i < adj.size(); i++) //" if for example, 10 - 1 - 2 - 4 - 5 - 6 - 7 - 8 - 9 ; if this adj iterate from middle (lets say 4, it might visit 9/10 at last , so this code will break. ...it only detect 9/10 as leaf
        {
            if (adj[i].size() == 1 && visited.find(i) == visited.end()) //leaf will only have one connection to its "parents", and if it is un-visited yet.
            {
                int other = adj[i][0]; //the only connected node 
                adj[other].erase(find(adj[other].begin(), adj[other].end(), i)); //for circle nodes, even if it removed a leaf node from it, there are at least still 2 nodes connecting, so will never be able to size==1;
                visited.insert(i);
                cont = true;
            }
        }
        if (cont)
            heigth++;
        else
            break;
    }


    if(visited.size()!=adj.size()-1)
        cout<<"not a tree"<<endl;
    else
        cout<<"min height is "<<heigth<<endl; //the remaining 6 is the root for this possible MHT
}

int main(int argc, char *argv[])
{
    addEdge(0, 3);
    addEdge(1, 3);
    addEdge(1, 11);
    addEdge(11,12);
    addEdge(2, 3);
    addEdge(3, 4);
    addEdge(4, 5);
    addEdge(4, 10);
    addEdge(5, 6);
    addEdge(6, 7);
    addEdge(6, 8);
    addEdge(8, 9);
    addEdge(9, 6);
   // addEdge(11, 10);
    findMinHTree();

    return 0;
}

