
#include "getExecutionTime.h";
//http://yucoding.blogspot.com/2015/06/leetcode-question-course-schedule.html
//
/*
There are a total of n courses you have to take, labeled from 0 to n - 1.
Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]
Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?
For example:
2, [[1,0]]
There are a total of 2 courses to take. To take course 1 you should have finished course 0. So it is possible.
2, [[1,0],[0,1]]
There are a total of 2 courses to take. To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.

*/
int N = 8;
vector<vector<int>> Courses(N+1);
vector<vector<int>> Pres(N + 1);

void addEdge(int v, int w)
{
    Courses[v].push_back(w);
    Pres[w].push_back(v);
}

bool canFinish(const vector<int>& startPoints)
{
    set<int> visited;
    for (int j = 0; j < startPoints.size(); j++)
    {
        deque<int> curr;
 
        curr.push_back(startPoints[j]);
        visited.insert(startPoints[j]);

        while (!curr.empty())
        {
            int cur = curr.front();
            curr.pop_front();
            for (int i = 0; i < Pres[cur].size(); i++) //which course has prerequisite of cur, the question is one course can only ONE prerequisites, or None.
            {
                if (visited.find(Pres[cur][i]) == visited.end())
                {
                    curr.push_back(Pres[cur][i]);
                    visited.insert(Pres[cur][i]);
                }
            }
        }
    }

    if(visited.size()==N)
        return true;
    
    return false;
}
int main(int argc, char *argv[])
{
    addEdge(1, 2);
    addEdge(6, 4);
    addEdge(2, 3);
    addEdge(5, 3);
    addEdge(3, 6);
    addEdge(7, 8);

    vector<int> startPoints;
    for (int i =1; i < Courses.size(); i++)
    {
        if (Courses[i].size() == 0) //no prerequisites
            startPoints.push_back(i);
    }

    //if all courses have prerequisites, then not possible to start!
    if(startPoints.size()==0)
        cout << "can not";

    cout << boolalpha << canFinish(startPoints) << endl;
    return 0;
}

