/*
graphs may contain cycles, so we may come to the same node again.To avoid processing a node more than once, we use a boolean visited array.
*/
#include "getExecutionTime.h";
#define TDV 9




//get shortest path from src to all other nodes,BFS. 0 means no direct connection

vector<vector<int> > g3 = {
                            { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
                            { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
                            { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
                            { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
                            { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                            { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
                            { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
                            { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
                            { 0, 0, 2, 0, 0, 0, 6, 7, 0 }
};

void dijkstra(int src) {

    vector<int> dist(TDV,INT_MAX);
    dist[src] = 0;
    deque<int> q;
    q.push_back(src);

    //BFS
    while (!q.empty()) 
    {    
        int i = q.front(); 
        q.pop_front();

        for (int k = 0; k < TDV; k++) {
            if (g3[i][k] == 0) //got edge connecting the points?
                continue;
            
            if (dist[k] == INT_MAX) 
            {
                q.push_back(k);
                dist[k] = dist[i] + g3[i][k]; //plus the edge weight
                continue;
            }

            if (dist[k] > g3[i][k] + dist[i])
                dist[k] = g3[i][k] + dist[i];
        }
    }

    for (int i=0;i<TDV;i++)
    {
        cout << "from 0 to " << i << " minimum distance is :" << dist[i] << endl;
    }
}

void findPathLongerThan(int start, int end, int target) {

    vector<int> dist(TDV, INT_MAX);
    dist[start] = 0;
    deque<int> q;
    q.push_back(start);

    //BFS
    while (!q.empty())
    {
        int i = q.front();
        q.pop_front();

        for (int k = 0; k < TDV; k++) {
            if (g3[i][k] == 0) //got edge connecting the points?
                continue;

            if (dist[k] == INT_MAX)
            {
                q.push_back(k);
                dist[k] = dist[i] + g3[i][k]; //plus the edge weight
                continue;
            }

            if (dist[k] < g3[i][k] + dist[i])
                dist[k] = g3[i][k] + dist[i];
        }
    }

    cout << "the max distance from " << start << " to " << end << " is " << dist[end] << endl;

    cout << boolalpha << "is it longer than " << target << "? " << (dist[end] > target) << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
    //dijkstra(0);

    findPathLongerThan(0, 7, 21);
    return 0;
}

