
#include "getExecutionTime.h";
#include "graph_infra.h"
unordered_map<int, vector<int> > adj;
vector<edge> edges;
set<char> vertex;

bool isTree(int i)
{
    deque<int> cur;
    set<int> visited;

    cur.push_back(i);
    visited.insert(i);

    while (!cur.empty())
    {
        int val = cur.front();
        cur.pop_front();

        for (auto& a : adj[val])
        {
            if (find(cur.begin(), cur.end(), a) != cur.end())//no circles
                return false; 

            if (visited.find(a) == visited.end())
            {
                visited.insert(a);
                cur.push_back(a);
            }
        }
    }

    if (visited.size() == adj.size()) //no islands
        return true;

    return false;
}

int main(int argc, char *argv[])
{    
    addEdge(0, 1);
    addEdge(1, 2); //comment out this to make it return true
    addEdge(2, 3);
    addEdge(3, 0);

    //choose a random vertex, do BFS, 
    if (isTree(0))
        cout << "is tree" << endl;
    else
        cout << "is not tree" << endl;

    return 0;
}

