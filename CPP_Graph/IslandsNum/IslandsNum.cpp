#include "getExecutionTime.h";
vector<vector<int> > matrix;
constexpr int sz = 5;

void fillMatrix()
{
    matrix.resize(sz);
    for (int i = 0; i < sz; i++)
    {
        matrix[i] = vector<int>(sz);
        for(int j=0;j<sz;j++)
            matrix[i][j] = RndEng() % 2; //this one is none constexpr, not sure the matrix will be generated at compile time or run time
    }    
}

void calculateIslandNum()
{
    int islandNum = 0;
    for (int i = 0; i < sz; i++)
    {
        for (int j = 0; j < sz; j++)
        {
            if (matrix[i][j] == 1)  //found an 1, start a BFS, and mark all the adjecent 1 to 0, so the outer loop will not count them again
            {
                islandNum++;
                deque<pair<int, int> > curr;
                curr.push_back(make_pair(i, j));
                while (!curr.empty())
                {
                    pair<int, int> cur = curr.front();
                    curr.pop_front();
                    matrix[cur.first][cur.second] = 0;
                    if (cur.first - 1 >= 0 && matrix[cur.first - 1][cur.second] == 1)
                        curr.push_back(make_pair(cur.first - 1, cur.second));
                    if (cur.first + 1 < sz && matrix[cur.first + 1][cur.second] == 1)
                        curr.push_back(make_pair(cur.first + 1, cur.second));
                    if (cur.second - 1 >= 0 && matrix[cur.first][cur.second - 1] == 1)
                        curr.push_back(make_pair(cur.first, cur.second - 1));
                    if (cur.second + 1 < sz && matrix[cur.first][cur.second + 1] == 1)
                        curr.push_back(make_pair(cur.first, cur.second + 1));
                }
            }
        }
    }
    cout << "island num is " << islandNum << endl;

}
/* why need to check left and up,
    1 1 0 1 0
    0 1 1 0 1
    1 0 1 0 0 
    0 0 0 1 0
    1 0 1 1 1 // the 4th item 1, it will NOT detect the left side (3rd position 1). so it will increase one more island
                    
*/

int main(int argc, char *argv[])
{
    fillMatrix();
    calculateIslandNum(); //copilot generted;
    return 0;
}

