//this is to find the minimum total weight, to connect all the vertixes
#include "getExecutionTime.h"
#include "graph_infra.h"

vector<edge> edges;
unordered_map<int, vector<int> > adj;
set<char> vertex;

int N = 6;
unordered_map<char, char> Roots;
void kruskal()
{
    vector<edge> res;
    for (auto& e : edges)
    {
        char p1Root = Roots[e.p1];
        char p2Root = Roots[e.p2];
        if (p1Root != p2Root) //if the two vertixes are not in the same set
        {
            res.push_back(e);
            //really, it is much easier and readable with normal for loop;
            std::for_each(Roots.begin(), Roots.end(), 
                [&p1Root,&p2Root](pair<const char, char>& p) 
                {
                    if(p.second == p1Root) 
                        p.second = p2Root;
                }
            );
        }
    }

    cout << accumulate(res.begin(), res.end(), 
        (long long)0, [](long long& init, const edge& rhs) 
        {return init + rhs.w; })
        << endl;
}

int main(int argc, char *argv[])
{
    addEdge('a', 'b', 4);
    addEdge('a', 'f', 2);
    addEdge('b', 'f', 5);
    addEdge('b', 'c', 6);
    addEdge('c', 'd', 3);
    addEdge('c', 'f', 1);
    addEdge('f', 'e', 4);
    addEdge('d', 'e', 2);
    
    sort(edges.begin(), edges.end());
    for (char x : vertex)
        Roots[x] = x;

    kruskal();

    return 0;
}
