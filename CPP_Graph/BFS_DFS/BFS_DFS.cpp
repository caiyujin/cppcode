/*
graphs may contain cycles, so we may come to the same node again.To avoid processing a node more than once, we use a boolean visited array.
*/
#include "getExecutionTime.h";


class Graph
{
private:
    int sz;
    int root = INT_MIN;
    vector<vector<int> > adj;

    bool DFS_recursive(int node, int v, unordered_map<int, bool>& visited);
    bool BFS_recursive(int v, deque<int>& q, unordered_map<int, bool>& visited);
public:

    Graph(int V);
    void addEdge(int v, int w);
    bool BFS_iterative(int v);
    bool DFS(int v);
    bool BFS(int v);
};

Graph::Graph(int Val)
{
    sz = Val;
    adj = vector<vector<int> >(sz, vector<int>());
}

void Graph::addEdge(int v, int w){
    if (root == INT_MIN)
    {
        root = v;
    }
    adj[v].push_back(w);
}

bool Graph::BFS_iterative(int v) 
{
    unordered_map<int, bool>  visited;
    deque<int> q;

    q.push_back(root);
    visited[root] = true;

    while (!q.empty()) 
    {
        int val = q.front();
        cout << val << " ";
        q.pop_front();

        if (val == v)
            return true;

        for (auto it = adj[val].begin(); it != adj[val].end(); it++)
        {
            if (visited.find(*it) == visited.end())
            {
                visited[*it] = true;
                q.push_back(*it);
            }
        }
    }

    return false;
}

bool Graph::DFS_recursive(int node, int v, unordered_map<int, bool>& visited)
{
    visited[node] = true;
    cout << node << " ";
    if (v == node)
        return true;

    for (auto it = adj[node].begin(); it != adj[node].end(); it++)
    {
        if (visited.find(*it)==visited.end())
        {
            if (DFS_recursive(*it, v, visited))
                return true;
            //false, got to continue search all sibling nodes.
        }
    }
    return false;
}

bool Graph::DFS(int v)
{
    unordered_map<int, bool> visited;
    return DFS_recursive(root, v, visited); //start from root.
}

bool Graph::BFS_recursive(int v, deque<int>& q, unordered_map<int, bool>& visited)
{
    if (q.empty())
        return false;

    int val = q.front();
    q.pop_front();

    cout << val << " ";

    if (val == v)
        return true;

    for (auto it = adj[val].begin(); it != adj[val].end(); it++)
    {
        if (visited.find(*it) == visited.end())
        {
            visited[*it] = true;
            q.push_back(*it);
        }
    }
    return BFS_recursive(v, q, visited);
}

bool Graph::BFS(int v)
{
    unordered_map<int, bool>  visited;
    deque<int> q;

    q.push_back(root);
    visited[root] = true;

    return BFS_recursive(v, q, visited);
}

int _tmain(int argc, _TCHAR* argv[])
{
    //8-number of nodes/vertice
    Graph g1(8);
    g1.addEdge(0, 1);
    g1.addEdge(0, 2);
    g1.addEdge(0, 5);
    g1.addEdge(1, 2);
    g1.addEdge(1, 3);
    g1.addEdge(2, 0);
    g1.addEdge(2, 3);
    g1.addEdge(3, 3);
    g1.addEdge(3, 4);
    g1.addEdge(3, 5);
    g1.addEdge(3, 6);
    g1.addEdge(5, 7);

    if (g1.DFS(5))
        cout << "found" << endl;
    if (!g1.DFS(10))
        cout << "cannnot found" << endl;

    if (g1.BFS(7))
        cout << "found" << endl;
    if (!g1.BFS(9))
        cout << "cannnot found" << endl;

    if (g1.BFS_iterative(3))
        cout << "found" << endl;
    if (!g1.BFS_iterative(10))
        cout << "cannnot found" << endl;
 

    return 0;
}

