// console1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "console2.h"
#include "class1.h"
#include "class2.h"

//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>


using namespace std;
bool stopSideThread = false;

void t1Func()
{
    while (!stopSideThread)
    {
        cout << __FUNCSIG__ << " Running " << endl;
        this_thread::sleep_for(3s);
    }

    cout <<__FUNCSIG__ << " exiting "<< endl;
}

void t3Func()
{
    ////this dll is loaded when the executable start (implicite loading, 
    class1 c1("n1");
    c1.memoryLeakSim();
   // c1.accessViolationSim();
    //c1.loopNsecDefault10s();
    //c1.takeDump();
    char a; 
    cout << "key in s to start call the increasing function" << endl; 
    cin >> a;

    for (int i = 0; i < 20; i++)
    {
        c1.callWithIncreasingExecutionTime(i);
    }
}

void LoadDLLExplicitely(){
    //this add.dll is loaded after below line. (explicte loading)
    HINSTANCE hGetProcIDDLL = LoadLibraryA("add.dll");
    if (hGetProcIDDLL != NULL)
    {
        typedef int(__stdcall *funcP)(int);
        funcP f1 = (funcP)GetProcAddress(hGetProcIDDLL, "addi");
        if (!f1)
        {
            //search for the name "returnMagicNumber" in class1.dll,u will found it is mangled C++ name. 
            //try to build the dll with extern "C" for the func in class 1 source code. 
            FreeLibrary(hGetProcIDDLL);
            std::cout << "CANNOT LOCATE FUNCTION IN DLL" << std::endl;
        }
        else
        {
            std::cout << "calling from the run time loaded dll with returnMagicNumber:" << f1(10) << std::endl;
        }
    }
    else
    {
        std::cerr << "CANNOT OPEN dll" << endl;
    }
}
int main()
{
    //LoadDLLExplicitely();

    //std::thread t1(t1Func);

    //std::thread t2([]()
    //    {    
    //        char a; 
    //        cout << "key in q to quit side thread" << endl; 
    //        cin >> a;
    //        stopSideThread = true; 
    //    });

    std::thread t3(t3Func);
    //t1.join();
    //t2.join(); //singal stop, t1 will stop. 
    t3.join();

    class2 c2;
    class2b c2b;

    getchar();
}
