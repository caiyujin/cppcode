#include "pch.h"
#include <utility>
#include <limits.h>
#include <iostream>
#include "MathLibrary.h"

static ull previous_;
static ull current_;
static unsigned index_;
static ull runUpto_;

void fibonacci_init(const ull a, const ull b, ull runUpto)
{
    index_ = 0;
    current_ = a;
    previous_ = b; // see special case when initialized
    runUpto_ = runUpto;
}

bool fibonacci_next()
{
    if (runUpto_ - previous_ < current_)
    {
        return false;
    }

    if (index_ > 0)
    {
        previous_ += current_;
    }
    std::swap(current_, previous_);
    ++index_;
    return true;
}

// Get the current value in the sequence.
ull fibonacci_current()
{
    return current_;
}

// Get the current index position in the sequence.
unsigned fibonacci_index()
{
    return index_;
}

//do NOT delete below two function. for c# marshalling.

void cppPrintString(char* str, int sz)
{   
    //std::string s(str);
    std::cout << "print out in cpp dll:";
    for (int i = 0; i < sz; i++)
        std::cout << str[i];
    std::cout << std::endl;

}

void cppPrintInt(int i)
{
    std::cout << "print out in cpp dll:" << i;
    std::cout << std::endl;
}

std::string CencryptedString::getOriginal() {
    std::string s2;
    for (auto& a : _s) {
        s2.push_back(a - 1);
    }
    return s2;
}

namespace CaiUtilities
{
    Birthday_dll::Birthday_dll() {
        std::stringstream ss_abc;
        ss_abc << "01" << "---" << "01" << "---" << "2000";
        abc = new std::string(ss_abc.str());
        cout << "Birthday_dll()" << endl;
    }

    Birthday_dll::Birthday_dll(int m, int n, int y) {
        std::stringstream ss_abc;
        ss_abc << m << "---" << n << "---" << y;
        abc = new std::string(ss_abc.str());
        cout << "Birthday_dll(int m, int n, int y)" << endl;
    }

    void People_dll::printInfo() {
        cout << name << "was born on ";
        dateOfBirth->printDate();
    }


    double SimpleMathOps::Add(double a, double b)
    {
        /*vector<int> v; v[10] = 10;*/
        return static_cast<double>(static_cast<int>(a) + static_cast<int>(b));
    }

    double SimpleMathOps::Multiply(double a, double b)
    {
        return static_cast<double>(static_cast<int>(a) * static_cast<int>(b));
    }
}

void SampleClassTest1(void) {
    std::cout << "func in dll source code dllmain.cpp" << std::endl;
    return;
}

void SampleClassTest2(char* c) {
    std::cout << "func in dll source code SampleClass.cpp. " << c << std::endl;
    return;
}
