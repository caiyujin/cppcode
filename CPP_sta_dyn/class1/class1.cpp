#include "pch.h"
#include "class1.h"
#include "createDumpNcallStack.h"

using namespace std;

struct AllocationMetrics
{
    long long TotalAllocated = 0;
    long long TotalFreed = 0;
    long long CurrentUsage() { return TotalAllocated - TotalFreed; }
};

static AllocationMetrics sAM;
void* operator new(size_t size)
{
    if (size >100) 
        cout << "allocation with size of :" << size << endl;
    sAM.TotalAllocated += size;
    return malloc(size);
}

void operator delete(void* memory, size_t size)
{
    if (size > 100)
        cout << "deletion with size of :" << size << endl;
    sAM.TotalFreed += size;
    free(memory);
}

struct BigObjStr2
{
    vector<long long> mV1;
    BigObjStr2() {
        mV1 = vector<long long>(1000, 99);
    }
};

void class1::sleepForNSec(int i,string callerName)
{
    int j = 0;
    while (true)
    {
        cout << callerName <<  " j:" << j++ << endl;
        std::this_thread::sleep_for(1000ms);

        if (j == i)
        {
            return;
        }
    }
}

void class1::loopNsecDefault10s(int i)
{
    sleepForNSec(i, string(__FUNCSIG__));
}

void class1::takeDump(int i)
{
    try
    {
        sleepForNSec(i, string(__FUNCSIG__));
        TakeADumpImpl();
        throw std::out_of_range(std::string(__FUNCTION__) + "Out of range execption from");
    }

    catch (const std::out_of_range& e1)
    {
        std::cout << e1.what() << std::endl;
    }
}

void class1::hangSim(int i)
{
    while (true)
    {
        sleepForNSec(i,string(__FUNCSIG__));        
        getName();
    }
}

void class1::accessViolationSim()
{
    //int i = 0;
    //while (i < 10)
    //{
    //    std::this_thread::sleep_for(1000ms);
    //    i++;
    //}
    //long* pl = nullptr;
    //cout << *pl;

    int* p = nullptr;
    p = new int(100);
    char in;
    cout << "key in 'd' to simulate access violation:" << endl;
    cin >> in;
    if (in == 'd')
    {
        delete p;
        cout << "p is deleted" << endl;
    }
    cout << *p << endl;
}

void class1::memoryLeakSim(int i)
{
    vector<vector<long long> > longVV;
    int leakCount = 10;
    int j = 0;

    while (j < leakCount)
    {
        longVV.push_back(vector<long long>(100000,99));
        std::cout << "side thread, allocated 100K * 8 Bytes size of vector, sleep for 1 sec" << std::endl;
        BigObjStr2* p = new BigObjStr2();
        this_thread::sleep_for(1s); //cpu NON-busy waiting. allowed cpu to context - switch 
        j++;
    }

    longVV.clear();
    cout << "sAM.CurrentUsage: " << sAM.CurrentUsage() << endl;
}
void class1::callWithIncreasingExecutionTime(int i)
{
    vector<int> toBeSort(100000, INT_MAX);
    cout << "i:" << i << endl;
    do
    {
        for (auto& a : toBeSort)
            a = RndEng() % 1000;
        std::sort(toBeSort.begin(), toBeSort.end());
        i--;
    } while (i - 6 > 0);

}

void class1::printStaticMessage() {
    std::cout << "__FUNCSIG__" << std::endl;
}
