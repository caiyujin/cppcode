#pragma once
#include "getExecutionTime.h"

class class1
{
public:
    __declspec(dllexport) class1(std::string s): m_name(s)
    {
        std::cout << __FUNCSIG__ << std::endl;
    }

    __declspec(dllexport) void loopNsecDefault10s(int i = 10);

    __declspec(dllexport) void takeDump(int i=10);

    __declspec(dllexport) void hangSim(int i=10);

    __declspec(dllexport) void accessViolationSim();

    __declspec(dllexport) void memoryLeakSim(int i=10);
    
    __declspec(dllexport) void callWithIncreasingExecutionTime(int i);

    __declspec(dllexport) static void printStaticMessage();

    __declspec(dllexport) std::string& getName() {return m_name;};

private:
    void sleepForNSec(int i, string callerName);
    std::string m_name;
};

extern "C"
{
    int __declspec(dllexport) returnMagicNumber()
    {
        class1 obj1(string("obj1"));
        //obj1.hangSim();
        return 666;
    }

    int __declspec(dllexport) returnMagicNumber2(int val)
    {
        class1 obj2(string("obj2"));
        obj2.loopNsecDefault10s();
        return 777;
    }
}
