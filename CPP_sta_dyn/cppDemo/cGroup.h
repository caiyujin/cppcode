#pragma once
#include "cD1.h"
#include "cD2.h"
#include <iostream>
struct cGroup
{
    static int count;
    cD1 o1;
    cD2 o2;
    cGroup(const cD1& _o1, const cD2& _o2): o1(_o1), o2(_o2)
    { 
        std::cout << __FUNCSIG__ << std::endl;
        std::cout << &cBase::g2 << std::endl;
        count++;
    }
    ~cGroup() { count--; }
    void printMemberClassNames();
};
