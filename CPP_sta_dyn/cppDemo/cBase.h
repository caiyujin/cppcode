#pragma once
#include <iostream>
class cBase
{
public:
    cBase();
    cBase(int i):cid(i)
    {
        std::cout << "cBase::cBase(int i)" << std::endl;
    }

    virtual void printClassName();
    cBase& operator=(const cBase& rhs)
    {
        cid = rhs.cid * 1.0;
        return *this;
    }

    int& getcid() { return cid; }

    void setcid(int value) { cid = value; }

    virtual ~cBase() 
    {    
    }

    inline static std::string g2 = "g2";; //unique copy.
private:
    int cid;
};

static std::string g1 = "g1"; //have 3 copies in three TU.
inline static std::string g3 = "g3"; //have 3 copies in three TU.