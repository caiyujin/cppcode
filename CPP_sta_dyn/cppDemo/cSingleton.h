#pragma once
#include <map>
#include <string>
#include <iostream>
class cSingleton
{
public:
    static inline cSingleton& getInstance()
    {
            static cSingleton* instance = new cSingleton();
            //this line will be skipped for 2nd run onwards
            std::cout << "the pointer addr:" << instance << std::endl;
            return *instance;
 
    }
    ~cSingleton() { delete &(getInstance()); }
    
    std::map<int, std::string>& getMap();// { return mMap; }
private:
    cSingleton() {};//keep private, so no client directly create a new instance.
    std::map<int, std::string> mMap;
};

