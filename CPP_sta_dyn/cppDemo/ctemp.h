#pragma once
#include <vector>

template<typename T>
class ctemp
{
public:
    ctemp();
    ctemp(const T& _t);
    std::string convertToString();
    long convertTolong();
    T m_v;
};

#include <sstream>
#include <string>
template<typename T>
ctemp<T>::ctemp()
{
    m_v = T();
}

template<typename T>
ctemp<T>::ctemp(const T& _t)
{
    m_v = _t;
}

template<typename T>
std::string ctemp<T>::convertToString()
{
    std::stringstream ss;
    ss << m_v;
    return ss.str();
}

template<typename T>
long ctemp<T>::convertTolong()
{
    return static_cast<long>(m_v);
}