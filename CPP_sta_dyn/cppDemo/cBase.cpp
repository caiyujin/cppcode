#include "cBase.h"
//#include <iostream>
#include "iostream"  //file not end with .h, purpusely to distinguish with C language
cBase::cBase():cid(0) 
{ 
    std::cout << __FUNCSIG__<< std::endl; 
    std::cout << &g2 << std::endl;

}

int X2(int i); 
//declare here, so that cBase.cpp when compile (TU only) knows there is a declaration of such file. body can be found during linking.
//or simply group all the functions (commonly used by other TU also) into a declartion header file.



void cBase::printClassName() 
{ 
    std::cout << __FUNCSIG__ << std::endl;
    std::cout << g1 << " " << &g1 << std::endl;
    std::cout << g3 << " " << &g3 << std::endl;

    std::cout << X2(19) << std::endl;
};