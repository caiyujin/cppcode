#pragma once
#include <vector>
class cComp
{
public:
    std::vector<double> oneMilItems;
    bool assigned = false;
};

class cContainer
{
public:
    cContainer(cComp* _c);
    ~cContainer() { delete c; }
    std::vector<int>& getV() { return idxForLargeVal; }
private:
    cComp* c;
    std::vector<int> idxForLargeVal;
};