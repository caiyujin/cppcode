#pragma once
#include "cBase.h"
#include "cSingleton.h"

class cD1 : public cBase
{
public:
    cD1();
    virtual void printClassName();
    double& getc1id() { return c1id; }
    void setc1id(double value) { c1id = value; }
    virtual ~cD1() {}
private:
    double c1id;
};

//static void sameFuncName(long p);