#include "cBase.h"
#include "cD1.h"
#include "cD2.h"
#include "cGroup.h"
#include "ctemp.h"
#include "cSingleton.h"
#include "cComp.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <list>
#include <deque>
#include <thread>
#include <exception>
#include "cDefaultFuncs.h"
#include "Source.h"
#include "class1.h"
using namespace std::literals::chrono_literals;

int globalInt = 999;

extern "C"
{
    bool __declspec(dllexport)  test();
}

template<typename t1, typename t2>
long add(const t1& _t1, const t2& _t2)
{
    return (long)_t1 + (long)_t2;
}

void fillUpVs_byValue(std::vector<std::string> vs)
{
    for (int i = 1; i <= 100; i++)
    {
        std::string s(3, i % 26 + 64);
        globalInt++;
        vs.push_back(s);
    }
}

void inline fillUpVs(std::vector<std::string>* _vs)
{
    for (int i = 1; i <= 100; i++)
    {
        std::string s(3, i % 26 + 64);
        _vs->push_back(s);
    }
}
void fillUpVsRef(std::vector<std::string>& _vs)
{
    for (int i = 1; i <= 100; i++)
    {
        std::string s(3, i % 26 + 64);
        _vs.push_back(s);
    }
}

//default is extern. can be used by cBase.cpp
int X2(int i)
{
    cout << __FUNCSIG__ << endl;
    return i * 2;
}

bool test()
{
    int i = 100;
    if (i >= 99)
        return true;

    return false;
}

int main()
{   
     /* pointer casting, alignment study*/
    char ch = '9';
    int *int_ptr = (int *)&ch;
    char *char_ptr = (char *)int_ptr;
    std::cout << *int_ptr << std::endl;
    std::cout << *char_ptr << std::endl;
    //char x;  //this is the same size of below buffer (1), it could cause alighment issue...

    //unsigned char buffer1[10] = { '1','2','3','4','6','7','8','9','10' };
    //unsigned char buffer2[10] = { '1','2','3','4','6','7','8','9','10' };
    ////int newval = *(int*)(buffer1 + 3) + 88; //(int*)(buffer1+3), will cast '4''6''7''8' as a single integer
    //int newval = *(buffer1 + 3) + 8; 
    //*(int*)(buffer2 + 1) = newval; //it will overwrite '2''3''4''6' with the 4 byte integer
    
    std::cout << static_cast<char>(4) << std::endl;
    std::cout << static_cast<char>(5) << std::endl;
    cDefaultFuncs o1a;
    cDefaultFuncs o2a(o1a);
    cDefaultFuncs o3a;

    o3a = o1a;

    {
        cDefaultFuncs o4;
    }

    cBase o1; //cBase is type

    {
        cBase o6(10);
        o6.printClassName();
    }

    cBase o2;
    o1.printClassName();
    o1.setcid(10);
    std::cout << o1.getcid() << std::endl;
    o2 = o1;//user defined an assignment operator , so default is not used;
    {
        cBase cobj3(o2); //invoke default copy constructor.
    }//invoke default destructor upon scope exit

    cD1 oD1;
    oD1.printClassName();
    oD1.setcid(11);
    oD1.setc1id(11.1);
    std::cout << oD1.getcid() << "\t c1id:" << oD1.getc1id() << std::endl;

    cD2 oD2;
    oD2.printClassName();
    oD2.setcid(12);
    oD2.c2id = std::string("c2obj");
    std::cout << oD2.getcid() << "\t c2id:" << oD2.c2id << std::endl;

    std::cout << "main  " << g1 << " " << &g1 << std::endl;
    std::cout << "main  " << g3 << " " << &g3 << std::endl;

    std::cout << cGroup::count << std::endl;
    cGroup cGroupObj(oD1, oD2);
    std::cout << cGroupObj.count << std::endl;
    cGroupObj.printMemberClassNames();
    {
        cGroup cGroupObj2(oD1, oD2);
        std::cout << cGroupObj2.count << std::endl;
    }
    std::cout << cGroupObj.count << std::endl;

    ctemp<double> oT1(12.2);
    ctemp<double> oT2;
    std::cout << oT2.convertToString() << std::endl;
    std::cout << oT1.convertTolong() << std::endl;
    ctemp<std::string> ct3(std::string("new String"));
    //std::cout << ct3.convertTolong(); //if no use, the long ctemp<string>::convertTolong() will NOT be Generated, so no error.

    int i = 10;
    float f = 10.1;
    double d = 10.2;
    auto g = add(i, d);

    //singleton;
    std::map<int, std::string>& m1 = cSingleton::getInstance().getMap();
    m1[1] = "111";
    m1[2] = "222";
    m1[3] = "333";
    m1[4] = "111";

    std::cout << cSingleton::getInstance().getMap().size();

    class1 c1("in CppDemo");
   // c1.accessViolation();

    //why need pointer. 
    //1. avoid copy big data structure
    cComp* comp1 = new cComp();
    comp1->oneMilItems = std::vector<double>{ 1,12,2,14,31,3,5,18,19};
    cContainer container1(comp1);
    for (auto& idx : container1.getV())
        std::cout << idx << "\t";

    //2. can pass by reference, or pass by pointer to get value updated in other scope!
    std::vector<std::string> vsmain{ "init" };
    
    fillUpVs(&vsmain);//& addr operator, so create a pointer to vs and pass in
    
    std::vector<std::string> vsmain2{ "init" };
    fillUpVsRef(vsmain2); //by reference, underneath implemented by "pointner"
    
    std::vector<std::string> vsmain3{ "init" };
    fillUpVs_byValue(vsmain3); //by value
    std::cout << vsmain.size() << std::endl;

    //vectors; it is a template class,
    std::vector<cD1> vc1{ cD1(), cD1() };

    std::vector<cBase*> vc{new cD1(), new cD2(), new cBase()};
    //polymophism.
    for (auto& a : vc)
        a->printClassName();
    //....
    for (auto& a : vc)
        delete a;

    std::vector<int> v{ 1,2,2,4,5,2,7,9,2,11,7,6 }; //== c# list
    for (int i = 0; i < v.size(); i++)
        std::cout << v[i];

    auto vit = v.begin();

    //for (; vit != v.end();) {
    //    if (*vit == 2)
    //        vit = v.erase(vit);
    //    //else
    //    vit++;
    //}
    int vcount = count(v.begin(), v.end(), 2);

    std::list<int> l{ 1,2,3,4,5,6,7,4,2 };
    auto lit = l.begin();
    int mcount = count(l.begin(), l.end(), 4);

    vit = min_element(v.begin(), v.end());
    transform(l.begin(), l.end(), l.begin(), X2);

    return 0;
}