#include "cComp.h"

cContainer::cContainer(cComp* _c)
{
    c = _c;
    _c->assigned = true;
    auto start = c->oneMilItems.begin();
    for (auto it = start ; it != c->oneMilItems.end(); it++)
    {
        if (*it >= 10.0)
        {
            idxForLargeVal.push_back(it - start);
        }
    }
}