#pragma once
#include <iostream>
class cDefaultFuncs
{
public:
    //constructor,
    cDefaultFuncs(){std::cout << "cDefaultFuncs()" <<std::endl; }
    //copy constructor.
    cDefaultFuncs(const cDefaultFuncs& rhs) { std::cout << "cDefaultFuncs(const cDefaultFuncs& rhs)" << std::endl; }
    //destructor.
    ~cDefaultFuncs() { std::cout << "~cDefaultFuncs() " << std::endl; }
    //assingment operator
    cDefaultFuncs& operator=(const cDefaultFuncs& rhs) 
    {
        std::cout << "operator=(const cDefaultFuncs& rhs) " << std::endl; 
        return *this; 
    }
};

