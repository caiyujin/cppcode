
#include "Pool.hpp"

void taskTakeLongTime() {
	for (int i = 0; i <= 10; i++) {
		stringstream ss;
		ss << "thread::id()" << this_thread::get_id() << " with i value of :" << i;
		this_thread::sleep_for(chrono::milliseconds(50));
	}
	return;
}


int main(int argc, char* argv[])
{
	//*************many threadssync with a atomic<bool> ************* 
	WorkerPool m_worker_pool("AggregationWorkerPool", 10);
	m_worker_pool.start();
	for (int i = 9900; i <= 10000; i++) {
		WorkerPool::Task::ptr task = std::make_shared<WorkerPool::Task>(taskTakeLongTime, i);
		m_worker_pool.post(task); //each one will take 10sec, if no mutlithread, 10*10 will be 100second.
	}
	return 0;
}