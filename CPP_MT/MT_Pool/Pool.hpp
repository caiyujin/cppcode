#pragma once
#include "getExecutionTime.h"
class WorkerPool
{
public:
  using ptr = std::shared_ptr<WorkerPool>;

public:
  class Task
  {
  public:
    using ptr = std::shared_ptr<Task>;
  public:
    Task(const std::function<void()>& handler,int task_id);
    virtual ~Task();
    void operator()();
    chrono::nanoseconds get_task_handle_delay();    
    int getTask_id(void){return m_task_id;}
  private:
    std::function<void()> m_handler;
    int m_task_id;
    chrono::high_resolution_clock::time_point m_time_created;
    chrono::high_resolution_clock::time_point m_time_processed;
  };

public:
  /// Constructor.
  WorkerPool(
      const std::string& name,
      const size_t num_workers);

  /// Destructor.
  ~WorkerPool();
  const std::string& get_name( ) const;
  bool start();
  bool stop();
  void post(const Task::ptr& task);
  void signal();

private:
  class Worker
  {
  public:
    Worker(
        const std::string& name,
        WorkerPool& worker_pool);
    ~Worker();
    bool start();
    bool stop();
    bool isTasksEmpty();
  private:
    void process_tasks();
    const std::string m_name;
    WorkerPool& m_worker_pool;
    std::atomic<bool> m_shutdown_flag;
    std::future<bool> m_future_stop;
  };

private:
  friend class Worker;
  Task::ptr get_task();
  const std::string m_name;
  std::vector<std::unique_ptr<Worker>> m_workers;

  std::mutex m_mutex;
  std::condition_variable m_cv;
  std::deque<Task::ptr> m_tasks; //temporary changed from queue to vector, and FILO for now...
};
