// MT6_Future.cpp : Defines the entry point for the console application.
//

#include "getExecutionTime.h"
#include <semaphore>
#include <latch>

const int NUM_THREADS = 5;
std::counting_semaphore<2> semaphore(2);

std::latch l(NUM_THREADS);


void ThreadFunction(int threadId)
{
    semaphore.acquire(); // Acquire a semaphore permit
    std::cout << "Thread " << threadId << " acquired a permit." << std::endl;

    // Simulate some work
   
    std::this_thread::sleep_for(std::chrono::milliseconds(RndEng() % 1000)); //	this_thread::sleep_for(1000ms);

    std::cout << "Thread " << threadId << " released a permit." << std::endl;
    semaphore.release(); // Release the semaphore permit
}

void ThreadFunctionLatch(int threadId, int waitTimeMsec)
{
    // Simulate some work
    std::this_thread::sleep_for(std::chrono::milliseconds(waitTimeMsec));

    l.count_down();
}


int main()
{
    {
        cGetTime timer1;
        std::thread threads[NUM_THREADS];

        for (int i = 0; i < NUM_THREADS; ++i)
            threads[i] = std::thread(ThreadFunction, i);

        for (int i = 0; i < NUM_THREADS; ++i)
            threads[i].join();

    }

    {
        cGetTime timer1;
        std::thread threads[NUM_THREADS];

        for (int i = 0; i < NUM_THREADS; ++i)
            threads[i] = std::thread(ThreadFunctionLatch, i, 1000 + i*100);

        l.wait(); // Wait until the latch count reaches zero

        std::cout << "All threads have completed their work." << std::endl;

        for (int i = 0; i < NUM_THREADS; ++i)
            threads[i].join();
    }

    return 0;
}
