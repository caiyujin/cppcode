//cannot include the "windows.h",which will include winsock.h, and conflict with asio.hpp(include winsock2.h)
//the variadic template function unpack issue is solved by using the latest VS2022, 15.12, the ealier one is 15.6 which has the issue
#include <boost/asio.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <iostream>
#include <string>
#include <memory>

using boost::asio::awaitable;
using boost::asio::co_spawn;
using boost::asio::detached;
using boost::asio::ip::tcp;
namespace asio = boost::asio;

// Function to asynchronously read a line from the client
awaitable<void> handle_client(tcp::socket socket) {
    try {
        auto executor = co_await asio::this_coro::executor;
        asio::streambuf buffer;

        std::cout << "Client connected: " << socket.remote_endpoint() << "\n";

        while (true) {
            // Asynchronously read a line
            std::size_t n = co_await asio::async_read_until(socket, buffer, '\n', asio::use_awaitable);

            // Convert buffer to a string
            std::istream input_stream(&buffer);
            std::string message;
            std::getline(input_stream, message);

            if (message.empty()) {
                std::cout << "Client disconnected.\n";
                break;
            }

            std::cout << "Received: " << message << "\n";

            // Echo the message back to the client
            std::string response = "Server: " + message + "\n";
            co_await asio::async_write(socket, asio::buffer(response), asio::use_awaitable);
        }
    }
    catch (std::exception& e) {
        std::cerr << "Error in handle_client: " << e.what() << "\n";
    }
}

// Function to accept incoming client connections
awaitable<void> start_server(unsigned short port) {
    auto executor = co_await asio::this_coro::executor;
    tcp::acceptor acceptor(executor, tcp::endpoint(tcp::v4(), port));

    std::cout << "Server running on port " << port << "\n";

    while (true) {
        // Accept a new client connection asynchronously
        tcp::socket socket = co_await acceptor.async_accept(asio::use_awaitable);

        // Spawn a coroutine to handle the client connection
        co_spawn(executor, handle_client(std::move(socket)), detached);
    }
}

int main() {
    try {
        asio::io_context io_context;

        // Spawn the server coroutine
        co_spawn(io_context, start_server(12345), detached);

        // Run the io_context
        io_context.run();
    }
    catch (std::exception& e) {
        std::cerr << "Server error: " << e.what() << "\n";
    }

    return 0;
}