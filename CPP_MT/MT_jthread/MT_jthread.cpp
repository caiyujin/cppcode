#include "getExecutionTime.h"

vector<int> szs(3, 10);
//the func without stop_token st also works, this is implicitly add during jthread ctor
string make_data(stop_token st, int ith, string& s)
{
    //std::string s = "";
    for (int i = 0; i < szs[ith]; i++)
    {
        //the sleep in main will allow this run a few loops, when out of the scope , the jthread's internal stop_srouce will request stop
        //here it will be break out of the for loop
        // 
        //on average, each char will wait for 50 msec; evenly distributed from 0-100msec;
        if (!st.stop_requested())
            s += RndEngWithDelay() % 26 + 'A'; 
        else
            break;
    }
    s += "__";
    stringstream ss;
    ss << ith << "__" << this_thread::get_id();

    s += ss.str();
    cout << s << endl;
    return s;
}


//string make_data(int sz)
//{
//    std::string s = "";
//    for (int i = 0; i < sz; i++)
//    {
//        s += RndEngWithDelay() %26 + 'a';
//    }
//    s += "__";
//    stringstream ss;
//    ss << this_thread::get_id();
//
//    s += ss.str();
//    cout << s << endl;
//    return s;
//}



int main(int argc, char* argv[])
{
    //jthread account for 99% of cases for manually starting/managing threads.
    //async can be used where u want a result
    //use thread, if u have no other choices.

  unsigned const thread_count = 3;
  vector<string> data(thread_count);

  bool printit = false;


  {
      vector<jthread> threads;  //jthread a movable value type

      for (unsigned i = 0; i < thread_count; i++)
      {
          threads.push_back
          (
              jthread
              (
                  make_data, i, std::ref(data[i])
              )
          );
      }
      this_thread::sleep_for(100ms);
      threads[1].get_stop_source().request_stop();//let the 2nd thread stop earlier
      this_thread::sleep_for(100ms);

  }
  //if without this {},  nothing will be print out. 
  //this } will invoke destructor of jthread, call src.request_stop(), and thread.join()
  //if no join, then  main thread will be the 1st one to be quit, side thread no chance print out anything.

  return 0;
}