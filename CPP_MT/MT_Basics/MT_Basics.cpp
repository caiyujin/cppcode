// MT.cpp : Defines the entry point for the console application.
//
#include "getExecutionTime.h"

mutex mu1;

void shared_print(string callerFunc, int i)
{
	mu1.lock();
	cout << "from " << callerFunc << ":" << i << endl;
	mu1.unlock();
}

void func1()
{
	for (int i = 0; i < 150; i++)
		shared_print("func1", i);
}

static std::exception_ptr eP = nullptr;

void func2Expt() {
    try {
        this_thread::sleep_for(2s);
        throw runtime_error("catch me at Main");
    }
    catch(...){
        eP = current_exception();
    }
}

void CallBack1(int* p){

	this_thread::sleep_for(1000ms);
    cout << "CallBack1 p = " << p << " with value of = " << *p << endl;
	int i = *p;
}

void startNewThread(void){
	int* p = new int(10);
	thread t(CallBack1, p);
	//t.detach(); 
    //t.join();
	//the p could be destoryed before CallBack1() use it, 
	//make sure use t.join() to keep p alive till the CallBack1 finished  
	cout << *p;
    delete p;
    t.join();
} 

class logfile {
    ofstream f;
    mutex mu1;
    mutex mu2;
    once_flag _flag;
public:
    logfile() {
    }
    ~logfile() {
        f.close();
    }
    void shared_print(string callerFunc, int value) {
        /*
            1. prefer using SINGLE mutex. if need two lock, make it same order.use lock to play safe
            2. try NOT lock the mutex, then call the USER func(exception - prone code)!!!
            - fine - grained lock : protect small amount of data-- - more exposed to deadlock
            - coarse - grained lock : protect big amount of data-- - lose benefit of parallessim
        */

        ////if func1 lock it, and func2 waited here... then f will open twice.. deadlock
        //if (!f.is_open()){
        //	std::unique_lock<mutex> locker2(mu_file); 
        //	f.open("log.txt"); 
        //}

        ////this is solution to address above issue, however 
        ////it will cause code to do LOT unnecessary action by locking/unlocking just to check if the file is open.
        //	std::unique_lock<mutex> locker2(mu_file); 
        //	if (!f.is_open()){
        //		f.open("log.txt");
        //	}

        call_once(_flag, [&]() {f.open("log.txt"); });


        //      lock_guard<mutex> locker(mu1);

        //      this_thread::sleep_for(100ms);

        //      lock_guard<mutex> locker2(mu2);
        //      
        //    this_thread::sleep_for(10ms);

        std::scoped_lock doubleLock(mu1, mu2);

        f << "shared_print: " << callerFunc << ": " << value << endl;
    }

    //this is for demo purpuse, func1 shall just call shared_print instead.
    void shared_print2(string callerFunc, int value) {

        call_once(_flag, [&]() {f.open("log.txt"); });

        //lock_guard<mutex> locker2(mu2);

        //this_thread::sleep_for(100ms);

        //lock_guard<mutex> locker1(mu1); 

        //this_thread::sleep_for(10ms);

        std::scoped_lock doubleLock(mu1, mu2);

        f << "Different Format,  shared_print2:" << callerFunc << ": " << value << endl;
    }

    //ofstream& getstream(void){ return f;} //very bad, if return f to outside, others can use f without lock!.
};

void func2(logfile& log1) {
    for (int i = 0; i < 10; i++)
        log1.shared_print2("func2", i);
}

int main(int argc, char* argv[]){
	thread t1(func1);	
	//it will immediately run upon instantiation 
	//if main finished earlier, it will have NO chance execute its own code.
	//so it is undeterministic when two/more threads running independantly; 
	//either join() or detach() can only be called once, and only one of them. 
	//once detached, it is not joinabled, once joined, not detachable!!

	for (int i = 0; i > -100; i--)
		shared_print("main", i);

	t1.join();//without join() or below wait, the excution will crash, because the thread takes longer time to finish than the main;

    startNewThread();

    thread t2(func2Expt);
    t2.join();

    if (eP != nullptr) {
        try {
            rethrow_exception(eP);
        }
        catch (const exception& ePm) {
            cout << "exception is:" << ePm.what() << endl;
        }
    }


    {
        logfile log1;
        thread t1(func2, std::ref(log1));

        t1.detach();

        for (int i = 0; i > -10; i--)
            log1.shared_print("main", i);

        this_thread::sleep_for(1s);
    }
	return 0;
}