
#include "getExecutionTime.h"
//A coroutine is a function that can be suspended and resumed
/*
First Called(Initial execution).
Suspended(via co_yield, co_await, or co_return).
Resumed(via resume, e.g., move_next in our example).
*/

//******************************example 1********************************
// Awaitable Type
struct Generator {
    
    struct promise_type
    {
        int current_value;

        auto get_return_object() {
            return Generator{ std::coroutine_handle<promise_type>::from_promise(*this) };
        }
        //when Generator instance is created, its handler will invoke the <T>'s intitial_suspend.
        //here it means it will suspend immediately upon object creation..
        //eg, no code in the coroutine (function) is run 
        std::suspend_always initial_suspend() { return {}; }
        
        std::suspend_always final_suspend() noexcept { return {}; }
        
        void return_void() {}
        
        std::suspend_always yield_value(int value) {
            current_value = value;
            return {};
        }
        void unhandled_exception() { std::terminate(); }
    };

    std::coroutine_handle<promise_type> handle;

    Generator(std::coroutine_handle<promise_type> h) : handle(h) {}
    ~Generator() {
        if (handle) handle.destroy();
    }

    bool move_next() {
        handle.resume(); //resume/done are both coroutine_handle's member functions.
        //handle.done(): Checks if the coroutine has finished execution. eg, end of function.
        return !handle.done();
    }

    int current_value() const { return handle.promise().current_value; }
};

Generator generate_numbers(int start, int end) {
    for (int i = start; i <= end; ++i) {
        //when run to below line of code, it already executed it. 
        //it called the <T>'s YIELD_VALUE and suspended the coroutine.
        co_yield i;
    }
    //upon exiting the loop, it will call the <T>'s final_suspend and suspend the coroutine.
}

//******************************example 2********************************
// 
// Awaitable to simulate non-blocking async operation
struct AsyncOperation {
    std::future<int> future;

    AsyncOperation(std::future<int>&& f) : future(std::move(f)) {}

    // Check if the future is ready (non-blocking check)
    bool await_ready() const {
        return future.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
    }

    // Suspend the coroutine and return control to the caller
    void await_suspend(std::coroutine_handle<>) {
        // No-op here
    }

    // Resume the coroutine and retrieve the result
    int await_resume() {
        return future.get();
    }
};

// Simulated asynchronous fetch using a separate thread
AsyncOperation fetch_data_async(const std::string& source) {
    std::cout << "Fetching data from " << source << "...\n";
    auto promise = std::make_shared<std::promise<int>>();
    std::future<int> fut = promise->get_future();

    // Launch data fetching in a separate thread
    std::thread([promise, source]() {
        std::this_thread::sleep_for(std::chrono::seconds(2)); // Simulate delay
        int result = source.size();
        std::cout << "Data fetched from " << source << ". Length: " << result << " in a new thread \n";
        promise->set_value(result);
        }).detach(); //this thread can outlive this function fetch_data_async();

    return AsyncOperation(std::move(fut));
}

// Coroutine to fetch data in parallel
struct ParallelTask {
    struct promise_type {
        auto get_return_object() {
            return ParallelTask{ std::coroutine_handle<promise_type>::from_promise(*this) };
        }
        std::suspend_always initial_suspend() { return {}; }
        std::suspend_always final_suspend() noexcept { return {}; }
        void return_void() {}
        void unhandled_exception() { std::terminate(); }
    };

    std::coroutine_handle<promise_type> handle;
    ParallelTask(std::coroutine_handle<promise_type> h) : handle(h) {}
    ~ParallelTask() {
        if (handle)
            handle.destroy();
    }
};

ParallelTask fetch_all_data_parallel() {
    //these two co_await will make them run in serial, not parallel.

    int result1 = co_await fetch_data_async("Source 1"); //fetch_data_async return AsyncOperation object (contain a future object)
    std::cout << "Result from Source 1: " << result1 << "\n";

    int result2 = co_await fetch_data_async("Source 2");
    std::cout << "Result from Source 2: " << result2 << "\n";

}

int fetch_data(const std::string& source) {
    std::this_thread::sleep_for(std::chrono::seconds(2));
    return source.size();
}

//******************************example 3********************************
// Awaitable for processing incoming data
class DataAwaitable {
public:
    std::future<std::vector<std::string>> fut;

    // Check if data is ready
    bool await_ready() const noexcept {
        return fut.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
    }

    // Suspend the coroutine and wait for data
    void await_suspend(std::coroutine_handle<> handle) {
        std::thread([this, handle]() {
            fut.wait();  // Wait for data to arrive
            handle.resume();  // Resume the coroutine when data is ready
            }).detach();
    }

    // Return the incoming batch of data
    std::vector<std::string> await_resume() {
        return fut.get();
    }
};


struct Task {
    struct promise_type {
        Task get_return_object() { return Task{ std::coroutine_handle<promise_type>::from_promise(*this) }; }
        std::suspend_always initial_suspend() noexcept { return {}; }
        std::suspend_always final_suspend() noexcept { return {}; }
        void unhandled_exception() { std::terminate(); }
        void return_void() {}
    };

    std::coroutine_handle<promise_type> handle;

    Task(std::coroutine_handle<promise_type> h) : handle(h) {}
    ~Task() { if (handle) handle.destroy(); }

    void resume() { if (!handle.done()) handle.resume(); }
};


// A producer-consumer "data stream" simulation
class DataStream {
public:
    std::queue<std::vector<std::string>> dataQueue;
    std::mutex mtx;
    std::condition_variable cv;
    bool isFinished = false;

    // Produce a batch of data
    void produce(std::vector<std::string> batch) {
        std::unique_lock<std::mutex> lock(mtx);
        dataQueue.push(std::move(batch));
        cv.notify_one();
    }

    // Signal that no more data is coming
    void finish() {
        std::unique_lock<std::mutex> lock(mtx);
        isFinished = true;
        cv.notify_one();
    }

    // Retrieve the next batch of data
    std::future<std::vector<std::string>> get_next_batch() {
        std::promise<std::vector<std::string>> prom;
        auto fut = prom.get_future();

        std::thread([this, p = std::move(prom)]() mutable {
            std::unique_lock<std::mutex> lock(mtx);
            cv.wait(lock, [this]() { return !dataQueue.empty() || isFinished; });
            if (!dataQueue.empty()) {
                auto batch = std::move(dataQueue.front());
                dataQueue.pop();
                std::this_thread::sleep_for(std::chrono::seconds(1));  // Simulate processing time
                p.set_value(std::move(batch));
            }
            else if (isFinished) {
                p.set_value({});  // Send empty vector to signal end
            }
            }).detach();

            return fut;
    }
};

// Coroutine to process incoming batches of data
Task process_data_stream(DataStream& stream) {
    while (true) {
        DataAwaitable awaitable{ stream.get_next_batch() }; //the thread will keep running (waiting for data). the data will be fed in main()

        //co_await followed by awaitable object (must contain a future member data)./
        auto batch = co_await awaitable;

        if (batch.empty()) {
            std::cout << "No more data. Exiting...\n";
            break;
        }

        std::cout << "Processing batch...\n";
        for (const auto& str : batch) {
            std::cout << "String length: " << str.size() << std::endl;
        }
    }
}

int main() {

    //******************************example 1********************************

    auto gen = generate_numbers(1, 5);

    /*
        When handle.resume() is called, the control is transferred to the suspension point (where the coroutine last stopped). Specifically:

        If the coroutine is starting for the first time:
        Execution begins from the start of the coroutine function until the first co_yield, co_await, or co_return.
        If the coroutine is resuming:
        Execution continues immediately after the last suspension point.
    
    */


    while (gen.move_next()) {
        std::cout << gen.current_value() << std::endl;
    }

    //******************************example 2********************************
    {
        cGetTime t1;
        auto task = fetch_all_data_parallel();
        while (!task.handle.done()) {
            task.handle.resume();
        }
    }

    {
        cGetTime t1;
        std::future<int> f1 = std::async(std::launch::async, fetch_data, "Source 1");
        std::future<int> f2 = std::async(std::launch::async, fetch_data, "Source 2");

        // Polling loop to wait for futures to complete
        while (f1.wait_for(std::chrono::milliseconds(100)) != std::future_status::ready ||
            f2.wait_for(std::chrono::milliseconds(100)) != std::future_status::ready) {
            this_thread::sleep_for(100ms);
        }

        int result1 = f1.get();
        int result2 = f2.get();

        std::cout << "Result 1: " << result1 << ", Result 2: " << result2 << "\n";
    }

    //******************************example 3********************************
    {
        cGetTime t1;
        DataStream stream;

        // Start the coroutine to process the data stream
        auto task = process_data_stream(stream);

        // Simulate producing data
        std::this_thread::sleep_for(std::chrono::seconds(1));
        stream.produce({ "Hello", "World", "Coroutines" });

        std::this_thread::sleep_for(std::chrono::seconds(2));
        stream.produce({ "C++20", "Async", "Stream" });

        std::this_thread::sleep_for(std::chrono::seconds(1));
        stream.produce({ "Final", "Batch", "Example" });

        // Finish producing
        std::this_thread::sleep_for(std::chrono::seconds(1));
        stream.finish();

        // Run the coroutine to completion
        while (!task.handle.done()) {
            task.resume();
        };

    }

    return 0;
}