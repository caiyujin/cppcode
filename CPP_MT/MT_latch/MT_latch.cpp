#include "getExecutionTime.h"

/*
What is the difference between these two mechanisms to coordinate threads ? 
You can use a std::latch only once, but you can use a std::barrier more than once.
A std::latch is useful for managing one task by multiple threads; a std::barrier is helpful for managing repeated tasks by multiple threads.Additionally, 
a std::barrier enables you to execute a function in the so - called completion step.The completion step is the state when the counter becomes zero.

What use cases do latches and barriers support that cannot be done in C++11 with futures, threads, or condition variables combined with locks ? 
Latches and barriers address no new use cases, but they are a lot easier to use.They are also more performant because they often use a lock - free mechanism internally.
*/


unsigned const thread_count = 3;
latch l1(thread_count); //single use counter 
latch l2(1); //used by main thread to quit the side thread.

void make_data(int ith, string& s)
{
    cGetTime c0(ith);
    for (int i = 0; i < 10; i++)
    {
        s += RndEngWithDelay() % 26 + 'a';
    }

    s += "__";
    stringstream ss;
    ss << ith << "__" << this_thread::get_id();
    s += ss.str();     

    {
        cGetTime c1(ith+10);
        l1.count_down();
        l1.wait();

        //or use done.arrive_and_wait() here
    }

    //l2.wait();
}

int main(int argc, char* argv[])
{
  vector<string> data(thread_count);  
  {
      vector<jthread> ts;
      for (unsigned i = 0; i < thread_count; i++)
      {
          ts.push_back
          (
              jthread
              (
                  make_data, i, std::ref(data[i])
              )
          );
      }
      //here NOT count down, so the 3 threads all finished their job and count down,
      //it will signal NOT only the 3 threads, but also this main thread
      //so here it is "blocking" main thread., blocking is due to the latch's counter (not accessible) not reach 0 yet.
      l1.wait(); 

      //l2.count_down(); //location1
  }
  // move location1 to location2.
  // this will be deadlock, the 3  threads join, but will NOT able to complete run the functions, since they are blocked 
  //by l2, got to move l2.count_down() before the closing };

  for (auto& a : data)
  {
      syncCout(a);
  }

  //l2.count_down();//location2

  return 0;
}