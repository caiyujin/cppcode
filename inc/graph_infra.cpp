#pragma once
#include "graph_infra.h"

bool operator<(const edge& e1, const edge& e2)
{
    return e1.w < e2.w;
}

void addEdge(int v, int w)
{
    if (adj.find(v) == adj.end())
        adj[v] = vector<int>();

    if (adj.find(w) == adj.end())
        adj[w] = vector<int>();

    adj[v].push_back(w);
    adj[w].push_back(v);
}

void addEdge(char p1a, char p2a, int wa)
{
    edges.emplace_back(p1a, p2a, wa);
    vertex.insert(p1a);
    vertex.insert(p2a);
}