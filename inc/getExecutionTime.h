#pragma once
//C++ core
#include <limits>
#include <ctime> //equavalent of c language time.h (ctime), see the name pattern c*, like cstring.
#include <cmath> //equavalent of c language math.h.
#include <cstdlib>//equavalent of c language stdlib.h.
#include <stdint.h> //uint_8, int_64
#include <chrono> 
#include <random>
#include <assert.h>
#include <stdlib.h>
#include <signal.h>
#include <exception>//base exception
#include <stdexcept> //std exceptions.. like invalidArgument
#include <functional>
#include <typeinfo>
#include <type_traits>
#include <memory>
#include <utility> //for the pair
#include <climits>  //INT_MIN, INT_MAX
#include <complex>
#include <optional>
#include <filesystem>

#include <concepts>
#include <ranges>
#include <variant>
#include <coroutine>
//C++11 MT
#include <mutex>
#include <thread>
#include <condition_variable>
#include <future>
#include <atomic>
#include <semaphore>
#include <latch>
#include <barrier>
//iostream
#include <cstring> //equavelant of c language string.h.
#include <string>
#include <iomanip>
#include <istream>
#include <iostream>
#include <sstream>
#include <fstream>

//Iterator and algorithm
#include <iterator>
#include <algorithm>
#include <numeric>

//STL
#include <bitset>
#include <vector>
#include <deque>
#include <list>
#include <unordered_set> //C++11 #if defined(_MSC_VER) && _MSC_VER >= 1900
#include <unordered_map>
#include <set>
#include <map>
#include <tuple>
#include <queue>
#include <array>

//c
#include <cctype>
#include <stdio.h>
#include <stdint.h>
#include <cstdint>
//#ifdef __GNUC__ /*code for GNU C compiler */
//#elif _MSC_VER /*usually has the version number in _MSC_VER, code specific to MSVC compiler*/
//#elif __BORLANDC__ /*code specific to borland compilers*/
//#elif __MINGW32__
#ifdef _MSC_VER
    #include <windows.h>
    #include <atlbase.h>//COM
#endif

#ifdef WIN32
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif

using namespace std;
using namespace chrono;
using vit = vector<int>::iterator;

//Define a template function to print STL container, with a description of the container.
template<typename T>
void print_info(T& t, const string s = "no message"s) {
	using itr = typename T::iterator;// it;
    cout << "the containter of " << s << " is:";
    for (itr it = t.begin(); it != t.end(); it++)
	{
        cout << " " << *it;
    }
    cout << endl;
}

//looks like for templated function. no need static.it will be in 
template<typename T>
void print_2D(T& t, const string s = "no message"s) {
    cout << "the containter of " << s << " is:" << endl;
    for (auto it = t.begin(); it != t.end(); it++) 
	{
        for (auto it2 = (*it).begin(); it2 != (*it).end(); it2++) 
		{
            cout << *it2 << " ";
        }
        cout << endl;
    }
    cout << endl;
}

static default_random_engine RndEng(static_cast<unsigned int>
	(chrono::steady_clock::now().time_since_epoch().count()));

static default_random_engine NegRndEng(static_cast<int>
    (chrono::steady_clock::now().time_since_epoch().count()));

static unsigned int RndEngWithDelay()
{
    unsigned int delay = RndEng() % 100;
    this_thread::sleep_for(chrono::milliseconds(delay));
    return delay;
}

static std::mutex coutMutex;
static void syncCout(const std::string s) {
    std::lock_guard<std::mutex> lo(coutMutex);
    std::cout << s << endl;
}

//Define a RAII based timer, using c++11 chrono;
class cGetTime {
public:
    cGetTime():_id(INT_MAX)
    {
        start = chrono::steady_clock::now();
        //start2 = chrono::high_resolution_clock::now();
    }
    //lets just assume if got id, it need to syncCout FOR NOW,
    cGetTime(int id):_id(id) 
    {
        start = chrono::steady_clock::now();
    }

    ~cGetTime() 
    {
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        if (_id != INT_MAX)
        {
            std::lock_guard<std::mutex> lo(coutMutex);
            cout << "_id: " << _id << "\t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " msec \t"
                << chrono::duration_cast<chrono::seconds>(end - start).count() << " sec" << endl;
        }
        else
        {
            cout << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " msec\t"
                << chrono::duration_cast<chrono::seconds>(end - start).count() << " sec" << endl;
        }
    }
private:
    chrono::steady_clock::time_point start;
    int _id;
    bool _syncCout;
};
#ifdef WIN32
static const string& currentDateTime()
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
    std::chrono::milliseconds now2 = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
    struct tm currentLocalTime;
    localtime_s(&currentLocalTime, &currentTime);
    char timeBuffer[80];
    std::size_t charCount{ std::strftime(timeBuffer, 80,
                                           "%D %T",
                                            &currentLocalTime)
    };

    if (charCount == 0) return "";

    stringstream ss;
    ss << timeBuffer << "." << std::setfill('0') << std::setw(3) << now2.count() % 1000 << std::endl;
    return ss.str();
}
#endif

//Defined an Vector generator, with Optional argument "hasNeg" (with negative value), and "UpTo" (the range/max).
 template<typename T> void generate_vector(vector<T>& vec1, bool hasNeg = false, int UpTo = 100) 
 {
    if (hasNeg) 
	{
        for (auto it = vec1.begin(); it != vec1.end(); it++) 
		{
            if (RndEng() % 4 >= 2) //2/3 are positive, 1/3 are negative , 2/3 positive, 0/1 negative , 50%
                *it = static_cast<T>(RndEng() % UpTo);
            else
                *it = static_cast<T>((RndEng() % UpTo) *(-1));
        }
    }
    else
    {
        for (auto it = vec1.begin(); it != vec1.end(); it++) {
            *it = static_cast<T>(RndEng() % UpTo);
        }
    }
}

//Defined a 2D vector of y (row), and x(Col), pass in by reference. either value 0 or 1; 1 is 75%
 static void generate2Dvector(int y, int x, vector<vector<int> >& arr2D)
 {
    arr2D.resize(y);
    for (int i = 0; i < y; i++) 
	{
        arr2D[i].resize(x); //reserve is better than resize (call Dflt Ctor)
        for (int j = 0; j < x; j++) 
		{
            if (RndEng() % 4 == 0)
                arr2D[i][j] = 0;
            else
                arr2D[i][j] = 1;
        }
    }
}

//Defined a string generator, with Optional Arugment diffChars(26 upper cases chars),
 static string generateString(int len, int diffChars = 26) {
    string s = "";
    for (int i = 0; i < len; i++) 
	{
        s += char( RndEng() % diffChars + 65);
    }
    return s;
}

constexpr int numberOrChars = 58;
static vector<int> stringToVec(const string& s) 
{
    vector<int> v(numberOrChars,0);//ascii A-Z, [ \ ] ^ _ `, a-z;
    for (char c : s) 
    {
        v[c - 'A'] += 1;
    }

    return v;
}

static void openFileAndProcessLine(int argc, char* argv[], void(*func)(string&)) {
	vector<string> args;
	args.resize(argc);
	copy(argv, argv + argc, args.begin());

	if (args.size() != 2) {
		throw invalid_argument("number of arguments is not 2");
	}

	std::ifstream f(args[1].c_str());

	if (!f.good()) {
		throw invalid_argument("the argument is not a valid file, cannot open");
	}

	if (f.is_open()) {
		std::string line;
		while (!f.eof()) {
			getline(f, line);
			func(line);
		}
		f.close();
	}

	return;
}

static int xMin = INT_MAX;
static int xMax = INT_MIN;
static int yMin = INT_MAX;
static int yMax = INT_MIN;

static void updateMinMax(int x, int y)
{
    if (x < xMin)
        xMin = x;
    if (x > xMax)
        xMax = x;
    if (y < yMin)
        yMin = y;
    if (y > yMax)
        yMax = y;
}

//struct AllocationMetrics
//{
//    long long totalAllocated = 0;
//    long long totalFreed = 0;
//    long long CurrentUsage() { return totalAllocated - totalFreed; }
//};
//
//static AllocationMetrics s_AllocationMetrics;
//
//void* operator new(size_t size)
//{
//    //s_AllocCount++;
//    std::cout << "allocate :" << size << endl;
//    return malloc(size);
//}
//
//void operator delete(void* memory, size_t size)
//{
//    std::cout << "Free: " << size << endl;
//    return free(memory);
//}

extern int truelyGlobalVariableAmongTU;