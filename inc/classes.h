#pragma once
#include "getExecutionTime.h"

class B1
{
  public:

    B1(){initlize(10,1,100);}

    B1(int val){initlize(val*10, val, val*100);}

    B1(const B1 &rhs)
    {
        initlize(rhs.m_B1_b, rhs.m_B1_a, *(rhs.m_pB1));
        m_Name = rhs.m_Name;
    }

    B1 &operator=(const B1 & rhs)
    {
        initlize(rhs.m_B1_b, rhs.m_B1_a, *(rhs.m_pB1));
        m_Name = rhs.m_Name;
        totalInstance--;
        return *this;
    }

    virtual ~B1()
    {    //do not call virtual func in ctor or dtor, it could be UB
        totalInstance--;
        delete m_pB1;
     }

    void set_B1_a(const int &val){ m_B1_a = val; }
    
    const int& get_B1_a() const { return m_B1_a; }

    void print(void) 
    {
        cout << __FUNCSIG__ << endl; 
    }

    void print1(int a) 
    { 
        cout << __FUNCSIG__ << endl; 
    }
    
    virtual void printInfo(void)
    { 
        cout << __FUNCSIG__ << endl; 
    }

    virtual void method(void) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    virtual void methodWithDefault(string s0, string s = "B1 Dflt")
    {
        cout << s0 << s << endl;
    }

    virtual bool operator==(B1 &rhs)
    {
        if (typeid(*this) != typeid(rhs))
            return false;

        return m_B1_b == rhs.m_B1_b && m_B1_a == rhs.m_B1_a;
    }

    void f(int d) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    void baseMethod(void)
    {
        method();
    }

    B1 operator*(const B1 &a) //return by value is so much better !
    {
        B1 res(m_B1_b * a.m_B1_b);
        return res; //actually moved
    }

    int m_B1_b;
    string m_Name;
    static int totalInstance;

protected:
    void initlize(int x2, int x1, int x3)
    {
        m_B1_b = x2;
        m_B1_a = x1;
        m_pB1 = new int(x3);
        totalInstance++;
        m_Name = "Obj_"s + char(totalInstance + 64); //1-A, 2-B ...etc
    }
    void wait(void)
    { 
        cout << __FUNCSIG__ << endl; 
    }

private:
    int *m_pB1;
    int m_B1_a;
};

int B1::totalInstance = 0;
class D1 : public B1
{
public:
    D1()
    {
        m_D1 = 0;
        m_Name = "D1-Obj";
    }

    D1(string s)
    {
        m_D1 = 0;
        m_Name = s;
    }

    virtual ~D1()
    {
        cout << __FUNCSIG__ << endl;
    }

    virtual void printInfo(void)
    {
        cout << __FUNCSIG__ << endl;
        cout << "access B1 private member m_B1_a from D1 class member function:" << get_B1_a()  << endl;// m_B1_a is inaccessible
    }

    virtual void method(void) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    virtual void methodWithDefault(string s0, string s = "D1 Dflt")
    {
        cout << s0 << s << endl;
    }

    virtual bool operator==(B1 &rhs)
    {
        if (!B1::operator==(rhs)) //it will check and make sure type of rhs same as *this , ie D1, so below can safely cast...
            return false;

        auto tmp = dynamic_cast<D1*>(&rhs);

        if (tmp != nullptr)
            return m_D1 == tmp->m_D1;
        else
            return false;
    }

    //using B1::f;
    void f(double d) 
    {
        cout << __FUNCSIG__ << endl; 
    }

    void f(const string& s) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    int m_D1;
};

class D2 : public B1
{
  public:
    D2()
    {
        m_Name = "D2-Obj";
    }

    D2(string s)
    {
        m_Name = s;
    }

    virtual ~D2()
    {
    }

    void print(void) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    void print1(const string& s) 
    {
        cout << __FUNCSIG__ << endl; 
    }

    virtual void printInfo(void) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    virtual void method(void) 
    { 
        cout << __FUNCSIG__ << endl; 
    }

    virtual void methodWithDefault(string s = "D2 Dflt")
    {
        cout << s << endl;
    }

    float f(float f) 
    { 
        cout << __FUNCSIG__ << endl; 
        return static_cast<float>(f + 2.3);
    }

    void iWait()
    {
        wait();
    }

    virtual bool operator==(B1 &rhs)
    {
        if (!B1::operator==(rhs))
            return false;
        return true; //D2 dont have its own data.
    }

    bool operator>(const D2 &rhs)
    {
        return this->m_B1_b > rhs.m_B1_b;
    }
};

bool operator<(const D2 &lhs, const D2 &rhs)
{
    return lhs.m_B1_b < rhs.m_B1_b;
}

D2 operator*(const D2 &lhs, const D2 &rhs)
{
    D2 res;
    res.m_B1_b = lhs.m_B1_b * rhs.m_B1_b;
    return res;
}

//valgrind --leak-check=yes ./class_condes_exe
class exceptionCtorDtor
{
  public:
      exceptionCtorDtor(int i)
    {
        mP_int = new int(i);
        if (i == 202)
            throw invalid_argument("e1"); //this will cause memory leak for mP_int
        try
        {
            if (i == 203)
                throw invalid_argument("e2");
        }
        catch (const invalid_argument &e)
        {
            cout << "exception at:" << e.what() << "cleaning ..." << endl;
            // the compiler smart engouth to do cleaning of those already constructed.
        }
    }

    ~exceptionCtorDtor()
    {
        try
        {
            if (*mP_int == 201)
                throw invalid_argument("e at ~B()");
        }
        catch (const invalid_argument &arg_expt)
        {
            cout << "exception at:" << arg_expt.what() << "cleaning ..." << endl;
        }
        delete mP_int;
    }

  private:
    int *mP_int;
};

namespace A_NS
{
    struct X{
    };

    void g(X xa){ cout << __FUNCSIG__ << endl;}
}

class Ccls1
{
public:
    void g(A_NS::X xa)
    {
        cout << __FUNCSIG__ << endl;
    }

    void j(void)
    {
        A_NS::X x2;
        g(x2);
        //always search matches inside class defination first, here it invoke Ccls1::g(A_NS::X xa)
    }
};

class CSingleton
{
    CSingleton() { mString = "Skeleton_string_Value"; }
    string mString;

  public:
    string val(void) { return mString; }
    void setVal(const string &newStr) { mString = newStr; }
    static CSingleton &Instance(void)
    {
        static CSingleton *instance = new CSingleton;
        return *instance; // always returns the same instance, dereferencing, then pass by reference! awesome
    }
    ~CSingleton() { delete &(Instance()); }
};

class dog /*final, not allowed to create derived class of dog*/
{
  public:
    //all the three initialization method used dog d1{1,1.1..} methods. so it is called uniform initialization!!!
    //3rd, aggregate initialization
    int age = 0;
    int weight = 0;
    std::string m_name = "";
    shared_ptr<dog> m_pfriend;
    weak_ptr<dog> m_pTrueFriend;
    dog(){
        cout << __FUNCSIG__ << endl;
    }

    //2nd, regular constructor;
    dog(int a, int w) : age(a + 1), weight(w + 1), m_name("dummy")
    {}

    //1st ; priority initialization_list
    /*dog(const initializer_list<int>& abc){ 
        for (initializer_list<int>::iterator itr=abc.begin();itr!=end();itr++){
            age=*itr; weight=(*itr)*10;    m_name = "dummy";
        }  
    }*/

    dog(string s) : m_name(s)
    {
        cout << __FUNCSIG__ << endl;
    }

    void bark(void)
    {
        cout << "dog " << m_name << " rules!" << endl;
    }

    virtual void func3(void) final{
        //NO derived class can allowed to override this func...useful only if this class is mid of class inheritance tree.
    }; 

    virtual ~dog()
    {
        cout << "dog " << m_name << "~dog()" << endl;
    }

    void makeFriends(shared_ptr<dog> f) { m_pfriend = f; }

    void makeTrueFriends(shared_ptr<dog> f) { m_pTrueFriend = f; }

    void showTrueFriends()
    { 
        if (!m_pTrueFriend.expired()) //in case it is already expired (associated object is released when come here)/
            cout << "true friend is " << m_pTrueFriend.lock()->m_name << endl;
        //lock will create a temporary shared_ptr<dog> here and it will
        //prevent the object been released (destroyed) when calling ->m_name, so a real mutex lock is necessary here.
    }
    
    dog &operator=(const dog &a) = delete; //effectively disable it.

    bool operator==(const dog &a)
    {
        return (this->m_name == a.m_name);
    }
};

class cStr
{
public:
    cStr() = default;
    cStr(const char* cs) :m_Data(cs) {}

    void* operator new(size_t size) {
        cout << __FUNCSIG__ << endl;
        void* ptr = ::operator new(size);
        return ptr;
    }
    operator bool()const {
        return m_size > 9;
    }

    operator long long() {
        return m_size;
    }

    int getSize() const {
        return m_size;
    }
    const string& getData() const {
        return m_Data;
    }
    void operator()() {
        cout << __FUNCSIG__ << endl;
    }

    void operator()(const double& b) {
        cout << __FUNCSIG__ << endl;
    }

    const string* operator->() const
    {
        return &m_Data;
    }

    bool operator<(const cStr& rhs) const //must be const, map.key is const, it need to be compared.
    {
        if (m_size < rhs.getSize())
            return true;
        {
            if (m_size != rhs.getSize())
                return false;
            else
                return m_Data[0] < rhs.getData()[0];
        }
    }
    ~cStr()
    {
        cout << __FUNCSIG__ << endl;
    }

    friend ostream& operator<<(ostream& os, const cStr& rhs);

private:
    string m_Data;
    uint32_t m_size;
};

ostream& operator<<(ostream& os, const cStr& rhs)
{
    os << rhs.m_Data << " with size of " << rhs.m_size << std::endl;
    return os;
}

namespace std {
    template<>
    struct hash<cStr>
    {
        size_t operator()(const cStr& key) const //the const at func signature is Very important, cannot forget
        {
            return hash<int>()(key.getSize()) + hash<char>()(key.getData()[0]);
        }
    };
}

struct cStrNewHasher
{
    size_t operator()(const cStr& key) const //map key (const) will call this func; 
    {
        return hash<string>()(string(key.getData())); //use build in string hash function
    }
};

struct cStrNewHasher2
{
    size_t operator()(const cStr& key) const //map key (const) will call this func; 
    {
        return hash<string>()(string(key.getData()))+base;//use build in string hash function, with a slight modification
    }

    inline static int base;
};

//***************** Template Class1 ****************************//

//rule of 0, if u do not define any of 5, the 5 will be auto generated for you.
//rule of 5, if you defined any of the 5, the rest will NOT be auto generated for you.
//because the compiler think it is unsafe to auto create(may require customized func).

template <typename T1>
class Bo {
    T1* arr;
    int sz;
public:
    Bo() {}
    ~Bo() {
        delete[] arr;
    }
    Bo(const initializer_list<T1>& c)
    {
        arr = new T1[c.size()];
        int i = 0;
        for (typename initializer_list<T1>::iterator itr = c.begin(); itr != c.end(); itr++, i++)
        {
            arr[i] = *itr;
        }
        sz = i;
    }

    Bo(const Bo<T1>& rhs) {
        arr = new T1[rhs.getSize_Bo()]; //ctor means "this" is empty at begining, so no Delete[] arr
        int i = 0;
        for (; i < rhs.getSize_Bo(); i++)
        {
            arr[i] = rhs.get_Bo(i);
        }
        sz = i;
    }

    Bo& operator=(const Bo<T1>& rhs) {
        if (this == &rhs)
            return *this;

        delete[] arr;
        arr = new T1[rhs.getSize_Bo()];
        int i = 0;
        for (; i < rhs.getSize_Bo(); i++)
        {
            arr[i] = rhs.get_Bo(i);
        }
        sz = i;
        return *this;
    }

    Bo(Bo<T1>&& rhs)
    {//base_type(rhs.mArr).
        arr = rhs.arr;
        sz = rhs.sz;
        rhs.arr = nullptr;
        rhs.sz = 0;
    }

    Bo& operator=(Bo<T1>&& rhs)
    {
        if (this == &rhs)
            return *this;

        delete[] arr;
        arr = rhs.arr;
        sz = rhs.sz;
        rhs.arr = nullptr;
        rhs.sz = 0;
        return *this;
    }

    void print_Bo(void) const
    {
        for (int i = 0; i < sz; i++)
            cout << arr[i] << endl;
    }

    Bo<T1> operator*(const Bo<T1>& rhs2) const {
        Bo<T1> b;
        b.arr = new int[rhs2.getSize_Bo()];
        int i = 0;
        for (; i < rhs2.getSize_Bo(); i++)
        {
            b.arr[i] = this->arr[i] * rhs2.get_Bo(i);
        }

        b.sz = i;
        return b;
    }

    T1 get_Bo(int i) const { return arr[i]; }

    int getSize_Bo(void) const { return sz; }
};

template<typename t1>
class b {
public:
    b() { cout << "b var1 type of:" << typeid(var1).name() << endl; }
    t1 var1;
};

template<typename t2>
class d1 : public b<t2> {
public:
    d1() {
        cout << "t1:" << typeid(b<t2>::var1).name() << endl;
        cout << "t2:" << typeid(var2).name() << endl;
    }
    //float -f, int-i, point to const float, p-K-f
    t2 var2;
};

template<typename t1, typename t2>
class d2 : public b<t1> {
public:
    t2 var2;
};

template<typename T, size_t SIZE>
struct Array
{
    static_assert(!std::is_pointer<T>::value);
    T* data() const
    {
        return std::addressof(mData[0]);
    }
    constexpr size_t size() const { return SIZE; }
    T* begin() { return data(); }
    T* end() { return data() + SIZE; }
    T& operator[](size_t idx) { return mData[idx]; }
    T mData[SIZE];
    //using aggregate initialization, 
    //1. all member be pubilc, 
    //2. no user defined constructor.
    //3. no base class or virutal func;
};

template<typename T>
class Converter
{
public:
    Converter(const T& x) :mX{ x }
    {}

    template<typename U>
    Converter<T>& operator=(const U& u)
    {
        mX = static_cast<T>(u);
        return *this;
    }

private:
    T mX;
};

template<size_t N>
using CharArray = std::array<char, N>;

template<typename T>
concept IntergralCpt = std::is_integral_v<T>;


template<typename T>
concept SortableCpt = requires(T & x, T & y)
{
    { x.swap(y) } noexcept;
    { x.size() }-> std::convertible_to<std::size_t>;
};
template<typename T>
    requires SortableCpt<T>
class SortableClass
{
public:
    SortableClass(T& x){}
};

template <int N>
    requires (N == 4) || (N == 8)

class ip
{
public:
    ip(const string& s)
    {
        parseString(s);
    }

    int get_group(size_t idx) const
    {
        return mVal[idx];
    }

private:
    std::array<int, N> mVal;

    void parseString(const string& s)
    {
        int findpos = s.find(',');
        array<int, N> ret;
        stringstream ss(s.substr(0, findpos + 1)); //intentionally include a char (here the ',') for below extraction.

        //string temp = s.substr(findpos + 1); //for future use.
        int i;
        char c;
        int count = 0;

        if (N == 4)
        {
            while (ss >> dec >> i >> c)
            {
                mVal[count] = i;
                count++;
            }
        }
        else
        {
            while (ss >> hex >> i >> c)
            {
                mVal[count] = i;
                count++;
            }
        }
    }
};

template<typename T>
struct DefaultDeleter {
    void operator()(T* ptr) const {
        delete ptr;
    }
};
template<typename T, typename D = DefaultDeleter<T>>
class uniquePtr 
{
public:
    uniquePtr():mPtr{nullptr } {}
    explicit uniquePtr(T* ptr): mPtr{ptr} {}
    uniquePtr(T* ptr, const D& deleter) noexcept:mPtr{ ptr }, mDeleter{ deleter } {}
    uniquePtr(T* ptr, D&& deleter) noexcept:mPtr{ ptr }, mDeleter{ std::move(deleter) } {}

    uniquePtr(const uniquePtr& rhs) = delete;
    uniquePtr& operator=(const uniquePtr& rhs) = delete;

    T* release() noexcept
    {
        return std::exchange(mPtr, nullptr);//the exchange call wil return 1st param, and the 2nd param value will assign to the 1st;
    }

    void reset(T* ptr = nullptr) noexcept
    {
        mPtr = std::exchange(ptr, mPtr);
        if (ptr)
            mDeleter(ptr);
    }

    uniquePtr(uniquePtr&& rhs) noexcept 
        :mPtr{ rhs.release() }, mDeleter{ std::move(rhs.mDeleter)} 
    {
    }

    uniquePtr& operator=(uniquePtr&& rhs) noexcept    
    {
        if (this != &rhs)
        {
            reset(rhs.release());//take the other and destroy the current one.
            mDeleter = std::move(rhs.mDeleter);
        }
        return *this;
    }
    T* get() const noexcept{ return mPtr;}
    T* operator->() const noexcept { return mPtr; }
    T& operator*() const noexcept { return *mPtr; }

    D& get_deleter() noexcept { return mDeleter; }
    const D& get_deleter() const noexcept { return mDeleter; }

    ~uniquePtr()
    {
        reset();
    }
private:
    T* mPtr;
    [[msvc::no_unique_address]] D mDeleter; 
    //no_unique_address is a c++20 feature, it will not add any size to the class.
    //if the object is empty, or else need 1 byte, and 8bytes aligment, means it will need 8 bytes.
};

template<typename T, typename... Args>
uniquePtr<T> make_uniquePtr(Args&&... args)
{
    return uniquePtr<T>(new T(std::forward<Args>(args)...));
}