#pragma once
#include "getExecutionTime.h"
#include "classes.h"

template<typename T>
class VEC : public std::vector<T> {
public:
	using vector<T>::vector; //use the constructor from vector (under name of CaiVec)

	T& operator[](int i) 
	{
		return vector<T>::at(i);
	}

	const T& operator[](int i) const 
	{
		return vector<T>::at(i);
	}
};

namespace ExtStd {
	using namespace std;

	template<typename C>
	void sort(C& c) {
		sort(c.begin(), c.end());
	}

	template<typename C, typename Pred>
	void sort(C& c, Pred p) {
		sort(c.begin(), c.end(), p);
	}
}

void VecCapGrowAndReallocation(void){
	vector<long> vec(4l, 100l);
	//vec.reserve(1005); //it can prevent realloaction
	vector<long long> capV;
	long long cap = vec.capacity();
	capV.push_back(cap);
	
	vector<long long> addrV;
	long long addr = (long long)&(vec[0]);
	addrV.push_back(addr);

	for (long long i = 0; i < 1000; i++)
	{
		vec.push_back(100 + i);
		
		if (cap != vec.capacity())
		{
			cap = vec.capacity();
			capV.push_back(vec.capacity());
			addrV.push_back((long long)&(vec[0]));
		}
	}
	print_info(capV, "cap growth");
	print_info(addrV, "1st item address change");
	//cap from 4,6,9,13,19,28,42,63,94,1421,211,316,.... 50%, addr also changed everytime cap expanded!!!
}

void InitVecPriorC11(vector<int>& v) {
	//vector<Dog> v(6)    // 6 dog created with default constructor, size=6, capacity=6;
	//votr<Dog> v2;		// size=0;capacity=0
	//v2.resize(6);		// ***** 6 dog created with default constructor, size=6, capacity=6;
                        // if current is 4, 2 additional DOg will be created, if current is 8, the last two will be destroyed.
	//vector<Dog> v3;  v3.reserve(6);	//NO dog created, size =0, capacity=6; //reserve only Allocates memory.
	vector<int> v2(4, 100);
	vector<int> v3(v2.begin(), v2.end() - 2);
	vector<int> v4(v2);
	v2.swap(v2); //C++ 03 trim of the reduntand reserved memory. same as v2.shrink_to_fit()
	v2.resize(6); //add two 0 at back
	v2.push_back(50); //100,100,100,100,0,0,50

	int arr2[] = { 4, 5, 7, 8, 5, 1, 12, 7, 8, 11, 1, 2,99 };
	vector<int> v6(arr2, arr2 + sizeof(arr2) / sizeof(int));
	ExtStd::sort(v6);

	vector<int> v7;
	v7.swap(v6);
	v = v7;
}

void VectorErase() {
	vector<int> v{ 1,2,2,4,5,2,7,9,2,11,7,6 };
	vector<int> v2 = v;
  
    auto it = v.begin();
	for (; it != v.end();) {
		if (*it == 2)
			it = v.erase(it);//it will still the 2nd item position after erase.
		else
			it++;
	}
    //v.erase(it); //*********exception will be thrown since right now "it" is pointing to v.end()

	auto it2 = std::remove(v2.begin(), v2.end(), 7);//erase remove idiom
	v2.erase(it2, v2.end());	
}

class CObject {
public:
	CObject() { cout << "ctor" << endl; }
	~CObject() { cout << "dtor" << endl; }

};

void VectorAsSet(void) {
	vector<int> v{ 4, 5, 7, 8, 5, 1, 12, 7, 8, 11, 1, 2,99 };
	std::sort(v.begin(), v.end());
	std::binary_search(v.begin(), v.end(), 11); //it will only return true or false.

    //prefer use below to get exact returned iterator if found!
	vector<int>::iterator it = lower_bound(v.begin(), v.end(), 7);
	if (it != v.end() && *it ==7)
        cout << "found " << endl;
}

void VectorAsQueue(void) {
	int FrontIndex = 0;
	vector<int> v{ 4, 5, 7, 8, 5, 1, 12, 7, 8, 11, 1, 2,99 };

	int j = v[FrontIndex];
	FrontIndex++; //using this variable to track the starting of the q. nice. but lets hope the queue wont be so long
}

//++++++++++++++++++++++++++++MAP+++++++++++++++++++
//void MapPrintEntry(const map<char, int>& map1){
//the subscribe operator provides write ops, so the container can NOT be const; so if want to use it can NOT pass in as const &
//the subscribe operator [ can NOT be used by multimap/set,because they do NOT have unique 'keys'
void MapPrintEntry(map<char, int>& map1){
	cout << map1['c'] << endl;
}

typedef map<string, map<string, string> > Str2StrMap;
typedef map<string, string> Str2Str;
typedef pair<string, string> StrPStr;

void NestedMap(void) {
	Str2Str m1;
	Str2Str::const_iterator it2;
	m1.insert(make_pair("AA", "aa0")); //insert entry to map with three methods, 1st make_pair
	m1.insert(StrPStr("BB", "bb0"));
	m1["CC"] = "cc0";//3rd one. direct assignment.

	Str2StrMap Str2StrMap_1;
	Str2StrMap::const_iterator it1;

	Str2StrMap_1["1"] = m1;
	m1["AA"] = "aa2"; m1["BB"] = "bb2"; m1["CC"] = "cc2";
	Str2StrMap_1["2"] = m1;

	for (it1 = Str2StrMap_1.begin(); it1 != Str2StrMap_1.end(); it1++) {
		cout << "Key = " << it1->first << endl;
		for (it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
			cout << "\tVal =" << it2->first << "-" << it2->second << endl;
		}
	}
}

void MapSubscribing(void) {
	//case1:
	map<string, int> MagicMap;
	MagicMap.insert(make_pair("abcd", 1234));
    MagicMap.insert(make_pair("waitTime", 3)); //NOT waiTimes
	cout << MagicMap["abcq"] << endl;
	cout << MagicMap.size() << endl;
	cout << "wah, size changed, when did i insert the second pair??" << endl;
	
    int mTimeOUt = MagicMap["waitTimes"]; //mTimeOut is 0,could be wait forever,
	//[] operator only provide Non-const version.and it potentionally insert like above.
	//so for const map, use map1.at(key) instead, at has const version.

	const map<string, int> MagicMap2{ make_pair("abc"s,123),make_pair("def"s,456), make_pair("adg"s,333)}; 
	const int& val = MagicMap2.at("def"s);
	int val2 = MagicMap2.at("def"s);
	//int& val3 = MagicMap2.at("def"s); //const int& cannot be assigned to int&
	//int val3 = MagicMap2.at("hij"s);//std::out_of_range Exception!
	
	//so still the best way to access a data is to check if it is there by call .find(). before access it with .at

	//the output will really follow the sorted order based on the key.
	for (auto& [key, val] : MagicMap2)
	{
		const string& s = key;
		const int& i = val;
		cout << "key: " << key << "\t val: " << val << endl;

	}
}

void PropertiesOfUnorderedMap(unordered_map<string, string>& un_map)
{
	cout << "total bucket #" << un_map.bucket_count() << endl;
	cout << "load_factor = " << un_map.load_factor() << endl;
	cout << "element with key  'ab' is in bucket #" << un_map.bucket("ab") << endl;
}

class c_lsb_lss{
public:
	bool operator()(int x, int y){
		return (x % 10)<(y % 10);
	}
};
//C++11 lambda function, ... function without a name;  [](int x){return x<10;} //x is the parameter pass into function. it equal to below function.
bool smaller_than_10(int x){
	return x<10;
}

bool LargerThanFifteen(double a) {
	return a>15.0;
}

bool mymax(int x, int y){
	return (x % 10)<(y % 10);
}

int X2(int x){
	return x * 2;
}