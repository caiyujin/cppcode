#pragma once
#include <windows.h>
//#include <processthreadsapi.h>
//#include <libloaderapi.h>
//#include <synchapi.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>

void TakeADumpImpl(bool isFullDump = false)
{
    std::ostringstream commandStream;
    std::string procNameProve = "";
    commandStream << "C:\\procdump.exe -accepteula ";// -ma == full dump ; -mm == mini dump/default
    //full dump are the most useful when you are troubleshooting unknown issues. 
    //A support engineer can use these files to look anywhere in memory to locate any object, 
    //pull up the variable that was loaded on any call stack,

    if(isFullDump)
        commandStream << "-ma ";

    DWORD pid = ::GetCurrentProcessId();

    commandStream << pid;
    //it use current dir, if run from VS, or run the console1.exe in file explore
    //the dump file endup in different locations
    commandStream << " dump_file_" << static_cast<int>(100*rand()) << ".dmp";

    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    std::string str = commandStream.str();
    std::wstring wstr = std::wstring(str.begin(), str.end());
    wchar_t* commandLine = const_cast<wchar_t*>(wstr.c_str());

    //const_cast<LPSTR>(commandStream.str().c_str())
    DWORD error = 0;
    if (!CreateProcess(NULL, commandLine, NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi))
    {
        error = GetLastError();
    }
    else
    {
        WaitForSingleObject(pi.hProcess, INFINITE);
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }
}

#include <string>
#include <vector>
#include <dbghelp.h>

//got to include dbghelp.lib in the linker input
void printStack()
{
    std::cout << "print call stack" << std::endl;
    typedef USHORT(WINAPI* CaptureStackBackTraceType)(__in ULONG, __in ULONG, __out PVOID*, __out_opt PULONG);
    CaptureStackBackTraceType func = (CaptureStackBackTraceType)(GetProcAddress(LoadLibrary(L"kernel32.dll"), "RtlCaptureStackBackTrace"));

    if (func == NULL)
        return;

    // Quote from Microsoft Documentation: Windows Server 2003 and Windows XP: 
    //The sum of the FramesToSkip and FramesToCapture parameters must be less than 63.
    
    const int kMaxCallers = 62;
    void* callers_stack[kMaxCallers];
    unsigned short frames;
    std::vector<SYMBOL_INFO> symbolVec(kMaxCallers);
    HANDLE process;
    process = GetCurrentProcess();
    SymInitialize(process, NULL, TRUE);
    frames = (func)(0, kMaxCallers, callers_stack, NULL); //caller_stack is a point (array) to  void*

    //starting at 1, to skip this "printStack()" , starting from it is caller as bottom call.
    for (unsigned int i = 1; i < frames; i++)
    {
        std::stringstream out;
        symbolVec[i].MaxNameLen = 255;
        symbolVec[i].SizeOfStruct = sizeof(SYMBOL_INFO);

        SymFromAddr(process, (DWORD64)(callers_stack[i]), 0, &symbolVec[i]);
        out << "*** " << i << ": ";

        //symbol->name : typedef __possibly_notnullterminated CHAR *PNZCH;
        for (unsigned int j = 0; j <= symbolVec[i].NameLen; j++)
            out << (symbolVec[i].Name)[j];
        out << std::endl;
        std::cout << out.str();
    }
}
