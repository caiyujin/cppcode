#pragma once
#include <vector>
#include <unordered_map>
#include <set>
using namespace std;

struct edge
{
    char p1;
    char p2;
    int w;
    edge() :p1(' '), p2(' '), w(-1)
    {}
    edge(char p1a, char p2a, int wa) :p1(p1a), p2(p2a), w(wa)
    {}
};


extern unordered_map<int, vector<int> > adj;
extern vector<edge> edges;
extern set<char> vertex;

bool operator<(const edge& e1, const edge& e2);
void addEdge(int v, int w);
void addEdge(char p1a, char p2a, int wa);
