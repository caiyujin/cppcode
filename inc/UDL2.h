#ifndef _UDL_H
#define _UDL_H

    template<int M, int L, int T> 
    class Quantity {
    public:
         explicit constexpr  Quantity(double val = 0.0) : value(val) {}

         constexpr Quantity(const Quantity& x) : value(x.value) {}

         constexpr Quantity& operator+=(const Quantity& rhs) const {
            value += rhs.value;
            return *this;
        }

         constexpr Quantity& operator-=(const Quantity& rhs) const {
            value -= rhs.value;
            return *this;
        }

         constexpr double Convert(const Quantity& rhs) const {
            return value / rhs.value;
        }

         constexpr double getValue() const {
            return value;
        }

    private:
        double value;
    };

    template <int M, int L, int T>
     constexpr Quantity<M, L, T> operator+(const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return Quantity<M, L, T>(lhs) += rhs; //is it instantiate a new object with lhs (copy ctor), then invoke the += operator, which will return ref of the newly created obj.
    }

    template <int M, int L, int T>
     constexpr Quantity<M, L, T> operator-(const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return Quantity<M, L, T>(lhs) -= rhs;
    }

    template <int M1, int L1, int T1, int M2, int L2, int T2>
     constexpr Quantity<M1 + M2, L1 + L2, T1 + T2> operator*(const Quantity<M1, L1, T1>& lhs, const Quantity<M2, L2, T2>& rhs)
    {    // M-Mass, L-length, T-time;   
         //interesting, if lenght ( M1=0; L1=1; T1=0) and  ( M2=0; L2=1; T2=0) 
         //the newly created object will be Quantity <0,2,0>, ie area is lenght sqaure!!.nice.
        return Quantity<M1 + M2, L1 + L2, T1 + T2>(lhs.getValue()*rhs.getValue());
    }

    template <int M, int L, int T>
     constexpr Quantity<M, L, T> operator*(const double& lhs, const Quantity<M, L, T>& rhs)
    {
        return Quantity<M, L, T>(lhs*rhs.getValue());
    }

    template <int M1, int L1, int T1, int M2, int L2, int T2>
     constexpr Quantity<M1 - M2, L1 - L2, T1 - T2> operator/(const Quantity<M1, L1, T1>& lhs, const Quantity<M2, L2, T2>& rhs)
    {
        return Quantity<M1 - M2, L1 - L2, T1 - T2>(lhs.getValue() / rhs.getValue());
    }

    template <int M, int L, int T>
     constexpr Quantity<-M, -L, -T> operator/(double x, const Quantity<M, L, T>& rhs) {
        return Quantity<-M, -L, -T>(x / rhs.getValue());
    }

    template <int M, int L, int T>
     constexpr Quantity<M, L, T> operator/(const Quantity<M, L, T>& rhs, double x) 
    {
        return Quantity<M, L, T>(rhs.getValue() / x);
    }

    template <int M, int L, int T>
     constexpr bool operator==(const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return (lhs.getValue() == rhs.getValue());
    }

    template <int M, int L, int T>
     constexpr bool operator!=(const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return (lhs.getValue() != rhs.getValue());
    }

    template <int M, int L, int T>
     constexpr bool operator<=(const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return lhs.getValue() <= rhs.getValue();
    }

    template <int M, int L, int T>
     constexpr bool operator>=(const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return lhs.getValue() >= rhs.getValue();
    }

    template <int M, int L, int T>
     constexpr bool operator< (const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return lhs.getValue()<rhs.getValue();
    }

    template <int M, int L, int T>
     constexpr bool operator> (const Quantity<M, L, T>& lhs, const Quantity<M, L, T>& rhs)
    {
        return lhs.getValue()>rhs.getValue();
    }

    typedef Quantity<1, 0, 0> Mass; //Mass is a class from template Quantity.
    typedef Quantity<0, 1, 0> Length;
    typedef Quantity<0, 2, 0> Area;
    typedef Quantity<0, 3, 0> Volume;
    typedef Quantity<0, 0, 1> Time;
    typedef Quantity<0, 1, -1> Speed;
    typedef Quantity<0, 1, -2> Acceleration;
    typedef Quantity<0, 0, -1> Frequency;
    typedef Quantity<1, 1, -2> Weight;//weight = mass times gravitational acceleration.gravitational acceleration is always 9.8 m/s^2, //m -meter/ s-second
    typedef Quantity<1, -1, -2> Pressure;//weight/area

    constexpr Length metre(1.0); //it is an object instance of class Quantity (with template value of 1,0,0 for M,L,T respectively,
    constexpr Length decimetre = metre / 10; //divided by numerical value, so it will be the same type.
    constexpr Length centimetre = metre / 100;
    constexpr Length millimetre = metre / 1000;
    constexpr Length kilometre = 1000 * metre;
    constexpr Length inch = 2.54 * centimetre;
    constexpr Length foot = 12 * inch;
    constexpr Length yard = 3 * foot;
    constexpr Length mile = 5280 * foot;
    constexpr Frequency Hz(1.0);

    constexpr Area kilometre2 = kilometre * kilometre;
    constexpr Area metre2 = metre * metre;
    constexpr Area decimetre2 = decimetre * decimetre;
    constexpr Area centimetre2 = centimetre * centimetre;
    constexpr Area millimetre2 = millimetre * millimetre;
    constexpr Area inch2 = inch * inch;
    constexpr Area foot2 = foot * foot;
    constexpr Area mile2 = mile * mile;

    constexpr Volume kilometre3 = kilometre2 * kilometre;
    constexpr Volume metre3 = metre2 * metre;
    constexpr Volume decimetre3 = decimetre2 * decimetre;
    constexpr Volume litre = decimetre3;
    constexpr Volume centimetre3 = centimetre2 * centimetre;
    constexpr Volume millimetre3 = millimetre2 * millimetre;
    constexpr Volume inch3 = inch2 * inch;
    constexpr Volume foot3 = foot2 * foot;
    constexpr Volume mile3 = mile2 * mile;

    constexpr Time second(1.0);
    constexpr Quantity<0, 0, 2> second2(1.0);
    constexpr Time minute = 60 * second;
    constexpr Time hour = 60 * minute; // hour is an instance of templated class Time
    constexpr Time day = 24 * hour;

    constexpr Mass kg(1.0);
    constexpr Mass gramme = 0.001 * kg;
    constexpr Mass tonne = 1000 * kg;
    constexpr Mass ounce = 0.028349523125 * kg;
    constexpr Mass pound = 16 * ounce;
    constexpr Mass stone = 14 * pound;

#endif

