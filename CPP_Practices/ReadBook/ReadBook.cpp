#include "getExecutionTime.h"

unordered_map<wstring, int> res;
int topSz = 20;

void PrintOuput(void)
{
    vector<pair<wstring, int>> topWords(topSz, pair<wstring, int>(L"", 0));
    unordered_map<wstring, int>::iterator it = res.begin();
    for (; it != res.end(); it++)
    {
        vector<pair<wstring, int>>::iterator found =
            lower_bound(topWords.end() - topSz, topWords.end(), it->second,
                        [](const pair<wstring, int> &lhs, int val) -> bool { return lhs.second < val; });
        if (found > topWords.end() - topSz)
        { //if it is larger than any of the top20.
            topWords.insert(found, make_pair(it->first, it->second));
        }
    }

    for (int i = topWords.size() - 1; i >= topWords.size() - topSz; i--)
    {
        if (topWords[i].second != 0)
            wcout << topWords[i].second << " " << topWords[i].first << endl;
    }
}

void resUpdate(const wstring &word)
{

    unordered_map<wstring, int>::iterator it = res.find(word);

    if (it != res.end())
    {
        (*it).second++;
        return;
    }
    else
    {
        res[word] = 1;
        return;
    }
}

//Tokenize each line of input to words vector
void tokenize(wstring &line)
{
    //!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~          ispunct
    replace_if(line.begin(), line.end(), iswpunct, L' ');

    for (size_t i = 0, linelength = line.size(); i < linelength;)
    {
        while (iswspace(line[i]) && i < linelength)
            i++; //skip wide white space

        size_t begin = i; //begin is 1st non-white space

        while (!iswspace(line[i]) && i < linelength)
            i++; //find next wide white space

        size_t end = i;
        if (end > begin)
        {
            resUpdate(line.substr(begin, end - begin));
        }

        if (i >= linelength)
            return;
        i++;
    }
}

int main(int argc, char *argv[])
{    //moby.txt in the command arguments
    cGetTime t;
    std::wifstream file(argv[1]);
    wstring oneLine;
    for (; getline<wchar_t>(file, oneLine);)
    {
        tokenize(oneLine);
    }
    PrintOuput();
    return 0;
}