#include "getExecutionTime.h"

template<typename Key>
struct KeyHasher
{
    std::size_t operator()(const Key& k) const
    {
        return static_cast<size_t>(10*(k.first) ^k.second);
    }
};
vit theBegin;
unordered_map<pair<int, int>, int, KeyHasher<pair<int,int>> > cache;

void MaxRegInHistogram(vit start, vit end, bool skip, int currMin) 
{
    if (start == end)
    {
        cache[make_pair(start - theBegin, end - theBegin)] = (end - start + 1)* (*start);
        return;
    }

    if (cache.find(make_pair(start-theBegin, end - theBegin)) != cache.end())
    {
        return;
    }

    if (!skip)
    {
        vit it = min_element(start, end+1);
        currMin = *it;
    }

    cache[make_pair(start - theBegin, end - theBegin)] = (end - start+1)* currMin;

    vit tempStart = start;

    if(*start == currMin)
        MaxRegInHistogram(++start, end, false, INT_MAX);
    else
        MaxRegInHistogram(++start, end, true, currMin);

    if(*end == currMin)
        MaxRegInHistogram(tempStart, --end, false, INT_MAX);
    else
        MaxRegInHistogram(tempStart, --end, true, currMin);

    return;
}

int main() {
    vector<int> v(8);
    generate_vector(v, false, 100);
    { 
        cGetTime t1;
        theBegin=v.begin();
        MaxRegInHistogram(v.begin(),
                          v.end()-1,
                          false, INT_MAX);
    }

    int maxReg = INT_MIN;
    pair<int, int> maxIdx;
    for (auto& a : cache)
    {
        if (a.second > maxReg)
        {
            maxIdx = a.first;
            maxReg = a.second;
        }
            
    }

    cout << maxIdx.first << " to " << maxIdx.second << "has the largest rectangular size of : " << cache[maxIdx] << endl;

    return 0;
}



