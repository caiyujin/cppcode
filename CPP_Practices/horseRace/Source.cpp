#include "msoftcon.h"
#include <iostream>
#include <cstdlib>
#include <chrono>
#include <random>
#include <thread>

using namespace std;
static default_random_engine RndEng(static_cast<unsigned int>(chrono::steady_clock::now().time_since_epoch().count()));
const int CPF = 10;//columns per furlong.
const int maxHorses = 7;
class track;//forward declaring. the colleciton 

vector<string> players = { 
    "qiang",
    "yujing",
    "santhosh",
    "vilva",
    "fadleen",
    "Choo",
    "boontech" 
};

class horse
{
private:
    const track* mPTrack;
    const int mID; //this horse id
    float mTotalTime;
    float mCurrPos;
    bool mFinished;
    int mLastPos;
public:
    horse(const int id, const track* pTrack, bool finished):mID(id),mPTrack(pTrack), mTotalTime(0.0), mCurrPos(0.0), mFinished(finished), mLastPos(1)
        {   }
    ~horse(){}
    bool getFinished() const { return mFinished; }
    void display_horses(const float mElapsedTime);
};

class track
{
private:
    vector<horse*> mHorses;
    int mTotalHorses;
    const float mLength;//length in furlong
    float mElapsedTime;
public:
    track(float lenT, int nH);
    ~track();
    void display_track();
    void run();
    bool allFinished();
    float getLength() const { return mLength; };
    vector<horse*>& getHorses() { return mHorses; }
};

void horse::display_horses(float mElapsedTime)
{
    set_cursor_pos(mLastPos, 2 + mID * 2);
    set_color(static_cast<color>(cBLUE + mID));
    //clean up previous printed horse id.
    for(int i= mLastPos; i < mCurrPos * CPF + 1;i++)
        _putch(' ');

    _putch('0' + mID);
    mLastPos = mCurrPos * CPF + 1; //update last position

    if (mCurrPos < (mPTrack->getLength() + 1.0 / CPF))//one more "column" after last one;
    {
        mCurrPos += 0.1*(RndEng() % 2+1); //1/2/3 column for each refresh
        //mCurrPos += mBoost ? 0.1 : 0;
        mTotalTime = mElapsedTime;
    }
    else
    {
        if (!mFinished)
        {
            int mins = int(mTotalTime) / 60;
            int secs = int(mTotalTime) - mins * 60;
            cout << " '" <<players[mID].c_str() <<"' Time =" << mins << ":" << secs;
            mFinished = true;
        }
    }
}

track::track(float lenT, int nH) :mLength(lenT), mTotalHorses(nH), mElapsedTime(0.0)
{
    init_graphics();
    int id = 0;
    mTotalHorses = (mTotalHorses > maxHorses) ? maxHorses : mTotalHorses;
    for (int j = 0; j < mTotalHorses; j++)
    {
        mHorses.push_back(new horse(id++, this, false));
    }    
    display_track();
}

track::~track()
{
    for (int j = 0; j < mTotalHorses; j++)
        delete mHorses[j];
}

void track::display_track()
{
    clear_screen();

    for (int f = 0; f <= mLength; f++)
    {
        for (int r = 1; r <= mTotalHorses * 2; r++)
        {
            set_cursor_pos(f*CPF+1, r);
            if (f == 0 || f == mLength)
                cout << '|';
            else
                cout << '-';
        }
    }
}

void track::run()
{
    while (!allFinished())
    {
        mElapsedTime += 1.0; //time is not tally with below 50ms. for simulation only. and more meaningful time.
        for (int j = 0; j < mTotalHorses; j++)
        {
            mHorses[j]->display_horses(mElapsedTime);
        }

        this_thread::sleep_for(100ms);
    }
    cout << endl;
}

bool track::allFinished()
{
    for (auto& a : mHorses)
    {
        if (a->getFinished() == false)
            return false;
    }
    return true;
}


int main()
{
    track theTrack(8, 9); //track length , num of horse
    theTrack.run();

    getchar();
    return 0;
    
}