/* Returns true if the there is a subarray of arr[] with sum equal to 'sum'otherwise returns false. Also, prints the result */
#include "getExecutionTime.h"

bool isTriplet(int arr[], int n)
{
    sort(arr, arr + n);
    for (int i = n - 1; i >= 2; i--)
    {
        int l = 0;
        int r = i - 1;
        while (l < r)
        {
            if (arr[l] + arr[r] == arr[i])
            {
                //cout << arr[l] << " " << arr[r] << " " << arr[i] << endl;
                return true;
            }
            (arr[l] + arr[r] < arr[i]) ? l++ : r--; 
        }
    }
    return false;
}

struct cIntPairHasher
{
    size_t operator()(const pair<int, int>& key) const //the const at func signature is Very important, cannot forget
    {
        return hash<int>()(10 * key.first) + hash<int>()(key.second);
    }
};
unordered_map<pair<int, int>, int, cIntPairHasher> cache2;
//buy and sell stock can be easily solved with this!
int subArraySumGreater2(int sum, vit s,vit e, vit bg, int target)
{
    if (cache2.find(make_pair(s - bg, e - bg)) != cache2.end())
        return cache2[make_pair(s - bg, e - bg)];

    while (sum > target && (e-s >= 0) )
    {
        int leftMin = subArraySumGreater2(sum - *s, s + 1, e,bg, target);
        int RightMin = subArraySumGreater2(sum - *e,s, e-1, bg, target);
        cache2[make_pair(s - bg, e - bg)] = ( leftMin < RightMin ) ? leftMin : RightMin;
        return cache2[make_pair(s - bg, e - bg)];
    }

    return e - s + 1 + 1;
}


bool subArraySumEqual(vector<int> &v, int target)
{
    int sum = 0;
    queue<int> idx;
    vit start = v.begin();
    for (auto it = v.begin(); it != v.end(); it++)
    {
        while (sum > target && it > start) //the position is not the start index
        {
            sum -= *start;
            idx.pop();
            start++;
        }

        if (sum == target)
        {
           //print all index values in the idx; 
           return true;
        }

        sum += *it;
        idx.push(it -v.begin());
    }

    return false;
}

int bigger(int a, int b, bool& isA)
{
    if (a > b)
    {
        isA = true;
        return a;
    }
    else
    {
        isA = false;
        return b;
    }
    //return (a > b) ? a : b;
}

void getMaxSlice(int arr[], int n)
{
    int max_ending = 0;
    int max_slice = 0;
    int startIdx = 0;
    int endIdx = 0;
    bool isA;
    for (int i = 0; i < n; i++)
    {
        max_ending = bigger(0, max_ending + arr[i],isA); 
        //magic, it actually reset the max_ending to value 0 if any subarray sum is less than 0.        
        max_slice = bigger(max_slice, max_ending,isA);
    }
}

bool GetTapeEquibriumPoint(int arr[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += arr[i];
    }

    int tempSum = 0;
    for (int i = 0; i < n; i++)
    {
        if (sum - tempSum - arr[i] == tempSum)
        {
            return true;
        }
        tempSum += arr[i];
    }
    return false;
}

multimap<int, int> Meetings;
multimap<int, int> Results;
void createMeetings(void)
{
    while (Meetings.size() < 1000)
    {                             //create 1000 meetings with start time in mins
        int s = RndEng() % 1441; //can change to 24*60+1, become minutes!!! awesome
        int e = RndEng() % 1441;
        if (e > s)
        {
            Meetings.emplace(e, s); //sorted by end time
        }
        if (s > e)
        {
            Meetings.emplace(s, e);
        }
    }
}

//arrange as much possible meeting in one room.
void GetMaxMeetingInOneDay(void)
{
    Results.insert(*(Meetings.begin()));
    int end = Meetings.begin()->first;
    for (multimap<int, int>::iterator it = Meetings.begin(); it != Meetings.end(); it++)
    {   //follow the order, find the first meeting entry's whose start time is later than the last inserted meeting's end time
        if (it->second > end)
        { 
            Results.insert(*it);
            end = it->first;
        }
    }
}

int GetFirstNoneRepeativeChar(void)
{
    char c = 0;
    int result = -1;
    vector<int> v1(58, 0); //26+ 6+26=58 chars. from A->z
    int i = 1;
    while (c >= 0 && c != 0x5f)
    { 
        std::cout << "value: ";
        std::cin >> c;

        if (!isalpha(c))
            return -1;
        
        switch (v1[c - 'A'])
        {
            case 0:
                v1[c - 'A'] = i; //i could be 1 to any positve number, i is keep incremental , the position of the char
                break;
            case -1: //alrady multiple time
                break;
            default:
                v1[c - 'A'] = -1;
                break;
        }
        i++;
    }
    //find the smallest positive number in the v1. and the number's position +'A' will be the result;
    int Pos = INT_MAX;
    for (int i = 0; i < 58; i++)
    {   //>0 to take out those multiple char, and non-existing char.
        if (v1[i] > 0 && v1[i] < Pos)
        {
            Pos = v1[i];
            result = 'A' + i; //the char
        }
    }
    return result;
}


int main(int argc, char *argv[])
{
    {
        //vector<int> vec1(10);
        //generate_vector(vec1, false);
        vector<int> vec1 = { 1,10,91,34,20,3,5,9 };
        subArraySumEqual(vec1, 23);
    }

    {
        vector<int> v2{ 0,5,6,7,2,2,0,1,5,16,2 };
        int sum = accumulate(v2.begin(), v2.end(), 0);
        cout << subArraySumGreater2(sum, v2.begin(), v2.end()-1, v2.begin(), 19) << endl;

        //print, find the min value in the cache2...cool!
    }

    int arr3[] = {9, 1, 16, 36, 25};
    int n3 = sizeof(arr3) / sizeof(arr3[0]);
    isTriplet(arr3, n3) ? cout << "Yes" : cout << "No";
    cout << endl;

    int arr4[] = {-9, 2, 1, -3, 5, 3, 2, -3, 3, 0, 6, 7, -6, -7, 8, 9, 10, 11};
    int n4 = sizeof(arr4) / sizeof(arr4[0]);
    getMaxSlice(arr4, n4);

    int arr5[] = {-9, 4, 0, 1, 3, 2, -11, 5};
    int n5 = sizeof(arr5) / sizeof(arr5[0]);
    GetTapeEquibriumPoint(arr5, n5);

    createMeetings();
    GetMaxMeetingInOneDay();

    GetFirstNoneRepeativeChar();

    return 0;
}
