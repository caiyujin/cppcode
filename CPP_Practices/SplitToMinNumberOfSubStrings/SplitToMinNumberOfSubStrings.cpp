#include "getExecutionTime.h"
set<string> comps = { "1", "12345", "2345", "56", "23", "4567" ,"12","3","4","123"}; 
string s = "1234567"s;

//it shall be uniques, since we allowed multiple 14 been used in the partition, so effectively same as multiset!
using sit = string::iterator;
sit Start = s.begin();
sit End = s.end(); 
vector<int> cached(s.size(), INT_MIN);

int solution(const sit it1) {

    if (it1 == End)
        return 0;

    if (cached[it1 - Start] != INT_MIN)
        return cached[it1 - Start];

    vector<string> founds;
    string s = "";

    for (sit it = it1; it != End; it++) 
	{
        s += *it;
        if (comps.find(s) != comps.end())
            founds.push_back(s);
    }

    if (founds.size() == 0)
    {
        if (cached[it1 - Start] == INT_MIN)
            cached[it1 - Start] = -1;
        return -1;
    }

    int minCnt = INT_MAX;

    for (auto it = founds.begin(); it != founds.end(); it++) 
    {
        int count = solution((*it).length() + it1); //recursively look for the stripped front,
        
        if (count == -1)
        {
            if(cached[it1 - Start] == INT_MIN)//without this check.. the valid value could be overwritten by -1;
                cached[it1 - Start] = -1;
        }
        else
        {
            if ((count + 1) < minCnt)
            {
                minCnt = count + 1;
                cached[it1 - Start] = minCnt;
            }
        }
    }

    return minCnt!=INT_MAX ? minCnt : -1;

}

int main(int argc, char** argv) 
{
    cout<<"the minimum number of partitions for the string is:" << solution(s.begin()) << endl;
    
    return 0;
}