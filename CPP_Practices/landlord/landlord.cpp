#include "landlord.h"
//////////////////////global functions////////////////////////
void getaLine(string& inStr) // get line of text
{
    char temp[21];
    cin.get(temp, 20, '\n');
    cin.ignore(20, '\n');
    inStr = temp;
}


char getaChar() // get a character
{
    char ch = cin.get();
    cin.ignore(80, '\n');
    return ch;
}

tenant::tenant(string n, int aNo) : name(n), aptNumber(aNo)
{ /* empty */
}

tenant::~tenant()
{ /* empty */
}

int tenant::getAptNumber()
{
    return aptNumber;
}

bool operator < (const tenant& t1, const tenant& t2)
{
    return t1.name < t2.name;
}
//--------------------------------------------------------------
bool operator == (const tenant& t1, const tenant& t2)
{
    return t1.name == t2.name;
}
//--------------------------------------------------------------
ostream& operator << (ostream& s, const tenant& t)
{
    s << t.aptNumber << '\t' << t.name << endl; return s;
}

////////////////method for class tenantInputScreen//////////////
void tenantInputScreen::getTenant() //get tenant info
{
    cout << "Enter tenant's name(George Smith) : ";
    getaLine(tName);
    cout << "Enter tenant's apartment number(101) : ";
    cin >> aptNo;
    cin.ignore(80, '\n'); //make tenant
    tenant* ptrTenant = new tenant(tName, aptNo);
    ptrTenantList->insertTenant(ptrTenant); //send to tenant list
}
////////////////////////////////////////////////////////////////
bool compareTenants::operator () (tenant* ptrT1, tenant* ptrT2) const
{
    return *ptrT1 < *ptrT2;
}
//--------------------------------------------------------------
///////////////////methods for class tenantList/////////////////
tenantList::~tenantList() //destructor
{
    for (auto& i : setPtrsTens)
    {
        delete i;
    }
    setPtrsTens.clear();
} 
//--------------------------------------------------------------
void tenantList::insertTenant(tenant* ptrT)
{
    setPtrsTens.insert(ptrT); //insert
}
//--------------------------------------------------------------
int tenantList::getAptNo(string tName) //name on list?
{
    int aptNo;
    tenant dummy(tName, 0);
    iter = setPtrsTens.find(&dummy);
    if (iter != setPtrsTens.end())
    {
        return (*iter)->getAptNumber();
    }
    return -1;
}
//--------------------------------------------------------------
void tenantList::display() //display tenant list
{
    cout << "\nApt#\tTenant name\n------------------ - \n";

    for (auto& i : setPtrsTens)
    {
        cout << *i;
    }

} // end display()
//--------------------------------------------------------------
/////////////////////methods for class rentRow//////////////////
rentRow::rentRow(int an) : aptNo(an) // 1-arg constructor
{
    fill(&rent[0], &rent[12], 0);
}
//--------------------------------------------------------------
void rentRow::setRent(int m, float am)
{
    rent[m] = am;
}
//--------------------------------------------------------------
float rentRow::getSumOfRow() // sum of rents in row
{
    return accumulate(&rent[0], &rent[12], 0);
}
//--------------------------------------------------------------
bool operator < (const rentRow& t1, const rentRow& t2)
{
    return t1.aptNo < t2.aptNo;
}
//--------------------------------------------------------------
bool operator == (const rentRow& t1, const rentRow& t2)
{
    return t1.aptNo == t2.aptNo;
}
//--------------------------------------------------------------
ostream& operator << (ostream& s, const rentRow& an)
{
    s << an.aptNo << '\t'; //print apartment number
    for (int j = 0; j < 12; j++) //print 12 rents
    {
        if (an.rent[j] == 0)
            s << " 0 ";
        else
            s << an.rent[j] << " ";
    }
    s << endl;
    return s;
}
////////////////////////////////////////////////////////////////
bool compareRows::operator () (rentRow* ptrR1,
    rentRow* ptrR2) const
{
    return *ptrR1 < *ptrR2;
}
///////////////////methods for class rentRecord/////////////////
rentRecord::~rentRecord() //destructor
{
    for (auto& i : setPtrsRR)
    {
        delete i;
    }
    setPtrsRR.clear();
}
//--------------------------------------------------------------
void rentRecord::insertRent(int aptNo, int month, float amount)
{
    rentRow searchRow(aptNo);
    iter = setPtrsRR.find(&searchRow);
    if (iter != setPtrsRR.end())
    {
        (*iter)->setRent(month, amount);
        return;
    }
    
    rentRow* ptrRow = new rentRow(aptNo);
    ptrRow->setRent(month, amount);
    setPtrsRR.insert(ptrRow);
}
//--------------------------------------------------------------
void rentRecord::display()
{
    cout << "\nAptNo\tJan Feb Mar Apr May Jun "
        << "Jul Aug Sep Oct Nov Dec\n"
        << "-------------------------------- - "
        << "-------------------------------- - \n";

    for (auto& i : setPtrsRR)
    {
        cout << *i;
    }
}
//--------------------------------------------------------------
float rentRecord::getSumOfRents() // return sum of all rents
{
    float sumRents = 0.0;

    for (auto& i : setPtrsRR)
    {
        sumRents += i->getSumOfRow();
    }
    return sumRents;
}
//--------------------------------------------------------------
/////////////////methods for class rentInputScreen//////////////
void rentInputScreen::getRent()
{
    string renterName;
    float rentPaid;
    int month;
    int aptNo;

    cout << "Enter tenant's name : ";
    getaLine(renterName);
    aptNo = ptrTenantList->getAptNo(renterName);
    if (aptNo > 0) 
    {
        cout << "Enter amount paid(345.67) : ";
        cin >> rentPaid;
        cin.ignore(80, '\n');
        cout << "Enter month rent is for (1 - 12) : ";
        cin >> month;
        cin.ignore(80, '\n');
        month--; // (internal is 0-11)
        ptrRentRecord->insertRent(aptNo, month, rentPaid);
    }
    else
        cout << "No tenant with that name.\n";
}

bool operator < (const expense& e1, const expense& e2)
{
    if (e1.month == e2.month)
        return e1.day < e2.day;
    else
        return e1.month < e2.month; // compare months
}

bool operator == (const expense& e1, const expense& e2)
{
    return e1.month == e2.month && e1.day == e2.day;
}
//--------------------------------------------------------------
ostream& operator << (ostream& s, const expense& exp)
{
    s << exp.month << ' / ' << exp.day << '\t';// << exp.payee << '\t';
    s << exp.amount << '\t' << exp.category << endl;
    return s;
}

expenseRecord::~expenseRecord()
{
    for (auto& i : vectPtrsExpenses)
    {
        delete i;
    }
    vectPtrsExpenses.clear();
}

void expenseRecord::insertExp(expense* ptrExp)
{
    vectPtrsExpenses.push_back(ptrExp);
}
//--------------------------------------------------------------
void expenseRecord::display()
{
    cout << "\nDate\tPayee\t\tAmount\tCategory\n"
        << "----------------------------------------\n";

    //less<expense>() can be used if vector is collection of objects itself.
    //because here they are pointers, so compareDates are converting pointers to actual object's comparator(friend operator<)
    sort(vectPtrsExpenses.begin(), vectPtrsExpenses.end(), compareDates()); 
    for (auto& i : vectPtrsExpenses)
    {
        cout << *i;
    }

}
float expenseRecord::displaySummary() // used by annualReport
{
    float totalExpenses = 0;
    if (vectPtrsExpenses.size() == 0)
    {
        cout << "\tAll categories\t0\n";
        return 0;
    }
    sort(vectPtrsExpenses.begin(),vectPtrsExpenses.end(), compareCategories());
    iter = vectPtrsExpenses.begin();
    string tempCat = (*iter)->category;
    float sumCat = 0.0;
    while (iter != vectPtrsExpenses.end())
    {
        if (tempCat == (*iter)->category)
            sumCat += (*iter)->amount;
        else
        {
            cout << '\t' << tempCat << '\t' << sumCat << endl;
            totalExpenses += sumCat; // add previous category
            tempCat = (*iter)->category;
            sumCat = (*iter)->amount; // add the first item for the new category 
        }
        iter++;
    } // end while
    totalExpenses += sumCat; // add final category
    cout << '\t' << tempCat << '\t' << sumCat << endl;
    return totalExpenses;
}
//////////////methods for class expenseInputScreen//////////////
expenseInputScreen::expenseInputScreen(expenseRecord* per) : ptrExpenseRecord(per)
{
}
//-----------------------------------------------------------
void expenseInputScreen::getExpense()
{
    int month, day;
    string category;// , payee;
    float amount;
    cout << "Enter month(1 - 12) : ";
    cin >> month;
   cin.ignore(80, '\n');
    cout << "Enter day(1 - 31) : ";
    cin >> day;
    cin.ignore(80, '\n');
    cout << "Enter expense category(Repairing, Utilities) : ";
    getaLine(category);
    //cout << "Enter payee (Bob's Hardware, Big Electric Co) : ";
    //getaLine(payee);
    cout << "Enter amount(39.95) : ";
    cin >> amount;
    cin.ignore(80, '\n');
    expense* ptrExpense = new expense(month, day, category, /*payee,*/ amount);
    ptrExpenseRecord->insertExp(ptrExpense);
}
//-----------------------------------------------------------
//////////////////methods for class annualReport/////////////
annualReport::annualReport(rentRecord* pRR, expenseRecord* pER) : ptrRR(pRR), ptrER(pER)
{ /* empty*/
}
//-----------------------------------------------------------
void annualReport::display()
{
    cout << "Annual Summary\n--------------\n";
    cout << "Income\n";
    cout << "\tRent\t\t";
    rents = ptrRR->getSumOfRents();
    cout << rents << endl;
    cout << "Expenses\n";
    expenses = ptrER->displaySummary();
    cout << "\nBalance\t\t\t" << rents - expenses << endl;
}
//-----------------------------------------------------------
////////////////methods for class userInterface//////////////
userInterface::userInterface()
{
    ptrTenantList = new tenantList;
    ptrRentRecord = new rentRecord;
    ptrExpenseRecord = new expenseRecord;

    ptrTenantInputScreen = new tenantInputScreen(ptrTenantList);
    ptrRentInputScreen = new rentInputScreen(ptrTenantList, ptrRentRecord);
    ptrExpenseInputScreen = new expenseInputScreen(ptrExpenseRecord);
    ptrAnnualReport = new annualReport(ptrRentRecord, ptrExpenseRecord);
}
//-----------------------------------------------------------
userInterface::~userInterface()
{
    delete ptrTenantList;
    delete ptrRentRecord;
    delete ptrExpenseRecord;
}
//-----------------------------------------------------------
void userInterface::interact()
{
    while (true)
    {
        cout << "Enter 'i' to input data, \n"
            << "       'd' to display a report, \n"
            << "        'q' to quit program : ";
        ch = getaChar();
        if (ch == 'i') // enter data
        {
            cout << "Enter 't' to add tenant, \n"
                << "       'r' to record rent payment, \n"
                << "       'e' to record expense : ";
            ch = getaChar();
            switch (ch)
            {
                //input screens exist only while being used
            case 't': ptrTenantInputScreen->getTenant();
                break;
            case 'r': ptrRentInputScreen->getRent();
                break;
            case 'e': ptrExpenseInputScreen->getExpense();
                break;
            default: cout << "Unknown input option\n";
                break;
            }
        }
        else if (ch == 'd')
        {
            cout << "Enter 't' to display tenants, \n"
                << " 'r' to display rents\n"
                << " 'e' to display expenses, \n"
                << " 'a' to display annual report : ";
            ch = getaChar();
            switch (ch)
            {
            case 't': ptrTenantList->display();
                break;
            case 'r': ptrRentRecord->display();
                break;
            case 'e': ptrExpenseRecord->display();
                break;
            case 'a': ptrAnnualReport->display();
                break;
            default: cout << "Unknown display option\n";
                break;
            }
        }
        else if (ch == 'q')
            return; // quit
        else
            cout << "Unknown option.Enter only 'i', 'd' or 'q'\n";
    }
} 