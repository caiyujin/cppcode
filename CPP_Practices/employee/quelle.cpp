#include "getExecutionTime.h"

const int MAXEM = 100;

enum employee_type 
{ 
    tmanager, 
    tscientist, 
    tlaborer
};

class employee
{
private:
    string mName;
    long mAge;
    static int n;
    static vector<employee*> arrap;
public:
    virtual void getdata()
    {
        cin.ignore(10, '\n');
        cout << "Enter name:"; cin >> mName;
        cout << "Enter age:"; cin >> mAge;
    }
    virtual void putdata()
    {
        cout << "\tName:" << mName << "\tAge:" << mAge << endl;;
    }

    virtual employee_type get_type();
    static void add();
    static void display();
    static void read();
    static void write();
    static void clear();
};

int employee::n = 0;

vector<employee*> employee::arrap;

class manager :public employee
{
private: 
    string mTitle;
    int mRanking;
public:
    virtual void getdata()
    {
        employee::getdata();
        cout << "Enter title:"; cin >> mTitle;
        cout << "Enter level:"; cin >> mRanking;
    }
    virtual void putdata()
    {
        employee::putdata();
        cout << "\tTitle:"<< mTitle << "\tlevel:" << mRanking<<endl;
    }
};

class scientist :public employee
{
private:
    int mPubs;
public:
    virtual void getdata()
    {
        employee::getdata();
        cout << "Enter number of publication:"; cin >> mPubs;
    }
    virtual void putdata()
    {
        employee::putdata();
        cout << "\tNumber of publication:" << mPubs <<endl;
    }
};

class laborer : public employee
{
};

void employee::add()
{
    if (n >= MAXEM)
    {
        cout << "Max item allowed are: " << MAXEM << endl;
        return;
    }

    char ch;
    cout << "'m' to add a manager, \n's' to add a scientist, \n'l' to add a laborer, \nEnter selection:";
    cin >> ch;

    switch (ch)
    {
        case 'm': arrap.push_back(new manager); break;
        case 's': arrap.push_back(new scientist); break;
        case 'l': arrap.push_back(new laborer); break;
        default: cout << "Unknown employee type" << endl; return;
    }

    arrap[n++]->getdata();
}

//display all employee

void employee::display()
{
    for (int i = 0; i < n; i++)
    {
        cout << "#:"<<(i + 1) <<":" ;
        switch (arrap[i]->get_type())
        {
        case tmanager: cout << "Manager "; break;
        case tscientist: cout << "Scientist "; break;
        case tlaborer: cout << "Laborer "; break;
        default: cout << "Unknown type "; break;
        }
        arrap[i]->putdata();
        cout << endl;
    }
}

employee_type employee::get_type()
{
    if (typeid(*this) == typeid(manager))
        return tmanager;

    if (typeid(*this) == typeid(scientist))
        return tscientist;

    if (typeid(*this) == typeid(laborer))
        return tlaborer;

    return tmanager;
}

void employee::write()
{
    int sz=0;
    cout << "writing: " << n << " employee" << endl;
    ofstream ouf;
    employee_type etype;

    ouf.open("employ.dat", ios::trunc | ios::binary);
    
    for (int i = 0; i < n; i++)
    {
        etype = arrap[i]->get_type();
        ouf.write((char*)&etype, sizeof(etype));
        switch (etype)
        {
        case tmanager: sz = sizeof(manager); break;
        case tscientist: sz = sizeof(scientist); break;
        case tlaborer: sz = sizeof(laborer); break;
        default: cout << "unkown" << endl;
        }
        ouf.write((char*)(arrap[i]), sz);
        if(!ouf)
        {
            cout << "cannot write!" << endl;
        }
    }

}

void employee::read()
{
    int sz = 0;
    ifstream inf;
    employee_type etype;

    inf.open("employ.dat",  ios::binary);
    
    clear();
    while (true)
    {
        inf.read((char*)&etype, sizeof(etype));
        if (inf.eof())
            break;
        if (!inf)
            cout << "cannot read type" << endl;

        switch (etype)
        {
            case tmanager: arrap.push_back(new manager); sz = sizeof(manager); break;
            case tscientist: arrap.push_back(new scientist); sz = sizeof(scientist); break;
            case tlaborer: arrap.push_back(new laborer); sz = sizeof(laborer); break;
            default: cout << "unknown type" << endl; return;
        }

        //employee* e = new laborer;
        //inf.read((char*)(e), sz);
        //delete e;

        inf.read((char*)(arrap[n++]), sz);
        if (!inf)
            cout << "cannot read type" << endl;
    }
    cout << "reading " << n << " employee" << endl;
}

void employee::clear()
{
    for (int i = 0; i < n; i++)
    {
        delete arrap[i];
    }
    arrap.clear();
    n = 0;
}

int main()
{
    char ch;
    while (true)
    {
        cout << "'a' -- add data for an employee"
            "\n'd' -- display data for all employee"
            "\n'w' -- write all employee data to file"
            "\n'r' -- read all employee data from file"
            "\n'e' -- exit"
            "\nEnter Selection:";
        cin >> ch;

        switch (ch)
        {
        case 'a': employee::add(); break;
        case 'd': employee::display(); break;
        case 'w': employee::write(); break;
        case 'r': employee::read(); break;
        case 'e': exit(0); break;
       // case 'e':employee::clear(); break; // a, w, r, e (the item read from file, when delete. having issue...)
       // confirmed is inf.read((char*)(arrap[n++]), sz) issue, damn!.
       // You are (de-)serializing complex structures as though they were POD's. The line file.read((char*)&fdata,...) is going to cause you trouble. 
       // You are essentially reading back pointers (contained in your std::strings for example) that happened to be valid when you wrote them to a file.
        default: cout << "Unknown selection";
        }

    }
    return 0;
    //...
}