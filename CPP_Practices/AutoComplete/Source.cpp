#include "getExecutionTime.h"

struct trie {
    char c;
    bool newC;
    trie* lastP;//not owning pointer.
    vector<trie*> children;

    trie(const char& _c) :c(_c), newC(false),  lastP(this)
    {
    }

    ~trie() 
    {
        for (auto& p : children)
            delete p;
    }

    void insertWord(const string& _s)
    {
        resetCP(); //before insert, lastP is reset to "this", eg root!
        for (auto& c : _s)
            insertChar(c);
    }

    void insertChar(char _c) 
    {
        if(newC)
        {
            insertNewChar(_c);
            return;
        }

        if(!findChar(_c))
            insertNewChar(_c);
    }

    void insertNewChar(char _c)
    {
        trie* temp = new trie(_c);
        lastP->children.push_back(temp);
        lastP = temp;
        newC = true; //if already "start at certain layer/level", found a new char, then the rest of the chars of the words will all be New 
    }
    
    trie* findWord(const string& _s)
    {
        resetCP(); //before insert, lastP is reset to "this", eg root!
        for (auto& c : _s)
            if (!findChar(c)) //each layer/level, got to find a matching char!
                return nullptr;
        return lastP;
    }

    bool findChar(char _c) 
    {
        for (auto& a : lastP->children)
        {
            if (_c == a->c) //at least find One path !
            {
                lastP = a;
                return true;
            }
        }
        return false;
    }

    vector<string> readAllWords() {

        if (children.size() == 0)
            return vector<string>{};

        vector<string> vc;
        for (auto& a : children)
        {
            vector<string> vctemp = a->readAllWords();
            
            if (vctemp.empty())
            {
                vc.push_back(string(1, a->c)); //string(size_t n, char c), instantiate a string with n number of char!.
            }
            else
            {
                for (auto& s : vctemp) 
                {
                    s = a->c + s;
                    vc.push_back(s);
                }
            }
        }

        return vc;
    }
    
    void resetCP() 
    {
        newC = false;
        lastP = this;//this is the key
    }
};

vector<string> dictionary;
trie t('_');

vector<string> autocomplete(const string& _s) {

    trie* p = t.findWord(_s);

    vector<string> res;

    if (p != nullptr)
        res = p->readAllWords();

    return res;
}


int main() {
    dictionary = vector<string>{ "ddc","ddce","dark", "cat","door", "dodddddj", "dodge","did1a","did1b","elli", "dig","dix","bcd","efg","xij","did2"};
    for (auto& s : dictionary) 
        t.insertWord(s);

    vector<string> res = autocomplete("di");
    print_info(res, "auto complete");
    return 0;
}