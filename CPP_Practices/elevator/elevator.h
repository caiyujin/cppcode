#pragma once
//#include "elevator.h"
#include "msoftcon.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <conio.h>
#include <stdlib.h>
#include <process.h>
#include <vector>

using namespace std;
const int LOAD_TIME = 3;
const int SPACING = 7;
const int BUF_LENGTH = 80;
const int NUM_FLOORS = 20;
const int NUM_CARS = 4;

class building; //forward declaring.

enum direction { UP, DN, STOP };
class elevator
{
private:
    building* ptrBuilding;
    const int car_number;
    int current_floor;
    int old_floor;
    direction current_dir;
    bool destination[NUM_FLOORS];
    int loading_timer;
    int unloading_timer;
    ifstream f;
public:
    elevator(building* b, int i);
    void car_tick1();
    void car_tick2();
    void car_display();
    void display_destinations() const;
    void decide();
    void move();
    void get_destinations();
    void get_destinations_file();
    int get_floor() const;
    direction get_direction() const;
};

class building
{
private:
    vector<elevator*> car_list;
    int num_cars;
    bool floor_request[2][NUM_FLOORS];
    ifstream f;
public:
    building();
    ~building();
    void master_tick();
    int get_cars_floor(const int carNo) const;
    direction get_cars_dir(const int carNo) const;
    bool get_floor_req(const int dir, const int floor) const;
    void set_floor_req(const int dir, const int floor, const bool b);
    void floor_requests();
    void floor_requests_file();
    void show_floor_reqs() const;
};