#include "elevator.h"
building::building()
{
    f.open("building.txt");
    init_graphics();
    clear_screen();
    for (int j = 0; j < NUM_CARS; j++)
    {
        car_list.push_back(new elevator(this, j));
    }
    for (int j = 0; j < NUM_FLOORS; j++)
    {
        set_cursor_pos(3, NUM_FLOORS - j);
        cout << setw(3) << j;
        floor_request[UP][j] = false;
        floor_request[DN][j] = false;
    }
}
building::~building()
{
    for (int k = 0; k < NUM_CARS; k++)
        delete car_list[k];
}

void building::master_tick()
{
    int j;
    show_floor_reqs();
    for (j = 0; j < NUM_CARS; j++)
        car_list[j]->car_tick1();
    for (j = 0; j < NUM_CARS; j++)
        car_list[j]->car_tick2();
}

int building::get_cars_floor(const int carNo) const
{
    return car_list[carNo]->get_floor();
}

direction building::get_cars_dir(const int carNo) const
{
    return car_list[carNo]->get_direction();
}

bool building::get_floor_req(const int dir, const int floor) const
{
    return floor_request[dir][floor];
}

void building::set_floor_req(const int dir, const int floor, const bool b)
{
    floor_request[dir][floor] = b;
}

void building::show_floor_reqs() const
{
    for (int j = 0; j < NUM_FLOORS; j++)
    {
        set_cursor_pos(SPACING,NUM_FLOORS - j);
        if (floor_request[UP][j] == true)
            cout << 'U';
        else
            cout << ' ';
        set_cursor_pos(SPACING+3, NUM_FLOORS - j);
        if (floor_request[DN][j] == true)
            cout << 'D';
        else
            cout << ' ';
    }
}

void building::floor_requests_file()
{
    int iFloor;
    char chDirection;

    f >> iFloor;

    if (iFloor == -1)
    {
        return;
    }

    f >> chDirection;

    if (chDirection == 'u' || chDirection == 'U')
        floor_request[UP][iFloor] = true;
    if (chDirection == 'd' || chDirection == 'D')
        floor_request[DN][iFloor] = true;
}

void building::floor_requests()
{
    int iFloor;
    char chDirection;
    
    set_cursor_pos(1, 22);
    cout << "Enter the floor you are on (-1 to quit): "; cin >> iFloor;

    if (iFloor == -1)
    {
        set_cursor_pos(1, 22); clear_line();
        return;
    }

    cout << "Enter the direction you want to go (u or d):"; cin >> chDirection;

    if (chDirection == 'u' || chDirection == 'U')
        floor_request[UP][iFloor] = true;
    if (chDirection == 'd' || chDirection == 'D')
        floor_request[DN][iFloor] = true;

    set_cursor_pos(1, 22); clear_line();
    set_cursor_pos(1, 23); clear_line();
    set_cursor_pos(1, 24); clear_line();
}

elevator::elevator(building* b, int carNo):ptrBuilding(b),car_number(carNo)
{
    current_floor = 0;
    old_floor = 0;
    current_dir = STOP;
    for (int j = 0; j < NUM_FLOORS; j++)
        destination[j] = false;

    loading_timer = 0;
    unloading_timer = 0;
    string filename = "elevator";
    filename += char('0' + carNo) + ".txt";
    f.open(filename.c_str());
}

void elevator::car_tick1()
{
    car_display();
    
    display_destinations();
    
    if (loading_timer!=0)
    {
        --loading_timer;
    }
    if (unloading_timer!=0)
    {
        --unloading_timer;
        if (unloading_timer == 0)
            destination[current_floor] = false;
    }
    
    decide();
}

void elevator::car_tick2()
{
    move();
}
void elevator::car_display()
{
    set_cursor_pos(SPACING + (car_number + 1)*SPACING, NUM_FLOORS - old_floor);
    cout << "   ";

    set_cursor_pos(SPACING + (car_number + 1)*SPACING, NUM_FLOORS - current_floor);
    switch (loading_timer)
    {
    case 3: cout << "@##" << endl; break;
//    case 2: cout << "#@#" << endl; get_destinations(); break;
    case 2: cout << "#@#" << endl; get_destinations_file(); break;
    case 1: cout << "###" << endl; break;
    case 0: cout << "###" << endl; break;
    }

    set_cursor_pos(SPACING + (car_number + 1)*SPACING, NUM_FLOORS - current_floor);
    switch (unloading_timer)
    {
    case 3: cout << "#@#" << endl; break;
    case 2: cout << "##@" << endl; break;
    case 1: cout << "###" << endl; break;
    case 0: cout << "###" << endl; break;
    }
    old_floor = current_floor;
}

void elevator::display_destinations() const
{
    for (int j = 0; j < NUM_FLOORS; j++)
    {
        set_cursor_pos(SPACING - 2 + (car_number + 1)*SPACING, NUM_FLOORS - j);
        if (destination[j] == true)
            cout << '*';
        else
            cout << ' ';
    }
}

void elevator::decide()
{
    int j;
    bool destAbove = false;
    bool destBelow = false;
    bool reqAbove = false;
    bool reqBelow = false;

    int nearest_higher_req = 0;
    int nearest_lower_req = 0;
    
    if((current_floor == NUM_FLOORS-1 && current_dir == UP)||(current_floor == 0 && current_dir == DN))
        current_dir=STOP;

    //need to unload some one;
    if (destination[current_floor] == true && unloading_timer == 0)
    {
        unloading_timer = LOAD_TIME;//this is same for load/unload
        return;
    }

    //need to load some one if current floor is a request floor, 
    if(ptrBuilding->get_floor_req(UP, current_floor) && current_dir !=DN)
    { 
        current_dir = UP;
        ptrBuilding->set_floor_req(UP, current_floor, false);
        if (loading_timer == 0 )
            loading_timer = LOAD_TIME;
        return;
    }

    if (ptrBuilding->get_floor_req(DN, current_floor) && current_dir != UP)
    {
        current_dir = DN;
        ptrBuilding->set_floor_req(DN, current_floor, false);
        if (loading_timer == 0)
            loading_timer = LOAD_TIME;
        return;
    }

    //check requests in both direction, and destinations.
    for(j=current_floor+1; j<NUM_FLOORS;j++)
    {
        if (destination[j])
            destAbove = true;
        
        if (ptrBuilding->get_floor_req(DN, j) || ptrBuilding->get_floor_req(UP, j))
        {
            reqAbove = true;
            if (nearest_higher_req==0)
                nearest_higher_req = j; //cannot break,  becasue we need to find the two values. 
        }
    }
    
    for (j = 0; j < current_floor; j++)
    {
        if (destination[j])
            destBelow = true;

        if (ptrBuilding->get_floor_req(DN, j) || ptrBuilding->get_floor_req(UP, j))
        {
            reqBelow = true;
            if (nearest_lower_req == 0)
                nearest_lower_req = j; //cannot break,  becasue we need to find the two values. 
        }
    }

    //when no more destination and no more request. stop here.
    if (!destAbove && !reqAbove && !destBelow && !reqBelow)
    {
        current_dir = STOP;
        return;
    }

    //below two already in right direction for current car(elevator), so keep moving.
    //imagine current only elevator 0 is at level 5 and moving to level 8, and the new request is level 4 and up,
    //even though elevator 1/2/3 all STOP at level 0, and further away from the new request as compared to elevator0;
    //still one of them have to take the action. NOT #0.
    if(destAbove == true && current_dir != DN)
    {
        current_dir = UP;
        return;
    }

    if (destBelow == true && current_dir != UP)
    {
        current_dir = DN;
        return;
    }

    bool btwnUp = false;
    bool btwnDn =false;
    bool oppUp = false;
    bool oppDn =false;

    int floor;
    direction dir;
    for (j = 0; j < NUM_CARS; j++)
    {
        if (j != car_number)
        {
            floor = ptrBuilding->get_cars_floor(j);
            dir = ptrBuilding->get_cars_dir(j);
            //in between current car and nearest_higher
            if ( dir != DN && reqAbove)
            {   //if in between, confirm the car is nearer to (this) car.
                if ((floor > current_floor && floor < nearest_higher_req) || (floor == current_floor && j < car_number))
                    btwnUp = true;
            }

            //in the opposite side of nearest_higher (vs current car) 
            if (dir != UP && reqAbove)
            {
                if ((nearest_higher_req < floor) && (floor - nearest_higher_req < nearest_higher_req - current_floor))
                    oppDn = true;
            }

            //in between current car and nearest_lower
            if (dir != UP && reqBelow)
            {   //if in between, confirm the car is nearer to (this) car.
                if ((floor < current_floor && floor > nearest_lower_req) || (floor == current_floor && j < car_number))
                    btwnDn = true;
            }

            //in the opposite side of nearest_lower (vs current car) 
            if (dir != DN && reqBelow)
            {
                if ((nearest_lower_req > floor) && (nearest_lower_req - floor < current_floor- nearest_lower_req))
                    oppUp = true;
            }
        }//end for 
    }

    if (dir != DN && reqAbove && !btwnUp && !oppDn)
    {
        current_dir = UP;
        return;
    }

    if (dir != UP && reqBelow && !btwnDn && !oppUp)
    {
        current_dir = DN;
        return;
    }

    current_dir = STOP;
}
void elevator::move()
{
    if (loading_timer!=0 ||unloading_timer != 0)
        return;

    if (current_dir == UP)
        current_floor++;

    if (current_dir == DN)
        current_floor--;
}

//right after loaded, people will press which floors to go 
void elevator::get_destinations_file()
{
    int dest_floor;

    if (!(f >> dest_floor))
    {
        f.close();
        return;
    }

    if (dest_floor == current_floor)
    {
        return;
    }

    if (current_dir == STOP)
        current_dir = (dest_floor < current_floor) ? DN : UP;

    destination[dest_floor] = true;
}

//right after loaded, people will press which floors to go 
void elevator::get_destinations() 
{
    int dest_floor;
    while(true)
    {
        set_cursor_pos(1, 22); clear_line();
        set_cursor_pos(1, 22);
        cout << "Car :" << car_number << " has stopped at floor " << current_floor << ". Enter destination floors (-1 to quit):";

        cin >> dest_floor;
        if (dest_floor == -1)
        {
            set_cursor_pos(1, 22); clear_line();
            set_cursor_pos(1, 22);
            return;
        }

        if(dest_floor==current_floor)
        {
             continue;
        }
        
        if (current_dir == STOP)
            current_dir = (dest_floor < current_floor) ? DN : UP;

        destination[dest_floor] = true;
    }
}

int elevator::get_floor() const
{
    return current_floor;
}
direction elevator::get_direction() const 
{
    return current_dir;
}