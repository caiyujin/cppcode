#include "getExecutionTime.h"
struct box
{
    int L;
    int W;
    int H;
    box(int L1, int W1, int H1) : L(L1), W(W1), H(H1)
    {}
};

const int N = 100;
vector<box* > boxes;
vector<vector<int> > smallers;
vector<int> maxHeight;

void generateAndSortBoxes()
{
    for (int i = 0; i < N; i++)
    {
        int L= RndEng() % 10 +1;
        int W = RndEng() % 10 +1;
        int H = RndEng() % 10+1;
        boxes.push_back(new box(L, W, H));
    }

    sort(boxes.begin(), boxes.end(), [](box* lhs, box* rhs)->bool 
        {
            if (lhs->L < rhs->L)
            {
                return true;
            }
            else
            {
                if (lhs->L == rhs->L)
                    return lhs->W < rhs->W;

                // lhs->L > rhs->L
                return false;// 
            }
        }    
    );
}

void createAsyncGraph()
{
    for (int i = 0; i < boxes.size(); i++)
    {
        smallers.push_back(vector<int>());
        vector<int>& last = smallers.back();

        for (int j = 0; j < i; j++)
        {
            if (boxes[j]->W <= boxes[i]->W)
                last.push_back(j);
        }
    }
}

void initializeHeight()
{
    for (int i = 0; i < boxes.size(); i++)
    {
        if (smallers[i].size() == 0)
            maxHeight.push_back(boxes[i]->H);
        else
            maxHeight.push_back(INT_MIN);
    }
}

void findMaxHeight(int i)
{
    //the key is the sorted boxes, we cannot simply look for width less than current box in the smallers[i];
    //it could be possible we are at L3W4, and we got to skip L2W3(if H is 1), and go for L1W4 (if H is 9)directly!

    //very similar to ... longest increasing subarray (may not continous)
    int tmp = INT_MIN;

    for (auto& a : smallers[i])
    {
        if (maxHeight[a] > tmp)
            tmp = maxHeight[a];
    }

    maxHeight[i] = boxes[i]->H + tmp;
}


void findMaxHeightForEachBox()
{
    for (int i = 0; i < boxes.size(); i++)
    {
        if (maxHeight[i] == INT_MIN)
            findMaxHeight(i);
    }
}


int main() {

    generateAndSortBoxes();
    createAsyncGraph(); //this is the key!
    initializeHeight();

    findMaxHeightForEachBox();
    vector<int>::iterator it = max_element(maxHeight.begin(), maxHeight.end());
    cout << *it << endl;
    return 0;
}
