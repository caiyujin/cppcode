#include "getExecutionTime.h"
const int totaldays = 18;
vector<long long> Cache(totaldays, -1);

long long calculateTotalFromOne(int days)
{
    //means no more reproduciton, return itself.
    if (days <= 0)
        return 1;

    if (Cache[days] != -1)
        return Cache[days];

    Cache[days] = calculateTotalFromOne(days - 7) + calculateTotalFromOne(days - 9);
    return Cache[days];
}
//li qiang
long long calculateTotalFromOne2(vector<int>& in, int days)
{
    vector<int> v(9, 0);
    int sz = 9;
    for (auto& a : in)
        v[sz - 1 - a]++;

    for (int i = 1; i <= days; i++)
    {
        int temp = v.back();
        rotate(v.rbegin(), v.rbegin() + 1, v.rend());
        v[2] += temp;
    }

    return accumulate(v.begin(), v.end(), 0);
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream file(argv[1]);

        int x;
        char c;
        vector<int> in;

        while (file >> x)
        {
            in.push_back(x);
            file >> c;
        }

        long long  count = 0;
        for (auto a : in)
        {            
            count += calculateTotalFromOne(totaldays-a);//set to all 0 days (align all fish to 0)
        }

        long long  count2 = calculateTotalFromOne2(in, totaldays);

        cout << count << endl;
        cout << count2 << endl;
    }

    return 0;
}