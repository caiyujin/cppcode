#include "getExecutionTime.h"
const int maxNodes = 10;
const int maxWeight = 20;

void fillInputVectors(vector<int>& from, vector<int>& to, vector<int>& weight)
{
    vector<int> getUniq(maxWeight + 1);

    for (size_t i = 1; i < getUniq.size(); i++) 
    {
        getUniq[i] = i;
    }

    for (int i = 1; i <= maxNodes; i++) 
    {
        for (int j = i + 1; j <= maxNodes; j++) 
        {
            int edgeNum = RndEng() % (maxNodes / 5) + 1; // average is 100/5 /2 =10 edges with other maxNodes.
            vector<int> getUniq2(getUniq);

            for (int l = 1; l <= edgeNum; l++) 
            {
                int idx = RndEng() % maxWeight + 1;
                if (getUniq2[idx] != -1) 
                {
                    from.push_back(i);
                    to.push_back(j);
                    weight.push_back(getUniq2[idx]);
                    getUniq2[idx] = -1; //don't use this value any more. for this pair (i,j)
                }
                else 
                {
                    l--;
                }
            }
        }
    }
}

void maxShared(vector<int>& from, vector<int>& to, vector<int>& weight) 
{
    //y is maxNodes, the respective pos of bit in the bitset is for each weight. 
    vector<bitset<maxWeight+1> > vv(maxNodes +1, bitset<maxWeight + 1>{"0"});
    for (size_t i = 0; i < weight.size();i++) 
    {
        int val = weight[i];
        int fromIdx = from[i];
        int toIdx = to[i];
		vv[fromIdx].set(val); //it just say one weight is shared between fromIdx, but did not specify which node it shared with
	    vv[toIdx].set(val);
    }
	vector<vector<int> > vw(maxNodes+1,vector<int>(maxNodes+1,0));
    int maxFrom = 0;
    int maxTo = 0;
    int maxCnt = INT_MIN;

	// => vv[i] & vv[j] will definitely <=vv[i] <maxCnt;
	for (size_t i = 1; i < vw.size() && (static_cast<int>(vv[i].count()) >= maxCnt); i++) 
    {
        size_t j = i + 1;
		for ( ;j < vw.size(); j++) {
            
			vw[i][j] = (vv[i] & vv[j]).count(); 
			//how many 1's are in common pos!  same weight 
			//both owned by node i and node j, but NOT necessary
			//the edge link with these two nodes.
            
			if ((maxCnt < vw[i][j]) ||
				(maxCnt == vw[i][j] && (i * j) > (maxFrom * maxTo))) //count same, check their multiple
			{
                maxCnt = vw[i][j];
                maxFrom = i;
                maxTo = j;
            }
        }
    }
    cout << "maxFrom:" << maxFrom << "\tmaxTo:" << maxTo << "\tnumber of pairs: " << maxCnt  <<"\tProduct is:" << maxFrom * maxTo <<endl;
}

int main(){
    vector<int> from;
    vector<int> to;
    vector<int> weight;
	fillInputVectors(from, to, weight);
    {
        cGetTime t;
        maxShared(from, to, weight);
    }

    return 0;
}

