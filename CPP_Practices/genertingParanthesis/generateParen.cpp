#include "getExecutionTime.h"

void addPair(string& s1, unordered_set<string>& results) 
{
    int len = s1.length();
    string org1 = s1; 
    string org2;
    for (int i = 0; i < len+1; i++) 
    {
        s1 = org1; //org1 keep track of original 
        s1.insert(i, "(");
        
        org2 = s1; //org2 keep the first insert.
        for (int j = i + 1; j < len+1+1; j++) 
        {
            s1 = org2; //org2 keep track of 1st modification. 
            s1.insert(j, ")");
            results.insert(s1); //auto skip the duplicate set (unordered_set is hash table with no "value", only unique key
        }
    }
    s1 = org1;//restore back.
}

unordered_set<string> genBraces(int i) 
{
    unordered_set<string> vs;
    if (i == 1)
        return unordered_set<string>{"()"};

    unordered_set<string> s = genBraces(i - 1);

    for(auto& s1: s)
        addPair(const_cast<string&>(s1), vs);

    return vs;
}

int main() {
    for(auto& a:genBraces(7))
        cout<<a <<endl;
    return 0;
}