#include "getExecutionTime.h"
unordered_map<string, string> cachedRes;

bool foundInString(vector<int>& sv, const vector<int>& tv, const bool& needCheck = false) {
    if (needCheck == true) //incremental check. to speed up 
        return true;

    for (int i = 0; i < tv.size();i++) {
        if (sv[i] < tv[i])
            return false;
    }
    return true;
}

string minWindow(string s, vector<int>& sv, const string& t, const vector<int>& tv, const bool& needCheck = false) {
    
    string org_s = s;
    
    if (cachedRes.find(s) != cachedRes.end()) //dynamic programing
        return cachedRes[s];
    
    if (!foundInString(sv, tv, needCheck))
        return "";

    //triming left
    int i = 0;
    for (; i < s.size(); i++)
    {
        if (tv[s[i] - 'A'] != 0)
            break;
    }
    s = s.substr(i); //not yet removed the char (found in target), include i,

    //triming right
    int j = s.size() - 1;
    for (; j >= 0; j--)
    {
        if (tv[s[j] - 'A'] != 0)
            break;
    }
    s = s.substr(0, j + 1);  //not yet removed the char (found in target), include j,

    sv[s[0] - 'A']--;
    string sl;
    if(sv[s[0] - 'A'] >= tv[s[0]-'A'])
        sl = minWindow(s.substr(1), sv, t, tv, true);
    else
        sl = minWindow(s.substr(1), sv, t, tv, false);
    sv[s[0] - 'A']++;
       
    sv[s.back() - 'A']--;
    string sr;    
    if (sv[s.back() - 'A'] >= tv[s.back() - 'A'])
        sr = minWindow(s.substr(0, s.size() - 1), sv, t, tv, true);
    else
        sr = minWindow(s.substr(0, s.size() - 1), sv, t, tv, false);
    sv[s.back() - 'A']++;

    if (sl == "" && sr == "") 
    {
        cachedRes[org_s] = s;
        return s;
    }
    else
    {
        if (sl.size() < sr.size())
        {
            cachedRes[org_s] = sl; 
            return sl;
        }
        else { //this case include "sl == "" && sr != "" 
            cachedRes[org_s] = sr;
            return sr;
        }
    }
    
    return s;
}
int main() {
    //string s = "ADOBECODEBANC";
    //string t = "ABC";
    string s = "cwirwjbfntstplnenpabdttnbiagcnrglbyhnbnavhvmtlqgaqkdmdtnltvpipwuquddvseqabctmsbmllsxrlmegjupyqlpmqsjlyalaegozjbkxtjogxsmgodhgqwsjqeureftknhlwixvdgjjfeyoudvburvdjzxafetqtbdplblrjwcpccdxgyyarvfaxcbciwubzysnzfekeizgledredrvzyyyazakxvlxvfkwlqgpyixjmbargtohrmftngfldskyywwlmccmkzwzayshugontwhicovfhffhbdsphucutatwalfutviorrxvhscoyhvbmntujvofxjbxwispdcexvdscvvtveozresnnpbsmmvjifdxlhdicgchexazcqavusikhlevxaffhkessicwqffuchugyudspncwahuxjzeslll";
    string t = "ftpejujeztahrwljlao";
    vector<int> tv = stringToVec(t);
    vector<int> sv = stringToVec(s);

    {
        cGetTime c1;
        std::cout << minWindow(s, sv, t, tv, false) << endl;
    }

    return 0;

}