// bitPopulation.cpp : Defines the entry point for the console application.
/*
18: 0 001 0010   
001 0010->1101101 +1 -> 1101110
-18:1 110 1110
*/
#include "getExecutionTime.h";

vector<int> bitVec(256);

int GetHowManyOne(int i) {
    int count = 0;
    for (int j = 0; j < 8; j++) 
    {
        if (i >> j & 1)
            count++;
    }
    return count;
}

void populateLookUpTable(void) {
    for (int i = 0; i < 256; i++) 
    {
        bitVec[i] = GetHowManyOne(i);
    }
}

void DecimalToBinary(int i) {
    vector<int> bin;

    while (i / 2 != 0) 
    {
        bin.push_back(i % 2);
        i = i / 2;
    }
}

int MinFlipAlternative(int i) {
    int target1 = 0xAAAAAAAA; //A 1010
    int target2 = 0x55555555; //5 0101
    int count1 = 0;
    int count2 = 0;
    for (int j = 0; j < 32; j++)
    {
        //if not the same for that bit, just increment count, 
        //till all 32 bits been compared same. such an elegent solution.
        if (((target1 >> j) & 0x1) != ((i >> j) & 0x1))
            count1++;
        if (((target2 >> j) & 0x1) != ((i >> j) & 0x1))
            count2++; 
    }

    return (count1 < count2) ? count1 : count2;
}

void NextSparseNumber(int n) {
    //A number is Sparse if there are NO two adjacent 1s in its binary representation.
    //Given a number x, find the smallest Sparse number which greater than or equal to x.
    for (int i = 0; i < 32; i++) { 
        if ( n & (0x1 << i) 
             && 
             n & (0x1 << (i + 1))
            ) {//from right to  left.         
            n += (1 << i);
            n = (n >> i) << i; // zero(verb) all lower bits(incl the i'th)
        }
    }
}

int main()
{
    int k = 345439;
    populateLookUpTable();
    cout << bitVec[k & 255] + 
            bitVec[(k >> 8) & 255] + 
            bitVec[(k >> 16) & 255] + 
            bitVec[(k >> 24) & 255] << endl;

    DecimalToBinary(k);

    cout << MinFlipAlternative(k) << endl;    

    NextSparseNumber(44);

    return 0;
}

