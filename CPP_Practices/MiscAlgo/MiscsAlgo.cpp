// FindPrimeNumber.cpp : Defines the entry point for the console application.

#include "getExecutionTime.h"

vector<int> FindAllPrimeUpTo(int Upto)
{
    vector<int> res{2,3};

    for (int i = 4; i <= Upto; i++)
    {
        bool isPrime = true;

        auto firstLarger = lower_bound(res.begin(), res.end(), (int)sqrt(i)+1);
        
        vector<int>::iterator it = res.begin();
        for (; it != firstLarger; )
        {
            if (i % (*it) == 0) 
            {
                isPrime = false;
                it = firstLarger;
            }
            else
                it++;
        }
        if(isPrime)
            res.push_back(i);
    }

    return res;
}

int gcd(int a, int b)
{
    if (a == 0)
        return b;

    return gcd(b % a, a); //magic!!quite intuitive. 9 and 30.
}



void find14distinctChars(const string& s)
{
    //std::array<char, 26> stdarr26{0,0,0....}
    vector<int> vchar26(26, 0);
    int count = 0;
    int count2 = 0;
    for (auto c : s)
    {   
        count2++;
        if (vchar26[c - 'a'] == 0)
        {
            vchar26[c - 'a'] = 1;
            count++;
            if (count == 14)
            {
                cout << count2 << endl;
                break;
            }
        }
    }
}

int power(unsigned long long x, unsigned long long y)
{
    int res = 1;

    while (y > 0)
    {
        if (y & 1) //check if it is odd number.
            res = res * x;
        y = y >> 1;
        x = x * x; //nice .. X8 = (X2)4
    }
    return res;
}

// the champagne glass pyramid 
// 
vector<vector<double>> vv;
void FillWater(int i, int j, double water)
{
    if (water + vv[i][j] <= 1.0)
    {
        vv[i][j] += water;
        return;
    }

    if (water + vv[i][j] > 1.0)
    {
        water = water + vv[i][j] - 1.0;
        water /= 2.0;
        vv[i][j] = 1.0;

        if (vv.size() == i + 1) //expand one more row
            vv.push_back(vector<double>(i + 1 + 1, 0));

        FillWater(i + 1, j, water);// to left,, i -->row number??
        FillWater(i + 1, j + 1, water); //to right, j ->column number
    }
}

unordered_set<string> PermHashT;
void permute(string str, string out)
{
    if (str.size() == 0)
    {
        PermHashT.insert(out); //this will guarantee uniqueness, will not insert duplicate ones.
        return;
    }

    for (size_t i = 0; i < str.size(); i++)
    {
        permute(str.substr(1), out + str[0]);
        rotate(str.begin(), str.begin() + 1, str.end());  
        //0122 -> 1220, abcd, if right now str=cd, when c is done. we rotate to dc
        //the key is i is not even used to refer to any char, it is just to make sure run i times.
        //next iteration it still refer to first of the str, which now become d. that make sense!
    }
}


int findDuplicate2(vector<int>& v)
{
    set<int> settmp;
    pair<set<int>::iterator, bool> ret1;
    //so total will be O(N*ln(N)), bad
    for (vit it = v.begin(); it != v.end(); it++)
    {
        ret1 = settmp.insert(*it); 
        //O(ln(N)),actually this map/set insert can be used as a sorting method, only cost is extra space and not duplicate.
        // Shall just use unordered_map, if cannot find.. just insert, if can find that is it !
        if (ret1.second == false)
            return *it;
    }
    return 0;
    //findDuplicate3, simply sort ,and travese, and compare neighbore if got duplicate.
}
unordered_map<string,string> dyn;
bool isPl(const string& s) {  
    int len = s.length();
  
    if (len == 0)
        return 0;

    for (int i = 0; i < s.length() / 2; i++) {
        if (s[i] != s[len - 1 - i])
            return false;
    }

    return true;
}

string maxPl(string &s)
{
    int len = s.length();

    if (dyn.find(s) != dyn.end())
        return dyn[s];

    if (isPl(s)) {
        dyn[s] = s;
        return s;
    }

    string L = maxPl(s.substr(1));
    string R = maxPl(s.substr(0,len-1));

    string result = L.length() >= R.length() ? L : R;
    dyn[s] = result;
    return result;
}

void generateIP(string &str)
{
    for (auto& a : str)
        if (!isdigit(a))
            return;

    string outputString = str;
    for (size_t i = 0; i < str.length() - 3; i++)
    {
        for (size_t j = i + 1; j < str.length() - 2; j++)
        {
            for (size_t k = j + 1; k < str.length() - 1; k++)
            {
                outputString.insert(i + 1, 1, '.');
                outputString.insert(j + 1 + 1, 1, '.');
                outputString.insert(k + 1 + 1 + 1, 1, '.');
                cout << outputString << endl;
                outputString = str;
            }
        }
    }
}

/*1. The amount of petrol that every petrol pump has.
2. Distance from that petrol pump to the next petrol pump.
Your task is to complete the function tour which returns an integer 
denoting the first point from where a truck will be able to complete the circle
*/
vector<int> v3(100, -1);
vit CircularTour(void)
{    //this is really the 1st positive number, NO more Positve between v3.begin() to this Iterator.
    auto firstPositive = std::find_if(v3.begin(), v3.end(), [](int &a) -> bool { return a >= 0; });
    int sum = 0;
    for (auto it = firstPositive; ; it++)
    {
        if (it == v3.end())
        {    //there is NO more fresh positive item from the begin of the v to the firstPositive.
            //so once found total sum is neg, that is the end...
            for (auto it = v3.begin(); it != firstPositive; it++)
            {
                sum += *it;
            }
            if (sum >= 0)
            {
                return firstPositive;
            }
            return v3.end();
        }

        sum += *it;

        if (sum < 0)
        {
            firstPositive = std::find_if(it, v3.end(), [](int &a) -> bool { return a >= 0; });
            if (firstPositive == v3.end())
            {
                return v3.end();
            }
            else
            {    //reset to the next positive number.
                it = firstPositive;
                sum = *it;
            }
        }
    }
    return v3.end();
}

bool BracketMatchs(char &a, char &b)
{
    switch (a)
    {
    case '{':
        return b == '}' ? true : false;
    case '[':
        return b == ']' ? true : false;
    case '<':
        return b == '>' ? true : false;
    case '(':
        return b == ')' ? true : false;
    default:
        return false;
    }
    return false;
}

//a simple application, check if bracket matches, () {}  [] <>
void checkBrackets(void)
{
    string MainTestProgram = "mainfunc1(){ { if(abc(edf[adfg-x]))hel}loW vit it=a.begin(); newarry[5]];}";
    string openning = "{[(<";
    string closing = "}])>";
    vector<char> brackets;
    for (string::iterator it = MainTestProgram.begin(); it != MainTestProgram.end(); it++)
    {
        if (string::npos != openning.find(*it))
            brackets.push_back(*it);
        if (string::npos != closing.find(*it))
        {
            if (BracketMatchs(*(brackets.rbegin()), *it))
            {
                brackets.pop_back();
            }
            else
            {
                cout << "bracket NOT matching" << endl;
                return;
            }
        }
    }

    if (brackets.size() == 0)
    {
        cout << "bracket matching" << endl;
    }
}

int main()
{
    FindAllPrimeUpTo(46);

    cout << "greatest common divider of (10,25) is:" << gcd(10, 25) << endl;
    cout << "greatest common divider of (21,22) is:" << gcd(21, 22) << "they are also known as co-prime" << endl;
    cout << "(9*7)%5 = ((9%5) *(7%5))%5" << endl;

    //Input:  x = 2, y = 3, p = 5 ; Output: 3 ; Explanation : 2 ^ 3 % 5 = 8 % 5 = 3.
    cout << "power (3,5):" << power(2,4) << endl;

    vv.push_back(vector<double>(0 + 1, 0));
    FillWater(0, 0, 12);

    vector<int> v1(100);
    int i = 1;
    for (vit it = v1.begin(); it != v1.end(); it++)
    {
        *it = i;
        i++;
    }
    v1.push_back(55);
    //random_shuffle(v1.begin(), v1.end());

    cout << findDuplicate2(v1) << endl;
    
    generate_vector(v3, true, 10);
    auto it = CircularTour();
    if (it != v3.end())
    {
        cout << "find at position:" << std::distance(v3.begin(), it) << " with value of:" << *it << endl;
    }

    string s0 = generateString(6);
    permute(s0, "");
    permute2(s0);

    {
        //string s1 = generateString(1000, 26);
        string s1 = "aaaa";
        cGetTime t;
        cout<<maxPl(s1)<<endl;
    }

    string s3 = "112113"; //"12347789";
    generateIP(s3);

    checkBrackets();
    return 0;
}