#include "STL.h"
int main(int argc, char *argv[])
{
	const char* cs1 = "this string is going to be longer than 32 chars, if not believe, count urself.";
	std::string sx("this string is going to be longer than 32 chars, if not believe, count urself.");
	return 0;

    //std::string sx("sx");
    //string sxplus = sx + string("abcde");
    ////const char* csx = (sx + string("abcde")).c_str();
    //const char* csx = sxplus.c_str();
    //vector<int> vi(100);
    //vi.push_back(10);//vi.size=101


	cout << "\n+++++++++++++++Construction and access elements++++++++++++++++++++++" << endl;
	//string is templated class with underlying type of char, basic_string<T>
	//string s0 = "Cherno" + "hello"; compilation error, bcos Const Char* do NOT have overloaded function + defined.
	//cast to string with "Cherno"s; or use string literal. s0="Cherno"s+ "Hello"; using namespace std::string_literal.

	string s1("Goodbye");															 //so stick with this one. got many variaties of overloaded funcs
	cout << "s1.size()" << s1.size() << "\t s1.capacity()" << s1.capacity() << endl; //.size() exactly same as; cout<<"s1.length()"<<s1.length()<<endl;
	s1.reserve(100);
	s1.resize(9); //Goodbye\0\0
	s1.resize(3);

	//single element access (similar with vector) at(2), check boundary condition, throw exception
	cout << "s1[0]:" << s1[0] << "\t sizeof(s1[0])" << sizeof(s1[0]) << "\t s1[2]:" << s1[2] << "\t s1.at(2):" << s1.at(2) << endl;
	cout << "*(s1.begin():" << *(s1.begin()) << endl;

	cout << "\n***************ranged Access, Assign, append, insert, replace.,*********************" << endl;
	string s2 = "Clash Of Clan";
	s1.assign(s2, 2, 4); //s1.assign(s2,0,s2.size()); 	 //always stick with three parameter calls.
	s1.append(" over");  //can append a c-type string also.
	s1 = s1 + " already";
	s1.insert(5, "es");
	s1.replace(2, 5, "abcdefg", 3, 4); //"defg" replace "ashes" ... replace sub string..
	s1.erase(2, 4);					   //"defg".
	s1.swap(s2);

	cout << "\n***************memeber funcs, copy, find, compare,*********************" << endl;
	s1 = "game F of throne Fxf";
	s2 = "games F of throne Fxf";
	string s3 = "   games F of throne Fxf  ";
	bool isFound = s3.find("throne") != string::npos;				   //below 0 is starting pos, is default value.
	cout << "s1.find('throne',0) at:" << s1.find("throne", 0) << endl; //s1.find('h',0) == s1.find("h",0);  if (s1.find("throne", 0) != string::npos)
	cout << "s1.find_first_of('yfFzx',0) at:" << s1.find_first_of("yfFzx", 0) << endl;
    cout << "s1.find_first_of('yfFzx',10) at:" << s1.find_first_of("yfFzx", 10) << endl; 
	cout << "s1.find_last_of('yfFzx') at:" << s1.find_last_of("yfFzx") << endl;
	int fnd1 = s3.find_first_not_of(" ");
	int fnd2 = s3.find_last_not_of(" ");

	cout << "s3 (after trimmed):" << s3.substr(fnd1, fnd2 - fnd1 + 1) << endl;
	cout << "s1.compare(1,2,s2):" << s1.compare(1, 2, "am"s) << endl;
	cout << "count(s1.begin(),s1.end(),'e'):" << count(s1.begin(), s1.end(), 'e') << endl;
	string::iterator s_itr = s1.begin();
	s1.erase(s_itr, s_itr + 5);
	replace(s1.begin(), s1.end(), 'o', 'O'); //replacing char.

	cout << "\n******************long string vs short string******************" << endl;
	vector<string> v{"a is a long string of charactors",
					 "b is a long string of charactors"};
	// initializer_list is implemented by creating a hidden array for you, so the approximate equivalene is
	//const string data[] {"a is a long string of charactors","b is a long string of charactors" }; //heap alloc 1/2
	// vector<string> v {initializer_list<string>{data, data+1}; //vector allo 3, copy of str1,2 to the vector alloc4/5
	// so five dynamic memory allocation for this simple code.

	vector<string> v1{"a", "b"}; //with "Small String optimization", only the vector need one dyanmic allocation. nice!

    string string1 = "haiTong";
    string string2 = "HAITONG";

    if ((string1.size() == string2.size()) && std::equal(string1.begin(), string1.end(), string2.begin(), [](char & c1, char & c2) {return (c1 == c2 || std::toupper(c1) == std::toupper(c2)); })) {
        cout << string1 << " is equal (case-insensitive) to " << string2 << endl;
    }
	
	return 1;
}