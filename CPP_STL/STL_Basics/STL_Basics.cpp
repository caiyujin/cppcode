/* Sequence Constainer,
Vector is like the swiss army knife of C++ STL containers. In the words of Bjarne Stroutsoup By default, use Vector when you need a container. 
1.vector,  modification=insert/removal
	dynamic one-dimetional array grow (by factor of 2 or 1.5...eg, 4->6->9....) in one direction. *it is the Only STL contiguous memory....very important,
	even though deque also can access with index, but deque is not Contiguous.
	1. fast insert/remove at the end  O(1); slow insert/remove at beginning or in the middle  O(N), 
	   since need to shift all elements by 1 positions, N (size of the container) is worst case, try avoid this.
	
	2. search: O(N) // boosted by cache hits.

	3. take out element: use MEMBER ERASE function.

	deque: can grow at two directions; so (vs Vectors) can FAST insert/remove at the begin/end.
	
	and deque reallocation/resizing linearly since it does NOT use contigous memory.

2.list,

good1: double sides linked list, each node point to front and end. so can fast insert/remove for EVERY element. 
		(Dont forget u need to traverse to target then can remove, which could be quite costly
		
bad1:  can NOT do random access, ie NO [] operator (vs Vector/deque).

bad2:  the searching requires still O(N). but MUCH MUCH slower than vector/deque, (chasing the pointers, cache miss)
		take out element: use MEMBER REMOVE function. pointer NOT invalided

Associative Container (as balanced (red and black) "binary tree") all the elements SORTED, 
when adding/removing item,the tree need to rebalanced(O(log(n)) and make it sorted again. (Costly).
so push_back/front does NOT make sense anymore.

3.map (multiple map: duplicate items allowed.)
sometimes user do NOT want to sort the VALUE of element directly, so "key" come into the picture; 
so the "set" is basically a map with no "value".

1. modify/search at O(log(N)); (costly traverse, chasing pointers again).

2. No random access,

take out element: use MEMBER REMOVE function. pointer NOT invalided	

4.unordered map
	implemented as "hash tables" ; or a bunch of buckets(lists).. each bucket may have 0 -N elements. 
	whenever access/modify the unordered set, it will go through a hash function to calculate which bucket to go, 
	then traverse every elements in the bucket.so the search time could be O(1) if each bucket hold one element,

	hash-collision: many elements in same bucket, in extreme case. when all the elements are in one bucket. it essentially become A linked list... 
	the search efficiency will be O(N) (to linear time)... very bad
*/
#include "STL.h"
#include "string_overflow.h"

#define rubbish
int main(int argc, char *argv[])
{
    const char *argv2[] = { "string_Basic.exe", "Jacob", "0123456789abcdefJacob" };
    testPassword(argv2);

	VEC<int> V1(1, 2);
	try
	{
		cout << V1[3] << endl;
	}
	catch (out_of_range e1)
	{
		cout << e1.what() << "it is out of range exception, let's continue run the program" << endl;
	}

	vector<int> v2;

	VecCapGrowAndReallocation();

	InitVecPriorC11(v2);

	VectorErase();

	v2.insert(v2.begin(), 2);

	cout << "v2[2]is :" << v2[2] << "\t v2.at(2) is :" << v2.at(2) << "\t v2.size() :" << v2.size() << "\t v2.capacity() :" << v2.capacity() << endl;
	//[]no range check; .at() got range check, and throw exception if necessary, instead of [] will raise SIGSEGM and normally exit program.

	vit f_itr;
	vector<int>::reverse_iterator r_itr;
	for (r_itr = v2.rbegin(); r_itr != v2.rend(); r_itr++)
	{
		//v2.rbegin() is the "element" before v2.end(); //v2.rend() is the "element" before v2.begin(),
		//very trick to use converter (forward) itr = r_itr.base(), base() will shifted by one towards rbegin..
		if (*r_itr == 5)
			f_itr = r_itr.base() - 1;
	}

	VectorAsSet();

	VectorAsQueue();

	cout << "\n++++++++++++++++++++++++++++LIST+++++++++++++++++++" << endl;
	list<double> l1{12.2, 10.2, 11.3, 14.2, 11.4, 11.3, 11, 11.3, 11.0, 9.0, 5.1, 6.9, 8.99, 34.4};
	list<double>::iterator it1 = find(l1.begin(), l1.end(), 10.2); //m_it
	
	list<double> l2;
    //lsit<double>l2 //without space also valid, bcos compiler know it is templated class!
	l2.splice(l2.begin(), l1, it1, l1.end()); //splice actually "Moved!", l1 shrinks, this is killer feature of list.
	l2.insert(it1, 15.3);					  //insert before
	l2.remove(11.3);
	//sort(l2.begin(), l2.end());//simply NOT works becauses list iterator is NOT random access iterator
	//l2.sort(); //this one works.

	//member "remove", tweak the two pointers of target node. benefit of knowing the data structor of list!!
    //in general use container's own function. it is optimized for that container.

	// Algo remove(l1.begin(), l1.end(),11.3), the find action also O(N), it behave same as Algo remove for vector
	// ALGO copy subsequenct by one position, leave last item duplicate; return iterator pointing to newlist.end(), so need to erase() it to complete.

	list<double>::iterator it2 = find(l2.begin(), l2.end(), 34.4);
	l2.remove(8.99); //NO pointer invalidation. for it2
	l2.remove(8.7);  //if element no exist, nothing happen
    //the list does not even have a call to capacity (the capcity is unlimited (=system main memory), so there is no ShrinkToFit either

	cout << "\n++++++++++++++++++++++++++++MAP+++++++++++++++++++" << endl;
	map<char, int> m2{ {'a', 10}, {'b', 11}, {'c', 12}, {'z', 36} };
	map<char, int>::iterator map_itr1 = m2.find('c');
    if (map_itr1 != m2.end())
        cout << (*map_itr1).first << " " << (*map_itr1).second << endl;
	// O(log(N)); VS O(N) if we use algo func find(m2.begin(), m2.end(), make_pair('c',12));
	// algo func suffered double penalties by checking the "value" (12); container with unique KEY, ie KEY matchs, no need to check VALUE anymore.

	MapPrintEntry(m2);
	m2['x'] = 24; //if this pair not exist yet, it will be inserted. if already exist, then just updated the value of the pair, so the operator[] is NOT on CONST object!!!

	pair<map<char, int>::iterator, bool> ret1;
	ret1 = m2.insert(make_pair('y', 35)); //success ret1.second == true, make_pair is templated function, the variable's type is deduced.

	map<char, int>::iterator it3 = ret1.first;
	m2.insert(it3, pair<char, int>('z', 13)); //it3 is hint, the opration can be O(1) if with good hint. but we most likely still need rebalancing (very costly)...

	//if iterator(pointer point to 'y;), after removed, invalidation happened! if pointed to z or x, Will NOT invalidated
	map<char, int>::iterator it4 = m2.find('y');
	if (it4 != m2.end())
		m2.erase(it4);

	m2.clear();

	NestedMap();

	MapSubscribing();

	cout << "\n+++++++++++++++++++++++unordered_map+++++++++++++++++++" << endl;
	unordered_map<string, string> UM1; // = { make_pair("abc", "def"), make_pair("def", "hij"), make_pair("klm", "nop"), make_pair("ab", "cd"), make_pair("ef", "hi"), make_pair("jk", "lm"), make_pair("no", "pq") };
    UM1["abc"] = "def";
    UM1["def"] = "hij";
	unordered_map<string, string>::iterator it5 = UM1.begin();
	it5 = UM1.find("jk");
	if (it5 != UM1.end())
		cout << (*it5).first << " " << (*it5).second << endl;
	UM1.insert(it5, make_pair("jk1", "lm1")); //hint,
	UM1["ef"] = "hi1";
	PropertiesOfUnorderedMap(UM1);
	UM1["abcd"] = "def";

	cout << "\n+++++++++++++++++++++++bitset+++++++++++++++++++" << endl;
	bitset<9> bs1{"1100011"}; //appended two 0 at back if rvalue is less than defined (9) length
	bs1.set(2);				  //set pos 2 to default ('1')
	bs1.set(1, 0);			  //set pos 1 to '0'
	bs1.reset(0);			  //reset pos 0 to '0'
	bs1 >>= 2;
	bitset<9> bs5(100); //use an int to construct a bitset
	bs1 &= bs5;
	bs5.set(); //set all bits in bs5
	if (bs1.any())
		cout << "Still got some bit set as 1" << endl;

	if (bs1.none())
		cout << "all are set as 0" << endl;

	bitset<9> bs2 = ~bs1;
	bitset<9> bs3 = bs2 << 2;
	int i_10 = 9998;
	bitset<8 * sizeof(int)> bs4 = i_10;
	cout << bs4.to_string() << endl;

	return 1;
}