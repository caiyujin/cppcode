#pragma once
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>


#define BUFFERSIZE 10
//to use strcpy,,_CRT_SECURE_NO_WARNINGS in the predefinition
//the two below are basically string next to each other, probably by some padding byte.
//when the copy argv[2] is long, it will write the password... and change the value.
char buffer[BUFFERSIZE] = "message";
char password[BUFFERSIZE] = "passw0rd";

void testPassword(const char *argv2[])
{
    strcpy(buffer, argv2[2]);
    if (strcmp(argv2[1], password) == 0)
    {
        std::cout << "password match" << std::endl;
    }
    else
    {
        std::cout << "password wrong" << std::endl;
    }
}