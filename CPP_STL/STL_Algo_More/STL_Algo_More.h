#pragma once
#include "STL.h"

uint32_t hash_char_func(uint32_t hash, char c)
{
	hash += static_cast<int>(c) * 2;
	return hash;
}

struct HashString
{
	void operator()(const string &s)
	{
        haVal = accumulate(s.begin(), s.end(), haVal, hash_char_func);
	}
	uint32_t haVal = 0;
};

template <typename C>
uint32_t hash_all_strings(const C &c)
{
	const auto hasher1 = for_each(c.begin(), c.end(), HashString());
	//one instance of type HashString is created, and used as functor (with state of hash)  here to apply to each of the string in the vector,
    //for each of the string in vector, it will go through hash = accumulate(.. and accumulate the value based on each char of the strings (hash_char function pointer)
	return hasher1.haVal;
}

struct Person
{
	string first;
	string middle;
	string last;
};
//with precedence last > first > middle
struct Employee : public Person
{
	enum class EmployeeType
	{
		Executive,
		Manager,
		Architect,
		Senior,
		Junior,
		Management = Manager,
		Individual = Architect
	};
	EmployeeType type;
};
