// STL_Algo_More.cpp : Defines the entry point for the console application.

#include "STL_Algo_More.h"

int main()
{
	cout << "\n++++++++STL algorithm user-defined ones++++++++++++" << endl;
	vector<string> v1{"one", "two", "three", "four", "five"};
	uint32_t hash = hash_all_strings(v1);
	cout << "Hash: " << hash << dec << endl;

	vector<Person> v{
		{"Joe", "P", "Smith"},
		{"Jane", "Q", "Jones"},
		{"Frank", "P", "Johnson"},
		{"Sarah", "B", "Smith"},
		{"Joe", "X", "Jones"},
		{"Mike", "M", "Gay"},
		{"Mike", "L", "Fish"},
		{"Joe", "A", "Smith"}};

	//sort and stable_sort
	sort(v.begin(), v.end(), [](const Person &a, const Person &b) { return a.middle < b.middle; });
	stable_sort(v.begin(), v.end(), [](const Person &a, const Person &b) { return a.first < b.first; });
	stable_sort(v.begin(), v.end(), [](const Person &a, const Person &b) { return a.last < b.last; });

	//partial_sort
	vector<int> v2{42, 17, 89, 22, 34, 78, 63, 12, 57, 99, 48, 98, 20};
    partial_sort(v2.begin(), v2.begin() + 5, v2.end()); //from v2b.begin() to vb2.end(), find the 5 least number, and keep the rest in order. default is less than.

	//partition
	vector<int> v3{12, 89, 31, 18, 7, 72, 69, 50, 49, 50, 51, 49, 7, 28, 25, 8, 4};
    vector<int> v3b(v3);
    vector<int> v3c(v3);
	partition(v3.begin(), v3.end(), [](const int &a) { return a < 50; });// 4 will be right after 12, 
    stable_partition(v3b.begin(), v3b.end(), [](const int &a) { return a < 50; });//4 will be last item in the first half of partition, order is preserved!

    //inner product
    vector<int> v9{ 2,1,0,2,2,3,4,5,6,7,8 };
    int sum = inner_product(v9.begin(), v9.begin() + 3, v9.end() - 3, 10); //10+ 2X6 + 1*7 +0x8;

     sum = inner_product(v9.begin(), v9.begin() + 3, v9.end() - 3, 10, multiplies<int>(), plus<int>()); //10 *( 2+6) *(1+7) * (0+8);

	//random_shuffle(v3.begin(), v3.end());
	vector<int> smaller;
	vector<int> bigger;
	partition_copy(v3.begin(), v3.end(), back_inserter(smaller), back_inserter(bigger), [](const int &a) { return a < 50; });
	/*
template<class _InIt,
class _OutIt1,
class _OutIt2,
class _Pr> inline
pair<_OutIt1, _OutIt2>
partition_copy(_InIt _First, _InIt _Last,
_OutIt1 _Dest_true, _OutIt2 _Dest_false, _Pr _Pred)
{	// copy true partition to _Dest_true, false to _Dest_false
*/

	vector<int> v4 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	vector<int> v5(v4);
	vit it = v4.begin() + 2;
	vit it2 = v4.begin() + 8;
	rotate(it, it + 2, it2);

	return 0;
}
