
#include "STL_Perf.h"

//30 Jan 2019.. now i know why my remove is not implemented as standard.
//my remove will change the order of remaining items! no good... 
//and there are circumstance that users really dont care about the order.
template <class _ForwardIT, class _Ty>
_ForwardIT remove_cai(_ForwardIT _First, _ForwardIT _Last, const _Ty &_Val)
{
	return (_First, remove_cai1(_First, _Last, _Val));
	//return (_Rechecked(_First, remove_cai1(_Unchecked(_First), _Unchecked(_Last), _Val))); //this _Unchecked will save time!!! failed compile at linux
}

template <class _ForwardIT, class _Ty>
_ForwardIT remove_cai1(_ForwardIT _First, _ForwardIT _Last, const _Ty &_Val)
{
	_ForwardIT _Next = _Last;

	for (; _First != _Last; ++_First)
	{
		if (*_First == _Val)
		{
			for (--_Next; _Next != _First; --_Next)
			{
				if (*_Next != _Val)
				{
					*_First = *_Next;
					_Last = _Next;
					break;
				}
			}
			if (_Next == _First)
			{
				_Last = _Next;
			}
		}
	}
	return _Last;
}

void ReadBackwards(void)
{
	vector<double> v;
	for (int i = 1; i < 1000000; i++) // one million of points. 30usec vs 2170usec; optimized(release build, max speed) 10usec vs 70usec
		v.push_back(RndEng() / 1000.0);

	{
		cGetTime t;
		double n1 = accumulate(v.begin(), v.end(), 0.0);
	}

	vector<double> v2;
	for (int i = 1; i < 1000000; i++) // one million of points. 30usec vs 2170usec; optimized(release build, max speed) 10usec vs 70usec
		v2.push_back(RndEng() / 1000.0);
	//random_shuffle(v.begin(), v.end());

	{
		cGetTime t;
		double n2 = accumulate(v2.rbegin(), v2.rend(), 0.0);
	}
}

void twoDArr(void)
{
	//allocate on STack int A2D[1000][500] will throw exceptions;
	vector<vector<double>> A2D(500, vector<double>(5000));
	for (int i = 0; i < 500; i++)
	{
		for (int j = 0; j < 5000; j++)
		{
			A2D[i][j] = RndEng() / 10000.0;
		}
	}

	double sum = 0;
	{
		cGetTime t;
		for (int i = 0; i < 500; i++)
		{
			for (int j = 0; j < 5000; j++)
			{
				sum += A2D[i][j];
			}
		}
	}

	sum = 0;
	{
		cGetTime t;
		for (int j = 0; j < 5000; j++)
		{
			for (int i = 0; i < 500; i++)
			{
				sum += A2D[i][j];
			}
		}
	}
}

void vectorErasePerfCompare(void)
{
	vector<int> mvec1;
    for (int i = 0; i < 1000; i++)
	{
		mvec1.push_back(RndEng() % 10); //1% is 1 , about 100 '1','2' each
	}
    vector<int> mvec2(mvec1);
    vector<int> mvec3(mvec1);

	{
		cGetTime t;
		int i = 1;
		vit it1 = remove(mvec1.begin(), mvec1.end(), i);
		//even though also have shifted, it ONLY SHIFT ONCE only (if remove multiple same elements, so very efficient), and it1 will point to the by-right .end()
		mvec1.erase(it1, mvec1.end());
	}

	{ //under such circumastance, when need to remove different values each time. prefer remove_cai.... 41mil vs 4mil
		//for now the unchecked not able to pass compilation..since it boost up the perf, so right now the remove_cai lose.
		cGetTime t;
		int i = 1;
		vit it2 = remove_cai(mvec2.begin(), mvec2.end(), i);
		mvec2.erase(it2, mvec2.end());
	}

	{ // if got multiple erase (M number of 1, vev_a size is N), the complexity is O(M*N); that is shifting forward complexity. which is most compilcated in the entire operations
		cGetTime t;
		for (vit it = mvec3.begin(); it != mvec3.end();)
		{
			if (*it == 1)
			{
				it = mvec3.erase(it);
			}
			else
			{
				it++;
			}
		}
	}
}

void FillVector(vector<BigTestStruct> &testVector)
{
	for (int i = 0; i < 10000; i++)
	{
		testVector.emplace_back(); //calling default constructor to construct an object on the stack.
	}
}

void TestFillVector(void)
{
	{
		cGetTime t;
        vector<BigTestStruct> t1;
		FillVector(t1);
	}
	{
		cGetTime t;
        vector<BigTestStruct> t1;
        t1.reserve(10000); //only 1/4 of the time without Reserve.
		FillVector(t1);
	}
}

void AssignmentVsInsertVsPushBack(void)
{
	vector<BigTestStruct> sourceVector, destinationVector, destinationVector2, destinationVector3;
	FillVector(sourceVector);
	{
		cGetTime t;
		destinationVector = sourceVector;
	}
	{
		cGetTime t;
		destinationVector2.insert(destinationVector2.end(), sourceVector.begin(), sourceVector.end());
	}

	{
		cGetTime t;
		//destinationVector3.reserve(10000); can save up to half the time with this line. after save,
		for (auto &e : sourceVector)
			destinationVector3.push_back(e);
	}
	//when access vector data,
	//1. using an iterator
	//2. using std::vector::at()   --this is
	//3. using subscripting -[] notation.
}

int main()
{
	//ReadBackwards();

	//twoDArr();

	vectorErasePerfCompare();

	TestFillVector();

	AssignmentVsInsertVsPushBack();
	return 1;
}