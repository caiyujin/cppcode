#include "getExecutionTime.h"
const size_t target = 10;
vector<pair<int,int> > Sack;

using SackIt = vector<pair<int, int> >::iterator;
int currmax = INT_MIN;

void recurisve(vector<SackIt>& curr, int wRem, int sum) 
{
    //SackIt b, pass in as argument
    //for (SackIt it = b ; it != Sack.end(); it++)  //only allowed to have unique items
    for (SackIt it = Sack.begin(); it != Sack.end(); it++)//allowed duplicate items
    {
        if ((*it).first <= wRem)
        {
            curr.push_back(it);
            recurisve(curr, wRem - (*it).first, sum + it->second); //it + 1, 
            curr.pop_back();
        }
        else
        {
            if (sum > currmax)
            {
                currmax = sum;
            }
            break;
        }
    }
}

unordered_map<int, int> cached;

int recurisve_Dyn(vector<SackIt>& curr, int wRem) 
{
    if (cached.find(wRem) != cached.end())
    {
        return cached[wRem];
    }

    int maxForWRem = 0;

    for (SackIt it = Sack.begin(); it != Sack.end(); it++)
    {
        if (it->first <= wRem)
        {
            curr.push_back(it);
            int temp = recurisve_Dyn(curr, wRem - (*it).first);
            if (temp + it->second > maxForWRem)
            {
                maxForWRem = temp + it->second;
            }
            curr.pop_back();
        }
        else
        {
            break;
        }
    }

    cached[wRem] = maxForWRem;
    return maxForWRem;
}


int main(){
    
    Sack.reserve(10);
    for (int i = 0; i < 10; i++)
    {
        Sack.emplace_back(i+2, RndEng() % 4 + 1);
    }

    {
        cGetTime t1;
        vector<SackIt> curr;
        int sum = 0;

        recurisve(curr, target, sum);
        cout << "result is:" << currmax << endl;
    }

    {
        cGetTime t1;
        vector<SackIt> curr;
        int sum = 0;

        recurisve_Dyn(curr, target);
        
        int currmax2 = INT_MIN;
        for (auto& p : cached)
        {
            p.second>currmax2?currmax2=p.second:currmax2;
        }
        cout << "result is:" << currmax2 << endl;
    }

    return 0;
}

