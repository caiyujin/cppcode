#include "getExecutionTime.h"
map<vit,int> cached;
map<vit, int> cached2;
vit s;
vit e;

int solution(vit it2) 
{
    if (cached.find(it2) != cached.end())
       return cached[it2];

    if (it2 - s == 0)
        return 0;    
    
    int mvSvn = INT_MAX;

    auto it = lower_bound(s, it2, *(it2 - 1) -6);

    //dont need to care if there is the day (the guy go travel) on the -7th day count from backwards.
   
    if (it != it2)
        mvSvn = solution(it) + 7; //7 days pass
    
    int mvOne = solution(it2 - 1) + 2; //1 day pass 

    int min = min(mvSvn, mvOne);
    
    if (min >= 25)
        min =25;

    cached[it2] = min;

    return min;
}

int solution2(vit it1)
{
    if (cached2.find(it1) != cached2.end())
        return cached2[it1];

    if (e- it1 == 0)
        return 0;

    int mvSvn = INT_MAX;

    auto it = lower_bound(it1, e, *it1 + 7);

    //dont need to care if there is the day (the guy go travel) on the -7th day count from backwards.

    if (it != e)
        mvSvn = solution2(it) + 7; //7 days pass

    int mvOne = solution2(it1 +1 ) + 2; //1 day pass 

    int min = min(mvSvn, mvOne);

    if (min >= 25)
        min = 25;

    cached2[it1] = min;

    return min;
}


int main() {

    int res = 0;
	vector<int> v = { 1,2,4,5,7,15,17,20,21,22,23,25,30 };
    s = v.begin();
    e = v.end();

    {
        cGetTime t1;

        if (v.size() >= 25) 
        {
            res = 25;
        }
        else 
        {
            res = solution( v.end());
        }

    }
	cout << res << endl;



    {
        cGetTime t1;

        if (v.size() >= 25)
        {
            res = 25;
        }
        else
        {
            res = solution2(v.begin());
        }

    }
    cout << res << endl;


    return 0;
}
