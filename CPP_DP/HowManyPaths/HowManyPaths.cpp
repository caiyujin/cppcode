// paths.cpp : 
// 09 feb 2019, rework, a fixed matrix, move only to right or down, find how many diff paths from top left to bottom right
// the 0 is a block, can not pass.for simplicity make sure these two points always with value of 1.
// for example below will give answer of 9

#include "getExecutionTime.h"
vector<vector<int>> matrix = {
    {1, 1, 1, 1, 1},
    {1, 0, 1, 0, 1},
    {1, 1, 1, 1, 1}, //,
    {0, 1, 1, 1, 0},
    {1, 1, 1, 1, 1} //,
                    //{0,1,1,1,1}
};

int _Y = matrix.size();
int _X = matrix[0].size();

//this can be Dynamical Programming, because from any point(cell), the paths to the end 
//is the same.
//and it will be used again and again, if the points' previous point is different!

vector<vector<int>> cache(_Y, vector<int>(_X, -1));

int findHowManyPath(int y, int x)
{
    if (y == _Y - 1 && x == _X - 1)
        return 1;

    if (cache[y][x] != -1)
        return cache[y][x];

    if (matrix[y][x] == 0)
        return 0;

    int R = 0;
    if (x < _X - 1)
    {
        R = findHowManyPath(y, x + 1);
    }

    int D = 0;
    if (y < _Y - 1 )
    {
        D = findHowManyPath(y + 1, x);
    }

    cache[y][x] = R + D;
    return cache[y][x];
}

int main()
{
    {
        cGetTime t1;
        cout << findHowManyPath(0, 0) << endl;
    }
    return 0;
}
