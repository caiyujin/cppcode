
#include "getExecutionTime.h"

//longest common subarray (may not be contious sub array!)
vector<vector<int> > Results;
vector<char> resChars;
int LCS(char arr1[], int n, char arr2[], int m){
    int res = 0;

    if (Results[n][m] != -1)
        return Results[n][m];

    if (n == 0 || m == 0) {
        Results[n][m] = 0;
        return 0;
    }

    if (arr1[n - 1] == arr2[m - 1])
    {
        res = 1 + LCS(arr1, n - 1, arr2, m - 1);
        //resChars.push_back(arr1[n - 1]);
    }
    else
    {
        int t1 = LCS(arr1, n - 1, arr2, m );
        int t2 = LCS(arr1, n, arr2, m - 1);
        res = t1 > t2 ? t1 : t2;
    }

    Results[n][m] = res;
    return res;
}

int main(){
    char arr1[] = { 'G', 'T','j', 'B','A','B','A' ,'C','H', 'M', 'L','Z'};
    char arr2[] = { 'G', 'T', 'T', 'X','B','A','B','A', 'Y', 'H','C','B','Z', 'M','L'};
    int n = sizeof(arr1) / sizeof(arr1[0]);
    int m = sizeof(arr2) / sizeof(arr2[0]);
    //cGetTime time1;
    Results = vector<vector<int> >(n + 1, vector<int>(m + 1, -1)); //skip the 0-based.
    cout << "LCS is :" << LCS(arr1, n, arr2, m) << endl;
    return 0;
}

