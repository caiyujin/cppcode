// Strings.cpp : Defines the entry point for the console application.
//
#include "getExecutionTime.h"
vector<int> v = { 3,2,6,4,5,1 };
void LIS(vector<int>& v) 
{
     
    vector<vector<int> > lis(v.size(), vector<int>());
    for (int i = 0; i < lis.size(); i++)
        lis[i].push_back(i);

    for (int i = 0; i < v.size(); i++) {
        for (int j = 0; j < i; j++) {
            if (v[j] < v[i] && lis[j].size() + 1 > lis[i].size())
            {
                lis[i] = lis[j];
                lis[i].push_back(i); //lis[i]  is lis[i1]+1 now
            }
        }
    }
}


int main(){

    {
        cGetTime t;
        LIS(v);
    }

    return 0;
}

