#include "getExecutionTime.h"
constexpr int MIN_BUFFER_SIZE = 100;
vector<int> m_readBuffer;
int* m_start = nullptr;
int* m_readHead = nullptr;
int* m_readTail = nullptr;


std::size_t remainingReadBufferCapacity()
{
    std::size_t remaining = m_readBuffer.empty() ? 0 : (&m_readBuffer.back() - m_readTail + 1);
    return remaining;
}

void initPtrs(){
    m_readBuffer.reserve(MIN_BUFFER_SIZE);
    m_readBuffer.push_back(0);
    m_start = &m_readBuffer[0];
    m_readHead = &m_readBuffer[0];
    m_readTail = &m_readBuffer[0];
    m_readBuffer.pop_back();
}

void testCase1() {
    m_readBuffer.resize(MIN_BUFFER_SIZE*5);
    m_start = &m_readBuffer[0];
    m_readHead = &m_readBuffer[0]+450;
    m_readTail = &m_readBuffer[0]+470;
    int i = 1;
    for (auto it = m_readHead; it != m_readTail; it++)
    {
        *it = 100 + i;
        i++;
    }
}

void printStats(){
    cout<< "header pos:" << m_readHead-m_start <<endl;
    cout<< "tail pos:" << m_readTail-m_start <<endl;    
    cout<< "cap:" << m_readBuffer.capacity() <<endl;      
    cout<< "size:" << m_readBuffer.size() <<endl;
    size_t remaining = remainingReadBufferCapacity() ;
    cout<< "remaining:" << remaining <<endl;

    cout<<endl;
}

void ensureReadBufferCapacity()
{
    printStats();

    if ( remainingReadBufferCapacity() >= MIN_BUFFER_SIZE)
    {
        cout <<"return"<<endl;
        return;
    }

    std::size_t unreadBytes = m_readTail - m_readHead;
    cout<< "unreadBytes:" << unreadBytes <<endl;
    vector<int> resizedBuffer;
    if (m_readBuffer.empty())
    {
        resizedBuffer.resize(MIN_BUFFER_SIZE);
    }
    else 
        if (m_readHead - &m_readBuffer[0] < MIN_BUFFER_SIZE * 4)
        {
            //with beginning remainingBuffer > Min_buffer, it will NOT any how call in below code to double the size.
            //unless when the m_readBuffer is growing much faster than consuming (read start point moving away from begin!)
            if (m_readBuffer.capacity() * 2 >= MIN_BUFFER_SIZE) 
            {
                resizedBuffer.resize(m_readBuffer.capacity() * 2);
            }
            else
            {
                resizedBuffer.resize(MIN_BUFFER_SIZE);
            }
        }
        else
        {
            std::swap(m_readBuffer, resizedBuffer); 
            //here no grow. combined with below memcpy and swap, effectively move the unread section 
            //to begining of the vector, sweet, 
            //CAI YUJING ----- isn't it more easier just do copy on-the-fly then update the head/tail pointer???!
            //it will save the cost of creating another vector ( new inside).
            //do in this else block with a "return";
        }

    if (unreadBytes > 0)
    {
        if (&resizedBuffer[0] != m_readHead)
        {
            ::memcpy(&resizedBuffer[0], m_readHead, unreadBytes*sizeof(int));
            //CAI YUJING, important. for c memcpy, the last parameter is number of byte. 
            //so have to *sizeof(ptr), which probably use 8byte in 64bits binary!
            //for the channel code, it is using vector<char>, char is one byte...
        }
    }

    std::swap(m_readBuffer, resizedBuffer); //the main buffer begining addr is changed. so got to update this beginging addr also
    m_readHead = &m_readBuffer[0];
    m_start = &m_readBuffer[0];//CAI yujing. important here!
    m_readTail = m_readHead + unreadBytes;
    printStats();
    assert(remainingReadBufferCapacity() > 0);
}

void ensureReadBufferCapacity_cai()
{
    long freeAtFront = m_readHead - &m_readBuffer[0]; //it is also read head start position (index)
    long unreadBytes = m_readTail - m_readHead;
    long freeAtBack = &m_readBuffer.back() - m_readTail + 1; //.size() - thisValue is the read tail start position (relative to begin of vector)
    if (freeAtBack >= MIN_BUFFER_SIZE)
    {
        return;
    }

    if (freeAtFront < MIN_BUFFER_SIZE * 4)
    {
        m_readBuffer.resize(m_readBuffer.capacity() * 2);
        m_readHead = &m_readBuffer[0] + freeAtFront; //update new position for the readhead (after resize)
    }

    for (int i = 0; i < unreadBytes; i++)
    {
        m_readBuffer[i] = *(m_readHead + i);
    }
    m_readHead = &m_readBuffer[0]; //update new position for the readhead (after move unread to front!)
    m_readTail = m_readHead + unreadBytes;

    assert(remainingReadBufferCapacity() > 0);
}


int main() {
    //testCase1();
    //ensureReadBufferCapacity_cai();
    //ensureReadBufferCapacity();

    initPtrs();
    for(int j=0;j<10;j++){
        cout << "\n************j is equal to:" <<j <<"****************\n"<<endl;
        ensureReadBufferCapacity();
        for (int i = 0; i < 100; i++) 
        {
            *m_readTail=j*100+i; 
            m_readTail++;
        }

        int consume = RndEng() % (m_readTail- m_readHead);
        cout<<"# of consumed is:"<<consume<<endl;
        for (int i = 0; i < consume; i++)
        {
            cout << *m_readHead << " ";
            m_readHead++;
        }
        cout<<endl;
    }

    return 0;
}