@echo off
setlocal enabledelayedexpansion

rem Set the source directory
set "source=%CD%"

rem Call the deleteHiddenDirs subroutine for the current directory and its subdirectories
call :deleteHiddenDirs "%source%"

echo All hidden directories named ".vs" have been deleted.
pause
exit

:deleteHiddenDirs
for /F "delims=" %%I in ('dir /A:HD /B /S "%~1\.vs"') do (
    echo Deleting "%%~fI"
    rd /s /q "%%~fI"
)
exit /b