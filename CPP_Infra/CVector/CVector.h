#pragma once
#include "getExecutionTime.h"

template <typename CVec>
class VectorIterator
{
public:
    using ValueType = typename CVec::ValueType; 
    using PointerType = ValueType*;
    using ReferenceType = ValueType&;

    VectorIterator(PointerType ptr) 
        : m_Ptr(ptr){}

    VectorIterator& operator++()
    {
        m_Ptr++;
        return *this;
    }

    VectorIterator operator++(int) //postfix
    {
        VectorIterator it = *this;
        m_Ptr++;
        return it;
    }

    VectorIterator& operator--() //prefix
    {
        m_Ptr--;
        return *this;
    }

    VectorIterator operator--(int)
    {
        VectorIterator it = *this;
        m_Ptr--;
        return it;
    }

    ReferenceType operator[](int idx)
    {
        return *(m_Ptr+idx);
    }

    PointerType operator->()
    {
        return m_Ptr;
    }

    ReferenceType operator*() //not multiple, it put in front !..!
    {
        return *m_Ptr;
    }

    bool operator==(const VectorIterator& rhs)
    {
        return m_Ptr == rhs.m_Ptr;
    }

    bool operator!=(const VectorIterator& rhs)
    {
        return m_Ptr != rhs.m_Ptr;
    }    
private:
    PointerType m_Ptr;
};

template <typename T>
class CVector
{
public:
    using ValueType = T;
    using Iterator = VectorIterator<CVector<T> >;
public:
    CVector(size_t size)
    {
        ReAlloc(size);
    }

    CVector()
    {
        ReAlloc(2);
    }

    CVector(const CVector& rhs)
    {
        ReAlloc(rhs.Size());
        cout<<__FUNCSIG__<<endl;
        for (size_t i = 0; i < rhs.Size(); i++)
            m_Data[i] = rhs[i];
        m_Size = rhs.Size();
    }

    ~CVector() 
    { 
        Clear();
    }
    
    Iterator begin()
    {
        return Iterator(m_Data);
    }

    Iterator end()
    {
        return Iterator(m_Data + m_Size);
    }

    void PushBack(const T& rhs)
    {
        if (m_Capacity <= m_Size)
            ReAlloc(m_Capacity * 1.5);
        m_Data[m_Size] = rhs;
        m_Size++;
    }

    void PushBack(T&& rhs)
    {
        if (m_Capacity <= m_Size)
            ReAlloc(m_Capacity * 1.5);

        m_Data[m_Size] = std::move(rhs);
        m_Size++;
    }

    template<typename... ArgsType>
    T& EmplaceBack(ArgsType&&... args)
    {
        if (m_Capacity <= m_Size)
            ReAlloc(m_Capacity * 1.5);
        new(&m_Data[m_Size]) T(std::forward<ArgsType>(args)...); //construct in-place...
        m_Size++;
        return m_Data[m_Size];
    }

    void PopBack()
    {
        if (m_Size > 0)
        {
            m_Data[m_Size - 1].~T();
            m_Size--;
        }
    }

    void Clear()
    {
        for(size_t i=0; i<m_Size; i++)
        {
            m_Data[i].~T();
        }
        m_Size = 0;
    }

   const T& operator[](size_t index) const 
    {
        if (index < m_Size && index >= 0)
            return m_Data[index];
        else
            throw std::out_of_range("Index out of range");
    }

    size_t Size() const { return m_Size; }
private:
    void ReAlloc(size_t newCap)
    {
        T* newBlock = new T[newCap];
        for (size_t i = 0; i < m_Size; i++)
            newBlock[i] = std::move(m_Data[i]);

        //if(m_Data != nullptr)
        //    delete[] m_Data;

        m_Data = newBlock;
        m_Capacity = newCap;
    }

    T* m_Data = nullptr;
    size_t m_Size = 0;
    size_t m_Capacity = 0;
};