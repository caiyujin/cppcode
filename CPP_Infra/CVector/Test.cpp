#include "CVector.h"
struct CTypex
{
    float x, y, z;
    CTypex() :x(0), y(0), z(0) {};
    CTypex(float scaler) :x(scaler), y(scaler), z(scaler) {};
    CTypex(float x, float y, float z) :x(x), y(y), z(z) {};
    CTypex(const CTypex& rhs) : x(rhs.x), y(rhs.y), z(rhs.z) { cout << "copied... the less the better" << endl; };
    CTypex(CTypex&& rhs) : x(rhs.x), y(rhs.y), z(rhs.z) {}
    //remember assignment operator return by reference, because the item is already there, no need to create a new one.
    CTypex& operator=(const CTypex& rhs) 
    {   //even the caller is A = A, it still works... for this case. 
        x = rhs.x; y = rhs.y; z = rhs.z; return *this; 
    }
    CTypex& operator=(CTypex&& rhs) { x = rhs.x; y = rhs.y; z = rhs.z; return *this; }
    //it is so good to avoid raw pointer.
    ~CTypex() {}
};

struct CTypexRsc
{
    int* m_Heap;
    CTypexRsc() { m_Heap = new int[10]; m_Heap[5] = 1.2; };
    CTypexRsc(float x)  { m_Heap = new int[10]; m_Heap[5] = x;};
    CTypexRsc(const CTypexRsc& rhs) = delete;
    CTypexRsc(CTypexRsc&& rhs)
    { 
        m_Heap = rhs.m_Heap;  
        rhs.m_Heap = nullptr; 
    }
    
    CTypexRsc& operator=(const CTypexRsc& rhs) = delete;
    CTypexRsc& operator=(CTypexRsc&& rhs) 
    { 
        if(this == &rhs) return *this; 
        delete m_Heap;//delete original first;
        m_Heap = rhs.m_Heap; 
        rhs.m_Heap = nullptr; 
        return *this; 
    }

    ~CTypexRsc() { delete[] m_Heap; } //will it delte 10 items ?? or more ?
};
template <typename T>
void print_CVec(const CVector<T>& v)
{
    for (size_t i = 0; i < v.Size(); i++)
    {
        cout << v[i] << endl;
    }
}
template <>
void print_CVec(const CVector<CTypex>& v)
{
    for (size_t i = 0; i < v.Size(); i++)
    {
        cout << v[i].x << ":" << v[i].y << ":" << v[i].z << endl;
    }
}
template <>
void print_CVec(const CVector<CTypexRsc>& v)
{
    for (size_t i = 0; i < v.Size(); i++)
    {
        cout << "v[i].m_Heap[5]:" << v[i].m_Heap[5] << endl;
    }
}

int main()
{
    CVector<string> c;
    string s = "s0";
    c.PushBack(s);
    c.PushBack("s1"s);
    c.PushBack("s2"s);
    cout << c[1] << endl;
    c.PushBack("s3"s);
    print_CVec(c);
    cout << c[1] << endl;
    //c.Clear();

    const CVector<string> c2(c);
    print_CVec(c2);//free func
    cout << c2[1] << endl;

    {
        CVector<CTypex> cx;
        cx.PushBack(CTypex());
        cx.PushBack(CTypex(1.0f));
        cx.PushBack(CTypex(1.0f, 2.0f, 3.0f));

        cx.EmplaceBack();
        cx.EmplaceBack(1.1f);
        cx.EmplaceBack(1.1f, 2.1f, 3.1f);

        cx.PopBack();
        print_CVec(cx);
    }
    {
        CVector<CTypexRsc> cx2;
        cx2.PushBack(CTypexRsc());
        cx2.PushBack(CTypexRsc(1.0f));
        cx2.PushBack(CTypexRsc(2.0f));
        cx2.PopBack();
        cx2.EmplaceBack();
        cx2.EmplaceBack(1.1f);
        cx2.EmplaceBack(2.1f);

        print_CVec(cx2);
        cx2.Clear();
    }
    CVector<long long> CVLong;
    CVLong.PushBack(1);
    CVLong.PushBack(10);
    CVLong.PushBack(100);
    CVLong.PushBack(1000);
    CVLong.PushBack(10000);

    CVector<long long>::Iterator it2 = CVLong.begin();
    cout << it2[3] << endl;
    it2++;
    ++it2;

    for (CVector<long long>::Iterator it = CVLong.begin();
        it != CVLong.end();
        it++)
    {
        cout << *it << endl;
        if (it == it2)
        {
            cout << "found it2" << endl;
        }
    }    
    return 0;
}