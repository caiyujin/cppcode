#pragma once
#include "getExecutionTime.h"

struct node 
{
    int data;
    unique_ptr<node> l;
    unique_ptr<node> r;
    node() :data(0), l(nullptr), r(nullptr) {}
    node(int a) :data(a), l(nullptr), r(nullptr) {}
};

class BinaryTree {
private:
    map<int, int> levelData; //pair of (AbsolutePosition, ValueOfNode).
    void makeBinaryTree(unique_ptr<node>& curr, int levels, bool isFull, bool hasNeg = false);

public:
    unique_ptr<node> root;
    
    BinaryTree():root(nullptr)
    {
    }

    BinaryTree(int levels, bool isFull, bool hasNeg = false)
    { 
        makeBinaryTree(root, levels, isFull, hasNeg);
    }

    void printPostOrder(node* curr);

    void printInOrder(node* curr);

    void printPreOrder(node* curr);

    int countNodes(node* curr);

    int countLeaves(node* curr);
    
    int MaxHeight(node* curr);

    int MinHeight(node* curr);

    bool isMaxHeap(node* curr);

    bool isBST(node* curr);

    bool isFullTree(node* curr);

    bool isCompleteTree(node* curr, int index, int TotalCount);
    
    bool isBalancedTree(node* curr);

    void printTree(node* curr);
    
    int maxPathThroughTheNode(node* curr);

    static int ShortestTimeToGetAllApples(node* curr);

private:
    void levelDataLayOut(node* curr, int level, int accumIndent, int maxH);

    void levelDataLayOutPrint(int level, int MaxH);
};
