//Given an array of numbers, return true if given array can represent preorder traversal of a Binary Search Tree, else return false. Expected time complexity is O(n).
//binary search trees (BST), in each node must be greater than or equal to any key stored in the left sub - tree, and less than or equal to any key stored in the right sub tree

#include "BinarySearchTree.h"
BinarySearchTree::BinarySearchTree(vector<int> &vec)
{
    for (int &a : vec)
        InsertNode(a);
}

// NO re-balancing height implemented!
bool BinarySearchTree::InsertNode(int a)
{
    node *tmp = root.get();
    if (tmp == nullptr)
    {
        root = unique_ptr<node>(new node(a));
        return true;
    }

    while (tmp != nullptr)
    {
        if (a > tmp->data)
        {
            if (tmp->r == nullptr)
            {
                tmp->r = unique_ptr<node>(new node(a));
                return true;
            }
            else
            {
                tmp = (tmp->r).get();
            }
        }

        if (a == tmp->data)
        {
            return false;
        }

        if (a < tmp->data)
        {
            if (tmp->l == nullptr)
            {
                tmp->l = unique_ptr<node>(new node(a));
                return true;
            }
            else
            {
                tmp = (tmp->l).get();
            }
        }
    }

    return false;
}

node* BinarySearchTree::findMaxOnLeftSubTree(node *curr)
{
    if (curr->r != nullptr)
        return findMaxOnLeftSubTree((curr->r).get());
    else
        return curr;
}


bool BinarySearchTree::DeleteNode(int a)// NO re-balancing height implemented!
{
    node* tmp = root.get();

    if (tmp == nullptr)
        return false;

    node *tmpP = tmp; //tmpP is the parent of target 
    bool leftOfParent = false;

    //if it never enter the while block, the tmpP will be same as tmp
    while (tmp != nullptr && tmp->data != a)
    {
        if (a > tmp->data && tmp->r == nullptr)
            return false;

        if (a < tmp->data && tmp->l == nullptr)
            return false;

        tmpP = tmp;
        if (a > tmp->data)
        {
            tmp = (tmpP->r).get();
            leftOfParent = false;
        }
        else
        {
            tmp = (tmpP->l).get();
            leftOfParent = true;
        }
    }

    if (tmp == nullptr)
        return false;

    //come to here, already locate the node with data == a;
    if (tmp->r == nullptr && tmp->l == nullptr)
    {
        if (tmpP == tmp)
            root = nullptr;
        else
            leftOfParent ? tmpP->l = nullptr : tmpP->r = nullptr;
        return true;
    }

    if (tmp->r == nullptr && tmp->l != nullptr)
    {
        if (tmpP == tmp)
            root = move(root->l);
        else
            leftOfParent ? tmpP->l = move(tmp->l) : tmpP->r = move(tmp->l);
        return true;
    }

    if (tmp->r != nullptr && tmp->l == nullptr)
    {
        if (tmpP == tmp)
            root = move(root->r);
        else
            leftOfParent ? tmpP->l = move(tmp->r) : tmpP->r = move(tmp->r);       
        return true;
    }

    if (tmp->r != nullptr && tmp->l != nullptr)
    {
        int tempLeftMax = findMaxOnLeftSubTree((tmp->l).get())->data; 
        //get either the max on left sub tree or min on right sub tree, that will be replace at the current node with value of (a, to be deleted)
        DeleteNode(tempLeftMax); //it can only be leaf, so definitely will be simple deletion(so replace)
        tmp->data = tempLeftMax;
        return true;
    }

    return false;
}

//Let T be a rooted tree. The lowest common ancestor between two nodes n1 and n2 is defined 
//as the lowest node in T that has both n1 and n2 as descendants (where we allow a node to be a descendant of itself).
//if node->value is in between num1 and num2 and if it is the first time into such condition, so for sure it will be the lowest possible common acestor

node *BinarySearchTree::LCA(node *node, int num1, int num2)
{
    if (node == nullptr)
        return nullptr;

    if (node->data > num2 && node->data > num1)
        return LCA((node->l).get(), num1, num2);

    if (node->data < num2 && node->data < num1)
        return LCA((node->r).get(), num1, num2);

    //if node-data is in between num1/num2. bingo . we got it
    return node;
}

bool BinarySearchTree::ArrayIsPreorderBST(vit s, vit e)
{
    if (e - s <= 1)
        return true;

    vit c = s;
    vit it = c + 1;

    while (it != e && *c > *it)
        it++;

    vit minRight = it;
    if (it == e)
        return true;
    else
        minRight = min_element(it, e);

    bool allRightGreaterThanCenter = (*minRight > * c) ? true : false;

    return allRightGreaterThanCenter &&
        ArrayIsPreorderBST(s + 1, it) &&
        ArrayIsPreorderBST(it, e);
}


//it will only do one action, either rotate left, or rotate right,
//or did NOT do anything.

void BinarySearchTree::rebalancing(unique_ptr<node>& curr, unique_ptr<node>& currParent, bool isLeft)
{
    if (curr == nullptr)
        return;

    int hL = MaxHeight(curr->l.get());
    int hR = MaxHeight(curr->r.get());

    bool isRoot = root.get() == curr.get() ? true : false;
    unique_ptr<node> newCurr;

    if (std::abs(hL - hR) <= 1)
        return;

    if (hL - hR > 1)
    {   //turn right
        newCurr = move(curr->l);
        if (newCurr->r != nullptr)
            curr->l = move(newCurr->r);
        newCurr->r = move(curr);
    }

    if (hR - hL > 1)
    {   //turn left
        newCurr = move(curr->r);
        if (newCurr->l != nullptr)
            curr->r = move(newCurr->l);
        newCurr->l = move(curr);
    }

    if (isRoot)
        root = move(newCurr);
    else
        isLeft ? currParent->l = move(newCurr) :
            currParent->r = move(newCurr);

    return;
}