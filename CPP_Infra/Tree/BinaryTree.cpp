
#include "BinaryTree.h"

//unique_ptr must pass in as a non-const reference (to be used only), or by value(sink, ownership is taken)
void BinaryTree::makeBinaryTree(unique_ptr<node> &curr, int n, bool isFull, bool neg)
{
    if (n == 0)
        return;

    if (neg && RndEng() % 2 == 0)
        curr = unique_ptr<node>(new node(-1*(RndEng() % 100)));
    else
        curr = unique_ptr<node>(new node(RndEng() % 100));

    if (isFull)
    {
        makeBinaryTree(curr->l, n - 1, isFull, neg);
        makeBinaryTree(curr->r, n - 1, isFull, neg);
        return;
    }
    else 
    {
        switch (RndEng() % 10)
        {
        case 0: //10%
            makeBinaryTree(curr->l, n - 1, isFull, neg);
            break;
        case 1: //10%
            makeBinaryTree(curr->r, n - 1, isFull, neg);
            break;
        default:
            makeBinaryTree(curr->l, n - 1, isFull, neg);
            makeBinaryTree(curr->r, n - 1, isFull, neg);
            break;
        }
    }
    return;
}
//post/pre/in is the data relative to 
void BinaryTree::printPostOrder(node *n)
{
    if (n == nullptr)
        return;
    printPostOrder((n->l).get());
    printPostOrder((n->r).get());
    cout << n->data << ",";
}

void BinaryTree::printInOrder(node *n)
{
    if (n == nullptr)
        return;
    printInOrder((n->l).get());
    cout << n->data << ",";
    //InOrderArray.push_back(curr->data); // it convert the tree to an vector.. inOrder
    printInOrder((n->r).get());
}

void BinaryTree::printPreOrder(node *n)
{
    if (n == nullptr)
        return;
    cout << n->data << ",";
    printPreOrder((n->l).get());
    printPreOrder((n->r).get());
}

int BinaryTree::countNodes(node *n)
{
    if (n == nullptr)
        return (0);
    //this is really no Overlapping. so no cache (memorization) needed.
    return (1 + countNodes((n->l).get()) + countNodes((n->r).get()));
}

int BinaryTree::MaxHeight(node *n)
{
    if (n == nullptr)
        return 0;
    else
    {
        int lheight = MaxHeight((n->l).get()) + 1;
        int rheight = MaxHeight((n->r).get()) + 1;
        return (lheight > rheight) ? lheight : rheight;
    }
}

int BinaryTree::MinHeight(node *n)
{
    if (n == nullptr)
        return 0;

    if (n->l == nullptr && n->r == nullptr)
        return 1;

    if (n->l == nullptr)
        return MinHeight((n->r).get()) + 1;

    if (n->r == nullptr)
        return MinHeight((n->l).get()) + 1;

    int l = MinHeight((n->l).get()) + 1;
    int r = MinHeight((n->r).get()) + 1;
    return (l > r) ? r : l;
}

int BinaryTree::countLeaves(node *curr)
{
    if (curr == nullptr)
        return 0;

    if (curr->l == nullptr && curr->r == nullptr)
        return 1;

    return countLeaves((curr->r).get()) + countLeaves((curr->l).get());
}

// heap also describes a binary tree in which each node value, 
// is larger than any value in the left or right subtrees.

bool BinaryTree::isMaxHeap(node *n)
{
    if (n == nullptr)
        return true;

    if (n->l == nullptr && n->r == nullptr)
        return true;

    if (n->l == nullptr && n->r != nullptr)
        return (n->data > (n->r)->data) && isMaxHeap((n->r).get());

    if (n->l != nullptr && n->r == nullptr)
        return (n->data > (n->l)->data) && isMaxHeap((n->l).get());

    bool res = (n->data > (n->r)->data) && 
                                   (n->data > (n->l)->data);

    return res &&
        isMaxHeap((n->r).get()) &&
        isMaxHeap((n->l).get());
}

bool BinaryTree::isBST(node *n)
{
    if (n == nullptr)
        return true;

    if (n->l == nullptr && n->r == nullptr)
        return true;

    if (n->l == nullptr && n->r != nullptr)
        return (n->data < (n->r)->data) && isBST((n->r).get());

    if (n->l != nullptr && n->r == nullptr)  
        return (n->data > (n->l)->data) && isBST((n->l).get());

    bool res = (n->data < (n->r)->data) && (n->data > (n->l)->data);

    return res &&
        isBST((n->r).get()) &&
        isBST((n->l).get());
}

// check if a tree is a full tree, full tree means the node either have No Children 
// //or Have both of them.
bool BinaryTree::isFullTree(node *n)
{
    if (n == nullptr)
        return true;

    if (n->l == nullptr && n->r == nullptr)
        return true;

    if ((n->l != nullptr) && (n->r != nullptr))
        return isFullTree((n->l).get()) && 
                isFullTree((n->r).get());

    return false;
}

bool BinaryTree::isCompleteTree(node *n, int index, int TotalCount)
{
    if (n == nullptr)
        return true;

    if ((n->r == nullptr && n->l != nullptr) ||
        (n->r != nullptr && n->l == nullptr))
        return false;

    if (index >= TotalCount)
        return false;

    return isCompleteTree((n->r).get(), 2 * index + 2, TotalCount) && // 2 * index + 2   is the theoratical index of right child
           isCompleteTree((n->l).get(), 2 * index + 1, TotalCount); // 2 * index + 1   is the theoratical index of left child
}

bool BinaryTree::isBalancedTree(node* n)
{ 
    if (n == nullptr)
        return true;

    if (n->l == nullptr && n->r == nullptr)
        return true;

    if (n->l == nullptr && n->r != nullptr)
    {
        return n->r->l != nullptr || n->r->r != nullptr;
    }

    if (n->l != nullptr && n->r == nullptr)
        return n->l->l != nullptr || n->l->r != nullptr;

    return isBalancedTree((n->l).get()) && isBalancedTree((n->r).get());
}

void BinaryTree::printTree(node* n)
{
    int MaxH = MaxHeight(n);
    for (int i = 1; i <= MaxH; i++)
    {
        levelDataLayOut(n, i, (1 << MaxH), MaxH);
        levelDataLayOutPrint(i, MaxH);
    }
}

//BFT,H is currnt node's remaining height to bottom;
void BinaryTree::levelDataLayOut(node *n, int l, int indent, int H)
{
    if (n == nullptr)
        return;

    if (l == 1)
        levelData[indent] = n->data;
    else
    {
        levelDataLayOut((n->l).get(), l - 1, indent - (1 << (H - 1)), H - 1);
        levelDataLayOut((n->r).get(), l - 1, indent + (1 << (H - 1)), H - 1);
    }
}

void BinaryTree::levelDataLayOutPrint(int level, int MaxH)
{
    int prevCtr = 0;
    stringstream ss;

    for (auto it = levelData.begin(); it != levelData.end(); it++)
    {
        cout << setw(it->first - prevCtr) << it->second; //right-aligned. data is printed out already.

        int currLeft = (it->first) - (1 << (MaxH - level));
        int currRight = (it->first) + (1 << (MaxH - level));

        if (prevCtr != 0)
            prevCtr += (1 << (MaxH - level));

        string s1(currLeft - prevCtr, ' ');
        ss << s1;

        string s2(currRight - currLeft, '-');
        ss << s2;

        prevCtr = it->first;
    }
    cout <<"\n" ;
    levelData.clear();    cout << ss.str()<<"\n" ;
    return;
}
extern unordered_map<node*, int> maxCache; //the max include only one side of the child node, 
extern unordered_map<node*, int> maxCache2; //cannot used by parent

int BinaryTree::maxPathThroughTheNode(node* n) {
    if (n == nullptr)
        return INT_MIN;

    if (maxCache.find(n) != maxCache.end())
        return maxCache[n];

    if (n->l == nullptr && n->r == nullptr)
    {
        maxCache[n] = n->data;
        maxCache2[n] = n->data;
        return maxCache[n];
    }

    int l = INT_MIN;
    if (n->l != nullptr)
        l = maxPathThroughTheNode(n->l.get());

    int r = INT_MIN;
    if (n->r != nullptr)
        r = maxPathThroughTheNode(n->r.get());

    int max1 = max(l, r);
    int max2 = max(max1 + n->data, n->data);

    maxCache[n] = max2; //l r both neg, use data only.

    int max3 = max(max2, l + r + n->data);
    maxCache2[n] = max3; //in case l and r are both pos, then use both side.

    return maxCache[n];
}

int BinaryTree::ShortestTimeToGetAllApples(node* curr) 
{
    if (curr == nullptr)
        return 0;

    if (curr->l == nullptr && curr->r == nullptr)
    {
        if(curr->data%2==1) //has apple
            return 2;

        return 0;    
    }

    int l = INT_MAX;
    if (curr->l != nullptr)
        l = ShortestTimeToGetAllApples(curr->l.get());

    int r = INT_MAX;
    if (curr->r != nullptr)
        r = ShortestTimeToGetAllApples(curr->r.get());

    if (l + r == 0) //if left and right both are 0, then it means no apple in the subtree.
        return 0;
    else
        return l + r + 2;
}