#pragma once
#include "BinaryTree.h"

class BinarySearchTree: public BinaryTree
{
public:
    BinarySearchTree() {};
    
    BinarySearchTree(vector<int>& vec);

    bool InsertNode(int a);

    node* findMaxOnLeftSubTree(node* curr);

    bool DeleteNode(int a);

    node* LCA(int num1, int num2) { return LCA(root.get(), num1, num2); }

    void rebalancing() {rebalancing(root, root, true);}

    static bool ArrayIsPreorderBST(vit s, vit e);

private:
    BinarySearchTree(const BinarySearchTree& A) = delete;
    BinarySearchTree& operator=(const BinarySearchTree& A) = delete;

    node* LCA(node* curr, int num1, int num2);
    void rebalancing(unique_ptr<node>& curr, unique_ptr<node>& currParent, bool isLeft = true);

};
