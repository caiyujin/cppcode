#include "BinaryTree.h"
#include "BinarySearchTree.h"
unordered_map<node*, int> maxCache;
unordered_map<node*, int> maxCache2;

int main()
{
    //BinaryTree t = BinaryTree(5,false);
    BinaryTree t = BinaryTree(4, true);
    node* rootp = t.root.get();
    //rootp->right->left.reset();
    //rootp->right->right.reset();
    int MaxH = t.MaxHeight(rootp);
    int MinH = t.MinHeight(rootp);
    int nodes = t.countNodes(rootp);
    int leaves = t.countLeaves(rootp);

    cout << "PreOrder:";
    t.printPreOrder(rootp);
    cout <<"\n" ;
    
    cout << "InOrder:";
    t.printInOrder(rootp);
    cout <<"\n" ;
    
    cout << "PostOrder:"; 
    t.printPostOrder(rootp);
    cout <<"\n" ;
    
    cout << "isMaxHeap: " <<std::boolalpha << t.isMaxHeap(rootp) <<"\n" ;
    cout << "isBinarySearchTree: "  << t.isBST(rootp)<<"\n" ;
    cout << "isFullTree: " << t.isFullTree(rootp)  <<"\n" ;
    cout << "isCompleteTree:"  << t.isCompleteTree(rootp, 0, nodes) <<"\n" ;
    cout << "isBalancedTree:"  << t.isBalancedTree(rootp) <<"\n" ;
    t.printTree(rootp);

    BinaryTree t2 = BinaryTree(4, false, true);
    t2.printTree(t2.root.get());

    cout <<t2.maxPathThroughTheNode(t2.root.get()) << endl; //shall make it static, since it does not required "this".    
    auto maxVal = std::max_element(maxCache2.begin(), maxCache2.end(),
        [](const pair<node*, int>& left, const pair<node*, int>& right) {
            return left.second < right.second;
        });

     BinaryTree t3 = BinaryTree(3, false, false);
     cout << "ShortestTimeToGetAllApples is:" << t3.ShortestTimeToGetAllApples(t3.root.get())  <<"\n" ;

    //********************* Binary Search Tree*******************
    //int pre2[] = {40, 30, 27, 35, 31, 38, 80, 70, 100, 90, 130};
    //int n = sizeof(pre2) / sizeof(pre2[0]);
    vector<int> vec1 { 40, 30, 27, 35, 31, 38, 80, 70, 100, 90, 130 };
    BinarySearchTree BST1(vec1);
    cout << boolalpha << BinarySearchTree::ArrayIsPreorderBST(vec1.begin(), vec1.end());

    vector<int> rand2{14, 11, 19, 7, 13, 22, 9, 12, 24, 10};
    BinarySearchTree BST2(rand2);
 
    BST2.DeleteNode(24);
    BST2.printInOrder(BST2.root.get());

    node *temp = BST2.LCA(7, 22);
    node* temp2 = BST2.LCA(22, 7);
    if (temp != nullptr)
        cout << "LCA of 7 and 22 is:" << temp->data <<"\n" ;

    //vector<int> rand3{ 5,4,2,3 }; //need rotate right
    vector<int> rand3{ 2,3,4,5 }; //need rotate left
    BinarySearchTree BST3(rand3);
    BST3.printTree(BST3.root.get());
    BST3.rebalancing();
    BST3.printTree(BST3.root.get());
    return 0;
}
