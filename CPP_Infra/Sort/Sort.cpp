// BubbleSort.cpp : Defines the entry point for the console application.
#include "getExecutionTime.h"

vector<char> vecRBGsource = { 'R','B','G' };
vector<char> vecRBG;
const int sizeOfvecRBG =10000;

void swapVecIdx(int idx1, int idx2) {
    char temp = vecRBG[idx1];
    vecRBG[idx1] = vecRBG[idx2];
    vecRBG[idx2] = temp;
}

void makeRBGvector(void) {
    vecRBG.clear();
    for (int i = 0; i < sizeOfvecRBG; i++) {
        vecRBG.push_back(vecRBGsource[RndEng() % 3]);
    }
}

void sortRBGsource(void) {
    int RightMostNonR = 0;
    int LeftMostNonG = sizeOfvecRBG - 1;
    int i = 0;
   // for (; i < sizeOfvecRBG; ) {

        while (vecRBG[RightMostNonR] == 'R')
            RightMostNonR++;

        if (i < RightMostNonR)
            i = RightMostNonR;

        while (vecRBG[LeftMostNonG] == 'G')
            LeftMostNonG--;
 //   }

    while (i < LeftMostNonG)
    {
        switch (vecRBG[i])
        {
        case 'R':
            swapVecIdx(i, RightMostNonR);
            RightMostNonR++;
            break;
        case 'G':
            swapVecIdx(i, LeftMostNonG);
            LeftMostNonG--;
            break;
        default:
            i++;
            break;
        }
    }
}

void sortRBGsource2(void) {
    vector<char> ordered = { 'B','R','G' };
    vector<int> orderedCount = vector<int>(ordered.size(), 0);
    unordered_map<char, int> orderedMap = { {'B',0},{'R',1},{'G',2} };
    for (auto& a : vecRBG) {
        orderedCount[orderedMap[a]]++;
    }

    int startPos = 0;
    for(int j=0;j< orderedCount.size();j++)
    {    
        for (int i = 0; i < orderedCount[j]; i++)
            vecRBG[startPos + i] = ordered[j];
        startPos += orderedCount[j];
    }
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(int arr[], int n){
    int i, j;
    for (i = 0; i < n - 1; i++)
    {
        // Last i elements are already in place   
        bool swapped = false;
        for (j = 0; j < n - i - 1; j++){ //key is here n-i-1, when entire For loop finished, it bubble up the "remaining" max value !
            if (arr[j] > arr[j + 1]){
                swap(&arr[j], &arr[j + 1]);
                swapped = true;
            }
        }
        if (swapped == false)
            break;
    }
}

//the 17th bit from LSB is to keep the frequency, the lower 16bits to keep the actual value.
//actually kind of complicated, shall just use merge sort, and print from right to left. 
void frequencySort(vector<int>& vec) {
    int a = 0x00010000;
    map<int,int> map1;
    for (vit it = vec.begin(); it != vec.end();it++) {
        pair<map<int, int>::iterator, bool> ret1 = map1.insert(make_pair(*it,a));
        if (ret1.second == true) {
            (ret1.first)->second += *it; //0x00010003 for the 1st '3';
        }
        else { //updated to 0x00020003 for the 2nd '3';
            (ret1.first)->second += (1 << 16); 
        }
    }
    map<int, int> map2;
    for (map<int, int>::iterator it = map1.begin(); it != map1.end(); it++) {
        map2.insert(make_pair(it->second, it->first));
    } //will be 00010007 00010009 00020002 00020004 00030001 , 7, 9 2,2 4,4, 1,1,1,

    for (map<int, int>::reverse_iterator it = map2.rbegin(); it != map2.rend(); it++) {
        for (int i = 0; i < (it->first >> 16); i++)
            cout << it->second << "    ";
    }
}

void mergeHalves(vector<int>& v1, vector<int>& v2, int leftStart, int rightEnd) {
    int leftEnd = (leftStart + rightEnd) / 2;
    int rightStart = leftEnd + 1;
    int size = rightEnd - leftStart + 1;

    int left = leftStart;
    int right = rightStart;
    int index = leftStart;
    while (left <= leftEnd && right <=rightEnd) {
        if (v1[left] <= v1[right]) {
            v2[index] = v1[left];
            left++;
        }
        else {
            v2[index] = v1[right];
            right++;
        }
        index++;
    }
    while (left <= leftEnd) {
        v2[index] = v1[left];
        left++;
        index++;
    }
    while (right <= rightEnd) {
        v2[index] = v1[right];
        right++;
        index++;
    }

    for (int i = leftStart; i <= rightEnd; i++) {
        v1[i] = v2[i];
    }
}

void mergeSort(vector<int>& v1, vector<int>& v2, int leftStart, int rightEnd) {
    if (leftStart >= rightEnd) {
        return;
    }
    int middle = (leftStart + rightEnd) / 2;
    mergeSort(v1, v2, leftStart, middle);
    mergeSort(v1, v2, middle + 1, rightEnd);
    mergeHalves(v1, v2, leftStart, rightEnd);
}

int _tmain(int argc, _TCHAR* argv[])
{
    int arr[] = { 64, 34, 25, 12, 22, 11, 90 };
    int n = sizeof(arr) / sizeof(arr[0]);
    bubbleSort(arr, n);

    {
        cGetTime c;
        makeRBGvector();
        sortRBGsource();
    }

    {
        cGetTime c;
        makeRBGvector();
        sortRBGsource2();
    }

    vector<int> vec = { 2,4,12,12,2,3,3,2,3,3,7,7,7,11,11,9,1 };
    frequencySort(vec);

    vector<int> vec1(10), vec2(10);
    generate_vector(vec1);
    mergeSort(vec1,vec2,0,vec1.size()-1);

    return 0;
}

