#include <iostream>
#include "MinHeapTree.h"

int main() {

    MinHeapTree t1(3);
    t1.insert(10);
    t1.insert(20);  
    t1.insert(30);

    t1.printHeap();

    t1.extract();
    t1.printHeap();
    t1.insert(25);

    t1.printHeap();

    vector<int> v1 = { 70, 10,  20, 60, 30, 40, 50};

    //find max 3 items;

    MinHeapTree t2(3);
    t2.insert(v1[0]);
    t2.insert(v1[1]);
    t2.insert(v1[2]);
    // O(nlogk);  instead of sorting all O(nlogn) and then get last 3 O(1).
    for (int i = 3; i < v1.size(); i++)
    {
        if (v1[i] > t2.get_heap()[0])
        {
            t2.extract();
            t2.insert(v1[i]);
        }
        
    }

    return 0;
}