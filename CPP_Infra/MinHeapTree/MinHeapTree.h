#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <math.h>

using namespace std;
class MinHeapTree{
private:
    int size;
    int capacity;
    vector<int> heap;
    int parent(int i) { return (i - 1) / 2; }
    int left(int i) { return (2 * i + 1); }
    int right(int i) { return (2 * i + 2); }
public:
    MinHeapTree(int capacity);
    void insert(int k);
    int extract();
    void heapify(int i);
    void printHeap();
    vector<int>& get_heap() { return heap; }

};

MinHeapTree::MinHeapTree(int capacity) {
    this->capacity = capacity;
    this->size = 0;
    heap.resize(capacity);
}

void MinHeapTree::insert(int k)
{
    if(size==capacity)
        return;

    size++;
    int idx = size - 1;
    heap[idx]=k;

    while (idx > 0 && heap[parent(idx)] > heap[idx]) {
        swap(heap[parent(idx)], heap[idx]);
        idx = parent(idx);
    }
}

void MinHeapTree::heapify(int i)
{
    int l= left(i);
    int r= right(i);

    int curr = i;

    if((l < size) && (heap[l] < heap[curr]))
        curr = l;

    if ((r < size) && (heap[r] < heap[curr]))
        curr = l;

    if (curr != i) 
    {
        swap(heap[i], heap[curr]);
        heapify(curr); //recursively heapify the affected sub-tree
    }
}


int MinHeapTree::extract()
{
    if (size == 0)
        return -1;

    if (size == 1)
    {
        size--;
        return heap[0];
    }

    int rtn = heap[0];
    heap[0] = heap[size - 1];
    size--;
    heapify(0);
    return rtn;
}

void MinHeapTree::printHeap()
{
    int power = 0;
    int value = 1;
    for (int i = 0; i < size; i++)
    {
        if (i == value)
        {
            cout << endl;
            power++;
            value += pow(2, power);
        }
        cout << heap[i] << " ";
    }
    cout << endl;
}