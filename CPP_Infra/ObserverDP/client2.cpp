#include "client2.h"

void client2::update(float temp, float humidity, float pressure) {
    std::cout << "---Client (" << id << ") Data---\tTemperature: " << temp
        << "\tHumidity: " << humidity
        << "\tPressure: " << pressure
        << std::endl;
}