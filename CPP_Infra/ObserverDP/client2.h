#pragma once
#include <iostream>
#include "Observer.h"
class client2:public Observer {
public:
    int id;
    client2(int i):id(i){}
    void update(float temp, float humidity, float pressure) ;
};