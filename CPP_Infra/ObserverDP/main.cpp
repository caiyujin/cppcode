#include <iostream>
#include "WeatherData.h"
#include "Client.h"
#include "client2.h"

int main() {
    WeatherData weatherStation;
    Client one(1), two(2), three(3);
    client2 four(4), five(5), six(6);
    float temp, humidity, pressure;

    weatherStation.registerObserver(&one);
    weatherStation.registerObserver(&two);
    weatherStation.registerObserver(&three);
    weatherStation.registerObserver(&four);
    weatherStation.registerObserver(&five);
    weatherStation.registerObserver(&six); //subscribe here

    std::cout << "Enter Temperature, Humidity, Pressure (seperated by spaces) << ";
    std::cin >> temp >> humidity >> pressure;

    weatherStation.setState(temp, humidity, pressure); //broad case here

    weatherStation.removeObserver(&two);

    std::cout << "\n\nEnter Temperature, Humidity, Pressure (seperated by spaces) << ";
    std::cin >> temp >> humidity >> pressure;

    weatherStation.setState(temp, humidity, pressure);

    return 0;
}