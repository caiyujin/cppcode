#pragma once
#include "getExecutionTime.h"

template <typename T>
class node
{
  public:
    T valp;
    node *next;
    node *prev;
    node(T val) : valp(val), prev(nullptr), next(nullptr){}
    node(const node &rhs) : valp(rhs.valp), prev(nullptr), next(nullptr){}
    const node& operator=(const node& rhs) = delete;
    ~node(){}
};

template <typename T>
class tList
{
  public:
    node<T> *root;
    node<T> *last;
    int size =0 ;
    tList() : root(nullptr), last(nullptr), size(0){}
    tList(const tList &rhs)
    {
        //this list's member been updated in the func addNode.
        node<T> *p = rhs.last;
        while (p != nullptr)
        {
            addNode(p->valp);
            p = p->prev;
        }
        size = rhs.size;
    }

    tList(vector<T> &C)
    {
        for (auto &a : C)
            addNode(a);
        size = C.size();
    }

    tList &operator=(const tList &rhs) = delete;

    ~tList()
    {
        clearlist();
    }

    node<T> *findVal(T val)
    {
        node<T> *p = root;
        while (p != nullptr && p->valp != val)
            p = p->next;
        return p;
    }

    vector<node<T> *> findAllVal(T val)
    {
        node<T> *p = root;
        vector<node<T> *> res;
        while (p != nullptr){
            if (p->valp == val)
                res.push_back(p);
            p = p->next;
        }
        return res;
    }

    void addNode(T p) //important to remember is to insert in front!
    {
        node<T>* nd = new node<T>(p);
        if (root == nullptr)
        {
            root = nd;
            last = nd;
        }
        else
        {
            node<T> *rootOld = root;
            root = nd; //insert at front more efficient
            root->next = rootOld;
            rootOld->prev = root;
        }
        size++;
    }

    void removeFirstVal(T val)
    {
        if (root == nullptr)
            return;

        node<T>* p = findVal(val);

        if (p == nullptr)
            return;

        if (p != last && p != root)
        {
            p->prev->next = p->next;
            p->next->prev = p->prev;
            p->prev = nullptr;
            p->next = nullptr;
        }
        else
        {
            if (p == root)
            {
                root = p->next;
                p->next = nullptr;
                root->prev = nullptr;
            }
            else //p==last
            {
                last = p->prev;
                last->next = nullptr;
                p->prev = nullptr;
            }
        }

        delete p; //it will be fine because both ->next and ->prev already nulled.
        size--;
        return;
    }

    void clearlist(void)
    {
        while (last != nullptr)
        {
            node<T> *p = last; //p become owner of the resource.
            last = last->prev;
            p->prev = nullptr; //it is NOT Null the previous node, it simply null the pointers originally point to previous node.
            p->next = nullptr;
            delete p;
        }
        root = nullptr;
        size = 0;
    }

    void ReverseList(void)
    {
        if (size <= 1)
            return;

        auto curr = last;
        while (curr != nullptr)
        {
            std::swap(curr->next, curr->prev);
            curr = curr->next;
        }
        std::swap(root, last);
    }

};