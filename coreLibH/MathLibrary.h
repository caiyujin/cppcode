#pragma once
// MathLibrary.h - Contains declarations of math functions
#include <string>
#include <sstream>

#ifdef MATHLIBRARY_EXPORTS
#define MATHLIBRARY_API __declspec(dllexport)
#else
#define MATHLIBRARY_API __declspec(dllimport)
#endif

using namespace std;

using ull = unsigned long long;
extern "C" MATHLIBRARY_API void fibonacci_init(
    const ull a, const ull b, ull runUpto);

extern "C" MATHLIBRARY_API bool fibonacci_next();

extern "C" MATHLIBRARY_API ull fibonacci_current();

extern "C" MATHLIBRARY_API unsigned fibonacci_index();

//do NOT delete below two function. for c# marshalling.
extern "C" MATHLIBRARY_API void cppPrintString(char* str, int sz);

extern "C" MATHLIBRARY_API void cppPrintInt(int i);

class MATHLIBRARY_API CencryptedString {
public:    
    CencryptedString(const std::string& s) {
        for (auto& a : s) {
            _s.push_back(a + 1);
        }
    }
    
    std::string getOriginal();
    std::string getString() {
        return _s;
    }
private:
    std::string _s;
};

void SampleClassTest1(void);
void SampleClassTest2(char* c);

namespace CaiUtilities
{

    class MATHLIBRARY_API Birthday_dll {
    public:
        Birthday_dll();

        Birthday_dll(int m, int n, int y);

        ~Birthday_dll()
        {
            delete abc;
        }

        Birthday_dll(const Birthday_dll& a) {
            //this is a deep copy... shallow copy will copy a pointer and not allocte new heap for new object, and caused tons of issue .(double delete)
            //or first deleted, second want to access again, memory violation.
            abc = new string(*(a.abc));
        }

        const Birthday_dll& operator=(const Birthday_dll& a) {
            *abc = *(a.abc);
            return *this;
        }

        void printDate() {
            cout << *abc << endl;
        }

    private:
        //declear copy/assignment func will overriden the default (compiler) one, 
        //make it private, so the main can NOT call. Effectively Disable it.
        //Birthday_dll & operator=(const Birthday_dll&) {}
        std::string* abc;
    };

    class MATHLIBRARY_API People_dll {
    public:
        People_dll(std::string x, Birthday_dll* bo) : name(x), dateOfBirth(bo) {}
        void printInfo();
        ~People_dll() {
            //delete dateOfBirth; // will have memory leak when taken out.
        }
    private:
        std::string name;
        Birthday_dll* dateOfBirth;//composition
    };

    class SimpleMathOps
    {
    public:
        // Returns a + b  
        static  double Add(double a, double b);

        // Returns a * b  
        static  double Multiply(double a, double b);

        static const int status1 = 99;

        int  get_prev(void) {
            return previous_op;
        }

        int current_op = 1;
    private:
        int previous_op = 0;
    };
}