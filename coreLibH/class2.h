#pragma once
#include <string>
#include <iostream>

class class2
{
public:
    class2();
    ~class2(){
        std::cout << __FUNCSIG__ << std::endl;
    }
private:
    std::string mStr;
    int id;
};


class class2b
{
public:
    class2b() :mStr(""), mKey(0) {}
    class2b(const std::string& s, int key);
    ~class2b()
    {
        std::cout << __FUNCSIG__ << std::endl;
    }

    const std::string& getStr() const noexcept { return mStr; }

private:
    std::string mStr;
    int mKey;
};
