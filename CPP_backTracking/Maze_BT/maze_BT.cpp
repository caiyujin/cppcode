// FindPrimeNumber.cpp : Defines the entry point for the console application.

#include "getExecutionTime.h"

// pos 0 it connect to 1 and 3; pos 1 it connect to 0 and 2

/*
        0 - 1 - 2
        !
        3 - 4 - 5
        !   !
        6   7 - 8
*/
vector<vector<int>> maze={
    {0,1,3},
    {1,0,2},
    {2,1},
    {3,0,4,6},
    {4,3,5,7},
    {5,4,},
    {6,3},
    {7,4,8},
    {8,7}
};
vector<int> visited;
vector<int> curr;
void findThePath(void)
{
    curr.push_back(0);
    visited.push_back(0);
    int currPos = curr.back();

    while(!curr.empty())
    {
        currPos = curr.back();
        
        if(currPos==8) 
            return;

        vit it =maze[currPos].begin();
        
        bool foundOutlet=false;
        
        while(!foundOutlet
            && it != maze[currPos].end())
		{
            if(find(visited.begin(),visited.end(),*it)!=visited.end())
			{
                it++;
			}
            else
			{   //found not yet visited, add to visited and curr, it is DFS;
                foundOutlet=true;
            }    
        }
		
        if(foundOutlet)
        {
            curr.push_back(*it);
            visited.push_back(*it);
        }
        else
        {
            curr.pop_back();
        }
    }//
}

int main()
{
    findThePath();
    print_info(curr,"the path"s);
    return 0;
}