#include "getExecutionTime.h"
vector<vector<char> > sample1 =
{
    {' ','7',' ',' ','2',' ',' ','4','6'},
    {' ','6',' ',' ',' ',' ','8','9',' '},
    {'2',' ',' ','8',' ',' ','7','1','5'},
    {' ','8','4',' ','9','7',' ',' ',' '},
    {'7','1',' ',' ',' ',' ',' ','5','9'},
    {' ',' ',' ','1','3',' ','4','8',' '},
    {'6','9','7',' ',' ','2',' ',' ','8'},
    {' ','5','8',' ',' ',' ',' ','6',' '},
    {'4','3',' ',' ','8',' ',' ','7',' '}
};

void print_board(vector<vector<char> >& board)
{
    for (int i = 0; i <9; i++) //'i == y'
    {
        for (int j = 0; j < 9; j++)
        {
            cout << board[i][j] << ",";
        }
        cout << endl;
    }
}

std::size_t get_cell(size_t row, size_t col)
{
    return 3*(row / 3) + col / 3;
}

std::size_t get_next_row(size_t row, size_t col)
{
    return row+(col + 1) / 9;
}

std::size_t get_next_col(size_t col)
{
    return (col + 1) % 9;
}

std::pair<size_t, size_t> next_empty_position(vector<vector<char> >& board, size_t row, size_t col)
{
    while (row != 9)
    {
        if (board[row][col] == ' ')
            return { row,col };

        row= get_next_row(row, col);
        col = get_next_col(col);
    }

    return { 9,0 };
}

bool solve(vector<vector<char> >& board, size_t row_start, size_t col_start,
    array<bitset<9>, 9>& row_contains,
    array<bitset<9>, 9>& col_contains,
    array<bitset<9>, 9>& cell_contains)
{
    auto [row,col] = next_empty_position(board, row_start, col_start);

    if (row == 9)
        return true;

    size_t cell= get_cell(row, col);
    std::bitset<9> contains = row_contains[row] | col_contains[col] | cell_contains[cell];
    
    if(contains.all()) //all bits are set, 1-9 all filled, No more left to be fill in current empty cell, it is a dead end
        return false;

    for (int num = 0; num < 9; num++)
    {
        if (!contains[num])
        {
            board[row][col] = '1' + num;
            row_contains[row].set(num);
            col_contains[col].set(num);
            cell_contains[cell].set(num);

            //recursively backtracking;
            if (solve(board, row, col, row_contains, col_contains, cell_contains))
            {
                return true;
            }


            row_contains[row].reset(num);
            col_contains[col].reset(num);
            cell_contains[cell].reset(num);
        }
    }
    board[row][col] = ' ';
    return false;
}


void solve(vector<vector<char> >& board)
{
    array<bitset<9>, 9> row_contains ={0,0,0,0,0,0,0,0,0};
    array<bitset<9>, 9> col_contains = { 0,0,0,0,0,0,0,0,0 };
    array<bitset<9>, 9> cell_contains = { 0,0,0,0,0,0,0,0,0 };

    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            if (board[i][j] != ' ')
            {
                int num = board[i][j] - '1';
                row_contains[i].set(num);
                col_contains[j].set(num);
                cell_contains[get_cell(i,j)].set(num);
            }
        }
    }
    solve(board, 0,0,row_contains, col_contains, cell_contains);
}

int main() 
{

    //fillDataToBitSets();
    print_board(sample1);
    solve(sample1);
    cout << "------------------------" << endl;
    print_board(sample1);
    return 0;
}

