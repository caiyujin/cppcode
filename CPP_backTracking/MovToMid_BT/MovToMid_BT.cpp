#include "getExecutionTime.h"




vector<vector<int> > inputs
{
    {3, 5, 4, 4, 7, 3, 4, 6, 3},
    {6, 7, 5, 6, 6, 2, 6, 6, 2},
    {3, 3, 4, 3, 2, 5, 4, 7, 2},
    {6, 5, 5, 1, 2, 3, 6, 5, 6},
    {3, 3, 4, 3, 0, 1, 4, 3, 4},
    {3, 5, 4, 3, 2, 2, 3, 3, 5},
    {3, 5, 4, 3, 2, 6, 4, 4, 3},
    {3, 5, 1, 3, 7, 5, 3, 6, 4},
    {6, 2, 4, 3, 4, 5, 4, 5, 1}
};

const int yMin2 = 0;
const int xMin2 = 0;
const int yMax2 = inputs.size();
const int xMax2 = inputs[0].size();

struct NumPairHasher
{
    size_t operator()(const pair<int, int>& key) const //the const at func signature is Very important, cannot forget
    {
        return key.first * 1000 + key.second;
    }

};

using posPairSetT = unordered_set<pair<int, int>, NumPairHasher >;

void print(vector<pair<int, int> >& currPath)
{
    for (int i = 0; i < currPath.size(); ++i)
    {
        cout << "(" << currPath[i].first << "," << currPath[i].second << ") ";
    }
    cout << endl;
}


void checkAndInsert(const pair<int, int>& pos, vector<pair<int, int> >& nexts, posPairSetT& visited)
{
    if (pos.first >= yMin2 && pos.first < yMax2 &&
        pos.second >= xMin2 && pos.second < xMax2 &&
        visited.find(pos) == visited.end())
    {
        nexts.push_back(pos);
    }
}   

vector<pair<int, int> > getNextMoves(pair<int, int>& pos, posPairSetT& visited)
{
    vector<pair<int, int> > nexts;
    int moveVal = inputs[pos.first][pos.second];
    
    checkAndInsert({ pos.first + moveVal, pos.second }, nexts, visited);
    checkAndInsert({ pos.first - moveVal, pos.second }, nexts, visited);
    checkAndInsert({ pos.first, pos.second + moveVal }, nexts, visited);
    checkAndInsert({ pos.first, pos.second - moveVal }, nexts, visited);
    return nexts;
}



//recursive, still more easier to understand

bool solve3(pair<int, int> pos, deque<pair<int, int> > q, posPairSetT& visited, vector<pair<int, int> >& currPath)
{
    if (pos.first == (yMax2 - 1) / 2 && pos.second == (xMax2 - 1) / 2)
    {
        cout << "Found a path" << endl;
        currPath.push_back(pos);
        print (currPath);
        return true;
    }
    else
    {
        q.push_back(pos);
        visited.insert(pos);
    }


    while (!q.empty())
    {
        pair<int, int> pos2 = q.front();
        q.pop_front();
        currPath.push_back(pos2);

        vector<pair<int, int> > nextPos = getNextMoves(pos2, visited);

        if (nextPos.size() == 0)
        {
            currPath.pop_back();
            return false;
        }
        else
        {
            for (int i = 0; i < nextPos.size(); ++i)
            {
                solve3(nextPos[i], q, visited, currPath);                
            }
            currPath.pop_back();
        }

    }

    return false;
}

int main() 
{
    deque<pair<int, int> > q;
    posPairSetT visited;
    vector<pair<int, int> > currPath;

    solve3({ 0,0 }, q, visited, currPath);
    return 0;
}

