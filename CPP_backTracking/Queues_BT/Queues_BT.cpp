#include "getExecutionTime.h"
const int N = 9;//9
vector<bitset<N> > sample1 = vector<bitset<N> >(N, bitset<N>(0));

bitset<N> col_used;

//this one probably cannot DP, because caching it is useless, the earlier set row's col will make the 
//sub problem NOT same (no duplicate works!)
void print2()
{
    for (int i = 0; i <N; i++) //'i == y'
    {
            cout << sample1[i];
            cout << endl;
    }
}

bitset<N> getAllPossibleCols(int row, vector<int>& sol)
{
    bitset<N> ret(0);
    if (row >= N)
        return ret;
    
    for (int i = 0; i < N; i++)
    {
        if(col_used[i] == 0)
        {
            ret[i]=1;
        }
    }
    
    if (row >= 1)
    {
        if (sol[row - 1] != -1) //lets avoid previous row's selection(col),left and right
        {
            if(sol[row-1]==0)
                ret[sol[row - 1] + 1] = 0;
            if(sol[row-1]==N-1)
                ret[sol[row - 1] - 1] = 0;
            if (sol[row - 1] > 0 && sol[row - 1] < N - 1)
            {
                ret[sol[row - 1] - 1] = 0;
                ret[sol[row - 1] + 1] = 0;
            }                
        }
    }
    return ret;
}

void solve(int row, vector<int>& sol)
{
    bitset<N> avail = getAllPossibleCols(row, sol); //each bit's position(index) represent possible column choices for this Row.

    if (avail == 0)
    {
        if (row == N)
        {
            print_info(sol, "solutions");
        }
        else
        {
            return; //let caller to remove current incomplete solution's last row's selection.
        }
    }

    for (int i = 0; i < N; i++)
    {
        if (avail[i] == 0)
            continue;   
        sol[row]= i;
        col_used[i] = 1;

        solve(row + 1, sol);
        sol[row] = -1;
        col_used[i] = 0;
    }    
}

void solve2(int row)
{
    vector<int> sol(N, -1);
    solve(row, sol);
}


int main() 
{
    print2();
    solve2(0);
    cout << "------------------------" << endl;
    print2();
    return 0;
}

