#include <iostream>
#include <vector>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_set>
using namespace std;
void permutation_BT(vector<char>& vecStr, vector<int>& res)
{
    if (res.size() == vecStr.size())
    {
        for (auto& a : res)
            cout << vecStr[a];
        cout << endl;
        return;
    }

    for (int i=0; i < vecStr.size();i++)
    {
        if (std::find(res.begin(),res.end(),i)==res.end())
        {
            res.push_back(i); 
            permutation_BT(vecStr, res);
            res.pop_back();
        }
    }
}

int main()
{
    const string s1 = "abcdef";
    vector<char> vecStr =vector<char>(s1.begin(), s1.end());
    //std::set_difference(vecStr.begin(), vecStr.end(),
    //    vecStr2.begin(), vecStr2.end(),
    //    std::back_inserter(availables)); //throw error if "abcdef" and "abcdfe"..lousy, all these diff/unions/intersections areq required to be sorted!

    vector<int> res;
    permutation_BT(vecStr, res);

    return 0;
}