#include "getExecutionTime.h"

//this solution shall be quite similar to the coin change problem
//https://leetcode.com/problems/coin-change/
vector<string> pieces =
{
     "i", "like", "sam", "sung", "samsung", "mobile", "ice",
        "and", "cream", "icecream", "man", "go", "mango"
};

vector<vector<int> > results; //keep index;
void solve2(int start, const string& input, vector<int>& result)
{

    for (int i=0; i< pieces.size(); i++)
    {
        string& next = pieces[i];
        if (input.substr(start, next.size()) == next)
        {
            result.push_back(i);
            solve2(start + next.size(), input,result);
            result.pop_back();
        }
    }

    if(start == input.size())
    {
        results.push_back(result);
    }
}

void solve(const string& input)
{
    vector<int> result;
    int start = 0;
    solve2(start, input, result);
}

int main() 
{
    std::string input("ilikeicecreamandmango");
    solve(input);
    cout << "------------------------" << endl;
    //print2();
    return 0;
}

