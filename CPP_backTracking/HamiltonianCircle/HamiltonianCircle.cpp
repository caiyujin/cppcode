#include <iostream>
#include <vector>
#include <set>
#include <deque>
using namespace std;

vector<vector<int> > adj(5);
void addAdj()
{
    adj[0] = { 1,3 };
    adj[1] = { 0,2,3,4 };
    adj[2] = { 1,4 };
    //adj[3] = { 0,1 };
    // adj[4]= {1,2};
    adj[3] = { 0,1,4 };
    adj[4] = { 1,2,3};
}


bool hamiltonianCircle_BT(deque<int>& curr)
{
    int cur = curr.back();
    // //curr.front() is the first element in the queue, find the circle, exit condition
    if (curr.size() == adj.size() &&
        std::find(adj[cur].begin(), adj[cur].end(), curr.front()) != adj[cur].end())
        {
        for (auto& a : curr)
            cout << a;
        cout << curr.front();
        cout << endl;
        return true;
    }

    for (int i = 0; i < adj[cur].size(); i++)
    {
        int next = adj[cur][i];
        if (std::find(curr.begin(), curr.end(), next) == curr.end())//if not yet in current queue
        {
            curr.push_back(next);
            if (hamiltonianCircle_BT(curr))
                return true;//early exit.
            curr.pop_back();
        }
    }

    return false;
}

void hamiltonianCircle()
{
    deque<int> curr;
    curr.push_back(0);
    cout << boolalpha << hamiltonianCircle_BT(curr) << endl;
}

int main()
{
    addAdj();
    hamiltonianCircle();
    return 0;
}