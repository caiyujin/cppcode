#include <iostream>
#include <vector>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>
using namespace std;

vector<char> op1{ 'B','A','S','E' };// { 'S', 'E', 'N', 'D' };
vector<char> op2{ 'B','A', 'L','L' };// { 'M', 'O', 'R', 'E' };
vector<char> res{ 'G','A','M','E','S' };// { 'M', 'O', 'N', 'E', 'Y' };
vector<char> allLetters;
unordered_map<char, int> allLettersPos; // 'S'-0; 'E'-1...etc
vector<int> availNew{ 0,1,2,3,4,5,6,7,8,9 };

void printRes(vector<int>& curr)
{
    for (auto& c : allLetters)
    {
        cout << c << "\t" << curr[allLettersPos[c]] << "\n";
    }
    cout << endl;
}


bool testPass(vector<int>& curr)
{
    long op1l=0;
    for (auto& c : op1)
    { 
        op1l *= 10;
        op1l += curr[allLettersPos[c]];
    }
    long op2l = 0;
    for (auto& c : op2)
    {
        op2l *= 10;
        op2l += curr[allLettersPos[c]];
    }
    long resl = 0;
    for (auto& c : res)
    {
        resl *= 10;
        resl += curr[allLettersPos[c]];
    }

    return op1l + op2l == resl;
}


bool cryptWords_BT(int currPos,  vector<int>& curr, vector<int>& avail)
{
    if (curr.back()!=INT_MIN) 
    {
        if (testPass(curr))
        {
            printRes(curr);
            return true;
        }
        else
            return false;
    }

    avail = availNew;
    for (auto& c : curr)
    {
        if (c != INT_MIN)
            avail[c] = INT_MIN;
    }

    for (auto& c : avail)
    {
        if (c != INT_MIN)
        {
            curr[currPos] = c;
            cryptWords_BT(currPos+1, curr, avail);
            curr[currPos] = INT_MIN;//undo; backtrack
        }
    }
}

int main()
{
    set<char> allLettersSet;
    for (auto& c : op1)
        allLettersSet.insert(c);
    for (auto& c : op2)
        allLettersSet.insert(c);
    for (auto& c : res)
        allLettersSet.insert(c);

    allLetters = vector<char>(allLettersSet.begin(), allLettersSet.end());

    int i = 0;
    for (auto it = allLetters.begin(); it != allLetters.end(); it++)
    {
        allLettersPos[*it] = i;
        i++;
    }

    vector<int> curr(allLetters.size(), INT_MIN);
    vector<int> avail(availNew);
    cryptWords_BT(0, curr, avail);
    return 0;
}