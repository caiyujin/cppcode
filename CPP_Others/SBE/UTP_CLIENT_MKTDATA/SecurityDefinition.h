/* Generated SBE (Simple Binary Encoding) message codec */
#ifndef _UTP_CLIENT_MKTDATA_SECURITYDEFINITION_H_
#define _UTP_CLIENT_MKTDATA_SECURITYDEFINITION_H_

#if defined(SBE_HAVE_CMATH)
/* cmath needed for std::numeric_limits<double>::quiet_NaN() */
#  include <cmath>
#  define SBE_FLOAT_NAN std::numeric_limits<float>::quiet_NaN()
#  define SBE_DOUBLE_NAN std::numeric_limits<double>::quiet_NaN()
#else
/* math.h needed for NAN */
#  include <math.h>
#  define SBE_FLOAT_NAN NAN
#  define SBE_DOUBLE_NAN NAN
#endif

#if __cplusplus >= 201103L
#  include <cstdint>
#  include <string>
#  include <cstring>
#endif

#if __cplusplus >= 201103L
#  define SBE_CONSTEXPR constexpr
#  define SBE_NOEXCEPT noexcept
#else
#  define SBE_CONSTEXPR
#  define SBE_NOEXCEPT
#endif

#if !defined(__STDC_LIMIT_MACROS)
#  define __STDC_LIMIT_MACROS 1
#endif
#include <cstdint>
#include <cstring>
#include <limits>
#include <stdexcept>

#include <ostream>

#if defined(WIN32) || defined(_WIN32)
#  define SBE_BIG_ENDIAN_ENCODE_16(v) _byteswap_ushort(v)
#  define SBE_BIG_ENDIAN_ENCODE_32(v) _byteswap_ulong(v)
#  define SBE_BIG_ENDIAN_ENCODE_64(v) _byteswap_uint64(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_16(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_32(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_64(v) (v)
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define SBE_BIG_ENDIAN_ENCODE_16(v) __builtin_bswap16(v)
#  define SBE_BIG_ENDIAN_ENCODE_32(v) __builtin_bswap32(v)
#  define SBE_BIG_ENDIAN_ENCODE_64(v) __builtin_bswap64(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_16(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_32(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_64(v) (v)
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#  define SBE_LITTLE_ENDIAN_ENCODE_16(v) __builtin_bswap16(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_32(v) __builtin_bswap32(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_64(v) __builtin_bswap64(v)
#  define SBE_BIG_ENDIAN_ENCODE_16(v) (v)
#  define SBE_BIG_ENDIAN_ENCODE_32(v) (v)
#  define SBE_BIG_ENDIAN_ENCODE_64(v) (v)
#else
#  error "Byte Ordering of platform not determined. Set __BYTE_ORDER__ manually before including this file."
#endif

#if defined(SBE_NO_BOUNDS_CHECK)
#  define SBE_BOUNDS_CHECK_EXPECT(exp,c) (false)
#elif defined(_MSC_VER)
#  define SBE_BOUNDS_CHECK_EXPECT(exp,c) (exp)
#else
#  define SBE_BOUNDS_CHECK_EXPECT(exp,c) (__builtin_expect(exp,c))
#endif

#define SBE_NULLVALUE_INT8 (std::numeric_limits<std::int8_t>::min)()
#define SBE_NULLVALUE_INT16 (std::numeric_limits<std::int16_t>::min)()
#define SBE_NULLVALUE_INT32 (std::numeric_limits<std::int32_t>::min)()
#define SBE_NULLVALUE_INT64 (std::numeric_limits<std::int64_t>::min)()
#define SBE_NULLVALUE_UINT8 (std::numeric_limits<std::uint8_t>::max)()
#define SBE_NULLVALUE_UINT16 (std::numeric_limits<std::uint16_t>::max)()
#define SBE_NULLVALUE_UINT32 (std::numeric_limits<std::uint32_t>::max)()
#define SBE_NULLVALUE_UINT64 (std::numeric_limits<std::uint64_t>::max)()

#include "TimeOfDay.h"
#include "MessageHeader.h"
#include "SecurityUpdateAction.h"
#include "MarketDataType.h"
#include "PriceNull.h"
#include "MDUpdateAction.h"
#include "MonthYearDay.h"
#include "GroupSize.h"
#include "MDEntryType.h"
#include "AggressorSide.h"

namespace UTP_CLIENT_MKTDATA {

class SecurityDefinition
{
private:
    char *m_buffer = nullptr;
    std::uint64_t m_bufferLength = 0;
    std::uint64_t m_offset = 0;
    std::uint64_t m_position;
    std::uint64_t m_actingVersion;

    inline std::uint64_t *sbePositionPtr() SBE_NOEXCEPT
    {
        return &m_position;
    }

public:
    enum MetaAttribute
    {
        EPOCH, TIME_UNIT, SEMANTIC_TYPE, PRESENCE
    };

    union sbe_float_as_uint_u
    {
        float fp_value;
        std::uint32_t uint_value;
    };

    union sbe_double_as_uint_u
    {
        double fp_value;
        std::uint64_t uint_value;
    };

    SecurityDefinition() = default;

    SecurityDefinition(
        char *buffer,
        const std::uint64_t offset,
        const std::uint64_t bufferLength,
        const std::uint64_t actingBlockLength,
        const std::uint64_t actingVersion) :
        m_buffer(buffer),
        m_bufferLength(bufferLength),
        m_offset(offset),
        m_position(sbeCheckPosition(offset + actingBlockLength)),
        m_actingVersion(actingVersion)
    {
    }

    SecurityDefinition(char *buffer, const std::uint64_t bufferLength) :
        SecurityDefinition(buffer, 0, bufferLength, sbeBlockLength(), sbeSchemaVersion())
    {
    }

    SecurityDefinition(char *buffer, const std::uint64_t bufferLength, const std::uint64_t actingBlockLength, const std::uint64_t actingVersion) :
        SecurityDefinition(buffer, 0, bufferLength, actingBlockLength, actingVersion)
    {
    }

    static SBE_CONSTEXPR std::uint16_t sbeBlockLength() SBE_NOEXCEPT
    {
        return (std::uint16_t)74;
    }

    static SBE_CONSTEXPR std::uint16_t sbeTemplateId() SBE_NOEXCEPT
    {
        return (std::uint16_t)18;
    }

    static SBE_CONSTEXPR std::uint16_t sbeSchemaId() SBE_NOEXCEPT
    {
        return (std::uint16_t)100;
    }

    static SBE_CONSTEXPR std::uint16_t sbeSchemaVersion() SBE_NOEXCEPT
    {
        return (std::uint16_t)2;
    }

    static SBE_CONSTEXPR const char * sbeSemanticType() SBE_NOEXCEPT
    {
        return "d";
    }

    std::uint64_t offset() const SBE_NOEXCEPT
    {
        return m_offset;
    }

    SecurityDefinition &wrapForEncode(char *buffer, const std::uint64_t offset, const std::uint64_t bufferLength)
    {
        return *this = SecurityDefinition(buffer, offset, bufferLength, sbeBlockLength(), sbeSchemaVersion());
    }

    SecurityDefinition &wrapAndApplyHeader(char *buffer, const std::uint64_t offset, const std::uint64_t bufferLength)
    {
        MessageHeader hdr(buffer, offset, bufferLength, sbeSchemaVersion());

        hdr
            .blockLength(sbeBlockLength())
            .templateId(sbeTemplateId())
            .schemaId(sbeSchemaId())
            .version(sbeSchemaVersion());

        return *this = SecurityDefinition(
            buffer,
            offset + MessageHeader::encodedLength(),
            bufferLength,
            sbeBlockLength(),
            sbeSchemaVersion());
    }

    SecurityDefinition &wrapForDecode(
        char *buffer, const std::uint64_t offset, const std::uint64_t actingBlockLength,
        const std::uint64_t actingVersion, const std::uint64_t bufferLength)
    {
        return *this = SecurityDefinition(buffer, offset, bufferLength, actingBlockLength, actingVersion);
    }

    std::uint64_t sbePosition() const SBE_NOEXCEPT
    {
        return m_position;
    }

    std::uint64_t sbeCheckPosition(const std::uint64_t position)
    {
        if (SBE_BOUNDS_CHECK_EXPECT((position > m_bufferLength), false))
        {
            throw std::runtime_error("buffer too short [E100]");
        }
        return position;
    }

    void sbePosition(const std::uint64_t position)
    {
        m_position = sbeCheckPosition(position);
    }

    std::uint64_t encodedLength() const SBE_NOEXCEPT
    {
        return sbePosition() - m_offset;
    }

    const char * buffer() const SBE_NOEXCEPT
    {
        return m_buffer;
    }

    char * buffer() SBE_NOEXCEPT
    {
        return m_buffer;
    }

    std::uint64_t bufferLength() const SBE_NOEXCEPT
    {
        return m_bufferLength;
    }

    std::uint64_t actingVersion() const SBE_NOEXCEPT
    {
        return m_actingVersion;
    }

    static const char * SecurityUpdateActionMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Char";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t securityUpdateActionId() SBE_NOEXCEPT
    {
        return 980;
    }

    static SBE_CONSTEXPR std::uint64_t securityUpdateActionSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool securityUpdateActionInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= securityUpdateActionSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t securityUpdateActionEncodingOffset() SBE_NOEXCEPT
    {
        return 0;
    }

    static SBE_CONSTEXPR std::size_t securityUpdateActionEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    SecurityUpdateAction::Value securityUpdateAction() const
    {
        char val;
        std::memcpy(&val, m_buffer + m_offset + 0, sizeof(char));
        return SecurityUpdateAction::get((val));
    }

    SecurityDefinition &securityUpdateAction(const SecurityUpdateAction::Value value)
    {
        char val = (value);
        std::memcpy(m_buffer + m_offset + 0, &val, sizeof(char));
        return *this;
    }

    static const char * LastUpdateTimeMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "UTCTimestamp";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t lastUpdateTimeId() SBE_NOEXCEPT
    {
        return 779;
    }

    static SBE_CONSTEXPR std::uint64_t lastUpdateTimeSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool lastUpdateTimeInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= lastUpdateTimeSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t lastUpdateTimeEncodingOffset() SBE_NOEXCEPT
    {
        return 1;
    }

    static SBE_CONSTEXPR std::uint64_t lastUpdateTimeNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT64;
    }

    static SBE_CONSTEXPR std::uint64_t lastUpdateTimeMinValue() SBE_NOEXCEPT
    {
        return 0x0L;
    }

    static SBE_CONSTEXPR std::uint64_t lastUpdateTimeMaxValue() SBE_NOEXCEPT
    {
        return 0xfffffffffffffffeL;
    }

    static SBE_CONSTEXPR std::size_t lastUpdateTimeEncodingLength() SBE_NOEXCEPT
    {
        return 8;
    }

    std::uint64_t lastUpdateTime() const
    {
        std::uint64_t val;
        std::memcpy(&val, m_buffer + m_offset + 1, sizeof(std::uint64_t));
        return SBE_LITTLE_ENDIAN_ENCODE_64(val);
    }

    SecurityDefinition &lastUpdateTime(const std::uint64_t value)
    {
        std::uint64_t val = SBE_LITTLE_ENDIAN_ENCODE_64(value);
        std::memcpy(m_buffer + m_offset + 1, &val, sizeof(std::uint64_t));
        return *this;
    }

    static const char * SymbolMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "String";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t symbolId() SBE_NOEXCEPT
    {
        return 55;
    }

    static SBE_CONSTEXPR std::uint64_t symbolSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool symbolInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= symbolSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t symbolEncodingOffset() SBE_NOEXCEPT
    {
        return 9;
    }

    static SBE_CONSTEXPR char symbolNullValue() SBE_NOEXCEPT
    {
        return (char)0;
    }

    static SBE_CONSTEXPR char symbolMinValue() SBE_NOEXCEPT
    {
        return (char)32;
    }

    static SBE_CONSTEXPR char symbolMaxValue() SBE_NOEXCEPT
    {
        return (char)126;
    }

    static SBE_CONSTEXPR std::size_t symbolEncodingLength() SBE_NOEXCEPT
    {
        return 16;
    }

    static SBE_CONSTEXPR std::uint64_t symbolLength() SBE_NOEXCEPT
    {
        return 16;
    }

    const char *symbol() const SBE_NOEXCEPT
    {
        return m_buffer + m_offset + 9;
    }

    char *symbol() SBE_NOEXCEPT
    {
        return m_buffer + m_offset + 9;
    }

    char symbol(const std::uint64_t index) const
    {
        if (index >= 16)
        {
            throw std::runtime_error("index out of range for symbol [E104]");
        }

        char val;
        std::memcpy(&val, m_buffer + m_offset + 9 + (index * 1), sizeof(char));
        return (val);
    }

    SecurityDefinition &symbol(const std::uint64_t index, const char value)
    {
        if (index >= 16)
        {
            throw std::runtime_error("index out of range for symbol [E105]");
        }

        char val = (value);
        std::memcpy(m_buffer + m_offset + 9 + (index * 1), &val, sizeof(char));
        return *this;
    }

    std::uint64_t getSymbol(char *const dst, const std::uint64_t length) const
    {
        if (length > 16)
        {
            throw std::runtime_error("length too large for getSymbol [E106]");
        }

        std::memcpy(dst, m_buffer + m_offset + 9, sizeof(char) * length);
        return length;
    }

    SecurityDefinition &putSymbol(const char *const src) SBE_NOEXCEPT
    {
        std::memcpy(m_buffer + m_offset + 9, src, sizeof(char) * 16);
        return *this;
    }

    std::string getSymbolAsString() const
    {
        const char *buffer = m_buffer + m_offset + 9;
        size_t length = 0;

        for (; length < 16 && *(buffer + length) != '\0'; ++length);
        std::string result(buffer, length);

        return result;
    }

    #if __cplusplus >= 201703L
    std::string_view getSymbolAsStringView() const SBE_NOEXCEPT
    {
        const char *buffer = m_buffer + m_offset + 9;
        size_t length = 0;

        for (; length < 16 && *(buffer + length) != '\0'; ++length);
        std::string_view result(buffer, length);

        return result;
    }
    #endif

    #if __cplusplus >= 201703L
    SecurityDefinition &putSymbol(const std::string_view str)
    {
        const size_t srcLength = str.length();
        if (srcLength > 16)
        {
            throw std::runtime_error("string too large for putSymbol [E106]");
        }

        std::memcpy(m_buffer + m_offset + 9, str.data(), srcLength);
        for (size_t start = srcLength; start < 16; ++start)
        {
            m_buffer[m_offset + 9 + start] = 0;
        }

        return *this;
    }
    #else
    SecurityDefinition &putSymbol(const std::string& str)
    {
        const size_t srcLength = str.length();
        if (srcLength > 16)
        {
            throw std::runtime_error("string too large for putSymbol [E106]");
        }

        std::memcpy(m_buffer + m_offset + 9, str.c_str(), srcLength);
        for (size_t start = srcLength; start < 16; ++start)
        {
            m_buffer[m_offset + 9 + start] = 0;
        }

        return *this;
    }
    #endif

    static const char * SecurityIDMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t securityIDId() SBE_NOEXCEPT
    {
        return 48;
    }

    static SBE_CONSTEXPR std::uint64_t securityIDSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool securityIDInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= securityIDSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t securityIDEncodingOffset() SBE_NOEXCEPT
    {
        return 25;
    }

    static SBE_CONSTEXPR std::int32_t securityIDNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_INT32;
    }

    static SBE_CONSTEXPR std::int32_t securityIDMinValue() SBE_NOEXCEPT
    {
        return -2147483647;
    }

    static SBE_CONSTEXPR std::int32_t securityIDMaxValue() SBE_NOEXCEPT
    {
        return 2147483647;
    }

    static SBE_CONSTEXPR std::size_t securityIDEncodingLength() SBE_NOEXCEPT
    {
        return 4;
    }

    std::int32_t securityID() const
    {
        std::int32_t val;
        std::memcpy(&val, m_buffer + m_offset + 25, sizeof(std::int32_t));
        return SBE_LITTLE_ENDIAN_ENCODE_32(val);
    }

    SecurityDefinition &securityID(const std::int32_t value)
    {
        std::int32_t val = SBE_LITTLE_ENDIAN_ENCODE_32(value);
        std::memcpy(m_buffer + m_offset + 25, &val, sizeof(std::int32_t));
        return *this;
    }

    static const char * SecurityIDSourceMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t securityIDSourceId() SBE_NOEXCEPT
    {
        return 22;
    }

    static SBE_CONSTEXPR std::uint64_t securityIDSourceSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool securityIDSourceInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= securityIDSourceSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t securityIDSourceEncodingOffset() SBE_NOEXCEPT
    {
        return 29;
    }

    static SBE_CONSTEXPR std::uint8_t securityIDSourceNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT8;
    }

    static SBE_CONSTEXPR std::uint8_t securityIDSourceMinValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)0;
    }

    static SBE_CONSTEXPR std::uint8_t securityIDSourceMaxValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)254;
    }

    static SBE_CONSTEXPR std::size_t securityIDSourceEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    std::uint8_t securityIDSource() const
    {
        std::uint8_t val;
        std::memcpy(&val, m_buffer + m_offset + 29, sizeof(std::uint8_t));
        return (val);
    }

    SecurityDefinition &securityIDSource(const std::uint8_t value)
    {
        std::uint8_t val = (value);
        std::memcpy(m_buffer + m_offset + 29, &val, sizeof(std::uint8_t));
        return *this;
    }

    static const char * SecurityTypeMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t securityTypeId() SBE_NOEXCEPT
    {
        return 167;
    }

    static SBE_CONSTEXPR std::uint64_t securityTypeSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool securityTypeInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= securityTypeSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t securityTypeEncodingOffset() SBE_NOEXCEPT
    {
        return 30;
    }

    static SBE_CONSTEXPR std::size_t securityTypeEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    MarketDataType::Value securityType() const
    {
        std::int8_t val;
        std::memcpy(&val, m_buffer + m_offset + 30, sizeof(std::int8_t));
        return MarketDataType::get((val));
    }

    SecurityDefinition &securityType(const MarketDataType::Value value)
    {
        std::int8_t val = (value);
        std::memcpy(m_buffer + m_offset + 30, &val, sizeof(std::int8_t));
        return *this;
    }

    static const char * SettlDateMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "LocalMktDate";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t settlDateId() SBE_NOEXCEPT
    {
        return 64;
    }

    static SBE_CONSTEXPR std::uint64_t settlDateSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool settlDateInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= settlDateSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t settlDateEncodingOffset() SBE_NOEXCEPT
    {
        return 31;
    }

private:
    MonthYearDay m_settlDate;

public:
    MonthYearDay &settlDate()
    {
        m_settlDate.wrap(m_buffer, m_offset + 31, m_actingVersion, m_bufferLength);
        return m_settlDate;
    }

    static const char * Currency1MetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Currency";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t currency1Id() SBE_NOEXCEPT
    {
        return 30375;
    }

    static SBE_CONSTEXPR std::uint64_t currency1SinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool currency1InActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= currency1SinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t currency1EncodingOffset() SBE_NOEXCEPT
    {
        return 35;
    }

    static SBE_CONSTEXPR char currency1NullValue() SBE_NOEXCEPT
    {
        return (char)0;
    }

    static SBE_CONSTEXPR char currency1MinValue() SBE_NOEXCEPT
    {
        return (char)32;
    }

    static SBE_CONSTEXPR char currency1MaxValue() SBE_NOEXCEPT
    {
        return (char)126;
    }

    static SBE_CONSTEXPR std::size_t currency1EncodingLength() SBE_NOEXCEPT
    {
        return 3;
    }

    static SBE_CONSTEXPR std::uint64_t currency1Length() SBE_NOEXCEPT
    {
        return 3;
    }

    const char *currency1() const SBE_NOEXCEPT
    {
        return m_buffer + m_offset + 35;
    }

    char *currency1() SBE_NOEXCEPT
    {
        return m_buffer + m_offset + 35;
    }

    char currency1(const std::uint64_t index) const
    {
        if (index >= 3)
        {
            throw std::runtime_error("index out of range for currency1 [E104]");
        }

        char val;
        std::memcpy(&val, m_buffer + m_offset + 35 + (index * 1), sizeof(char));
        return (val);
    }

    SecurityDefinition &currency1(const std::uint64_t index, const char value)
    {
        if (index >= 3)
        {
            throw std::runtime_error("index out of range for currency1 [E105]");
        }

        char val = (value);
        std::memcpy(m_buffer + m_offset + 35 + (index * 1), &val, sizeof(char));
        return *this;
    }

    std::uint64_t getCurrency1(char *const dst, const std::uint64_t length) const
    {
        if (length > 3)
        {
            throw std::runtime_error("length too large for getCurrency1 [E106]");
        }

        std::memcpy(dst, m_buffer + m_offset + 35, sizeof(char) * length);
        return length;
    }

    SecurityDefinition &putCurrency1(const char *const src) SBE_NOEXCEPT
    {
        std::memcpy(m_buffer + m_offset + 35, src, sizeof(char) * 3);
        return *this;
    }

    SecurityDefinition &putCurrency1(
        const char value0,
        const char value1,
        const char value2) SBE_NOEXCEPT
    {
        char val0 = (value0);
        std::memcpy(m_buffer + m_offset + 35, &val0, sizeof(char));
        char val1 = (value1);
        std::memcpy(m_buffer + m_offset + 36, &val1, sizeof(char));
        char val2 = (value2);
        std::memcpy(m_buffer + m_offset + 37, &val2, sizeof(char));

        return *this;
    }

    std::string getCurrency1AsString() const
    {
        const char *buffer = m_buffer + m_offset + 35;
        size_t length = 0;

        for (; length < 3 && *(buffer + length) != '\0'; ++length);
        std::string result(buffer, length);

        return result;
    }

    #if __cplusplus >= 201703L
    std::string_view getCurrency1AsStringView() const SBE_NOEXCEPT
    {
        const char *buffer = m_buffer + m_offset + 35;
        size_t length = 0;

        for (; length < 3 && *(buffer + length) != '\0'; ++length);
        std::string_view result(buffer, length);

        return result;
    }
    #endif

    #if __cplusplus >= 201703L
    SecurityDefinition &putCurrency1(const std::string_view str)
    {
        const size_t srcLength = str.length();
        if (srcLength > 3)
        {
            throw std::runtime_error("string too large for putCurrency1 [E106]");
        }

        std::memcpy(m_buffer + m_offset + 35, str.data(), srcLength);
        for (size_t start = srcLength; start < 3; ++start)
        {
            m_buffer[m_offset + 35 + start] = 0;
        }

        return *this;
    }
    #else
    SecurityDefinition &putCurrency1(const std::string& str)
    {
        const size_t srcLength = str.length();
        if (srcLength > 3)
        {
            throw std::runtime_error("string too large for putCurrency1 [E106]");
        }

        std::memcpy(m_buffer + m_offset + 35, str.c_str(), srcLength);
        for (size_t start = srcLength; start < 3; ++start)
        {
            m_buffer[m_offset + 35 + start] = 0;
        }

        return *this;
    }
    #endif

    static const char * Currency2MetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Currency";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t currency2Id() SBE_NOEXCEPT
    {
        return 30376;
    }

    static SBE_CONSTEXPR std::uint64_t currency2SinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool currency2InActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= currency2SinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t currency2EncodingOffset() SBE_NOEXCEPT
    {
        return 38;
    }

    static SBE_CONSTEXPR char currency2NullValue() SBE_NOEXCEPT
    {
        return (char)0;
    }

    static SBE_CONSTEXPR char currency2MinValue() SBE_NOEXCEPT
    {
        return (char)32;
    }

    static SBE_CONSTEXPR char currency2MaxValue() SBE_NOEXCEPT
    {
        return (char)126;
    }

    static SBE_CONSTEXPR std::size_t currency2EncodingLength() SBE_NOEXCEPT
    {
        return 3;
    }

    static SBE_CONSTEXPR std::uint64_t currency2Length() SBE_NOEXCEPT
    {
        return 3;
    }

    const char *currency2() const SBE_NOEXCEPT
    {
        return m_buffer + m_offset + 38;
    }

    char *currency2() SBE_NOEXCEPT
    {
        return m_buffer + m_offset + 38;
    }

    char currency2(const std::uint64_t index) const
    {
        if (index >= 3)
        {
            throw std::runtime_error("index out of range for currency2 [E104]");
        }

        char val;
        std::memcpy(&val, m_buffer + m_offset + 38 + (index * 1), sizeof(char));
        return (val);
    }

    SecurityDefinition &currency2(const std::uint64_t index, const char value)
    {
        if (index >= 3)
        {
            throw std::runtime_error("index out of range for currency2 [E105]");
        }

        char val = (value);
        std::memcpy(m_buffer + m_offset + 38 + (index * 1), &val, sizeof(char));
        return *this;
    }

    std::uint64_t getCurrency2(char *const dst, const std::uint64_t length) const
    {
        if (length > 3)
        {
            throw std::runtime_error("length too large for getCurrency2 [E106]");
        }

        std::memcpy(dst, m_buffer + m_offset + 38, sizeof(char) * length);
        return length;
    }

    SecurityDefinition &putCurrency2(const char *const src) SBE_NOEXCEPT
    {
        std::memcpy(m_buffer + m_offset + 38, src, sizeof(char) * 3);
        return *this;
    }

    SecurityDefinition &putCurrency2(
        const char value0,
        const char value1,
        const char value2) SBE_NOEXCEPT
    {
        char val0 = (value0);
        std::memcpy(m_buffer + m_offset + 38, &val0, sizeof(char));
        char val1 = (value1);
        std::memcpy(m_buffer + m_offset + 39, &val1, sizeof(char));
        char val2 = (value2);
        std::memcpy(m_buffer + m_offset + 40, &val2, sizeof(char));

        return *this;
    }

    std::string getCurrency2AsString() const
    {
        const char *buffer = m_buffer + m_offset + 38;
        size_t length = 0;

        for (; length < 3 && *(buffer + length) != '\0'; ++length);
        std::string result(buffer, length);

        return result;
    }

    #if __cplusplus >= 201703L
    std::string_view getCurrency2AsStringView() const SBE_NOEXCEPT
    {
        const char *buffer = m_buffer + m_offset + 38;
        size_t length = 0;

        for (; length < 3 && *(buffer + length) != '\0'; ++length);
        std::string_view result(buffer, length);

        return result;
    }
    #endif

    #if __cplusplus >= 201703L
    SecurityDefinition &putCurrency2(const std::string_view str)
    {
        const size_t srcLength = str.length();
        if (srcLength > 3)
        {
            throw std::runtime_error("string too large for putCurrency2 [E106]");
        }

        std::memcpy(m_buffer + m_offset + 38, str.data(), srcLength);
        for (size_t start = srcLength; start < 3; ++start)
        {
            m_buffer[m_offset + 38 + start] = 0;
        }

        return *this;
    }
    #else
    SecurityDefinition &putCurrency2(const std::string& str)
    {
        const size_t srcLength = str.length();
        if (srcLength > 3)
        {
            throw std::runtime_error("string too large for putCurrency2 [E106]");
        }

        std::memcpy(m_buffer + m_offset + 38, str.c_str(), srcLength);
        for (size_t start = srcLength; start < 3; ++start)
        {
            m_buffer[m_offset + 38 + start] = 0;
        }

        return *this;
    }
    #endif

    static const char * RatePrecisionMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t ratePrecisionId() SBE_NOEXCEPT
    {
        return 30379;
    }

    static SBE_CONSTEXPR std::uint64_t ratePrecisionSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool ratePrecisionInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= ratePrecisionSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t ratePrecisionEncodingOffset() SBE_NOEXCEPT
    {
        return 41;
    }

    static SBE_CONSTEXPR std::uint8_t ratePrecisionNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT8;
    }

    static SBE_CONSTEXPR std::uint8_t ratePrecisionMinValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)0;
    }

    static SBE_CONSTEXPR std::uint8_t ratePrecisionMaxValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)254;
    }

    static SBE_CONSTEXPR std::size_t ratePrecisionEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    std::uint8_t ratePrecision() const
    {
        std::uint8_t val;
        std::memcpy(&val, m_buffer + m_offset + 41, sizeof(std::uint8_t));
        return (val);
    }

    SecurityDefinition &ratePrecision(const std::uint8_t value)
    {
        std::uint8_t val = (value);
        std::memcpy(m_buffer + m_offset + 41, &val, sizeof(std::uint8_t));
        return *this;
    }

    static const char * LeftDpsMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t leftDpsId() SBE_NOEXCEPT
    {
        return 30434;
    }

    static SBE_CONSTEXPR std::uint64_t leftDpsSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool leftDpsInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= leftDpsSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t leftDpsEncodingOffset() SBE_NOEXCEPT
    {
        return 42;
    }

    static SBE_CONSTEXPR std::uint8_t leftDpsNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT8;
    }

    static SBE_CONSTEXPR std::uint8_t leftDpsMinValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)0;
    }

    static SBE_CONSTEXPR std::uint8_t leftDpsMaxValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)254;
    }

    static SBE_CONSTEXPR std::size_t leftDpsEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    std::uint8_t leftDps() const
    {
        std::uint8_t val;
        std::memcpy(&val, m_buffer + m_offset + 42, sizeof(std::uint8_t));
        return (val);
    }

    SecurityDefinition &leftDps(const std::uint8_t value)
    {
        std::uint8_t val = (value);
        std::memcpy(m_buffer + m_offset + 42, &val, sizeof(std::uint8_t));
        return *this;
    }

    static const char * RightDpsMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t rightDpsId() SBE_NOEXCEPT
    {
        return 30435;
    }

    static SBE_CONSTEXPR std::uint64_t rightDpsSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool rightDpsInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= rightDpsSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t rightDpsEncodingOffset() SBE_NOEXCEPT
    {
        return 43;
    }

    static SBE_CONSTEXPR std::uint8_t rightDpsNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT8;
    }

    static SBE_CONSTEXPR std::uint8_t rightDpsMinValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)0;
    }

    static SBE_CONSTEXPR std::uint8_t rightDpsMaxValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)254;
    }

    static SBE_CONSTEXPR std::size_t rightDpsEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    std::uint8_t rightDps() const
    {
        std::uint8_t val;
        std::memcpy(&val, m_buffer + m_offset + 43, sizeof(std::uint8_t));
        return (val);
    }

    SecurityDefinition &rightDps(const std::uint8_t value)
    {
        std::uint8_t val = (value);
        std::memcpy(m_buffer + m_offset + 43, &val, sizeof(std::uint8_t));
        return *this;
    }

    static const char * CLSMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t cLSId() SBE_NOEXCEPT
    {
        return 30436;
    }

    static SBE_CONSTEXPR std::uint64_t cLSSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool cLSInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= cLSSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t cLSEncodingOffset() SBE_NOEXCEPT
    {
        return 44;
    }

    static SBE_CONSTEXPR std::uint8_t cLSNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT8;
    }

    static SBE_CONSTEXPR std::uint8_t cLSMinValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)0;
    }

    static SBE_CONSTEXPR std::uint8_t cLSMaxValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)254;
    }

    static SBE_CONSTEXPR std::size_t cLSEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    std::uint8_t cLS() const
    {
        std::uint8_t val;
        std::memcpy(&val, m_buffer + m_offset + 44, sizeof(std::uint8_t));
        return (val);
    }

    SecurityDefinition &cLS(const std::uint8_t value)
    {
        std::uint8_t val = (value);
        std::memcpy(m_buffer + m_offset + 44, &val, sizeof(std::uint8_t));
        return *this;
    }

    static const char * TR_BookDepthPriceThresholdMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Price";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t tR_BookDepthPriceThresholdId() SBE_NOEXCEPT
    {
        return 31373;
    }

    static SBE_CONSTEXPR std::uint64_t tR_BookDepthPriceThresholdSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool tR_BookDepthPriceThresholdInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= tR_BookDepthPriceThresholdSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t tR_BookDepthPriceThresholdEncodingOffset() SBE_NOEXCEPT
    {
        return 45;
    }

private:
    PriceNull m_tR_BookDepthPriceThreshold;

public:
    PriceNull &tR_BookDepthPriceThreshold()
    {
        m_tR_BookDepthPriceThreshold.wrap(m_buffer, m_offset + 45, m_actingVersion, m_bufferLength);
        return m_tR_BookDepthPriceThreshold;
    }

    static const char * SnapshotConflationIntervalMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Time";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t snapshotConflationIntervalId() SBE_NOEXCEPT
    {
        return 30437;
    }

    static SBE_CONSTEXPR std::uint64_t snapshotConflationIntervalSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool snapshotConflationIntervalInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= snapshotConflationIntervalSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t snapshotConflationIntervalEncodingOffset() SBE_NOEXCEPT
    {
        return 53;
    }

private:
    TimeOfDay m_snapshotConflationInterval;

public:
    TimeOfDay &snapshotConflationInterval()
    {
        m_snapshotConflationInterval.wrap(m_buffer, m_offset + 53, m_actingVersion, m_bufferLength);
        return m_snapshotConflationInterval;
    }

    static const char * IncRefreshConflationIntervalMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Time";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t incRefreshConflationIntervalId() SBE_NOEXCEPT
    {
        return 30438;
    }

    static SBE_CONSTEXPR std::uint64_t incRefreshConflationIntervalSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool incRefreshConflationIntervalInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= incRefreshConflationIntervalSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t incRefreshConflationIntervalEncodingOffset() SBE_NOEXCEPT
    {
        return 58;
    }

private:
    TimeOfDay m_incRefreshConflationInterval;

public:
    TimeOfDay &incRefreshConflationInterval()
    {
        m_incRefreshConflationInterval.wrap(m_buffer, m_offset + 58, m_actingVersion, m_bufferLength);
        return m_incRefreshConflationInterval;
    }

    static const char * TradesFeedConflationIntervalMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Time";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t tradesFeedConflationIntervalId() SBE_NOEXCEPT
    {
        return 30430;
    }

    static SBE_CONSTEXPR std::uint64_t tradesFeedConflationIntervalSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool tradesFeedConflationIntervalInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= tradesFeedConflationIntervalSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t tradesFeedConflationIntervalEncodingOffset() SBE_NOEXCEPT
    {
        return 63;
    }

private:
    TimeOfDay m_tradesFeedConflationInterval;

public:
    TimeOfDay &tradesFeedConflationInterval()
    {
        m_tradesFeedConflationInterval.wrap(m_buffer, m_offset + 63, m_actingVersion, m_bufferLength);
        return m_tradesFeedConflationInterval;
    }

    static const char * SecurityDefinitionConflationIntervalMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Time";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t securityDefinitionConflationIntervalId() SBE_NOEXCEPT
    {
        return 30431;
    }

    static SBE_CONSTEXPR std::uint64_t securityDefinitionConflationIntervalSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool securityDefinitionConflationIntervalInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= securityDefinitionConflationIntervalSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t securityDefinitionConflationIntervalEncodingOffset() SBE_NOEXCEPT
    {
        return 68;
    }

private:
    TimeOfDay m_securityDefinitionConflationInterval;

public:
    TimeOfDay &securityDefinitionConflationInterval()
    {
        m_securityDefinitionConflationInterval.wrap(m_buffer, m_offset + 68, m_actingVersion, m_bufferLength);
        return m_securityDefinitionConflationInterval;
    }

    static const char * DepthOfBookMetaAttribute(const MetaAttribute metaAttribute) SBE_NOEXCEPT
    {
        switch (metaAttribute)
        {
            case MetaAttribute::EPOCH: return "";
            case MetaAttribute::TIME_UNIT: return "";
            case MetaAttribute::SEMANTIC_TYPE: return "Int";
            case MetaAttribute::PRESENCE: return "required";
        }

        return "";
    }

    static SBE_CONSTEXPR std::uint16_t depthOfBookId() SBE_NOEXCEPT
    {
        return 30439;
    }

    static SBE_CONSTEXPR std::uint64_t depthOfBookSinceVersion() SBE_NOEXCEPT
    {
        return 0;
    }

    bool depthOfBookInActingVersion() SBE_NOEXCEPT
    {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
        return m_actingVersion >= depthOfBookSinceVersion();
#if defined(__clang__)
#pragma clang diagnostic pop
#endif
    }

    static SBE_CONSTEXPR std::size_t depthOfBookEncodingOffset() SBE_NOEXCEPT
    {
        return 73;
    }

    static SBE_CONSTEXPR std::uint8_t depthOfBookNullValue() SBE_NOEXCEPT
    {
        return SBE_NULLVALUE_UINT8;
    }

    static SBE_CONSTEXPR std::uint8_t depthOfBookMinValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)0;
    }

    static SBE_CONSTEXPR std::uint8_t depthOfBookMaxValue() SBE_NOEXCEPT
    {
        return (std::uint8_t)254;
    }

    static SBE_CONSTEXPR std::size_t depthOfBookEncodingLength() SBE_NOEXCEPT
    {
        return 1;
    }

    std::uint8_t depthOfBook() const
    {
        std::uint8_t val;
        std::memcpy(&val, m_buffer + m_offset + 73, sizeof(std::uint8_t));
        return (val);
    }

    SecurityDefinition &depthOfBook(const std::uint8_t value)
    {
        std::uint8_t val = (value);
        std::memcpy(m_buffer + m_offset + 73, &val, sizeof(std::uint8_t));
        return *this;
    }

    template<typename CharT, typename Traits>
    friend std::basic_ostream<CharT, Traits>& operator<<(
        std::basic_ostream<CharT, Traits>& builder, SecurityDefinition _writer)
    {
        SecurityDefinition writer(_writer.m_buffer, _writer.m_offset,
            _writer.m_bufferLength, _writer.sbeBlockLength(), _writer.m_actingVersion);
        builder << '{';
        builder << R"("Name": "SecurityDefinition", )";
        builder << R"("sbeTemplateId": )";
        builder << writer.sbeTemplateId();
        builder << ", ";

        //Token{signal=BEGIN_ENUM, name='SecurityUpdateAction', referencedName='null', description='null', id=-1, version=0, deprecated=0, encodedLength=1, offset=0, componentTokenCount=6, encoding=Encoding{presence=REQUIRED, primitiveType=CHAR, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Char'}}
        builder << R"("SecurityUpdateAction": )";
        builder << '"' << writer.securityUpdateAction() << '"';

        //Token{signal=ENCODING, name='TransactTime', referencedName='null', description='Start of event processing time in number of nanoseconds since Unix epoch', id=-1, version=0, deprecated=0, encodedLength=8, offset=1, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT64, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='UTCTimestamp'}}
        builder << ", ";
        builder << R"("LastUpdateTime": )";
        builder << +writer.lastUpdateTime();

        //Token{signal=ENCODING, name='Symbol', referencedName='null', description='Symbol', id=-1, version=0, deprecated=0, encodedLength=16, offset=9, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=CHAR, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='US-ASCII', epoch='null', timeUnit=null, semanticType='String'}}
        builder << ", ";
        builder << R"("Symbol": )";
        builder << '"';
        for (size_t i = 0;
            i < writer.symbolLength() && writer.symbol(i) > 0;
            i++)
        {
            builder << (char)writer.symbol(i);
        }
        builder << '"';

        //Token{signal=ENCODING, name='SecurityID', referencedName='null', description='integer Id of symbol', id=-1, version=0, deprecated=0, encodedLength=4, offset=25, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=INT32, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("SecurityID": )";
        builder << +writer.securityID();

        //Token{signal=ENCODING, name='SecurityIDSource', referencedName='null', description='SecurityIDSource always 8 for TR', id=-1, version=0, deprecated=0, encodedLength=1, offset=29, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("SecurityIDSource": )";
        builder << +writer.securityIDSource();

        //Token{signal=BEGIN_ENUM, name='MarketDataType', referencedName='null', description='null', id=-1, version=0, deprecated=0, encodedLength=1, offset=30, componentTokenCount=8, encoding=Encoding{presence=REQUIRED, primitiveType=INT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("SecurityType": )";
        builder << '"' << writer.securityType() << '"';

        //Token{signal=BEGIN_COMPOSITE, name='MonthYearDay', referencedName='null', description='Year, Month and Date', id=-1, version=0, deprecated=0, encodedLength=4, offset=31, componentTokenCount=5, encoding=Encoding{presence=REQUIRED, primitiveType=null, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='LocalMktDate'}}
        builder << ", ";
        builder << R"("SettlDate": )";
        builder << writer.settlDate();

        //Token{signal=ENCODING, name='Currency', referencedName='null', description='Currency', id=-1, version=0, deprecated=0, encodedLength=3, offset=35, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=CHAR, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='US-ASCII', epoch='null', timeUnit=null, semanticType='Currency'}}
        builder << ", ";
        builder << R"("Currency1": )";
        builder << '"';
        for (size_t i = 0;
            i < writer.currency1Length() && writer.currency1(i) > 0;
            i++)
        {
            builder << (char)writer.currency1(i);
        }
        builder << '"';

        //Token{signal=ENCODING, name='Currency', referencedName='null', description='Currency', id=-1, version=0, deprecated=0, encodedLength=3, offset=38, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=CHAR, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='US-ASCII', epoch='null', timeUnit=null, semanticType='Currency'}}
        builder << ", ";
        builder << R"("Currency2": )";
        builder << '"';
        for (size_t i = 0;
            i < writer.currency2Length() && writer.currency2(i) > 0;
            i++)
        {
            builder << (char)writer.currency2(i);
        }
        builder << '"';

        //Token{signal=ENCODING, name='RatePrecision', referencedName='null', description='RatePrecision', id=-1, version=0, deprecated=0, encodedLength=1, offset=41, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("RatePrecision": )";
        builder << +writer.ratePrecision();

        //Token{signal=ENCODING, name='LeftDps', referencedName='null', description='maximum number of permitted left decimal places', id=-1, version=0, deprecated=0, encodedLength=1, offset=42, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("LeftDps": )";
        builder << +writer.leftDps();

        //Token{signal=ENCODING, name='RightDps', referencedName='null', description='maximum number of permitted right decimal places', id=-1, version=0, deprecated=0, encodedLength=1, offset=43, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("RightDps": )";
        builder << +writer.rightDps();

        //Token{signal=ENCODING, name='CLS', referencedName='null', description='Indicates if the instrument is eligible for CLS', id=-1, version=0, deprecated=0, encodedLength=1, offset=44, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("CLS": )";
        builder << +writer.cLS();

        //Token{signal=BEGIN_COMPOSITE, name='PriceNull', referencedName='null', description='Price NULL', id=-1, version=0, deprecated=0, encodedLength=8, offset=45, componentTokenCount=4, encoding=Encoding{presence=REQUIRED, primitiveType=null, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Price'}}
        builder << ", ";
        builder << R"("TR_BookDepthPriceThreshold": )";
        builder << writer.tR_BookDepthPriceThreshold();

        //Token{signal=BEGIN_COMPOSITE, name='TimeOfDay', referencedName='null', description='Hour, Minute, Second and Millisecond', id=-1, version=0, deprecated=0, encodedLength=5, offset=53, componentTokenCount=6, encoding=Encoding{presence=REQUIRED, primitiveType=null, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Time'}}
        builder << ", ";
        builder << R"("SnapshotConflationInterval": )";
        builder << writer.snapshotConflationInterval();

        //Token{signal=BEGIN_COMPOSITE, name='TimeOfDay', referencedName='null', description='Hour, Minute, Second and Millisecond', id=-1, version=0, deprecated=0, encodedLength=5, offset=58, componentTokenCount=6, encoding=Encoding{presence=REQUIRED, primitiveType=null, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Time'}}
        builder << ", ";
        builder << R"("IncRefreshConflationInterval": )";
        builder << writer.incRefreshConflationInterval();

        //Token{signal=BEGIN_COMPOSITE, name='TimeOfDay', referencedName='null', description='Hour, Minute, Second and Millisecond', id=-1, version=0, deprecated=0, encodedLength=5, offset=63, componentTokenCount=6, encoding=Encoding{presence=REQUIRED, primitiveType=null, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Time'}}
        builder << ", ";
        builder << R"("TradesFeedConflationInterval": )";
        builder << writer.tradesFeedConflationInterval();

        //Token{signal=BEGIN_COMPOSITE, name='TimeOfDay', referencedName='null', description='Hour, Minute, Second and Millisecond', id=-1, version=0, deprecated=0, encodedLength=5, offset=68, componentTokenCount=6, encoding=Encoding{presence=REQUIRED, primitiveType=null, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Time'}}
        builder << ", ";
        builder << R"("SecurityDefinitionConflationInterval": )";
        builder << writer.securityDefinitionConflationInterval();

        //Token{signal=ENCODING, name='DepthOfBook', referencedName='null', description='The number of the price levels to be displayed in the Book for the instrument', id=-1, version=0, deprecated=0, encodedLength=1, offset=73, componentTokenCount=1, encoding=Encoding{presence=REQUIRED, primitiveType=UINT8, byteOrder=LITTLE_ENDIAN, minValue=null, maxValue=null, nullValue=null, constValue=null, characterEncoding='null', epoch='null', timeUnit=null, semanticType='Int'}}
        builder << ", ";
        builder << R"("DepthOfBook": )";
        builder << +writer.depthOfBook();

        builder << '}';
        return builder;
    }
};
}
#endif
