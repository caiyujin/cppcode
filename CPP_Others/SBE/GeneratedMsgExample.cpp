/*
 * Copyright 2013-2019 Real Logic Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <iostream>
#include <string>

#include <stdio.h>
#include <inttypes.h>
#include <iomanip>

#include "UTP_CLIENT_MKTDATA/MessageHeader.h"
#include "UTP_CLIENT_MKTDATA/MDFullRefresh.h"

using namespace std;
using namespace UTP_CLIENT_MKTDATA;

const int messageHeaderVersion = 0;



const char *format(MDEntryType::Value value)
{
    switch (value)
    {
        case MDEntryType::OFFER: return "OFFER";
        case MDEntryType::BID: return "BID";
        default: return "unknown";
    }
}

const char *format(MarketDataType::Value value)
{
    switch (value)
    {
        case MarketDataType::FXSPOT: return "FXSPOT";
        case MarketDataType::FXNDF: return "FXNDF";
        case MarketDataType::FXFWD: return "FXFWD";
        case MarketDataType::FXSWAP: return "FXSWAP";
        default: return "unknown";
    }
}


std::size_t encodeHdr(MessageHeader &hdr, MDFullRefresh &MDFullRefresh_, char *buffer, std::uint64_t offset, std::uint64_t bufferLength)
{
    // encode the header
    hdr.wrap(buffer, offset, messageHeaderVersion,  bufferLength)
       .blockLength(MDFullRefresh::sbeBlockLength())
       .templateId(MDFullRefresh::sbeTemplateId())
       .schemaId(MDFullRefresh::sbeSchemaId())
       .version(MDFullRefresh::sbeSchemaVersion());

    return hdr.encodedLength();
}


std::size_t decodeHdr(MessageHeader &hdr, char *buffer, std::uint64_t offset, std::uint64_t bufferLength)
{
    hdr.wrap(buffer, offset, messageHeaderVersion, bufferLength);
    cout << "messageHeader.blockLength=" << hdr.blockLength() << endl;
    cout << "messageHeader.templateId=" << hdr.templateId() << endl;
    cout << "messageHeader.schemaId=" << hdr.schemaId() << endl;
    cout << "messageHeader.schemaVersion=" << hdr.version() << endl;
    cout << "messageHeader.encodedLength=" << hdr.encodedLength() << endl;

    return hdr.encodedLength();
}

std::size_t encodeMDFullRefresh(MDFullRefresh &MDFullRefresh_, char *buffer, std::uint64_t offset, std::uint64_t bufferLength)
{
    MDFullRefresh_.wrapForEncode(buffer, offset, bufferLength)
       .lastMsgSeqNumProcessed(100)
       .securityID(2013)
       .tR_CmnRptSeq(99)
       .tR_GlobalOrdSeqNum(uint32_t(10))
       .transactTime(uint64_t(1293495634))
       .securityType(MarketDataType::FXSPOT);               // no need MarketDataType::Value::FXSPOT...what ??
       
    cout << "securityID:" << MDFullRefresh_.securityID() << endl;  

    // dont forget the ambsign again!
    MDFullRefresh::NoMDEntries NoMDEntries_s = MDFullRefresh_.noMDEntriesCount(2);

    NoMDEntries_s.next()
        .numberofOrders(4)                                  //MDUpdateAction::NEW)
        .mDEntryType(MDEntryType::OFFER)
        .mDEntrySize(1000000);


    NoMDEntries_s.next()
        .numberofOrders(3)                                  //MDUpdateAction::NEW)
        .mDEntryType(MDEntryType::BID)
        .mDEntrySize(2000000);
        
    return MDFullRefresh_.encodedLength();
}

std::size_t decodeMDFullRefresh(
    MDFullRefresh &MDFullRefresh_, char *buffer, std::uint64_t offset, std::uint64_t actingBlockLength,
    std::uint64_t actingVersion, std::uint64_t bufferLength)
{
    MDFullRefresh_.wrapForDecode(buffer, offset, actingBlockLength, actingVersion, bufferLength);

    std::cout.setf(std::ios::fixed);

    std::cout << "\nMDFullRefresh_.lastMsgSeqNumProcessed=" << MDFullRefresh_.lastMsgSeqNumProcessed();
    std::cout << "\nMDFullRefresh_.securityID=" << MDFullRefresh_.securityID();
    std::cout << "\nMDFullRefresh_.tR_CmnRptSeq=" << MDFullRefresh_.tR_CmnRptSeq();
    std::cout << "\nMDFullRefresh_.tR_GlobalOrdSeqNum=" << MDFullRefresh_.tR_GlobalOrdSeqNum();
    std::cout << "\nMDFullRefresh_.transactTime=" << MDFullRefresh_.transactTime();
    std::cout << "\nMDFullRefresh_.securityType=" << format (MDFullRefresh_.securityType()) ;


    MDFullRefresh::NoMDEntries& NoMDEntries_s = MDFullRefresh_.noMDEntries();
    
    while (NoMDEntries_s.hasNext())
    {
        NoMDEntries_s.next();
        
        std::cout << "\nNoMDEntries_s.numberofOrders()=" << (int)NoMDEntries_s.numberofOrders();
        std::cout << "\nNNoMDEntries_s.mDEntryType()=" << format(NoMDEntries_s.mDEntryType());       
        std::cout << "\nNNoMDEntries_s.mDEntrySize()=" << NoMDEntries_s.mDEntrySize() << endl;       
        
        
    }
    return MDFullRefresh_.encodedLength();
}


int main(int argc, const char* argv[])
{
    char buffer[2048];
    MessageHeader hdr;
    MDFullRefresh MDFullRefresh_;
    
    //encode and decode essentially manuplite the same string buffer, decoder does not change the content of the encoded string.
    //it print out the message.

    std::size_t encodeHdrLength = encodeHdr(hdr, MDFullRefresh_, buffer, 0, sizeof(buffer));//use same buffer, write in the buffer.    
    std::size_t encodeMsgLength = encodeMDFullRefresh(MDFullRefresh_, buffer, hdr.encodedLength(), sizeof(buffer));//use same buffer, 

    cout << "Encoded Lengths are " << encodeHdrLength <<  " + " << encodeMsgLength << endl;
    cout << "Encoded json: '" << MDFullRefresh_ << "'" << endl;

    std::size_t decodeHdrLength = decodeHdr(hdr, buffer, 0, sizeof(buffer));
    // hdr.encodedLength() will return the length of header,  which is exactly the offset from the buffer for the body 
    std::size_t decodeMsgLength = decodeMDFullRefresh(MDFullRefresh_, buffer, hdr.encodedLength(), hdr.blockLength(), hdr.version(), sizeof(buffer));

    cout << "Decoded Lengths are " << decodeHdrLength <<  " + " << decodeMsgLength << endl;
    
    if (encodeHdrLength != decodeHdrLength)
    {
        cerr << "Encode/Decode header lengths do not match\n";
        return EXIT_FAILURE;
    }

    if (encodeMsgLength != decodeMsgLength)
    {
        cerr << "Encode/Decode message lengths do not match\n";
        return EXIT_FAILURE;
    }
    
   
    return EXIT_SUCCESS;
}
