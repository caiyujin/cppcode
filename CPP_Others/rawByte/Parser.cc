#include "Parser.h"

Parser::Parser(int date, const std::string &outputFilename)
{
    mYear=date/10000;
    date =date%10000;
    mMon=(date)/100;
    date=date%100;
    mDay=date;
    
    int status = remove(outputFilename.c_str()); //do not need check status, if file not exist and delete failed, that is fine.
#ifdef LNX    
    mFd = _open(outputFilename.c_str(),O_CREAT|O_WRONLY,0666);
#else
    mFd.open(outputFilename.c_str(), std::ios::out | std::ios::binary);
#endif    
    ADD[eADD::A_TYPE].val += char(0);
    ADD[eADD::A_TYPE].val += char(1);    
    ADD[eADD::A_MSIZE].val += char(0);
    ADD[eADD::A_MSIZE].val += char(44);
    ADD[eADD::A_PAD].val += std::string(3, char(0));
    mMsgs["A"] = &ADD;

    EXECUTE[eEXECUTE::E_TYPE].val += char(0);
    EXECUTE[eEXECUTE::E_TYPE].val += char(2);
    EXECUTE[eEXECUTE::E_MSIZE].val += char(0);
    EXECUTE[eEXECUTE::E_MSIZE].val += char(40);      
    mMsgs["E"] = &EXECUTE;
    
    CANCEL[eCANCEL::X_TYPE].val += char(0);
    CANCEL[eCANCEL::X_TYPE].val += char(3);
    CANCEL[eCANCEL::X_MSIZE].val += char(0);
    CANCEL[eCANCEL::X_MSIZE].val += char(32);    
    mMsgs["X"] = &CANCEL;
    
    REPLACE[eREPLACE::R_TYPE].val += char(0);
    REPLACE[eREPLACE::R_TYPE].val += char(4);
    REPLACE[eREPLACE::R_MSIZE].val += char(0);
    REPLACE[eREPLACE::R_MSIZE].val += char(48);
    mMsgs["R"] = &REPLACE;
}

Parser::~Parser()
{
    mFd.close();
}

void Parser::writeToOutput()
{
    mOutput="";
    mOutput.reserve(OUTPUT_RESERVE_LEN);
    for (auto& a : *(mMsgs[mType])) {
        if (a.ordered == ORDERED::YES) {// ticker, side, padding.
            mOutput += a.val;
            int shortedLenghth= static_cast<unsigned int>(a.outSize)-a.val.length();
            if (shortedLenghth>0)
            {
                mOutput += std::string(shortedLenghth, char(0));
            }                
        }

        if (a.ordered == ORDERED::NO) {
            int l = a.val.length();
            for (unsigned int i = 1; i <= a.val.length(); i++)
                mOutput += a.val[l - i];
        }
    }
    /*
    static int messageCnt =1 ;
    std::cout<< "OutPut Message length is:"<<mOutput.length()<<std::endl;
    std::cout<< "messageCnt:"<<messageCnt<<std::endl;
    messageCnt++;
    */
    mFd.write(mOutput.c_str(), mOutput.length());
#ifdef LNX
    _write(mFd, mOutput.c_str(), mOutput.length());
#endif
}

void Parser::updatePrice(std::string & s1) {
    s1 = "";
    //dont konw why the rNOerpret_cast<char*>(&mPrice) NOT able to works consistently!
    void* voidP = &mPrice;
    char* buf2 = (char*)voidP;
    for (int i = 0; i < 8; i++) {
        //HEX_PRINT(buf2[i]);
        s1 += char(buf2[i]);
    }
}

std::string Parser::updateTime(long long i) {
    using namespace std::chrono;
    system_clock::time_point tp = system_clock::now();
    system_clock::duration tp_dtn = tp.time_since_epoch(); 
    
    if (i > mPrevTime && mPrevUTCtime != -1) {
        return intToBigEndian(mPrevUTCtime + (i-mPrevTime));
    }

    mPrevTime = i;
    time_t tnow = system_clock::to_time_t(tp);
    std::tm *date = std::localtime(&tnow);
    date->tm_hour = 0;
    date->tm_min = 0;
    date->tm_sec = 0;
    //USETODAYDATE, it is defined in makefile, if compile with -DUSETODAYDATE and uncomment below lines. 
    //it will use program run date (2019 Apr), instead of hardcoded 2018 06 12.    
//#ifndef USETODAYDATE
    date->tm_year= mYear-1900; //base 1900
    date->tm_mon= mMon-1; //base 0
    date->tm_mday= mDay;    
//#endif
    
    system_clock::time_point mn = system_clock::from_time_t(std::mktime(date)); //mn=midnight   
    system_clock::duration mn_dtn =mn.time_since_epoch();
    mPrevUTCtime = duration_cast<nanoseconds>(mn_dtn).count() + i;
    return intToBigEndian(mPrevUTCtime);
}

long long Parser::bigEndianToInt(const std::string& s)
{
    long long base = 1;
    base = base << 8 * (s.length() - 1);
    long long value = 0;
    for (auto& a : s) {
        //static_cast<int> -> negative number, surprised unsigned int also give rubbish huge number.
        value += (static_cast<unsigned char>(a))*base; 
        base = base >> 8;
    }
    return value;
}

template<typename T>
std::string Parser::intToBigEndian(T i)
{
    std::string s;
    int j = sizeof(i);
    long long base = 1; 
    while (j > 0) {
        int r = i / (base << 8 * (j - 1));
        s += char(r);
        i -= r * (base << 8 * (j - 1));
        j--;
    }
    return s;
}

int Parser::readFromInput(const std::string& s)
{
    mType = s[0];
    if (mMsgs.find(mType) == mMsgs.end()) {
        std::string s = mType + ": is not valid type! read abort";
        throw std::invalid_argument(s);
    }

//#define DEBUGPRINT //can also be defined in makefile   
#ifdef DEBUGPRINT
        STR_HEX_PRINT(s);
#endif
        
    int readBytes = 1;
    for (auto& a : *(mMsgs[mType])) {
        if (a.inStart != -1) {
            a.val = s.substr(a.inStart, a.inSize);
            readBytes += a.inSize;
        }
    }
    return readBytes;
}

void Parser::updateAdd(void) {    
    std::vector<cField>& msg = *(mMsgs[mType]);
    mRef = bigEndianToInt(msg[eADD::A_REF].val);    
    std::string& s = msg[eADD::A_TICKER].val;
    msg[eADD::A_TICKER].val = s.substr(0, s.find_last_not_of(" ") + 1);;
    msg[eADD::A_TIME].val = updateTime(bigEndianToInt(msg[eADD::A_TIME].val));
    mSide = msg[eADD::A_SIDE].val;
    mSize = bigEndianToInt(msg[eADD::A_OSIZE].val);
    mPrice = 0.0001 * bigEndianToInt(msg[eADD::A_PRICE].val);
    updatePrice(msg[eADD::A_PRICE].val);
    mBook[mRef] = cOrder{ msg[eADD::A_TICKER].val, mSide, mSize ,mPrice};
}

void Parser::updateExecute(void) {
    //let it throw if cannot find the item in the mBook map, that is treated as a termination signal.
    std::vector<cField>& msg = *(mMsgs[mType]);
    mRef = bigEndianToInt(msg[eEXECUTE::E_REF].val);
    msg[eEXECUTE::E_TICKER].val = mBook[mRef].mTicker;
    msg[eEXECUTE::E_TIME].val = updateTime(bigEndianToInt(msg[eEXECUTE::E_TIME].val));
    mSize = bigEndianToInt(msg[eEXECUTE::E_OSIZE].val);
    mPrice = mBook[mRef].mPrice;
    updatePrice(msg[eEXECUTE::E_PRICE].val); //diff1 vs cancel

    if(mBook[mRef].mSize == mSize)
        mBook.erase(mRef);
    else
        mBook[mRef].mSize -= mSize;
}

void Parser::updateCancel(void) {
    std::vector<cField>& msg = *(mMsgs[mType]);
    mRef = bigEndianToInt(msg[eCANCEL::X_REF].val);
    msg[eCANCEL::X_TICKER].val = mBook[mRef].mTicker;;
    msg[eCANCEL::X_TIME].val = updateTime(bigEndianToInt(msg[eCANCEL::X_TIME].val));
    mSize = bigEndianToInt(msg[eCANCEL::X_OSIZE].val);
    
    if (mBook[mRef].mSize == mSize)
        mBook.erase(mRef);
    else
        mBook[mRef].mSize -= mSize;

    msg[eCANCEL::X_OSIZE].val = intToBigEndian(mBook[mRef].mSize); //diff2 vs execute.
}

void Parser::updateReplace(void) {
    std::vector<cField>& msg = *(mMsgs[mType]);
    mRef = bigEndianToInt(msg[eREPLACE::R_REF].val);
    int mORDNEWREFNUM = bigEndianToInt(msg[eREPLACE::R_ORDNEWREFNUM].val);
    msg[eREPLACE::R_TICKER].val = mBook[mRef].mTicker;
    msg[eREPLACE::R_TIME].val = updateTime(bigEndianToInt(msg[eREPLACE::R_TIME].val));
    mSize = bigEndianToInt(msg[eREPLACE::R_ORDNEWSIZE].val);    //new size
    mPrice = 0.0001 * bigEndianToInt(msg[eREPLACE::R_ORDNEWPRICE].val); //new price
    updatePrice(msg[eREPLACE::R_ORDNEWPRICE].val);

    mBook[mORDNEWREFNUM] = cOrder{ msg[eREPLACE::R_TICKER].val, mBook[mRef].mSide ,mSize ,mPrice };
    mBook.erase(mRef);
}

void Parser::updateBook(void)
{
    switch (mType[0])
    {
    case 'A':
        updateAdd();
        break;
    case 'E':
        updateExecute();
        break;
    case 'X':
        updateCancel();
        break;
    case 'R':
        updateReplace();
        break;
    default:
        return;
    }
}

void Parser::onUDPPacket(const char *buf, size_t len)
{
    //for the 20k records test.in (generated by inputGen binary), takes 80msec on my machine,takeout this line, reduced to 47msec
    
    printf("Received packet of size %zu\n", len); 
    std::string s(buf, len);
    s = s.substr(PACKAGE_HEADER_LEN); //take out package header.
    while (s.length() > 0) {
        int readLength = readFromInput(s);
        s = s.substr(readLength);
        updateBook();
        writeToOutput();
    }
}
