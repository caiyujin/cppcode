#include "Parser.h"
#include <fstream>

const char *inputFile = "test.in";

int main(int argc, char **argv) {
    constexpr int currentDate = 20180612;
    Parser myParser(currentDate, "myTestFile");
#ifdef LNX
    int fd = _open(inputFile, O_RDONLY);//open as readonly
    if (fd == -1) {
        fprintf(stderr, "Couldn't open %s\n", inputFile);
        return 1;
    }
#else
    std::ifstream fd(inputFile, std::ios::in | std::ios::binary);

#endif
 
    char bigbuf[5000];
	// read() attempts to read up to count bytes from file descriptor fd into the buffer starting at buf.
	// package size is of 2 byte as shown in .pdf. which of type of unsigned, so it is integer (big endian)    
    //std::cout<< "the output order timestamp UTC is based on date 20180612 with seconds pass after midnight from inputfile" <<std::endl;

#ifdef LNX
    while (_read(fd, bigbuf, 2) != 0) {
#else
    while (fd.read(bigbuf, 2)) {
#endif
        uint16_t packetSize = htons(*(uint16_t *)bigbuf);//2byte
#ifdef LNX
        _read(fd, bigbuf + 2, packetSize - 2);
#else
        fd.read(bigbuf + 2, packetSize - 2);
#endif
		//so below bigbuf is a char string which contains one package date,
		//the package include its package head
        try{
            myParser.onUDPPacket(bigbuf, packetSize); 
        }
        catch(std::exception& e)
        {
            std::cout<<"exception caught with:" << e.what() <<std::endl;
        }    
    }

#ifdef LNX
    _close(fd); //open, read, close for linux
#else
    fd.close();
#endif
    return 0;
}
