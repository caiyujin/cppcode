
#include <iostream>
#include <chrono>
#include <ctime>
#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdint>
#include <arpa/inet.h> 
#include <sys/uio.h>
#include <chrono> 
#include <random>
#include <map>

std::default_random_engine RndEng(static_cast<unsigned int>(std::chrono::steady_clock::now().time_since_epoch().count()));
std::string s;
const char *perff = "test.in";
const long long oneSec = 1000000000;
const int additems=100000; //total output file will contains 20000 package,each with one message. 

void numToNbyteBEstring(long long val, int j){//j target size.
    long long base = 1;
    while (j > 0) {
        int r = val / (base << 8 * (j - 1));
        s += char(r);
        val -= r * (base << 8 * (j - 1));
        j--;
    }     
}

int main(int argc, char **argv) {
    int status = remove(perff);
        
    int pf = open(perff,O_CREAT|O_WRONLY,0666);
    if (pf == -1) {
        fprintf(stderr, "Couldn't open %s\n", perff);
        return 1;
    }

    char pbuf[60];

    int PackIDRefID=1;//used as refID and PackageID,
    long long times =1123456789;
    std::map<int,int> refQty;
	while (PackIDRefID<=additems) {
        s="";
        s.reserve(60);
        numToNbyteBEstring(40,2); //pack size
        numToNbyteBEstring(PackIDRefID,4); //pack id
        s +='A';
        numToNbyteBEstring(times,8); //time
        numToNbyteBEstring(PackIDRefID,8); //order Ref id
        (RndEng()%2==1) ? s+='B' : s+='S'; //side.
        
        refQty[PackIDRefID]=RndEng()%1000+100;
        numToNbyteBEstring(refQty[PackIDRefID],4); //size 100-1100        
        s += "a b c   ";//ticker
        numToNbyteBEstring(RndEng()%10000+10000,4); //price              
        std::cout<< s.length() <<std::endl;
        write(pf, s.c_str(), s.length());
        

        PackIDRefID++;
        times += oneSec;
    }
    std::cout<<refQty.size() <<std::endl;
    int nonCollisionBase=1;
    //PackIDRefID and times continue from above
	while (PackIDRefID<=additems*1.4) { //1.4-1.0 = 40% of addItems
        
        s="";
        s.reserve(60);
        numToNbyteBEstring(27,2); //pack size
        numToNbyteBEstring(PackIDRefID,4); //pack id
        s +='E';
        numToNbyteBEstring(times,8); //time
        
        int refId =RndEng()%2+nonCollisionBase;
        int Qty =refQty[refId];
        Qty = (RndEng()%3==0)? Qty/2 : Qty; //1/3 chance partial
        if(refQty[refId]==Qty)
            refQty.erase(refId);
        else
            refQty[refId] -= Qty;
        
        numToNbyteBEstring(PackIDRefID,8); 
        numToNbyteBEstring(Qty,4); //size 100-1100       
        
        std::cout<< s.length() <<std::endl;        
        write(pf, s.c_str(), s.length());
        
        nonCollisionBase +=2;
        PackIDRefID++;
        times += oneSec;
    }
    std::cout<<refQty.size() <<std::endl;  
    nonCollisionBase=1;
    //PackIDRefID and times continue from above    
	while (PackIDRefID<=additems*1.8) { // (1.8-1.4) , 40% of addItems . 
        
        s="";
        s.reserve(60);
        numToNbyteBEstring(27,2); //pack size
        numToNbyteBEstring(PackIDRefID,4); //pack id
        s +='X';
        numToNbyteBEstring(times,8); //time
        
        int refId =RndEng()%2+nonCollisionBase;
        if(refQty.find(refId)==refQty.end())
            if(refId%2==0)
                refId--; //this one for sure haven't been modified in execute.
            else
                refId++;
            
        int Qty =refQty[refId];
        Qty = (RndEng()%3==0)? Qty/2 : Qty; //1/3 chance partial
        if(refQty[refId]==Qty)
            refQty.erase(refId);
        else
            refQty[refId] -= Qty;
        
        numToNbyteBEstring(PackIDRefID,8); 
        numToNbyteBEstring(Qty,4); //size 100-1100       
        
        std::cout<< s.length() <<std::endl;        
        write(pf, s.c_str(), s.length());
        
        nonCollisionBase +=2;
        PackIDRefID++;
        times += oneSec;
    }    
    std::cout<<refQty.size() <<std::endl;
    auto it =refQty.begin();
    //PackIDRefID and times continue from above  
	while (it !=refQty.end()&&PackIDRefID<=additems*2.0) {//2.0-1.8= 20% of addItems
        s="";
        s.reserve(60);
        numToNbyteBEstring(39,2); //pack size
        numToNbyteBEstring(PackIDRefID,4); //pack id
        s +='R';
        numToNbyteBEstring(times,8); //time  
        numToNbyteBEstring(it->first,8); //old ref
        it++;
        numToNbyteBEstring(PackIDRefID,8); //new ref
        numToNbyteBEstring(RndEng()%10000+100,4); // new size 100-10100       
        numToNbyteBEstring(RndEng()%100000+10000,4); //new price  10000-110000              
        std::cout<< s.length() <<std::endl;        
        write(pf, s.c_str(), s.length());
        
        PackIDRefID++;
        times += oneSec;
    }    
    std::cout<<refQty.size() <<std::endl;      
    
    close(pf);
    fchmod(pf, 0777);
    return 0;
}
