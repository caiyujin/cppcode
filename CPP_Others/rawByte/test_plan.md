# Project Title

test the correctness of the parser code. 
run the test.in with parser binary and use its output(myTestFile) to compared with test.gold.

a few notes about the test.in:
and the test.in has all 4 kinds of messages, 
with partially executed/canceled order, 
and the package with one or two messeags.
there are different times and prices for the orders,

the test.gold is correct output file from the test.in as per the speficiction. It is been verified manually.

the test executable file is able to spot which message failed. it is more than just byte-to-byte compare. When results show fail, from the failed message can easily trace back which raw input order message caused the failure, hence help developer easily to fix bug.

## Getting Started

need to built the binaries for both "feed" and "test" with the default target in respective Makefile
in main parser dir, issue "make" command; an executable binary "feed" will generated
in the test sub dir, issue "make" command; an executable binary "test" will generated.

### Prerequisites

the test sub dir and main parser dir contains all the necessary files to conduct the test.
please do not rename any input and output file names. 

### Installing


## Running the tests

1. copy the "test.in" from test sub dir to the main parser dir

2. run ./feed at main parser dir,

3. copy the generated "myTestFile" to test sub dir,

4. go to test sub dir, and run ./test  

5. the terminal will show if pass or fail.

## Deployment

it will do byte to byte comparison

## Built With

GNU g++, and edited with notepad++.

## Versioning

1.0.0

## Authors

Cai YuJing

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


