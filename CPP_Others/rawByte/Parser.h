#pragma once
#include "Field.h"
#include <unordered_map>
#include <iostream>
#include <exception>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <cstdio>
#ifdef LNX
    #include <unistd.h>
    #include "WinSockHeader.h"
    #include <arpa/inet.h> //undo for linux by uncommenting.
    #include <sys/uio.h>
#endif
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdint>
#include <winsock2.h> //comment out for linux

//below code not been fully utilized yet.
#if defined(WIN32) || defined(_WIN32)
#  define SBE_BIG_ENDIAN_ENCODE_16(v) _byteswap_ushort(v)
#  define SBE_BIG_ENDIAN_ENCODE_32(v) _byteswap_ulong(v)
#  define SBE_BIG_ENDIAN_ENCODE_64(v) _byteswap_uint64(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_16(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_32(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_64(v) (v)
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define SBE_BIG_ENDIAN_ENCODE_16(v) __builtin_bswap16(v)
#  define SBE_BIG_ENDIAN_ENCODE_32(v) __builtin_bswap32(v)
#  define SBE_BIG_ENDIAN_ENCODE_64(v) __builtin_bswap64(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_16(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_32(v) (v)
#  define SBE_LITTLE_ENDIAN_ENCODE_64(v) (v)
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#  define SBE_LITTLE_ENDIAN_ENCODE_16(v) __builtin_bswap16(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_32(v) __builtin_bswap32(v)
#  define SBE_LITTLE_ENDIAN_ENCODE_64(v) __builtin_bswap64(v)
#  define SBE_BIG_ENDIAN_ENCODE_16(v) (v)
#  define SBE_BIG_ENDIAN_ENCODE_32(v) (v)
#  define SBE_BIG_ENDIAN_ENCODE_64(v) (v)
#else
#  error "Byte Ordering of platform not determined. Set __BYTE_ORDER__ manually before including this file."
#endif



#define HEX_PRINT(X) std::cout << std::setw(2) << std::setfill('0')<<std::hex << static_cast<int>(X) << " ";
#define STR_HEX_PRINT(ASTRING) { for(auto& a: ASTRING) HEX_PRINT(a);}

struct cOrder {
    std::string mTicker;
    std::string mSide; 
    int mSize;
    double mPrice;
};

class Parser {
  public:
    // date - the day on which the data being parsed was generated.
    // It is specified as an integer in the form yyyymmdd.
    // For instance, 18 Jun 2018 would be specified as 20180618.
    //
    // outputFilename - name of the file output events should be written to.
    Parser(int date, const std::string &outputFilename);

    // buf - points to a char buffer containing bytes from a single UDP packet.
    // len - length of the packet.
    void onUDPPacket(const char *buf, size_t len);
    ~Parser();
private:
    //helper functions to process fields, lilke time, price, 
    void updatePrice(std::string & s);
    std::string updateTime(long long i);
    long long bigEndianToInt(const std::string& s);
    template<typename T> std::string intToBigEndian(T i);
    
    void writeToOutput();
    //from the input to the cField objects.
    int readFromInput(const std::string& s);
	//int extractMsgFromInput(const std::string& s);

    //update the cField objects (prepare for output), and update the order book.
    void updateAdd(void);
    void updateExecute(void);
    void updateCancel(void);
    void updateReplace(void);

    //delegate to specific update function 
	void updateBook(void);

	//int mPackageSeqNum = 1;
#ifdef LNX    
    int mFd;
#endif

    std::ofstream mFd;
    std::string mOutput = "";

    //temporary variables for the current message
	std::string mType = "\0";
	long long mRef = 0; //REF NUM is 8 byte in raw data, for consistency, and for future.
    std::string mTicker = "";
    std::string mSide = "";
	int mSize = 0;
	double mPrice = 0; //need 8 byte, so can not use float.

    long long mPrevTime=-1;
    long long mPrevUTCtime=-1;

    //mMsgs got 4 diff types, basically re-using for same type message.
    //that is fine, because the life time of a message is short, and it is processed in serial.
    std::unordered_map<std::string, std::vector<cField>* > mMsgs;
	std::unordered_map<int, cOrder> mBook;

    int mYear=0;
    int mMon=0;
    int mDay=0;    
};
