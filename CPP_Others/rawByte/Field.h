#pragma once
#include <string>
#include <vector>

extern int PACKAGE_HEADER_LEN;
extern int OUTPUT_RESERVE_LEN;
enum class ORDERED : unsigned int { NO, YES };

struct cField {
    ORDERED ordered;
    std::string val;
    int inStart; //-1 indicate not input. the value will not be overwritten.
    int inSize;
    int outStart;
    int outSize;
};

//cannot use scoped enum, because implicite cast to int is needed in this application. 
//after struggling with the index, finally appreciate the boost::Multi-index. (so can get rid of this index-ing enum)
enum eADD {
    A_TYPE = 0,
    A_MSIZE,
    A_TICKER,
    A_TIME,
    A_REF,
    A_SIDE,
    A_PAD,
    A_OSIZE,
    A_PRICE
};
extern std::vector<cField> ADD;

enum eEXECUTE {
    E_TYPE = 0,
    E_MSIZE,
    E_TICKER,
    E_TIME,
    E_REF,
    E_OSIZE,
    E_PRICE
};
extern std::vector<cField> EXECUTE;

enum eCANCEL {
    X_TYPE = 0,
    X_MSIZE,
    X_TICKER,
    X_TIME,
    X_REF,
    X_OSIZE
};
extern std::vector<cField> CANCEL;

enum eREPLACE {
    R_TYPE = 0,
    R_MSIZE,
    R_TICKER,
    R_TIME,
    R_REF,
    R_ORDNEWREFNUM,
    R_ORDNEWSIZE,
    R_ORDNEWPRICE
};
extern std::vector<cField> REPLACE ;
