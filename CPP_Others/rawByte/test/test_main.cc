#include <unordered_map>
#include <iostream>
#include <exception>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdint>
#include <arpa/inet.h> 
#include <sys/uio.h>

/*
//temporary for windows, remove the block when undo for linux
#pragma comment(lib, "Ws2_32.lib") //undo ...
#include "Parser.h"
#include <cstdio>
#include <cstdint>
#include <fcntl.h>
#include <unistd.h> //unix standard. read  binary file as raw; i added in C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include
//#include <arpa/inet.h> //undo ...
#include <winsock2.h> // undo... replace for upper inet.h. for the hton func.
#include <sys/stat.h>
#include <sys/types.h>
//#include <sys/uio.h> //undo...  sys/uio.h - definitions for vector I/O operations
//temporary for windows, remove the block when undo for linux
*/

const char *goldFile = "test.gold";
const char *testedFile = "myTestFile";
const int msg_header_sz = 4;
//const int time_start_pos = 8; //for all output message, it is 9th element when take out the header(msg type+msg size).

bool compareHeaderRawCharA(const char* buf1, const char* buf2, int len){
    for(int i=0; i<len; i++){
        if (!(buf1[i]==buf2[i]))
            return false;
    }
    return true;
}

bool compareBodyRawCharA(const char* buf1, const char* buf2, int len, int msgType){

    for(int i=0; i<len; i++){
        //if(i==time_start_pos)
            //i += 8; //skip the next 8 byte (the time)
            
        if (!(buf1[i]==buf2[i]))
            return false;
    }
    return true;
}

int main(int argc, char **argv) {
    //constexpr int currentDate = 20180612;
    int goldf = open(goldFile, O_RDONLY);
    int testedf = open(testedFile, O_RDONLY);  
    if (goldf == -1) {
        fprintf(stderr, "Couldn't open %s\n", goldFile);
        return 1;
    }
    if (testedf == -1) {
        fprintf(stderr, "Couldn't open %s\n", testedFile);
        return 1;
    }    

    struct stat goldfStat;
    stat(goldFile, &goldfStat);
    struct stat testedfStat;
    stat(testedFile, &testedfStat);
    if(goldfStat.st_size!=testedfStat.st_size){
        std::cout<<goldfStat.st_size<<" "<<testedfStat.st_size <<std::endl;
        std::cout<<"failed with file size check"<<std::endl;
        return 1;  
    }
    
    char goldbuf[50];
    char testedbuf[50];
    int msgCnt=1;
    int byteCnt=0;
	while (read(goldf, goldbuf, msg_header_sz) != 0 && read(testedf, testedbuf, msg_header_sz) != 0 ) {        
        int tobeRead = static_cast<int>(goldbuf[2])-msg_header_sz; //the 3rd char, take out the already read message header 4 bytes.
        int msgType = static_cast<int>(goldbuf[0]); //the 1st char
        
        if(!compareHeaderRawCharA(goldbuf,testedbuf,msg_header_sz)){
            std::cout<<"failed msg header, msg type:"<<msgType <<"message count of:" <<msgCnt<< std::endl; //for debug
            return 1;
        }
        
        read(goldf, goldbuf, tobeRead);        
        read(testedf, testedbuf, tobeRead);
        
        if(!compareBodyRawCharA(goldbuf,testedbuf,tobeRead,msgType)){
            std::cout<<"failed msg body, msg type:"<<msgType <<"message count of:" <<msgCnt<< std::endl; //for debug.
            return 1;
        }
        else{
            //std::cout<<"pass msg body, msg type:"<<msgType <<"\t tobeRead:"<<tobeRead<< std::endl;      
        }
        
        msgCnt++;
        byteCnt = byteCnt +tobeRead+msg_header_sz;
    }
    
    std::cout<<"pass"<<std::endl;
    close(goldf);
    close(testedf);
    return 0;
}
