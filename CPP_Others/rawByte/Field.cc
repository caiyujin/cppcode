#include "Field.h"
int PACKAGE_HEADER_LEN=6; 
int OUTPUT_RESERVE_LEN=50; //50 is a number bigger than longest possible output string(replace,48)

std::vector<cField> ADD = std::vector<cField>{
    //MsgTYpe is input, but we mark it as output only, because the output string is fixed, need to count 1 input.    
    cField{ORDERED::NO, "",	   -1,  -1,    0,	2},
    cField{ORDERED::NO, "",	   -1,  -1,    2,	2},//44dec
    cField{ORDERED::YES,"",	   22,   8,    4,   8},
    cField{ORDERED::NO, "",		1,   8,   12,	8},
    cField{ORDERED::NO, "",		9,	 8,   20,	8},
    cField{ORDERED::YES,"",	   17,	 1,   28,	1},
    cField{ORDERED::YES,"",    -1,  -1,   29,	3},
    cField{ORDERED::NO, "",    18,   4,	  32,   4},
    cField{ORDERED::NO, "",    30,   4,   36,   8}
};

std::vector<cField> EXECUTE= std::vector<cField>{
    cField{ORDERED::NO, "",	   -1,  -1,    0,	2},
    cField{ORDERED::NO, "",    -1,  -1,	   2,	2},//40dec
    cField{ORDERED::YES,"",	   -1,  -1,	   4,	8},
    cField{ORDERED::NO, "",     1,   8,   12,   8},
    cField{ORDERED::NO, "",		9,   8,	  20,	8},
    cField{ORDERED::NO, "",    17,   4,	  28,   4},
    cField{ORDERED::NO, "",    -1,  -1,   32,   8}
};

std::vector<cField> CANCEL= std::vector<cField>{
    cField{ORDERED::NO, "",	   -1,  -1,    0,	2},
    cField{ORDERED::NO, "",    -1,  -1,	   2,	2}, //32dec
    cField{ORDERED::YES,"",	   -1,  -1,	   4,	8},
    cField{ORDERED::NO, "",		1,   8,   12,   8},
    cField{ORDERED::NO, "",		9,   8,	  20,	8},
    cField{ORDERED::NO, "",    17,   4,	  28,   4}
};

std::vector<cField> REPLACE= std::vector<cField>{
    cField{ORDERED::NO, "",    -1,  -1,    0,	2},
    cField{ORDERED::NO, "",    -1,  -1,    2,	2}, //48dec
    cField{ORDERED::YES,"",	   -1,  -1,    4,   8},
    cField{ORDERED::NO, "",		1,   8,   12,	8},
    cField{ORDERED::NO, "",		9,	 8,   20,	8},
    cField{ORDERED::NO, "",	   17,	 8,   28,	8},
    cField{ORDERED::NO, "",    25,   4,	  36,   4},
    cField{ORDERED::NO, "",    29,   4,   40,   8},
};
