#include <iostream>

#define ASIO_STANDALONE
//
//#include <chrono>
//#include <thread>
//#include <asio.hpp>
//#include <asio/ts/buffer.hpp>
//#include <asio/ts/internet.hpp>

#include "olc_net.h"

std::vector<char> vBuffer(2*1024);

void GrabSomeData(asio::ip::tcp::socket& socket1)
{
    //This function is used to asynchronously read data from the stream socket. 
    //it will read data when the server side got some data ready (via some sort of signal),
    //The function call always returns immediately, if there is No data avaialbe.
    
    socket1.async_read_some(asio::buffer(vBuffer.data(), vBuffer.size()),
 
        //the handler will not be invoked from within this function. 
        //Invocation of the handler will be performed in a manner equivalent to using boost::asio::io_context::post().
        [&](std::error_code ec, std::size_t len)
        {
            //after read All availalbe data, check if got error code.
            if (!ec)
            {
                std::cout << "\n\n Read " << len << " bytes\n\n";
                for (int i = 0; i < len; i++)
                    std::cout << vBuffer[i];

                //wait for next round, and keep it running all the time, 
                // (eg check if server side got new data ready)
                //it is NOT recursive thing. StateLess. !!!
                GrabSomeData(socket1);
            }
        });
}

enum class CustomMsgTypes  : int
{
    type1,
    type2
};

int main()
{
    ///*
    olc::net::message<CustomMsgTypes> m1;
    m1.header.id = CustomMsgTypes::type2;

    int a = 1;
    float b = 2.1;
    bool c = false;
    struct s
    {
        int d = 10;
        double f = 20.1;
    };
    s s1;
    m1 << a << b << c;
    std::cout << sizeof(s1) << std::endl; //it is 16 bytes, not 12 bytes.
    m1 << s1; //<< "abcd";
    m1 << 'a';
    //char

    int a1;
    float b1 ;
    bool c1;
    s s11{ 100,200.1 };
    char chr1;
    m1 >> chr1 >> s11 >> c1 >> b1 >> a1;

    
    //*/




    asio::error_code ec;
    asio::io_context context1;//create a space where asio could work
    //fake tasks to asio so the context does not finished. this work object will make sure the context1.run() not immediately exit..
    asio::io_context::work idleWork(context1); 
    std::thread thrContext = std::thread([&]() {context1.run();});
    asio::ip::tcp::endpoint endpoint1(asio::ip::make_address("51.38.81.49", ec), 80);
    asio::ip::tcp::socket socket1(context1); //socket to context, 1-1;
    socket1.connect(endpoint1, ec);

    if (!ec)
    {
        std::cout << "connected!" << std::endl;
    }
    else
    {
        std::cout << "Failed to connect!" << ec.message() << std::endl;
    }

    if (socket1.is_open())
    {
        //prime asio context with instruction to read data when data available.
        //it must be put before write data, or else the program (main) could end prematuraly
        GrabSomeData(socket1);

        std::string sRequest =
        "GET /index.html HTTP/1.1\r\n"
            "Host: david-barr.co.uk\r\n"
            "Connection: close\r\n\r\n";

        socket1.write_some(asio::buffer(sRequest.data(), sRequest.size()), ec);
        if (thrContext.joinable())
            thrContext.join();

        // Important: two issues with Sync way, 
        // 1. use a hard coded wait time, no no.you never know how long it takes, and it affect performance 
        // 2: when data avaialable, it may NOT all of them, incomplete read of data, very bad.
        
        ////using namespace std::chrono_literals;
        ////std::this_thread::sleep_for(200ms);
        
        //socket1.wait(socket1.wait_read);
        //size_t bytes = socket1.available();

        //std::cout << "bytes available:" << bytes << std::endl;
        //if (bytes > 0)
        //{
        //    std::vector<char> vBuffer(bytes);
        //    socket1.read_some(asio::buffer(vBuffer.data(), vBuffer.size()), ec);

        //    for (auto c : vBuffer)
        //        std::cout << c;
        //}
    }

    return 0;
}