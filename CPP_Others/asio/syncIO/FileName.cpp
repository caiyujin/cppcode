#include <boost/asio.hpp>
#include <iostream>

int main() {
    boost::asio::io_context io_context;

    // Create a TCP resolver
    boost::asio::ip::tcp::resolver resolver(io_context);
    auto endpoints = resolver.resolve("example.com", "80");

    // Create and connect a socket
    boost::asio::ip::tcp::socket socket(io_context);
    boost::asio::connect(socket, endpoints);

    // Send a request
    std::string request = "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n";
    boost::asio::write(socket, boost::asio::buffer(request));

    // Read response
    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");

    std::istream response_stream(&response);
    std::string http_version;
    response_stream >> http_version;
    std::cout << "HTTP Version: " << http_version << std::endl;

    return 0;
}