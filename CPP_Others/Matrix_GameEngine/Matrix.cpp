﻿#include "olcGameEngine.h"

class matrix : public olcConsoleGameEngine
{
public:
	matrix()
	{
		m_sAppName = L"Matrix";
	}

private:
	struct sStreamer
	{
		int nColumn = 0;
		float fPosition = 0;
		float fSpeed = 0;
		wstring sText;
		sStreamer() {}
	};

	std::vector<sStreamer> streamers = std::vector<sStreamer>(2*ScreenWidth());
    
protected:
	// Called by olcConsoleGameEngine
	virtual bool OnUserCreate()
	{
		for (int n = 0; n < streamers.size(); n++)
		{
			sStreamer& s= streamers[n];
			s.nColumn = rand() % ScreenWidth();
			s.fPosition = 0;
			s.fSpeed = rand() % 40 + 5;
			s.sText.clear();

			int nStreamerLength = rand() % 40 + 10;

			for (int i = 0; i < nStreamerLength; i++)
				s.sText.append(1, (wchar_t)(rand() % 93 + 33));
		}
		return true;
	}

	// Called by base class
	virtual bool OnUserUpdate(float fElapsedTime)
	{
		// Clear Screen
		Fill(0, 0, ScreenWidth(), ScreenHeight(), PIXEL_SOLID, 0);//call draw for each point, and the point's char values (are saved in the mBufScreen).

        for (auto &s : streamers)
		{
			s.fPosition += fElapsedTime * s.fSpeed;

			for (int i = 0; i < s.sText.size(); i++)
			{
				short color = s.fSpeed < 15.0f ? FG_DARK_GREEN : FG_GREEN;

				if (i == 0)
					color = FG_WHITE;
				else
					if (i <= 3)
						color = FG_GREY;

                Draw(s.nColumn, ((int)s.fPosition - i)% ScreenHeight(), s.sText[i], color); //call draw for each point, and the point's char values (are saved in the mBufScreen).
			}
		}

		return true;
	}
};


int main()
{
	matrix game;
	game.ConstructConsole(100, 80, 12, 12);
	game.Start();
	return 0;
}