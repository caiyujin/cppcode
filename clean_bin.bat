@echo off
setlocal enabledelayedexpansion

rem Set the source directory
set "source=%CD%"

rem Call the deleteFolders subroutine for the current directory and its subdirectories
call :deleteFolders "%source%"

echo All folders named "bin" have been deleted.
pause
exit

:deleteFolders
for /D %%I in ("%~1\*") do (
    if /I "%%~nxI"=="bin" (
        echo Deleting "%%~fI"
        rd /s /q "%%~fI"
    )
	
    if /I "%%~nxI"=="x64" (
        echo Deleting "%%~fI"
        rd /s /q "%%~fI"
    )
	
	
    if exist "%%~fI\*" (
        call :deleteFolders "%%~fI"
    )
)
exit /b